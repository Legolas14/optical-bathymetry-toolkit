$(document).ready(function() {

	$('form').on('submit', function(event) {

		$.ajax({
			data : {
				name : $('#nameInput').val(),
                email : $('#emailInput').val(),
                phys : $('#physics').val()
			},
			type : 'POST',
			url : '/process'
		})
		.done(function(data) {

			if (data.error) {
				$('#errorAlert').text(data.error).show();
				$('#successAlert').hide();
			}
			else {
				$('#successAlert').text(data.name).show();
				$('#errorAlert').hide();
			}

		});

		event.preventDefault();

	});

});
$.ajax({
	data: {
		filename: $('#filename').val()
	},
	type:"POST",
	url:"/store_file"
})
	.done(function(data){
		$('#data_head').text(data.input_data).show();
	

	if(data.error){
	$('#data_head').text(" Error parsing the file").show();
}
else {
	$('#data_head').text(data.error).show();
}
});




$(document).ready(function() {
    $("[name=load_data]").bind("click",function(){
        //var fd = new FormData(); 
        //var files = $('[name=csvfile]')[0].files[0];
       // fd.append('file', files); 
        $.ajax({
            data: {
                filename: $('[name=filename]').val(),
            },
            type:"POST",
            url:"/store_file"
        })
            .done(function(data)
            {
            if(data.error)
            {
                $('#data_head').text(data.error).show();
            }
            else
            {
                $('#data_head').text(data.input_data).show();
            }
        });

    });
   
});

