// console.log(" Data Type of e.target.result  "+typeof e.target.result);
            // console.log(" Data Type of returned_data  "+typeof returned_data);
            // console.log(" Data Type of returned_data with stringify "+typeof JSON.stringify(returned_data));   
            // console.log(" Data Type of Items "+typeof items);  


// clear localstorage at the beginning
console.log("Clearing local storage")
localStorage.clear();

// Add remove loading class on body element depending on Ajax request status
$(document).on({
    ajaxStart: function(){
        console.log(" AJAX STARTEDDD")
        $('#divLoading').show();
    },
    ajaxStop: function(){ 
        console.log(" AJAX ENDEDD")
        $('#divLoading').hide();
    }    
});

//for printing file name 
$(document).ready(function() {
    $('[name=file]').change(function(e){

    $('#filename').text(e.target.files[0].name).show();
    $('#filename').val(e.target.files[0].name);
    console.log(e.target.files[0]);
    var fn = $("#filename").val();
});
});

//for sending selected file to flask and back
function handleFileSelect(evt) {
    var files = evt.target.files; // FileList object
    // use the 1st file from the list
    f = files[0];
   // console.log(files);
   // console.log("New lone"+f);

    var reader = new FileReader();
    //JSON.parse(returned_data)
    // Closure to capture the file information.
    reader.onload = (function(theFile) {
        return function(e) {
            var to_flask=csvJSON(e.target.result);
           // console.log(" AS ARRAY :"+ to_flask);
            $.ajax({
                url:'/store_local',
                type:'POST',
                data:{akey: to_flask },
                success:function(returned_data){    
                              console.log(returned_data);
                },
                error:function(){alert('Select a proper CSV file');}
            });
          //jQuery( '#show_table' ).text( table_data ).show();
            var csv_text=e.target.result.split(/\r?\n|\r/);
            
            table_data1=csv_to_table(csv_text,"table1","Data sample");           
            localStorage.setItem("td1",table_data1);  
        };
      })(f);
      reader.readAsText(f);
      
    }

//for selecting file  
$(document).ready(function(){
    $('[name=file]').change(function(e){

        handleFileSelect(e);
        $("#show_flask_result").hide();
        //when file is changed - previous dropdowns should be removed:
        console.log($('#drop_down_menu').children().size());
        $('#process_options').hide();
        $('#ml_options').hide();
        $('#full_data_process').hide();
        $('#after_load').hide();
    });
});

//reset all selected values
$(document).ready(function(){
    $('[name=reset_data]').click(function(){
        window.location.reload();
    });
});                   
//for printing header table and FIRST DROPDOWN OPTION 
$(document).ready(function() {
    $('[name=load_data2]').click(function(e){
    data=localStorage.getItem("td1");
    $("#show_flask_result").show()
    document.getElementById('show_flask_result').innerHTML=data; 
    headers=JSON.parse(localStorage.getItem("headers"));
    var h1=headers;
    localStorage.setItem("original_feats",JSON.stringify(h1));
    //console.log("Header from Localstorage:"+headers) 
   
    // for the first dropdown alone :
    dropdown_first=set_dropdown_options(headers,"first");
    $('#feats_options').empty().append(dropdown_first);

    //for static dropdowns : 
    
    $('#feats_options1').empty().append(dropdown_first);
    
    $('#feats_options2').empty().append(dropdown_first);

    $('#feats_options3').empty().append(dropdown_first);

    $('#feats_options4').empty().append(dropdown_first);

    $('#feats_options5').empty().append(dropdown_first);

    $('#feats_options6').empty().append(dropdown_first);

    var wrapper = $('.field_wrapper');
    $(wrapper).show();   
    $("#after_load").show(); 
    if($('[name=show_graphs]').is(':checked'))
    {
        $("[name=show_graphs]").prop("checked", false);
        $('#graphs').hide();
    }
    $('#show_graphs_div').hide();
    $('#graph_options').hide();
    if($('[name=surface_plot_check]').is(':checked'))
    {
        $("[name=surface_plot_check]"). prop("checked", false);
    }
    $('#surface_plot_div').hide();
    
    if($('[name=full_go]').is(':checked'))
    {
        $("[name=full_go]"). prop("checked", false);
    }
    $('#full_go_div').hide();
      
    });
});

//for calculating distance - selected NO DISTANCE
// $(document).ready(function(){
//     $('[name=no_distance').click(function(e){
//         console.log(JSON.parse(localStorage.getItem("selected_feats")))
//         $.ajax({
//             url:'/calculate_distance',
//             type:'POST',
//             data:{akey: JSON.stringify("no_distance") },
//             success:function(returned_data){
//                 ret_data_json=JSON.stringify(returned_data);
//                 //convert json to csv
//                 ret_data_csv=ConvertToCSV(ret_data_json);
//              //   show the table of flask returned data :
//                 var ret_data_show=ret_data_csv.split(/\r?\n|\r/);
//                 table_data2=csv_to_table(ret_data_show,"table2");
//                 document.getElementById('updated_table').innerHTML=table_data2;           
//         },
//             error:function(){console.log('Oops! Something went wrong');}
//         });
// });
// });

// function get_headers(river_data)
// {
// var count=0;
// row_data=river_data[0].split(',');    
// var headers=[];
// for(i=0;i<row_data.length;i++)
// {
//     headers.push(row_data[i]);
// }
// console.log(" In function: "+headers)

// }

function set_dropdown_options(headers,index){

    if(index=="other")
{    
    var dropdown_new='<div>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<select class="custom-select mt-3" style="display:inline-block; width:500px; height:50px;" id="feats_options_dyn">';
    dropdown_new+='<option>None</option>';
    for(i=0; i<headers.length;i++)
    {
        var opt=headers[i];
        dropdown_new+='<option>'+opt+'</option>';
    }
    dropdown_new+='</select><a href="javascript:void(0);" style="display:inline-block;" class="remove_button mt-3"><img src="static/remove.png" width="32px" height="32px"></a></div>';
    return dropdown_new;
}
else if(index=="first")
{
    var dropdown_new='';
    dropdown_new+='<option>None</option>';
    for(i=0; i<headers.length;i++)
    {
        var opt=headers[i];
        console.log(opt)
        dropdown_new+='<option>'+opt+'</option>';
    }
    
   return dropdown_new;
//https://stackoverflow.com/questions/9173182/add-remove-input-field-dynamically-with-jquery
//https://www.codexworld.com/add-remove-input-fields-dynamically-using-jquery/
//https://www.itsolutionstuff.com/post/simple-add-remove-input-fields-dynamically-using-jquery-with-bootstrapexample.html
}
}

function set_dropdown_options_full(headers,index){

    if(index=="other")
{    
    var dropdown_new='<div>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<select class="custom-select mt-3" style="display:inline-block; width:500px; height:50px;" id="feats_options_dyn_full">';
    dropdown_new+='<option>None</option>';
    for(i=0; i<headers.length;i++)
    {
        var opt=headers[i];
        dropdown_new+='<option>'+opt+'</option>';
    }
    dropdown_new+='</select><a href="javascript:void(0);" style="display:inline-block;" class="remove_button_full mt-3"><img src="static/remove.png" width="32px" height="32px"></a></div>';
    return dropdown_new;
}
else if(index=="first")
{
    var dropdown_new='';
    dropdown_new+='<option>None</option>';
    for(i=0; i<headers.length;i++)
    {
        var opt=headers[i];
        console.log(opt)
        dropdown_new+='<option>'+opt+'</option>';
    }
    
   return dropdown_new;
//https://stackoverflow.com/questions/9173182/add-remove-input-field-dynamically-with-jquery
//https://www.codexworld.com/add-remove-input-fields-dynamically-using-jquery/
//https://www.itsolutionstuff.com/post/simple-add-remove-input-fields-dynamically-using-jquery-with-bootstrapexample.html
}
}
// FUnction related to add and remove of feature dropdown
$(document).ready(function(){
    var maxField = 10; //Input fields increment limitation
    var addButton = $('.add_button'); //Add button selector
    var wrapper = $('.field_wrapper'); //Input field wrapper
    var x = 1; //Initial field counter is 1
    
    //Once add button is clicked
    $(addButton).click(function(){
        headers=JSON.parse(localStorage.getItem("original_feats"));
        console.log("Header from Localstorage:"+headers) 
        var fieldHTML = set_dropdown_options(headers,"other");
        //Check maximum number of input fields
        if(x < maxField){ 
            x++; //Increment field counter
            $(wrapper).append(fieldHTML); //Add field html
        }
    });
    
    //Once remove button is clicked
    $(wrapper).on('click', '.remove_button', function(e){
        e.preventDefault();
        $(this).parent('div').remove(); //Remove field html
        x--; //Decrement field counter
    });
});

// function to proceed from feat selection btn
$(document).ready(function() {
    $("#count_feat").click(function(){ 
        var feats = [];
        $.each($(".field_wrapper option:selected"), function(){            
            feats.push($(this).val());
            console.log("other"+$(this).val());
        });
        
        console.log("You have selected the features : " +feats );
        localStorage.setItem("selected_feats",JSON.stringify(feats));
        $('#yesorno').show();
});
});
//for dropdown dynamic value select below
//https://www.tutorialrepublic.com/faq/how-to-get-the-value-of-selected-option-in-a-select-box-using-jquery.php
//for input dynamic //for input dynamic text below below
//https://stackoverflow.com/questions/16245947/jquery-get-value-from-dynamically-created-inputs#:~:text=%24('input%5Bid%5E,value%3A'%20%2B%20value)%3B%20%7D)%3B


// After selecting NO in distance present form
$(document).ready(function(){
    $('#no_distance').click(function(e){
        send_feats=JSON.parse(localStorage.getItem("selected_feats"));
        $('#updated_table_yes').hide();
        $('#dist_drop_down_menu').hide();
        $('#enter_ml_box').hide();
        $.ajax({
            //no need to json stringify-- they are already string format
            url:'/calculate_distance',
            type: 'POST',
            data:{yorn:"no_distance", sel_feat:JSON.stringify(send_feats)},
            success:function(updated_dataset){
                upd_data_json=JSON.stringify(updated_dataset);
                upd_data_csv=ConvertToCSV(upd_data_json);
                upd_data_table=csv_to_table(upd_data_csv.split(/\r?\n|\r/),"table3","Updated data sample");
                document.getElementById("updated_table_no").innerHTML=upd_data_table;
                $("#updated_table_no").show();
                $('#updated_table_yes').hide();
                $('#enter_ml_box').show();
            },
            error:function(){ console.log(" Problem recieving data from flask to ajax");}
        })
    });
});


// After Selecting YES in distance present form
$(document).ready(function(){
    $('#yes_distance').click(function(e){
        headers=JSON.parse(localStorage.getItem("original_feats"));
       // console.log(headers)
        dropdown_yes_opt=set_dropdown_options(headers,"first");
       // console.log("yes opt: "+dropdown_yes_opt)
        $('#feats_options_yes').empty().append(dropdown_yes_opt);
        var wrapper = $('.field_wrapper_1');
        $(wrapper).show();
        $('#dist_column').show();   
        $('#updated_table_no').hide();
        $('#enter_ml_box').hide();
       // send_feats=JSON.parse(localStorage.getItem("selected_feats"));
       // cdist=JSON.parse(localStorage.getItem("distance_pre_calc"));
       // send_feats.append(cdist)

    });
});

//Get the distance column name
$(document).ready(function(){
    $('#dist_column').click(function(e){
        console.log("entered here")
        var cdist = $('#feats_options_yes').find("option:selected").text();
        console.log("Distance :"+JSON.stringify(cdist));
        send_feats=JSON.parse(localStorage.getItem("selected_feats"));
        $.ajax({
            url:'/pre_distance',
            type:'POST',
            data:{cdist_send:JSON.stringify(cdist), sel_feat: JSON.stringify(send_feats)},
            success:function(updated_dataset){
                upd_data_json=JSON.stringify(updated_dataset);
                upd_data_csv=ConvertToCSV(upd_data_json);
                upd_data_table=csv_to_table(upd_data_csv.split(/\r?\n|\r/),"table4","Updated data sample");
                document.getElementById("updated_table_yes").innerHTML=upd_data_table;
                $("#updated_table_yes").show();
                $('#enter_ml_box').show();
               
            },
            error:function(){ console.log(" Problem recieving data from flask to ajax");}
        })
       
    });
});
//csv.split(/\r?n|\r/) -----------------ORIGINAL ONE ( has problem with character 'n')
//Function after entering into ML workflow - to diplay independent and dependent fetures - double flask return objects
$(document).ready(function(){
    $('[name=enter_ml]').change(function(){
        if($('[name=enter_ml]').is(':checked'))
        {
            send_feats=JSON.parse(localStorage.getItem("selected_feats"));
            $.ajax({
                url:'/indep_data',
                type:'POST',
                data:{sel_feat:JSON.stringify(send_feats)},
                success:function(independent_feats){
                    independent_feats_json=JSON.stringify(independent_feats);
                   // console.log("json"+independent_feats_json)
                    independent_feats_csv=ConvertToCSV(independent_feats_json)
                    indepedent_feats_table=csv_to_table(independent_feats_csv.split(/\r?\n|\r/),"table5","Independent Features")
                    document.getElementById("independent_features").innerHTML=indepedent_feats_table;
                    $("#independent_features").show();
                    dependen_func_call(send_feats);
                    $('#process_options').show();
                },
                error:function(){console.log(" Problem recieving independent/dependent data from flask to ajax");}
            })
        }

        else
        {
            $("#independent_features").hide();
            $("#target_feature").hide();
            
        }
    });

});

function dependen_func_call(send_feats){
    $.ajax({
        url:'/dep_data',
        type:'POST',
        data:{sel_feat:JSON.stringify(send_feats)},
        success:function(dependent_feats)
        { 
            dependent_feats_json=JSON.stringify(dependent_feats);
            dependent_feats_csv=ConvertToCSV(dependent_feats_json)
            depedent_feats_table=csv_to_table(dependent_feats_csv.split(/\r?\n|\r/),"table6","Dependent (Target) Feature")
            document.getElementById("target_feature").innerHTML=depedent_feats_table;
            $("#target_feature").show();
        },
        error:function(){console.log(" Problem recieving independent/dependent data from flask to ajax");}

        })       
    }


//Function to get options for processing and transformation - pass to flask and receive transformed data sample
$(document).ready(function(){
    $('[name=apply]').click(function(){
        var options=[];
        $.each($("input[name='process_options']:checked"),function(){
            options.push($(this).val());
    });
    console.log(" Selected options for processing data: "+options);
    localStorage.setItem("processing_options",JSON.stringify(options));
    send_feats=JSON.parse(localStorage.getItem("selected_feats"));
    $.ajax({
        url:'/process_train_data',
        type:'POST',
        data:{ process_options : JSON.stringify(options), sel_feat: JSON.stringify(send_feats)},
        success: function(processed_head){
            processed_head_json=JSON.stringify(processed_head);
            processed_head_csv=ConvertToCSV(processed_head_json);
            processed_head_table=csv_to_table(processed_head_csv.split(/\r?\n|\r/), tid="table7", caption=" Transformed Dataset Sample");
            document.getElementById("transformed_data").innerHTML=processed_head_table;
            $('#transformed_data').show();
            $('#ml_options').show();
        }
    })
});
});

$(document).ready(function(){
    $('[name=go]').click(function(){
        if($('[name=show_graphs]').is(':checked'))
        {
            $("[name=show_graphs]").prop("checked", false);
            $('#graphs').hide();
        }
        $('#show_graphs_div').hide();
        $('#graph_options').hide();

        ml_algo_name= $("#ml_algo option:selected").val();
        tts_value=$('#tts_value').val();
        send_feats=JSON.parse(localStorage.getItem("selected_feats"));
        options=JSON.parse(localStorage.getItem("processing_options"));
        localStorage.setItem("ml_algo_name",ml_algo_name)
        console.log(ml_algo_name, tts_value, send_feats)
        if($('[name=surface_plot_check]').is(':checked'))
        {
            $("[name=surface_plot_check]"). prop("checked", false);
        }
        $('#surface_plot_div').hide();
        
        if($('[name=full_go]').is(':checked'))
        {
            $("[name=full_go]"). prop("checked", false);
        }
        $('#full_go_div').hide();
        $('#full_data_process').hide();
        $('#post_train_metric_tables').hide();
        $('#metric_store').hide();

        $.ajax({
            url:'/ml_algorithms',
            type:'POST',
            data:{ml_algo:JSON.stringify(ml_algo_name), tts_value:JSON.stringify(tts_value), sel_feat: JSON.stringify(send_feats), process_options:JSON.stringify(options)},
            success: function(metric_scores_and_preds)
            {
                if(ml_algo_name=="Try all Models")
                {
                console.log("Try all")
                train_metrics_json=JSON.stringify(metric_scores_and_preds[0]);
                console.log(train_metrics_json)
                train_metrics_csv=ConvertToCSV(train_metrics_json);
                train_metrics_table=csv_to_table(train_metrics_csv.split(/\r?\n|\r/), tid="table8", caption=" Model training performance metrics on training data");
                document.getElementById("train_metrics").innerHTML=train_metrics_table;
                $('#train_metrics').show();
   
                validation_metrics_json=JSON.stringify(metric_scores_and_preds[1]);
                validation_metrics_csv=ConvertToCSV(validation_metrics_json);
                validation_metrics_table=csv_to_table(validation_metrics_csv.split(/\r?\n|\r/), tid="table9", caption=" Model performance metrics on validation data");
                document.getElementById("validation_metrics").innerHTML=validation_metrics_table;
                $('#validation_metrics').show();

                sample_preds_json=JSON.stringify(metric_scores_and_preds[2]);
                sample_preds_csv=ConvertToCSV(sample_preds_json);
                sample_preds_table=csv_to_table(sample_preds_csv.split(/\r?\n|\r/), tid="table10", caption=" Sample Predictions");
                document.getElementById("sample_preds").innerHTML=sample_preds_table;
                $('#sample_preds').show();
                $('#show_graphs_div').show();  
                $('#full_go_div').show();  
                $('#surface_plot_div').show();
                $('#metric_store').show();
                }
                else
                {
                train_metrics_json=JSON.stringify(metric_scores_and_preds[0]);
                console.log(train_metrics_json)
                train_metrics_csv=ConvertToCSV(train_metrics_json);
                train_metrics_table=csv_to_table_two_rows(train_metrics_csv.split(/\r?\n|\r/), tid="table8", caption=" Model training performance metrics on training data");
                document.getElementById("train_metrics").innerHTML=train_metrics_table;
                $('#train_metrics').show();

                validation_metrics_json=JSON.stringify(metric_scores_and_preds[1]);
                console.log(train_metrics_json)
                validation_metrics_csv=ConvertToCSV(validation_metrics_json);
                validation_metrics_table=csv_to_table_two_rows(validation_metrics_csv.split(/\r?\n|\r/), tid="table9", caption=" Model performance metrics on validation data");
                document.getElementById("validation_metrics").innerHTML=validation_metrics_table;
                $('#validation_metrics').show();

                sample_preds_json=JSON.stringify(metric_scores_and_preds[2]);
                sample_preds_csv=ConvertToCSV(sample_preds_json);
                sample_preds_table=csv_to_table(sample_preds_csv.split(/\r?\n|\r/), tid="table10", caption=" Sample Predictions");
                document.getElementById("sample_preds").innerHTML=sample_preds_table;
                $('#sample_preds').show();
                $('#show_graphs_div').show();
                $('#graph_options').hide();  
                $('#full_go_div').show();  
                $('#surface_plot_div').show();
                $('#metric_store').show();
                }
            },
            error: function(){ console.log('Something went wrong') }    
        })
    });
});


// Graphs
$(document).ready(function() {
    $('[name=show_graphs]').change(function(){
        if($('[name=show_graphs]').is(':checked'))
        {   
            ml_algo_name=localStorage.getItem("ml_algo_name")

            if(ml_algo_name!="Try all Models")
            {
            $('#graph_options').hide(); 
            console.log('Im in')
            var show_graphs_check="Yes";
            console.log(ml_algo_name)
            $.ajax({
                url:'/graphs_predictions_single_model',
                type:'POST',
                data:{show_graphs_check:JSON.stringify(show_graphs_check), ml_algo_name:JSON.stringify(ml_algo_name)},
                success: function(actual_and_preds)
                {
                    var trace1=
                        {
                            x:actual_and_preds.distance,
                            y:actual_and_preds.actual,
                            mode:'lines+markers',
                            type:'scatter',
                            name:'Original data lineplot'
                        };
                    
                    var trace2=
                        {
                            x:actual_and_preds.distance,
                            y:actual_and_preds.preds,
                            mode:'lines+markers',
                            type:'scatter',
                            name:ml_algo_name.concat(" Preds Lineplot")
                        };
                    
                    var layout = {
                    autosize: false,
                    width: 1200,
                    height: 500
                };

                var data=[trace1,trace2];
               
                Plotly.newPlot('graphs', data, layout);
                $('#graphs').show();
            },      
                error:function(){ console.log("Error getting actual and predicted depths ")}
            })
        }

        if(ml_algo_name=="Try all Models")
        {
            $('#graph_options').show(); 
            console.log(" In try all graphs ")
            var show_graphs_check="Yes";
            $.ajax({
                url:'/graphs_predictions_all_models',
                type:'POST',
                data:{show_graphs_check:JSON.stringify(show_graphs_check)},
                success:function(actual_and_preds)
                {
                    var trace1=
                        {
                            x:actual_and_preds.distance,
                            y:actual_and_preds.actual,
                            mode:'lines+markers',
                            type:'scatter',
                            name:'Original data lineplot'
                        };

                        var layout = {
                            autosize: false,
                            width: 1200,
                            height: 500
                        };
                        Plotly.newPlot('graphs', trace1, layout);
                        $('#graphs').show();

                    $("[name=none]").click(function()
                    {
                        console.log('none')
                        var trace1=
                        {
                            x:actual_and_preds.distance,
                            y:actual_and_preds.actual,
                            mode:'lines+markers',
                            type:'scatter',
                            name:'Original data lineplot'
                        };
                        var layout = {
                            autosize: false,
                            width: 1200,
                            height: 500
                        };
                        Plotly.newPlot('graphs', trace1 , layout);
                        $('#graphs').show();

                    });
                    $("[name=lr]").click(function()
                    {
                        console.log('lr')
                        var trace1=
                        {
                            x:actual_and_preds.distance,
                            y:actual_and_preds.actual,
                            mode:'lines+markers',
                            type:'scatter',
                            name:'Original data lineplot'
                        };
                        var trace2=
                        {
                            x:actual_and_preds.distance,
                            y:actual_and_preds.lr_preds,
                            mode:'lines+markers',
                            type:'scatter',
                            name:'LR data lineplot'
                        };

                        var layout = {
                            autosize: false,
                            width: 1200,
                            height: 500
                        };
        
                        var data=[trace1,trace2];
                       
                        Plotly.newPlot('graphs', data, layout);
                        $('#graphs').show();
                    });

                    $("[name=svr]").click(function()
                    {
                        console.log('svr')
                        var trace1=
                        {
                            x:actual_and_preds.distance,
                            y:actual_and_preds.actual,
                            mode:'lines+markers',
                            type:'scatter',
                            name:'Original data lineplot'
                        };
                        var trace2=
                        {
                            x:actual_and_preds.distance,
                            y:actual_and_preds.svr_preds,
                            mode:'lines+markers',
                            type:'scatter',
                            name:'SVR data lineplot'
                        };

                        var layout = {
                            autosize: false,
                            width: 1200,
                            height: 500
                        };
        
                        var data=[trace1,trace2];
                       
                        Plotly.newPlot('graphs', data, layout);
                        $('#graphs').show();
                    });

                    $("[name=nn]").click(function()
                    {
                        console.log('nn')
                        var trace1=
                        {
                            x:actual_and_preds.distance,
                            y:actual_and_preds.actual,
                            mode:'lines+markers',
                            type:'scatter',
                            name:'Original data lineplot'
                        };
                        var trace2=
                        {
                            x:actual_and_preds.distance,
                            y:actual_and_preds.nn_preds,
                            mode:'lines+markers',
                            type:'scatter',
                            name:'NN data lineplot'
                        };

                        var layout = {
                            autosize: false,
                            width: 1200,
                            height: 500
                        };
        
                        var data=[trace1,trace2];
                       
                        Plotly.newPlot('graphs', data, layout);
                        $('#graphs').show();
                    });

                    $("[name=rf]").click(function()
                    {
                        console.log('rf')
                        var trace1=
                        {
                            x:actual_and_preds.distance,
                            y:actual_and_preds.actual,
                            mode:'lines+markers',
                            type:'scatter',
                            name:'Original data lineplot'
                        };
                        var trace2=
                        {
                            x:actual_and_preds.distance,
                            y:actual_and_preds.rf_preds,
                            mode:'lines+markers',
                            type:'scatter',
                            name:'RF data lineplot'
                        };

                        var layout = {
                            autosize: false,
                            width: 1200,
                            height: 500
                        };
        
                        var data=[trace1,trace2];
                       
                        Plotly.newPlot('graphs', data, layout);
                        $('#graphs').show();
                    });

                    $("[name=xgb]").click(function()
                    {
                        console.log('xgb')
                        var trace1=
                        {
                            x:actual_and_preds.distance,
                            y:actual_and_preds.actual,
                            mode:'lines+markers',
                            type:'scatter',
                            name:'Original data lineplot'
                        };
                        var trace2=
                        {
                            x:actual_and_preds.distance,
                            y:actual_and_preds.xgb_preds,
                            mode:'lines+markers',
                            type:'scatter',
                            name:'XGB data lineplot'
                        };

                        var layout = {
                            autosize: false,
                            width: 1200,
                            height: 500
                        };
        
                        var data=[trace1,trace2];
                       
                        Plotly.newPlot('graphs', data, layout);
                        $('#graphs').show();
                    });

                    $("[name=all]").click(function()
                    {
                        console.log('all')
                        var trace1=
                        {
                            x:actual_and_preds.distance,
                            y:actual_and_preds.actual,
                            mode:'lines+markers',
                            type:'scatter',
                            name:'Original data lineplot'
                        };
                        var trace2=
                        {
                            x:actual_and_preds.distance,
                            y:actual_and_preds.lr_preds,
                            mode:'lines+markers',
                            type:'scatter',
                            name:'LR data lineplot'
                        };
                        var trace3=
                        {
                            x:actual_and_preds.distance,
                            y:actual_and_preds.svr_preds,
                            mode:'lines+markers',
                            type:'scatter',
                            name:'SVR data lineplot'
                        };
                        var trace4=
                        {
                            x:actual_and_preds.distance,
                            y:actual_and_preds.nn_preds,
                            mode:'lines+markers',
                            type:'scatter',
                            name:'NN data lineplot'
                        };
                        var trace5=
                        {
                            x:actual_and_preds.distance,
                            y:actual_and_preds.rf_preds,
                            mode:'lines+markers',
                            type:'scatter',
                            name:'RF data lineplot'
                        };
                        var trace6=
                        {
                            x:actual_and_preds.distance,
                            y:actual_and_preds.xgb_preds,
                            mode:'lines+markers',
                            type:'scatter',
                            name:'XGB data lineplot'
                        };

                        var layout = {
                            autosize: false,
                            width: 1200,
                            height: 500
                        };
        
                        var data=[trace1,trace2,trace3,trace4,trace5,trace6];
                       
                        Plotly.newPlot('graphs', data, layout);
                        $('#graphs').show();
                    });
                }

            })

        }
    }
        else if($('[name=show_graphs]').is(':checked')==false)
        {
            $('#graphs').hide();
        }
    });
});



// Surface plot
$(document).ready(function() {
    $('[name=surface_plot_check]').change(function(){
        if($('[name=surface_plot_check]').is(':checked'))
        {   
            var show_surface_plot_check="Yes";
            send_feats=JSON.parse(localStorage.getItem("selected_feats"));
            ml_algo_name=localStorage.getItem("ml_algo_name");

            $.ajax({
                url:'/surface_plot_data',
                type:'POST',
                data:{show_surface_plot_check:JSON.stringify(show_surface_plot_check), feats: JSON.stringify(send_feats), ml_algo_name: JSON.stringify(ml_algo_name) },
                success:function(xyz)
                {
                        var trace1=
                        [{
                            x:xyz.x,
                            y:xyz.y,
                            z:xyz.z,
                            type:'surface',
                            name:'Surface plot'
                        }];

                        var layout = {
                            autosize: false,
                            width: 1200,
                            height: 500
                        };
                        Plotly.newPlot('surface_plot', trace1, layout);
                        $('#surface_plot').show();
        },
        error:function(){ console.log(" sad nigga ")}                   
})
        }
        else
        {
            $('#surface_plot').hide();
        }


        });
    });


//Funtion for proceed to full river
$(document).ready(function(){
    $('[name=full_go]').change(function(){
        if($('[name=full_go]').is(':checked'))
        {
            $('#full_data_process').show();
        }
        else
        {
            $('#full_data_process').hide();
        }
    });

});


// Import and process full river data
$(document).ready(function(){
    $('#full_data-csv').change(function(e){
        $('#full_river_filename').text(e.target.files[0].name).show();
        $('#full_river_filename').val(e.target.files[0].name);
    });
});

$(document).ready(function(){
    $('[name=full_data-csv]').change(function(e){
        $('#divLoading').show();
        full_river_select_file(e);
        var wrapper = $('#actual_drop_down_menu');
        $(wrapper).hide();
        $('#full_drop_down_menu').hide();
        $('#actual_yesorno').hide();
        $('#ml_algo_div_full').hide();
        $('#predict').hide()
        $('#full_preds_sample_table').hide();
        $('#full_preds_metrics_table').hide();
        $('#model_save_div').hide();
        $('#divLoading').hide();
        $('full_river_table').hide();
    });
});


function full_river_select_file(e)
{
    $('#divLoading').show();
    var file=e.target.files[0];
    Papa.parse(file, {
        header: true,
        dynamicTyping: false,
        fastMode: true,
        complete: function(results) {
        console.log(results['data']);
        $.ajax({
            url:'/check_if_file_present',
            type:'POST',
            success:function(confirmation){
                console.log(confirmation)
                download(Papa.unparse(results['data']), "full_river_data_from_bctk_software.csv", "text/csv");
                alert(" Done ") 
            }
        })
        }
    });
    $('#divLoading').hide();
    }

// Function for pressing load data button
$(document).ready(function(){
    $('#load_full_data').click(function(){
        $('#full_drop_down_menu').hide();
        $('#actual_yesorno').hide();
        $('#actual_drop_down_menu').hide();
        $('#ml_algo_div_full').hide();
        $('#predict').hide();
        $('model_save_div').hide();

        $.ajax({
            url:'/full_river_input',
            type:'POST',
            success:function(full_river_data_sample){ 
                console.log(" papa parse success") 
                full_river_data_sample_json=JSON.stringify(full_river_data_sample.data);
                full_river_data_sample_csv=ConvertToCSV(full_river_data_sample_json);
                full_river_data_sample_table=csv_to_table(full_river_data_sample_csv.split(/\r?\n|\r/), tid="table11", caption=" Full river Sample data");
                document.getElementById("full_river_table").innerHTML=full_river_data_sample_table;
                $('#full_river_table').show(); 

                var full_headers=full_river_data_sample.columns;
                console.log("first print"+full_headers)
                localStorage.setItem("full_headers",JSON.stringify(full_headers));
                $('#proceed_1').show();
            }
        })             
    });
});
    
var flag_check=0        
function download(content, fileName, contentType) {
    const a = document.createElement("a");
    const file = new Blob([content], { type: contentType });
    a.href = URL.createObjectURL(file);
    console.log(a.href)
    console.log(filename)
    a.download = fileName;
    console.log(a.download)
    a.click();
    flag_check=1

   }


               
//for printing header table and FIRST DROPDOWN OPTION 
$(document).ready(function() {
    $('[name=proceed_1]').click(function(e)
    {
        full_headers=JSON.parse(localStorage.getItem("full_headers"));
        console.log("second print"+full_headers)
        dropdown_first=set_dropdown_options_full(full_headers,"first");
        // for the first dropdown alone :
        $('#full_feats_options').empty().append(dropdown_first);  

        //for static dropdowns : 
        
        $('#full_feats_options1').empty().append(dropdown_first);
        
        $('#full_feats_options2').empty().append(dropdown_first);

        $('#full_feats_options3').empty().append(dropdown_first);

        $('#full_feats_options4').empty().append(dropdown_first);

        $('#full_feats_options5').empty().append(dropdown_first);

        $('#full_feats_options6').empty().append(dropdown_first);

        var wrapper = $('.field_wrapper_full');
        $(wrapper).show();   
        $('#full_drop_down_menu').show();
        $('#actual_yesorno').show();
    });
});

// FUnction related to add and remove of feature dropdown FOR FULL DATA
$(document).ready(function(){
    var maxField = 10; //Input fields increment limitation
    var addButton = $('.add_button_full'); //Add button selector
    var wrapper = $('.field_wrapper_full'); //Input field wrapper
    var x = 1; //Initial field counter is 1
    
    //Once add button is clicked
    $(addButton).click(function(){
        headers=JSON.parse(localStorage.getItem("full_headers"));
        console.log("Full data Header from Localstorage:"+headers) 
        var fieldHTML = set_dropdown_options_full(headers,"other");
        //Check maximum number of input fields
        if(x < maxField){ 
            x++; //Increment field counter
            $(wrapper).append(fieldHTML); //Add field html
        }
    });
    
    //Once remove button is clicked
    $(wrapper).on('click', '.remove_button_full', function(e){
        e.preventDefault();
        $(this).parent('div').remove(); //Remove field html
        x--; //Decrement field counter
    });
});

var full_ml_algo=''
var actual_status='';
// Function for when Actual target is NOT present
$(document).ready(function(){
    $('#actual_no').click(function(e){
        actual_status="No";
        full_ml_algo="None";
        localStorage.setItem("actual_status",actual_status)
        $('#actual_drop_down_menu').hide()
        ml_algo_name=localStorage.getItem("ml_algo_name")
        if(ml_algo_name=="Try all Models")
        {
        $('#ml_algo_div_full').show();
        }
        else
        {
        $('#ml_algo_div_full').hide(); 
        }
        $('#predict').show()

    });
});


// Function for when Actual target is present
$(document).ready(function(){
    $('#actual_yes').click(function(e){
        actual_status="Yes"
        localStorage.setItem("actual_status",actual_status)
        headers=JSON.parse(localStorage.getItem("full_headers"));
        dropdown_yes_opt=set_dropdown_options_full(headers,"first");
        $('#actual_feat_options').empty().append(dropdown_yes_opt);
        var wrapper = $('#actual_drop_down_menu');
        $(wrapper).show();
        ml_algo_name=localStorage.getItem("ml_algo_name")
        if(ml_algo_name=="Try all Models")
        {
        $('#ml_algo_div_full').show();
        }
        else
        {
        $('#ml_algo_div_full').hide(); 
        }
        $('#predict').show()
    });
});


// function to proceed from feat selection btn
$(document).ready(function() {
    $("#predict").click(function(){ 
        var feats = [];
        $.each($(".field_wrapper_full option:selected"), function(){            
            feats.push($(this).val());
            console.log("other"+$(this).val());
        });
        
        console.log("You have selected the features : " +feats );
        localStorage.setItem("selected_feats_full",JSON.stringify(feats));
        actual_status=localStorage.getItem("actual_status")
        var actual_col = $('#actual_feat_options').find("option:selected").text();
        console.log("Actual :"+JSON.stringify(actual_col));
        ml_algo_name=localStorage.getItem("ml_algo_name")
        if(ml_algo_name=="Try all Models"){
            full_ml_algo= $("#ml_algo_full option:selected").val();
        }
        else
        {
            full_ml_algo=ml_algo_name;
        }
        processing_options=JSON.parse(localStorage.getItem("processing_options"))
        console.log(processing_options)
        send_feats_train=JSON.parse(localStorage.getItem("selected_feats"));
        localStorage.setItem("full_ml_algo",full_ml_algo)
        $.ajax({
            url:'/full_river_predictions',
            type:'POST',
            data:{ feats:JSON.stringify(feats), actual_status: actual_status, actual_variable: JSON.stringify(actual_col), full_ml_algo: JSON.stringify(full_ml_algo), process_options: JSON.stringify(processing_options), send_feats_train : JSON.stringify(send_feats_train) },
            success: function(full_preds_sample)
            {
                if(actual_status=="No")
                {
                    full_preds_sample_json=JSON.stringify(full_preds_sample);
                    full_preds_sample_csv=ConvertToCSV(full_preds_sample_json);
                    full_preds_sample_table=csv_to_table(full_preds_sample_csv.split(/\r?\n|\r/), tid="table11", caption=" Full river Sample data");
                    document.getElementById("full_preds_sample_table").innerHTML=full_preds_sample_table;
                    $('#full_preds_sample_table').show();
                    console.log("successfully done- with no actual column")
                }

                else if(actual_status=="Yes")
                {
                    full_preds_sample_json=JSON.stringify(full_preds_sample.preds);
                    full_preds_sample_csv=ConvertToCSV(full_preds_sample_json);
                    full_preds_sample_table=csv_to_table(full_preds_sample_csv.split(/\r?\n|\r/), tid="table11", caption=" Full river Sample data");
                    document.getElementById("full_preds_sample_table").innerHTML=full_preds_sample_table;

                    full_preds_sample__metrics_json=JSON.stringify(full_preds_sample.metrics);
                    full_preds_sample_metrics_csv=ConvertToCSV(full_preds_sample__metrics_json);
                    full_preds_sample_metrics_table=csv_to_table_two_rows(full_preds_sample_metrics_csv.split(/\r?\n|\r/), tid="table12", caption=" Full river Predictions vs Actual metric scores");
                    document.getElementById("full_preds_metrics_table").innerHTML=full_preds_sample_metrics_table;

                    $('#full_preds_metrics_table').show();
                    $('#full_preds_sample_table').show();
                    console.log("successfully done- with actual column")

                }

                $('#model_save_div').show();
            }
        })

});
});


// Function for model save to folder
$(document).ready(function(){
    $('[name=model_save]').change(function(){
        if($('[name=model_save]').is(':checked'))
        {
            $('#model_save_loc').show();
            $('#save_files').show();
            $('#save_files').click(function()
            {
                model_save_loc=$('#model_save_loc').val();
                ml_algo_name=localStorage.getItem("ml_algo_name")
                console.log(model_save_loc)
                $.ajax({
                    url:'/save_model_files',
                    type:'POST',
                    data:{ model_save_loc:JSON.stringify(model_save_loc), ml_algo_name: JSON.stringify(ml_algo_name) },
                    success:function(status)
                    {
                        if(status=="Done")
                        {
                            alert(" Model files saved to "+ model_save_loc+". Press OK to continue ")
                        }
                    },
                    error: function()
                    {
                        alert(" Something went wrong- could not save model files ")
                    }
                })
            });
        }  
        else{
            $('#model_save_loc').hide();
        }
    });  
});









//################################################## HELPER FUNCTIONS###########################################################################################
function csv_to_table(river_data,tid,caption)
{
    var table_data='<table class="table table-bordered table striped" id='+tid+'>';
    if(caption)
    {
        table_data+='<caption>'+caption+'</caption>';
    }      
    //console.log(" AS ARRAY :"+ CSVToArray(e.target.result,',')[1][0]+'...'+ CSVToArray(e.target.result,',')[2]+'...'+ CSVToArray(e.target.result,',')[3]);
    for(var count=0; count<6; count++)
    {
        var cell_data=river_data[count].split(',');
        table_data+='<tr>';
        // Collect only headers
        var headers=[];
            for(var cell_count=0; cell_count<cell_data.length; cell_count++)
            {	// triple 0 means ==0 and ==integer type(0's type)
                if(count===0)
                {   
                    table_data+='<th>'+cell_data[cell_count]+'</th>';
                    headers.push(cell_data[cell_count]);
                   // console.log("headers",headers)
                    //to store an array to locastorage- make it to json
                    
                    localStorage.setItem("headers",JSON.stringify(headers));
                }
                
                else
                {
                    table_data+='<td>'+cell_data[cell_count]+'</td>';
                }
            }
            table_data+='</tr>';
    }
    table_data+='</table>';
    return table_data;
}


//for non-5 header tables
function csv_to_table_two_rows(river_data,tid,caption)
{
    var table_data='<table class="table table-bordered table striped" id='+tid+'>';
    if(caption)
    {
        table_data+='<caption>'+caption+'</caption>';
    }      
    //console.log(" AS ARRAY :"+ CSVToArray(e.target.result,',')[1][0]+'...'+ CSVToArray(e.target.result,',')[2]+'...'+ CSVToArray(e.target.result,',')[3]);
    for(var count=0; count<2; count++)
    {
        var cell_data=river_data[count].split(',');
        table_data+='<tr>';
        // Collect only headers
        var headers=[];
            for(var cell_count=0; cell_count<cell_data.length; cell_count++)
            {	// triple 0 means ==0 and ==integer type(0's type)
                if(count===0)
                {   
                    table_data+='<th>'+cell_data[cell_count]+'</th>';
                    headers.push(cell_data[cell_count]);
                    //to store an array to locastorage- make it to json
                    
                    localStorage.setItem("headers",JSON.stringify(headers));
                }
                
                else
                {
                    table_data+='<td>'+cell_data[cell_count]+'</td>';
                }
            }
            table_data+='</tr>';
    }
    table_data+='</table>';
    return table_data;
}

// }); https://stackoverflow.com/questions/33556732/flask-return-pandas-dataframe-to-ajax-errno-10054
//https://thoughtbot.com/blog/ridiculously-simple-ajax-uploads-with-formdata
//https://stackoverflow.com/questions/20310134/post-a-file-to-flask-url-using-ajax?noredirect=1&lq=1

function CSVToArray( strData, strDelimiter ){
    // Check to see if the delimiter is defined. If not,
    // then default to comma.
    strDelimiter = (strDelimiter || ",");
    // Create a regular expression to parse the CSV values.
    var objPattern = new RegExp(
        (
            // Delimiters.
            "(\\" + strDelimiter + "|\\r?\\n|\\r|^)" +
            // Quoted fields.
            "(?:\"([^\"]*(?:\"\"[^\"]*)*)\"|" +
         // Standard fields.
            "([^\"\\" + strDelimiter + "\\r\\n]*))"
        ),
        "gi"
        );
    // Create an array to hold our data. Give the array
    // a default empty first row.
    var arrData = [[]];
  // Create an array to hold our individual pattern
    // matching groups.
    var arrMatches = null;
    // Keep looping over the regular expression matches
    // until we can no longer find a match.
    while (arrMatches = objPattern.exec( strData )){
        // Get the delimiter that was found.
        var strMatchedDelimiter = arrMatches[ 1 ];
      // Check to see if the given delimiter has a length
        // (is not the start of string) and if it matches
        // field delimiter. If id does not, then we know
        // that this delimiter is a row delimiter.
        if (
            strMatchedDelimiter.length &&
            strMatchedDelimiter !== strDelimiter
            ){
          // Since we have reached a new row of data,
            // add an empty row to our data array.
            arrData.push( [] );
        }
        var strMatchedValue;
        // Now that we have our delimiter out of the way,
        // let's check to see which kind of value we
        // captured (quoted or unquoted).
        if (arrMatches[ 2 ]){
          // We found a quoted value. When we capture
            // this value, unescape any double quotes.
            strMatchedValue = arrMatches[ 2 ].replace(
                new RegExp( "\"\"", "g" ),
                "\""
                );

        } else {
          // We found a non-quoted value.
            strMatchedValue = arrMatches[ 3 ];

        }
        // Now that we have our value string, let's add
        // it to the data array.
        arrData[ arrData.length - 1 ].push( strMatchedValue );
    }
    // Return the parsed data.
    return( arrData );
}

//var csv is the CSV file with headers
function csvJSON(csv){

    var lines=csv.split("\n"); 
    var result = []; 
    var headers=lines[0].split(","); 
    for(var i=1;i<lines.length;i++){
        var obj = {};
        var currentline=lines[i].split(",");
  
        for(var j=0;j<headers.length;j++){
            obj[headers[j]] = currentline[j];
        }
        result.push(obj);
    }  
    //return result; //JavaScript object
    return JSON.stringify(result); //JSON
  }

    // JSON to CSV Converter
    function ConvertToCSV(objArray) {
        //use parse to convert string to actual json object
        var array = typeof objArray != 'object' ? JSON.parse(objArray) : objArray;
        var str = '';
        var popy='';
        console.log(typeof objArray);
        console.log(typeof array);
        
        //to get headers first
        for (bar in array[1])
        {
            if (popy != '') popy += ','
            popy+=bar;
        }

        str+=popy+ '\r\n';
        for (var i = 0; i < array.length; i++) {
            var line = '';
            
            for (var index in array[i]) {
              
                if (line != '') line += ','
                
                line += array[i][index];
                
            }
           str += line + '\r\n';
        }
        return str;
    }

