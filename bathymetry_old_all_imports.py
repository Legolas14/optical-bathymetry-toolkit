from flask import send_file,Flask, render_template, url_for, flash, redirect,request,jsonify
import webbrowser
import threading
from threading import Timer
import pandas as pd, matplotlib.pyplot as plt, numpy, scipy
from werkzeug.utils import secure_filename
import os,numpy as np
import shutil
from flask import send_from_directory
import ujson,json
from scipy.stats import pearsonr 
from functools import reduce
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
import scipy as sc
import plotly_express as px
import plotly
import time
import plotly.graph_objects as go
from sklearn.ensemble import IsolationForest
import random
from sklearn.preprocessing import PolynomialFeatures
from sklearn.pipeline import make_pipeline
from sklearn.model_selection import train_test_split
from xgboost import XGBRegressor
from sklearn.linear_model import LinearRegression
from sklearn.ensemble import RandomForestRegressor
from sklearn.metrics import mean_squared_error as mse
from sklearn.metrics import r2_score
from sklearn.metrics import mean_absolute_error
from sklearn.metrics import mean_poisson_deviance
from sklearn.metrics import max_error
from sklearn.metrics import explained_variance_score
from sklearn.svm import SVR
from sklearn.tree import DecisionTreeRegressor
from sklearn.externals.six import StringIO  
from IPython.display import Image  
from sklearn.tree import export_graphviz                                        
from subprocess import call
import matplotlib.pyplot as plt
from sklearn.inspection import PartialDependenceDisplay
from sklearn.inspection import plot_partial_dependence
from rfpimp import permutation_importances
import shap
import os,sys
import joblib
from sklearn.cluster import KMeans
from sklearn.cluster import MiniBatchKMeans
import os,sys
from sklearn.decomposition import PCA
import time
from sklearn.linear_model import Lasso
from sklearn.feature_selection import SelectFromModel
from sklearn.model_selection import GridSearchCV 
from sklearn.manifold import TSNE
from sklearn.preprocessing import MinMaxScaler
from sklearn.preprocessing import StandardScaler
from sklearn.preprocessing import RobustScaler
from sklearn.preprocessing import PowerTransformer
from sklearn.cross_decomposition import PLSRegression
from sklearn.neural_network import MLPRegressor
import shutil
app=Flask(__name__)

# I set an secret key to protect from forgery and cyber attacks..get random values from python secret token_hex(16)
app.config['SECRET_KEY']='1f49b3737521e1dab26d590be6b56dab'

def open_browser():
      webbrowser.open_new('http://127.0.0.1:5000/')

def load_data(data_path):
    data=pd.read_csv('model_files/'+data_path)
    return data

def distance_cal(x1,y1,x2,y2):
   return np.sqrt(((x2-x1)**2)+((y2-y1)**2)) 

def tts(df,y,test_split):#0.15 for hutt
    x_train,x_test,y_train,y_test=train_test_split(df,y,test_size=test_split,random_state=0,shuffle=True)
    return x_train,x_test,y_train,y_test
    
def logperm_gen(x,r,g,b):
    x['ln(b/r)']=0
    x['ln(b/g)']=0
    x['ln(b/r)'] = x.apply(lambda row: np.log10((row[b]/row[r])), axis=1)
    x['ln(b/g)'] = x.apply(lambda row: np.log10((row[b]/row[g])), axis=1)
    return x

def greyscale_gen(x,r,g,b):
    x['Greyscale']=x.apply(lambda row: (0.3*row[r] + 0.59*row[g] + 0.11*row[b]), axis=1)
    return x

def cluster_data(data_new,r,g,b):
    data_new['RGB_Combo']=0
    data_new['RGB_Combo']=data_new.apply(lambda row: row[r]*row[g]*row[b], axis=1)
    data_new=data_new.reset_index().drop(['index'],axis=1)
    rgb=np.vstack((data_new[[r,g,b]].values))
    wcss=[]
    mapping={}
    for i in range(1,10):
      kmeans = MiniBatchKMeans(n_clusters=i,random_state=10).fit(rgb)
      wcss.append(kmeans.inertia_)
      mapping[i]=kmeans.inertia_

    optimal=0
    diff=[]
    for i in range(2,10):
      diff.append(-(mapping[i]-mapping[i-1]))
    diff_mean=np.mean(diff)
    for i in range(2,10):
      if((mapping[i-1]-mapping[i]>diff_mean)==False):
        optimal=i-1
        break
    kmeans = MiniBatchKMeans(n_clusters=3 ,random_state=10).fit(rgb)
    data_new.loc[:, 'point_cluster'] = kmeans.predict(rgb)
    joblib.dump(kmeans,'model_files/'+'kmeans.pkl')
    data_new['point_cluster']=data_new['point_cluster'].astype('int64')
    # store this csv to use for caluculating mean/med during full river predictions
    data_new.to_csv('model_files/'+'min_med_max.csv',index=False)
    max_indx=data_new.groupby(['point_cluster']).mean()['RGB_Combo'][data_new.groupby(['point_cluster']).mean()['RGB_Combo']==np.max(data_new.groupby(['point_cluster']).mean()['RGB_Combo'])].index
    min_indx=data_new.groupby(['point_cluster']).mean()['RGB_Combo'][data_new.groupby(['point_cluster']).mean()['RGB_Combo']==np.min(data_new.groupby(['point_cluster']).mean()['RGB_Combo'])].index
    indexs=list(data_new.groupby(['point_cluster']).mean().index)
    indexs.remove(min_indx)
    indexs.remove(max_indx)
    med_indx=indexs[0]
    data_new['Point_cluster']=''
    data_new['Point_cluster']=np.where(data_new['point_cluster']==max_indx[0],'High',data_new['Point_cluster'])
    data_new['Point_cluster']=np.where(data_new['point_cluster']==med_indx,'Medium',data_new['Point_cluster'])
    data_new['Point_cluster']=np.where(data_new['point_cluster']==min_indx[0],'Low',data_new['Point_cluster']) 
    data_new.drop(['point_cluster'],axis=1,inplace=True) 
    data_clean=peak_ratio_rgb(data_new,r,g,b)
    data_clean=pd.get_dummies(data_clean)
    data_clean.drop(['RGB_Combo'],axis=1,inplace=True)     
    return data_clean

def peak_ratio_rgb(hutt_full,r,g,b):
    peak_r=hutt_full.groupby(['Point_cluster']).max()[r]
    peak_g=hutt_full.groupby(['Point_cluster']).max()[g]
    peak_b=hutt_full.groupby(['Point_cluster']).max()[b]
    peak_rgb=hutt_full.groupby(['Point_cluster']).max()['RGB_Combo']
    hutt_full[[r,g,b,'RGB_Combo','Point_cluster']].to_csv('model_files/'+'groupby.csv',index=False)
    
    hutt_full['Ratio_peak_R']=0.0
    hutt_full['Ratio_peak_G']=0.0
    hutt_full['Ratio_peak_B']=0.0
    hutt_full['Ratio_peak_RGBCombo']=0.0
    
    #low
    hutt_full['Ratio_peak_R']=np.where(hutt_full['Point_cluster']=='Low',hutt_full[r]/peak_r['Low'],hutt_full['Ratio_peak_R'])
    hutt_full['Ratio_peak_G']=np.where(hutt_full['Point_cluster']=='Low',hutt_full[g]/peak_g['Low'],hutt_full['Ratio_peak_G'])
    hutt_full['Ratio_peak_B']=np.where(hutt_full['Point_cluster']=='Low',hutt_full[b]/peak_b['Low'],hutt_full['Ratio_peak_B'])
    hutt_full['Ratio_peak_RGBCombo']=np.where(hutt_full['Point_cluster']=='Low',hutt_full['RGB_Combo']/peak_rgb['Low'],hutt_full['Ratio_peak_RGBCombo'])
    
    #medium
    hutt_full['Ratio_peak_R']=np.where(hutt_full['Point_cluster']=='Medium',hutt_full[r]/peak_r['Medium'],hutt_full['Ratio_peak_R'])
    hutt_full['Ratio_peak_G']=np.where(hutt_full['Point_cluster']=='Medium',hutt_full[g]/peak_g['Medium'],hutt_full['Ratio_peak_G'])
    hutt_full['Ratio_peak_B']=np.where(hutt_full['Point_cluster']=='Medium',hutt_full[b]/peak_b['Medium'],hutt_full['Ratio_peak_B'])
    hutt_full['Ratio_peak_RGBCombo']=np.where(hutt_full['Point_cluster']=='Medium',hutt_full['RGB_Combo']/peak_rgb['Medium'],hutt_full['Ratio_peak_RGBCombo'])
    
    #high
    hutt_full['Ratio_peak_R']=np.where(hutt_full['Point_cluster']=='High',hutt_full[r]/peak_r['High'],hutt_full['Ratio_peak_R'])
    hutt_full['Ratio_peak_G']=np.where(hutt_full['Point_cluster']=='High',hutt_full[g]/peak_g['High'],hutt_full['Ratio_peak_G'])
    hutt_full['Ratio_peak_B']=np.where(hutt_full['Point_cluster']=='High',hutt_full[b]/peak_b['High'],hutt_full['Ratio_peak_B'])
    hutt_full['Ratio_peak_RGBCombo']=np.where(hutt_full['Point_cluster']=='High',hutt_full['RGB_Combo']/peak_rgb['High'],hutt_full['Ratio_peak_RGBCombo'])
    print(peak_r,peak_g,peak_b) 
    return hutt_full

def poly_creation_cluster(df,y):
    CAT_COUNTER=0
    if(all(x in df.columns for x in ['Point_cluster_Low','Point_cluster_Medium','Point_cluster_High'])):
        pcl=df[['Point_cluster_Low','Point_cluster_Medium','Point_cluster_High']].copy()
        df.drop(['Point_cluster_Low','Point_cluster_Medium','Point_cluster_High'],axis=1,inplace=True)
        CAT_COUNTER=1
    poly = PolynomialFeatures(2)
    x_poly=poly.fit_transform(df)
    poly_names=poly.get_feature_names(['Feature'+str(l) for l in range(1,len(np.array(poly.get_feature_names())))])
    df_poly=pd.DataFrame(x_poly,columns=poly_names)
    df_poly.drop(['1'],inplace=True,axis=1)
    #append the clusters
    if(CAT_COUNTER==1):
        df_poly=pd.concat([df_poly,pcl],axis=1)
    return df_poly

def poly_creation_no_cluster(df,y):
    print(df.dtypes)
    df=df.reset_index().drop(['index'],axis=1)
    df=pd.DataFrame(df)
    poly = PolynomialFeatures(2)
    x_poly=poly.fit_transform(df)
    poly_names=poly.get_feature_names(['Feature'+str(l) for l in range(1,len(np.array(poly.get_feature_names())))])
    df_poly=pd.DataFrame(x_poly,columns=poly_names)
    df_poly.drop(['1'],inplace=True,axis=1)
    #append the clusters
    return df_poly
  

def correl_cluster(df_poly,y,type):
    x_train,x_test,y_train,y_test=tts(df_poly,y,0.2)# tts coz we should not consider properties of test set -avoid data leak
    corr_features = set()
    CAT_COUNTER=0
    if(all(x in df_poly.columns for x in ['Point_cluster_Low','Point_cluster_Medium','Point_cluster_High'])):           
        #remove pcl again, pearsosn corr not appropriate for categorical variables
        pcl_tr=x_train[['Point_cluster_Low','Point_cluster_Medium','Point_cluster_High']].copy()
        x_train.drop(['Point_cluster_Low','Point_cluster_Medium','Point_cluster_High'],axis=1,inplace=True)
        CAT_COUNTER=1
    # create the correlation matrix (default to pearson)
    # updated corr analysis- if two have high corr- check which has <80 to target. If both high - leave. If both low- remove i.
    corr_matrix = x_train.corr()
    from sklearn.linear_model import Lasso
    from sklearn.feature_selection import SelectFromModel
    for i in range(len(corr_matrix.columns)):
        for j in range(i):
            if abs(corr_matrix.iloc[i, j]) > 0.90:
                corr_target_i,_=pearsonr(x_train[corr_matrix.columns[i]],y_train)
                corr_target_j,_=pearsonr(x_train[corr_matrix.columns[j]],y_train)
                if((abs(corr_target_i)>0.60) & (abs(corr_target_j)>0.60)):
                    print("both high", corr_target_i,corr_matrix.columns[i])
                    continue
                if((abs(corr_target_i)>0.60) & (abs(corr_target_j)<0.60)):
                    colname = corr_matrix.columns[j]
                    corr_features.add(colname)
                    print("J LOW", corr_matrix.columns[i],corr_matrix.columns[j])
                if((abs(corr_target_i)<0.60) & (abs(corr_target_j)>0.60)):
                    colname = corr_matrix.columns[i]
                    corr_features.add(colname)
                    print("I LOW", corr_matrix.columns[i],corr_matrix.columns[j])
                if((abs(corr_target_i)<0.60) & (abs(corr_target_j)<0.60)):
                    colname = corr_matrix.columns[i]
                    corr_features.add(colname)
                    print("BOTH LOW", corr_target_i,corr_matrix.columns[i])
    
    #print(corr_features)         
    x_train.drop(labels=corr_features, axis=1, inplace=True)
    
    if(CAT_COUNTER==1):
        #append back to use lasso on these columns...without any interactions among categorical variables by polynomial fc
        x_train=pd.concat([x_train,pcl_tr],axis=1)
    selected_feat=x_train.columns
    X_train_selected=df_poly[selected_feat]
    if(type=="rf"):
        X_train_selected.to_csv('model_files/'+'correl_cluster_rf.csv',index=False)
    if(type=="nn"):
        X_train_selected.to_csv('model_files/'+'correl_cluster_nn.csv',index=False)
    if(type=="common"):
        X_train_selected.to_csv('model_files/'+'correl_cluster_common.csv',index=False)    
    return X_train_selected

def correl_no_cluster(df_poly,y,type):
    x_train,x_test,y_train,y_test=tts(df_poly,y,0.2)# tts coz we should not consider properties of test set -avoid data leak
    corr_features = set()
    # create the correlation matrix (default to pearson)
    corr_matrix = x_train.corr()
    from sklearn.linear_model import Lasso
    from sklearn.feature_selection import SelectFromModel
    for i in range(len(corr_matrix.columns)):
        for j in range(i):
            if abs(corr_matrix.iloc[i, j]) > 0.90:
                corr_target_i,_=pearsonr(x_train[corr_matrix.columns[i]],y_train)
                corr_target_j,_=pearsonr(x_train[corr_matrix.columns[j]],y_train)
                if((abs(corr_target_i)>0.60) & (abs(corr_target_j)>0.60)):
                    print("both high", corr_target_i,corr_matrix.columns[i])
                    continue
                if((abs(corr_target_i)>0.60) & (abs(corr_target_j)<0.60)):
                    colname = corr_matrix.columns[j]
                    corr_features.add(colname)
                    print("J LOW", corr_matrix.columns[i],corr_matrix.columns[j])
                if((abs(corr_target_i)<0.60) & (abs(corr_target_j)>0.60)):
                    colname = corr_matrix.columns[i]
                    corr_features.add(colname)
                    print("I LOW", corr_matrix.columns[i],corr_matrix.columns[j])
                if((abs(corr_target_i)<0.60) & (abs(corr_target_j)<0.60)):
                    colname = corr_matrix.columns[i]
                    corr_features.add(colname)
                    print("BOTH LOW", corr_target_i,corr_matrix.columns[i])
                
    x_train.drop(labels=corr_features, axis=1, inplace=True)
    selected_feat=x_train.columns
    X_train_selected=df_poly[selected_feat]
    if(type=="rf"):
        X_train_selected.to_csv('model_files/'+'correl_cluster_rf.csv',index=False)
    if(type=="nn"):
        X_train_selected.to_csv('model_files/'+'correl_cluster_nn.csv',index=False)
    if(type=="common"):
        X_train_selected.to_csv('model_files/'+'correl_cluster_common.csv',index=False)      
    
    return X_train_selected

def lasso_reg(df,y):
    # scaler = StandardScaler()
    # scaler.fit(x_train)
    sel_=Lasso(alpha=0.01, max_iter=10e5)
    x_train,x_test,y_train,y_test=tts(df,y,0.2)
    #defining parameter range 
    param_grid = {'alpha': [0.01]}       
    ls = GridSearchCV(Lasso(max_iter=10e5), param_grid, refit = True, verbose = 3)
    
    ls.fit(x_train, y_train)
    best_coeff=ls.best_params_ 
    sel_=Lasso(alpha=best_coeff['alpha'],max_iter=10e5)
    sel_.fit(x_train, y_train)
    selected_feat = x_train.columns[sel_.coef_!=0]
    # st.write('total features: {}'.format((x_train.shape[1])))
    # st.write('selected features: {}'.format(len(selected_feat)))
    # st.write('features with coefficients shrank to zero: {}'.format(np.sum(sel_.coef_ == 0)))    
    X_train_selected =df[selected_feat]
    X_train_selected.to_csv('model_files/'+'lasso_reg.csv',index=False)
    return X_train_selected

def pca_reduction_no_cluster(temp,y,type):
    pca1=PCA(n_components=3)
    x_train,x_test,y_train,y_test=tts(temp,y,0.2)
    X_train= pca1.fit_transform(x_train)
    X_test = pca1.transform(x_test)
    if(type=="lr"):
        joblib.dump(pca1,'model_files/'+'pca_lr.pkl')
    if(type=="common"):
        joblib.dump(pca1,'model_files/'+'pca_common.pkl')   
    dxr=pd.DataFrame(X_train, columns=[['pca-one','pca-two','pca-three']],index=x_train.index)
    dxt=pd.DataFrame(X_test, columns=[['pca-one','pca-two','pca-three']],index=x_test.index)       
    sub=pd.concat([dxr,dxt],axis=0)
    sub.sort_index(inplace=True)
    return sub

def pca_reduction_cluster(temp,y,type):
    CAT_COUNTER=0
    if(all(x in temp.columns for x in ['Point_cluster_Low','Point_cluster_Medium','Point_cluster_High'])):
        pcl_tr=temp[['Point_cluster_Low','Point_cluster_Medium','Point_cluster_High']].copy()
        temp.drop(['Point_cluster_Low','Point_cluster_Medium','Point_cluster_High'],axis=1,inplace=True)
        CAT_COUNTER=1
    pca1=PCA(n_components=3)
    x_train,x_test,y_train,y_test=tts(temp,y,0.2)
    X_train= pca1.fit_transform(x_train)
    X_test = pca1.transform(x_test)
    if(type=="lr"):
        joblib.dump(pca1,'model_files/'+'pca_lr.pkl')
    if(type=="common"):
        joblib.dump(pca1,'model_files/'+'pca_common.pkl') 
    dxr=pd.DataFrame(X_train, columns=[['pca-one','pca-two','pca-three']],index=x_train.index)
    dxt=pd.DataFrame(X_test, columns=[['pca-one','pca-two','pca-three']],index=x_test.index)       
    sub=pd.concat([dxr,dxt],axis=0)
    sub.sort_index(inplace=True)
    sub.to_csv('model_files/'+'pca_x.csv',index=False)
    subb=pd.read_csv('model_files/'+'pca_x.csv')
    if(CAT_COUNTER==1):
        subb=pd.concat([subb,pcl_tr],axis=1)
        return subb
    else:
        return subb 

def pls_reduction_cluster(temp,y,type):
    CAT_COUNTER=0
    if(all(x in temp.columns for x in ['Point_cluster_Low','Point_cluster_Medium','Point_cluster_High'])):
        pcl_tr=temp[['Point_cluster_Low','Point_cluster_Medium','Point_cluster_High']].copy()
        temp.drop(['Point_cluster_Low','Point_cluster_Medium','Point_cluster_High'],axis=1,inplace=True)
        CAT_COUNTER=1
    pls2=PLSRegression(n_components=3,scale=False)
    x_train,x_test,y_train,y_test=tts(temp,y,0.2)
    X_train= pls2.fit_transform(x_train,y_train)
    X_test = pls2.transform(x_test,y_train)
    if(type=="lr"):
        joblib.dump(pls2,'model_files/'+'pls_lr.pkl')
    if(type=="nn"):
        joblib.dump(pls2,'model_files/'+'pls_nn.pkl')
    if(type=="common"):
        joblib.dump(pls2,'model_files/'+'pls2_common.pkl')
    dxr=pd.DataFrame(X_train, columns=[['pls-one','pls-two','pls-three']],index=x_train.index)
    dxt=pd.DataFrame(X_test, columns=[['pls-one','pls-two','pls-three']],index=x_test.index)       
    sub=pd.concat([dxr,dxt],axis=0)
    sub.sort_index(inplace=True)
    sub.to_csv('model_files/'+'pls_x.csv',index=False)
    subb=pd.read_csv('model_files/'+'pls_x.csv')
    if(CAT_COUNTER==1):
        subb=pd.concat([subb,pcl_tr],axis=1)
    return subb

def pls_reduction_no_cluster(temp,y):
    pls2=PLSRegression(n_components=3,scale=False)
    x_train,x_test,y_train,y_test=tts(temp,y,0.2)
    X_train= pls2.fit_transform(x_train,y_train)
    X_test = pls2.transform(x_test,y_test)
    dxr=pd.DataFrame(X_train, columns=[['pls-one','pls-two','pls-three']],index=x_train.index)
    dxt=pd.DataFrame(X_test, columns=[['pls-one','pls-two','pls-three']],index=x_test.index)       
    sub=pd.concat([dxr,dxt],axis=0)
    sub.sort_index(inplace=True)
    sub.to_csv('model_files/'+'pls_x.csv',index=False)
    subb=pd.read_csv('model_files/'+'pls_x.csv')
    return subb

def standard_scale_no_cluster(new_data,y,type):
    x_train,x_test,y_train,y_test=tts(new_data,y,0.2)
    sc=StandardScaler()
    X_train=sc.fit_transform(x_train)
    X_test=sc.transform(x_test)
    if(type=="lr"):
        joblib.dump(sc,'model_files/'+'sc_lr.pkl')
    if(type=="nn"):
        joblib.dump(sc,'model_files/'+'sc_nn.pkl')
    if(type=="common"):
        joblib.dump(sc,'model_files/'+'sc_common.pkl')        
    cols=new_data.columns
    dxr=pd.DataFrame(X_train, columns=cols,index=x_train.index)
    dxt=pd.DataFrame(X_test, columns=cols,index=x_test.index)       
    scaled_data=pd.concat([dxr,dxt],axis=0)
    scaled_data.sort_index(inplace=True)
    return scaled_data

def standard_scale_cluster(new_data,y,type):
    CAT_COUNTER=0
    if(all(x in new_data.columns for x in ['Point_cluster_Low','Point_cluster_Medium','Point_cluster_High'])):
        pcl_tr=new_data[['Point_cluster_Low','Point_cluster_Medium','Point_cluster_High']].copy()
        new_data.drop(['Point_cluster_Low','Point_cluster_Medium','Point_cluster_High'],axis=1,inplace=True)
        CAT_COUNTER=1
    x_train,x_test,y_train,y_test=tts(new_data,y,0.2) 
    sc=StandardScaler()
    X_train=sc.fit_transform(x_train)
    X_test=sc.transform(x_test)
    if(type=="lr"):
        joblib.dump(sc,'model_files/'+'sc_lr.pkl')
    if(type=="nn"):
        joblib.dump(sc,'model_files/'+'sc_nn.pkl')
    if(type=="common"):
        joblib.dump(sc,'model_files/'+'sc_common.pkl')
    cols=new_data.columns
    dxr=pd.DataFrame(X_train, columns=cols,index=x_train.index)
    dxt=pd.DataFrame(X_test, columns=cols,index=x_test.index)       
    scaled_data=pd.concat([dxr,dxt],axis=0)
    if(CAT_COUNTER==1):
        scaled_data=pd.concat([scaled_data,pcl_tr],axis=1)
    scaled_data.sort_index(inplace=True)
    return scaled_data

def min_max_scale_cluster(new_data,y,type):
    CAT_COUNTER=0
    if(all(x in new_data.columns for x in ['Point_cluster_Low','Point_cluster_Medium','Point_cluster_High'])):
        pcl_tr=new_data[['Point_cluster_Low','Point_cluster_Medium','Point_cluster_High']].copy()
        new_data.drop(['Point_cluster_Low','Point_cluster_Medium','Point_cluster_High'],axis=1,inplace=True)
        CAT_COUNTER=1
    x_train,x_test,y_train,y_test=tts(new_data,y,0.2) 
    ms=MinMaxScaler()
    X_train=ms.fit_transform(x_train)
    X_test=ms.transform(x_test)
    if(type=="lr"):
        joblib.dump(ms,'model_files/'+'ms_lr.pkl')
    if(type=="nn"):
        joblib.dump(ms,'model_files/'+'ms_nn.pkl')
    if(type=="common"):
        joblib.dump(ms,'model_files/'+'ms_common.pkl')
    cols=new_data.columns
    dxr=pd.DataFrame(X_train, columns=cols,index=x_train.index)
    dxt=pd.DataFrame(X_test, columns=cols,index=x_test.index)       
    scaled_data=pd.concat([dxr,dxt],axis=0)
    if(CAT_COUNTER==1):
        scaled_data=pd.concat([scaled_data,pcl_tr],axis=1)
    scaled_data.sort_index(inplace=True)
    return scaled_data

def min_max_scale_no_cluster(new_data,y,type):
    x_train,x_test,y_train,y_test=tts(new_data,y,0.2) 
    ms=MinMaxScaler()
    X_train=ms.fit_transform(x_train)
    X_test=ms.transform(x_test)
    if(type=="lr"):
        joblib.dump(ms,'model_files/'+'ms_lr.pkl')
    if(type=="nn"):
        joblib.dump(ms,'model_files/'+'ms_nn.pkl')
    if(type=="common"):
        joblib.dump(ms,'model_files/'+'ms_common.pkl')
    cols=new_data.columns
    dxr=pd.DataFrame(X_train, columns=cols,index=x_train.index)
    dxt=pd.DataFrame(X_test, columns=cols,index=x_test.index)       
    scaled_data=pd.concat([dxr,dxt],axis=0)
    scaled_data.sort_index(inplace=True)
    return scaled_data

def robust_scale_cluster(new_data,y,type):
    CAT_COUNTER=0
    if(all(x in new_data.columns for x in ['Point_cluster_Low','Point_cluster_Medium','Point_cluster_High'])):
        pcl_tr=new_data[['Point_cluster_Low','Point_cluster_Medium','Point_cluster_High']].copy()
        new_data.drop(['Point_cluster_Low','Point_cluster_Medium','Point_cluster_High'],axis=1,inplace=True)
        CAT_COUNTER=1
    x_train,x_test,y_train,y_test=tts(new_data,y,0.2) 
    rs=RobustScaler()
    X_train=rs.fit_transform(x_train)
    X_test=rs.transform(x_test)
    if(type=="lr"):
        joblib.dump(rs,'model_files/'+'rs_lr.pkl')
    if(type=="nn"):
        joblib.dump(rs,'model_files/'+'rs_nn.pkl')
    if(type=="common"):
        joblib.dump(rs,'model_files/'+'rs_common.pkl')
    cols=new_data.columns
    dxr=pd.DataFrame(X_train, columns=cols,index=x_train.index)
    dxt=pd.DataFrame(X_test, columns=cols,index=x_test.index)       
    scaled_data=pd.concat([dxr,dxt],axis=0)
    if(CAT_COUNTER==1):
        scaled_data=pd.concat([scaled_data,pcl_tr],axis=1)
    scaled_data.sort_index(inplace=True)
    return scaled_data

def robust_scale_no_cluster(new_data,y,type):
    x_train,x_test,y_train,y_test=tts(new_data,y,0.2)
    rs=RobustScaler()
    X_train=rs.fit_transform(x_train)
    X_test=rs.transform(x_test)
    if(type=="lr"):
        joblib.dump(rs,'model_files/'+'rs_lr.pkl')
    if(type=="nn"):
        joblib.dump(rs,'model_files/'+'rs_nn.pkl')
    if(type=="common"):
        joblib.dump(rs,'model_files/'+'rs_common.pkl')
    cols=new_data.columns
    dxr=pd.DataFrame(X_train, columns=cols,index=x_train.index)
    dxt=pd.DataFrame(X_test, columns=cols,index=x_test.index)       
    scaled_data=pd.concat([dxr,dxt],axis=0)
    scaled_data.sort_index(inplace=True)
    return scaled_data

def power_scale_cluster(new_data,y,type):
    CAT_COUNTER=0
    if(all(x in new_data.columns for x in ['Point_cluster_Low','Point_cluster_Medium','Point_cluster_High'])):
        pcl_tr=new_data[['Point_cluster_Low','Point_cluster_Medium','Point_cluster_High']].copy()
        new_data.drop(['Point_cluster_Low','Point_cluster_Medium','Point_cluster_High'],axis=1,inplace=True)
        CAT_COUNTER=1
    x_train,x_test,y_train,y_test=tts(new_data,y,0.2) 
    pt=PowerTransformer()
    X_train=pt.fit_transform(x_train)
    X_test=pt.transform(x_test)
    if(type=="lr"):
        joblib.dump(pt,'model_files/'+'pt_lr.pkl')
    if(type=="nn"):
        joblib.dump(pt,'model_files/'+'pt_nn.pkl')
    if(type=="common"):
        joblib.dump(pt,'model_files/'+'pt_common.pkl')
    cols=new_data.columns
    dxr=pd.DataFrame(X_train, columns=cols,index=x_train.index)
    dxt=pd.DataFrame(X_test, columns=cols,index=x_test.index)       
    scaled_data=pd.concat([dxr,dxt],axis=0)
    if(CAT_COUNTER==1):
        scaled_data=pd.concat([scaled_data,pcl_tr],axis=1)
    scaled_data.sort_index(inplace=True)
    return scaled_data

def power_scale_no_cluster(new_data,y,type):
    x_train,x_test,y_train,y_test=tts(new_data,y,0.2) 
    pt=PowerTransformer()
    X_train=pt.fit_transform(x_train)
    X_test=pt.transform(x_test)
    if(type=="lr"):
        joblib.dump(pt,'model_files/'+'pt_lr.pkl')
    if(type=="nn"):
        joblib.dump(pt,'model_files/'+'pt_nn.pkl')
    if(type=="common"):
        joblib.dump(pt,'model_files/'+'pt_common.pkl')
    cols=new_data.columns
    dxr=pd.DataFrame(X_train, columns=cols,index=x_train.index)
    dxt=pd.DataFrame(X_test, columns=cols,index=x_test.index)       
    scaled_data=pd.concat([dxr,dxt],axis=0)
    scaled_data.sort_index(inplace=True)
    return scaled_data

def train_result(model,x_train,y_train,model_name):
    predictions_all=pd.DataFrame(y_train)     
    y_pred_model=model.predict(x_train)
    col1=predictions_all.columns.tolist()[0]
    predictions_all['Model_full_Pred']=y_pred_model
    r2s=r2_score(predictions_all[col1],predictions_all['Model_full_Pred'])
    rmses=np.sqrt(mse(predictions_all[col1],predictions_all['Model_full_Pred']))
    maes=mean_absolute_error(predictions_all[col1],predictions_all['Model_full_Pred'])
    evars=explained_variance_score(predictions_all[col1],predictions_all['Model_full_Pred'])
    maxs=max_error(predictions_all[col1],predictions_all['Model_full_Pred'])
    tmp=[]
    tmp.append([model_name,rmses,maes,r2s,maxs,evars])
    print("train",tmp)

    final_scores_train=pd.DataFrame(tmp,columns=['1. Model Name','2. Root Mean Squared Error(RMSE)','3. Mean Absolute Error(MAE)','4. R2 Spearman Coefficient of Determination','5. Maximum Residual Error','6. Explained variance Score'])    
    return final_scores_train

def test_result(model,x_test,y_test,model_name):
    predictions_all=pd.DataFrame(y_test)     
    y_pred_model=model.predict(x_test)
    col1=predictions_all.columns.tolist()[0]
    predictions_all['Model_full_Pred']=y_pred_model
    r2s=r2_score(predictions_all[col1],predictions_all['Model_full_Pred'])
    rmses=np.sqrt(mse(predictions_all[col1],predictions_all['Model_full_Pred']))
    maes=mean_absolute_error(predictions_all[col1],predictions_all['Model_full_Pred'])
    evars=explained_variance_score(predictions_all[col1],predictions_all['Model_full_Pred'])
    maxs=max_error(predictions_all[col1],predictions_all['Model_full_Pred'])
    tmp=[]
    print("test",tmp)
    tmp.append([model_name,rmses,maes,r2s,maxs,evars])
    final_scores_test=pd.DataFrame(tmp,columns=['1. Model Name','2. Validation_Root Mean Squared Error(RMSE)','3. Validation_Mean Absolute Error(MAE)','4. Validation_R2 Spearman Coefficient of Determination','5. Validation_Maximum Residual Error','6. Validation_Explained variance Score'])    
    return final_scores_test



def LR_reg(totrain,x_train,x_test,y_train,y_test,predictions_all):
    lr=LinearRegression()
    lr.fit(x_train,y_train)
    #predictions_all=pd.DataFrame(y_train)
    y_pred_xgb_all=lr.predict(totrain)
    predictions_all['LR_full_Pred']=y_pred_xgb_all
    final_scores_test=train_result(lr,x_train,y_train,'Multi-Linear Regression (LR)')
    final_test=test_result(lr,x_test,y_test,'Multi-Linear Regression (LR)')
    return lr,predictions_all,final_scores_test,final_test
             
def XGB_reg(totrain,x_train,x_test,y_train,y_test,predictions_all):
    eval_set = [(x_test, y_test)]
    # A parameter grid for XGBoost
    params = {
            #'min_child_weight': [6,7,8],
           # 'subsample': [0.7,0.5],
           # 'max_depth': [5,7,8],
            'eta':[0.05 , 0.03 ]
            
            }
    
    # magic with cv
    from sklearn.model_selection import GridSearchCV 

    xgb = GridSearchCV(XGBRegressor(n_estimators=500,early_stopping_rounds=10, eval_metric='mae', eval_set=eval_set,random_state=10), params, refit = True, verbose = 3) 
    xgb.fit(x_train,y_train)    
    y_pred_xgb_all=xgb.predict(totrain)
    #predictions_all=pd.DataFrame(y_train)
    predictions_all['XGB_full_Pred']=y_pred_xgb_all
    final_scores_test=train_result(xgb,x_train,y_train,'Extreme Gradient Boosting (XGBoost)')
    final_test=test_result(xgb,x_test,y_test,'Extreme Gradient Boosting (XGBoost)')
    
    return xgb,predictions_all,final_scores_test,final_test

def RF_reg(totrain,x_train,x_test,y_train,y_test,predictions_all):
    rf=RandomForestRegressor(random_state=10)
    rf.fit(x_train,y_train)
    y_pred_xgb_all=rf.predict(totrain)
    #predictions_all=pd.DataFrame(y_train)
    predictions_all['RF_full_Pred']=y_pred_xgb_all
    final_scores_test=train_result(rf,x_train,y_train,'Random Forest Regressor (RF)')
    final_test=test_result(rf,x_test,y_test,'Random Forest Regressor (RF)')
    return rf,predictions_all,final_scores_test,final_test
                    

def NN_reg(totrain,x_train,x_test,y_train,y_test,predictions_all):
    ann=MLPRegressor(hidden_layer_sizes=(500,),activation='relu', solver='adam', alpha=0.001, batch_size='auto',learning_rate='constant', learning_rate_init=0.01, power_t=0.5, max_iter=1000, shuffle=True, random_state=9, tol=0.0001, verbose=False, warm_start=False, momentum=0.9, nesterovs_momentum=True,early_stopping=True, validation_fraction=0.1, beta_1=0.9, beta_2=0.999, epsilon=1e-08)
    ann.fit(x_train,y_train)
    y_pred_xgb_all=ann.predict(totrain)
    #predictions_all=pd.DataFrame(y_train)
    predictions_all['NN_full_Pred']=y_pred_xgb_all
    final_scores_test=train_result(ann,x_train,y_train,'Multi-layer Perceptron (Neural Network)')
    final_test=test_result(ann,x_test,y_test,'Multi-layer Perceptron (Neural Network)')
    
    return ann,predictions_all,final_scores_test,final_test
                                      
def SVR_reg(totrain,x_train,x_test,y_train,y_test,predictions_all):
    svr=SVR(kernel='rbf',C=10)
    #grid.fit(x_train,y_train)   
    #from sklearn.model_selection import GridSearchCV 
  
    # defining parameter range 
    #param_grid = {'C': [0.1, 1, 10, 100],  
    #              'gamma': [1, 0.1, 0.01, 0.001], 
    #              'kernel': ['rbf']}  
      
     
    # fitting the model for grid search 
    svr.fit(x_train, y_train) 
    y_pred_xgb_all=svr.predict(totrain)
    #predictions_all=pd.DataFrame(y_train)
    predictions_all['SVR_full_Pred']=y_pred_xgb_all
    final_scores_test=train_result(svr,x_train,y_train,'Support Vector Regressor (SVR)')
    final_test=test_result(svr,x_test,y_test,'Support Vector Regressor (SVR)')
 
    return svr,predictions_all,final_scores_test,final_test
               
def All_reg(sub_x,nn_x,rf_x,x_train,x_test,y_train,y_test,x_train2,x_test2,y_train2,y_test2,x_train3,x_test3,y_train3,y_test3,predictions_all):

    lr,predictions_all1,fts1,vts1=LR_reg(sub_x,x_train2,x_test2,y_train2,y_test2,predictions_all)
    xgb,predictions_all2,fts2,vts2=XGB_reg(rf_x,x_train,x_test,y_train,y_test,predictions_all1)
    rf,predictions_all3,fts3,vts3=RF_reg(rf_x,x_train,x_test,y_train,y_test,predictions_all2)
    dt,predictions_all4,fts4,vts4=NN_reg(nn_x,x_train3,x_test3,y_train3,y_test3,predictions_all3)
    svr,predictions_all5,fts5,vts5=SVR_reg(sub_x,x_train2,x_test2,y_train2,y_test2,predictions_all4)
    
    vtss=[vts1,vts4,vts3,vts2,vts5]
    valid_results=pd.concat(vtss,axis=0)

    ftss=[fts1,fts4,fts3,fts2,fts5]
    final_scores_test=pd.concat(ftss,axis=0)
    print("final train score",final_scores_test)
    print("final test score",valid_results)
    
    dfs=[predictions_all1,predictions_all2,predictions_all3,predictions_all4,predictions_all5]
    all_preds=pd.DataFrame(predictions_all1[predictions_all1.columns[0]])
    all_preds['LR_full_Pred']=predictions_all1['LR_full_Pred']
    all_preds['XGB_full_Pred']=predictions_all2['XGB_full_Pred']
    all_preds['RF_full_Pred']=predictions_all3['RF_full_Pred']
    all_preds['NN_full_Pred']=predictions_all4['NN_full_Pred']
    all_preds['SVR_full_Pred']=predictions_all5['SVR_full_Pred']
    #validation results:
    return lr,dt,rf,xgb,svr,all_preds,final_scores_test,valid_results


def get_download_path():
    """Returns the default downloads path for linux or windows"""
    if os.name == 'nt':
        import winreg
        sub_key = r'SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\Shell Folders'
        downloads_guid = '{374DE290-123F-4565-9164-39C4925E467B}'
        with winreg.OpenKey(winreg.HKEY_CURRENT_USER, sub_key) as key:
            location = winreg.QueryValueEx(key, downloads_guid)[0]
        return location
    else:
        return os.path.join(os.path.expanduser('~'), 'Downloads')


def calculate_metrics(model_name ,predictions_all,sel_tar, col_name):
    new_col_name=str(col_name)+'_Pred'
    r2s=r2_score(predictions_all[sel_tar],predictions_all[new_col_name])
    rmses=np.sqrt(mse(predictions_all[sel_tar],predictions_all[new_col_name]))
    maes=mean_absolute_error(predictions_all[sel_tar],predictions_all[new_col_name])
    evars=explained_variance_score(predictions_all[sel_tar],predictions_all[new_col_name])
    maxs=max_error(predictions_all[sel_tar],predictions_all[new_col_name])
    tmp=[]
    tmp.append([model_name,rmses,maes,r2s,maxs,evars])

    full_river_preds_metrics=pd.DataFrame(tmp,columns=['1. Model Name','2. Root Mean Squared Error(RMSE)','3. Mean Absolute Error(MAE)','4. R2 Spearman Coefficient of Determination','5. Maximum Residual Error','6. Explained variance Score'])    
    return full_river_preds_metrics

def calculate_metrics_temp(predictions_all, sel_tar, col_name):
    new_col_name=str(col_name)+'_Pred'
    r2s=r2_score(predictions_all[sel_tar],predictions_all[new_col_name])
    rmses=np.sqrt(mse(predictions_all[sel_tar],predictions_all[new_col_name]))
    maes=mean_absolute_error(predictions_all[sel_tar],predictions_all[new_col_name])
    evars=explained_variance_score(predictions_all[sel_tar],predictions_all[new_col_name])
    maxs=max_error(predictions_all[sel_tar],predictions_all[new_col_name])
    tmp=[]
    model_name="This_is_just_temp_variable"
    tmp.append([model_name,rmses,maes,r2s,maxs,evars])

    full_river_preds_metrics_temp=pd.DataFrame(tmp,columns=['1. Model Name_temp','2. Root Mean Squared Error(RMSE)_temp','3. Mean Absolute Error(MAE)_temp','4. R2 Spearman Coefficient of Determination_temp','5. Maximum Residual Error_temp','6. Explained variance Score_temp'])    
    return full_river_preds_metrics_temp






    
#############################################  FULL RIVER FUNCTIONS  ###############################################################
 
def logperm_gen_full(hutt_full,r,g,b):
    lnbr=[]
    lnbg=[]
    
        # check if 0 value is present in r,g,b
    if(hutt_full.describe()[r]['min']==0): 
        st.write('0 present')                  
        hutt_full[r]=np.where(hutt_full[r]==0,0.1,hutt_full[r])
    if(hutt_full.describe()[g]['min']==0):                   
        hutt_full[g]=np.where(hutt_full[g]==0,0.1,hutt_full[g])
    if(hutt_full.describe()[b]['min']==0):                   
        hutt_full[b]=np.where(hutt_full[b]==0,0.1,hutt_full[b])
    
    #hut full has r,g,b,w only
    r_in=hutt_full.columns.get_loc(r)
    g_in=hutt_full.columns.get_loc(g)
    b_in=hutt_full.columns.get_loc(b)
    te=hutt_full.values
    for i in range(len(te)):
        val=np.log10(te[i,b_in]/te[i,r_in])
        lnbr.append(val)      
        val1=np.log10(te[i,b_in]/te[i,g_in])
        lnbg.append(val1)
            
    hutt_full['ln(b/r)']=np.array(lnbr)
    hutt_full['ln(b/g)']=np.array(lnbg)
    return hutt_full

def greyscale_gen_full(hutt_full,r,g,b):
    tes=np.array(hutt_full)
    r_in=hutt_full.columns.get_loc(r)
    g_in=hutt_full.columns.get_loc(g)
    b_in=hutt_full.columns.get_loc(b)
    i=0
    grs=[]
    for i in range(len(tes)):
        combo=0.3*tes[i,r_in]+ 0.59*tes[i,g_in]+ 0.11*tes[i,b_in]
        grs.append(combo)
    
    hutt_full['Greyscale']=np.array(grs)
    return hutt_full 

def cluster_data_full(hutt_full,r,g,b):
    rgbc=[]
    r_in=hutt_full.columns.get_loc(r)
    g_in=hutt_full.columns.get_loc(g)
    b_in=hutt_full.columns.get_loc(b)
    te=hutt_full.values
    for i in range(len(te)):
        combo=te[i,r_in]*te[i,g_in]*te[i,b_in]
        rgbc.append(combo)
    
    hutt_full['RGB_Combo']=0
    hutt_full['RGB_Combo']=np.array(rgbc)
    #load kmeans
    kmeans=joblib.load('model_files/'+'kmeans.pkl')
    hutt_full.loc[:, 'point_cluster'] = kmeans.predict(hutt_full[[r,g,b]])
    hutt_full['point_cluster']=hutt_full['point_cluster'].astype('int64')
    mm=load_data('min_med_max.csv')
    
    max_indx=mm.groupby(['point_cluster']).mean()['RGB_Combo'][mm.groupby(['point_cluster']).mean()['RGB_Combo']==np.max(mm.groupby(['point_cluster']).mean()['RGB_Combo'])].index
    min_indx=mm.groupby(['point_cluster']).mean()['RGB_Combo'][mm.groupby(['point_cluster']).mean()['RGB_Combo']==np.min(mm.groupby(['point_cluster']).mean()['RGB_Combo'])].index
    indexs=list(mm.groupby(['point_cluster']).mean().index)
    indexs.remove(min_indx)
    indexs.remove(max_indx)
    med_indx=indexs[0]
    
    hutt_full['Point_cluster']=''
    hutt_full['Point_cluster']=np.where(hutt_full['point_cluster']==max_indx[0],'High',hutt_full['Point_cluster'])
    hutt_full['Point_cluster']=np.where(hutt_full['point_cluster']==med_indx,'Medium',hutt_full['Point_cluster'])
    hutt_full['Point_cluster']=np.where(hutt_full['point_cluster']==min_indx[0],'Low',hutt_full['Point_cluster'])
    hutt_full=hutt_full.reset_index().drop(['index'],axis=1)
    
    dn=load_data('groupby.csv')  
    #st.write(dn.head())

    gr=dn.columns[0]
    gg=dn.columns[1]
    gb=dn.columns[2]
    
    peak_r=dn.groupby(['Point_cluster']).max()[gr]
    peak_g=dn.groupby(['Point_cluster']).max()[gg]
    peak_b=dn.groupby(['Point_cluster']).max()[gb]
    peak_rgb=dn.groupby(['Point_cluster']).max()['RGB_Combo']
    # st.write(peak_r,peak_g,peak_b)
    
    hutt_full['Ratio_peak_R']=0.0
    hutt_full['Ratio_peak_G']=0.0
    hutt_full['Ratio_peak_B']=0.0
    hutt_full['Ratio_peak_RGBCombo']=0.0
    
    #low
    hutt_full['Ratio_peak_R']=np.where(hutt_full['Point_cluster']=='Low',hutt_full[r]/peak_r['Low'],hutt_full['Ratio_peak_R'])
    hutt_full['Ratio_peak_G']=np.where(hutt_full['Point_cluster']=='Low',hutt_full[g]/peak_g['Low'],hutt_full['Ratio_peak_G'])
    hutt_full['Ratio_peak_B']=np.where(hutt_full['Point_cluster']=='Low',hutt_full[b]/peak_b['Low'],hutt_full['Ratio_peak_B'])
    hutt_full['Ratio_peak_RGBCombo']=np.where(hutt_full['Point_cluster']=='Low',hutt_full['RGB_Combo']/peak_rgb['Low'],hutt_full['Ratio_peak_RGBCombo'])
    
    #medium
    hutt_full['Ratio_peak_R']=np.where(hutt_full['Point_cluster']=='Medium',hutt_full[r]/peak_r['Medium'],hutt_full['Ratio_peak_R'])
    hutt_full['Ratio_peak_G']=np.where(hutt_full['Point_cluster']=='Medium',hutt_full[g]/peak_g['Medium'],hutt_full['Ratio_peak_G'])
    hutt_full['Ratio_peak_B']=np.where(hutt_full['Point_cluster']=='Medium',hutt_full[b]/peak_b['Medium'],hutt_full['Ratio_peak_B'])
    hutt_full['Ratio_peak_RGBCombo']=np.where(hutt_full['Point_cluster']=='Medium',hutt_full['RGB_Combo']/peak_rgb['Medium'],hutt_full['Ratio_peak_RGBCombo'])
    
    #high
    hutt_full['Ratio_peak_R']=np.where(hutt_full['Point_cluster']=='High',hutt_full[r]/peak_r['High'],hutt_full['Ratio_peak_R'])
    hutt_full['Ratio_peak_G']=np.where(hutt_full['Point_cluster']=='High',hutt_full[g]/peak_g['High'],hutt_full['Ratio_peak_G'])
    hutt_full['Ratio_peak_B']=np.where(hutt_full['Point_cluster']=='High',hutt_full[b]/peak_b['High'],hutt_full['Ratio_peak_B'])
    hutt_full['Ratio_peak_RGBCombo']=np.where(hutt_full['Point_cluster']=='High',hutt_full['RGB_Combo']/peak_rgb['High'],hutt_full['Ratio_peak_RGBCombo'])
    
    hutt_full=pd.get_dummies(hutt_full)
    # to confirm if all 3 clusters are there..else add dummy column
    clus_all=['Point_cluster_Low','Point_cluster_Medium','Point_cluster_High']
    fk=[]
    for c in clus_all:
        if(c not in hutt_full.columns):
            fk.append(c)
            
    for fkc in fk:
        hutt_full[fkc]=0
    # To maintain the order in low, med ,high....
    lowp=hutt_full['Point_cluster_Low'].values
    medp=hutt_full['Point_cluster_Medium'].values
    highp=hutt_full['Point_cluster_High'].values
    
    hutt_full.drop(clus_all,axis=1,inplace=True)
    hutt_full['Point_cluster_Low']=lowp
    hutt_full['Point_cluster_Medium']=medp
    hutt_full['Point_cluster_High']=highp
    hf=hutt_full.drop(['point_cluster','RGB_Combo'],axis=1).copy()

    return hf

def poly_creation_cluster_full(new_data):
    pcl=new_data[['Point_cluster_Low','Point_cluster_Medium','Point_cluster_High']].copy()
    new_data.drop(['Point_cluster_Low','Point_cluster_Medium','Point_cluster_High'],axis=1,inplace=True)
    print(new_data.shape)
    poly = PolynomialFeatures(2)
    x_poly=poly.fit_transform(new_data)
    poly_names=poly.get_feature_names(['Feature'+str(l) for l in range(1,len(np.array(poly.get_feature_names())))])
    df_poly1=pd.DataFrame(x_poly,columns=poly_names)
    to_send=df_poly1.drop(['1'],axis=1)
    #append the clusters
    to_send=pd.concat([to_send,pcl],axis=1)
    return to_send    

def poly_creation_no_cluster_full(new_data):
    poly = PolynomialFeatures(2)
    x_poly=poly.fit_transform(new_data)
    poly_names=poly.get_feature_names(['Feature'+str(l) for l in range(1,len(np.array(poly.get_feature_names())))])
    df_poly1=pd.DataFrame(x_poly,columns=poly_names)
    to_send=df_poly1.drop(['1'],axis=1)
    return to_send

def correl_full(hf,type):
    if(type=="rf"):
        org=pd.read_csv('model_files/'+'correl_cluster_rf.csv')
    if(type=="nn"):
        org=pd.read_csv('model_files/'+'correl_cluster_nn.csv')
    if(type=="common"):
        org=pd.read_csv('model_files/'+'correl_cluster_common.csv')  
    spe_cols=list(org.columns)
    rf_data=hf[spe_cols].copy()
    return rf_data  

def lasso_reg_full(hf,org):
    spe_cols=list(org.columns)
    rf_data=hf[spe_cols].copy()
    return rf_data

def pca_reduction_no_cluster_full(hf,type):
    if(type=="lr"):
        pca=joblib.load('model_files/'+'pca_lr.pkl')
    if(type=="common"):
        pca=joblib.load('model_files/'+'pca_common.pkl') 
    pca_result1=pca.transform(hf)

    sub1=pd.DataFrame()
    sub1['pca-one']=0
    sub1['pca-two']=0
    sub1['pca-three']=0
    
    sub1['pca-one'] = pca_result1[:,0]
    sub1['pca-two'] = pca_result1[:,1] 
    sub1['pca-three'] = pca_result1[:,2]
    svr_data1=sub1.copy()
    return svr_data1

def pca_reduction_cluster_full(hf,type):
    CAT_COUNTER=0
    print(hf.columns)
    if(all(x in hf.columns for x in ['Point_cluster_Low','Point_cluster_Medium','Point_cluster_High'])):
        pcl_tr=hf[['Point_cluster_Low','Point_cluster_Medium','Point_cluster_High']].copy()
        hf.drop(['Point_cluster_Low','Point_cluster_Medium','Point_cluster_High'],axis=1,inplace=True)
        CAT_COUNTER=1
    if(type=="lr"):
        pca=joblib.load('model_files/'+'pca_lr.pkl')
    if(type=="common"):
        pca=joblib.load('model_files/'+'pca_common.pkl')    
    pca_result1=pca.transform(hf)

    sub1=pd.DataFrame()
    sub1['pca-one']=0
    sub1['pca-two']=0
    sub1['pca-three']=0
    
    sub1['pca-one'] = pca_result1[:,0]
    sub1['pca-two'] = pca_result1[:,1] 
    sub1['pca-three'] = pca_result1[:,2]
    svr_data1=sub1.copy()
    
    if(CAT_COUNTER==1):
        subb=pd.concat([svr_data1,pcl_tr],axis=1)
        return subb
    else:
        return svr_data1    

def standard_scale_no_cluster_full(svr_data1,type):
    if(type=="lr"):
        sc=joblib.load('model_files/'+'sc_lr.pkl')
    if(type=="nn"):
        sc=joblib.load('model_files/'+'sc_nn.pkl')
    if(type=="common"):
        sc=joblib.load('model_files/'+'sc_common.pkl')
    
    sv_data=sc.transform(svr_data1)
    cols=svr_data1.columns
    sv_data=pd.DataFrame(sv_data, columns=cols, index=svr_data1.index)
    return sv_data

def standard_scale_cluster_full(svr_data1,type):
    if(type=="lr"):
        sc=joblib.load('model_files/'+'sc_lr.pkl')
    if(type=="nn"):
        sc=joblib.load('model_files/'+'sc_nn.pkl')
    if(type=="common"):
        sc=joblib.load('model_files/'+'sc_common.pkl')
    CAT_COUNTER=0
    if(all(x in svr_data1.columns for x in ['Point_cluster_Low','Point_cluster_Medium','Point_cluster_High'])):
        pcl_tr=svr_data1[['Point_cluster_Low','Point_cluster_Medium','Point_cluster_High']].copy()
        svr_data1.drop(['Point_cluster_Low','Point_cluster_Medium','Point_cluster_High'],axis=1,inplace=True)
        CAT_COUNTER=1
    print(svr_data1.shape)    
    sv_data=sc.transform(svr_data1)
    print(sv_data.shape)
    cols=svr_data1.columns
    sv_data=pd.DataFrame(sv_data, columns=cols, index=svr_data1.index)
    if(CAT_COUNTER==1):
        scaled_data=pd.concat([sv_data,pcl_tr],axis=1)
    scaled_data.sort_index(inplace=True)    
    return scaled_data

def min_max_scale_cluster_full(svr_data1,type):
    if(type=="lr"):
        ms=joblib.load('model_files/'+'ms_lr.pkl')
    if(type=="nn"):
        ms=joblib.load('model_files/'+'ms_nn.pkl')
    if(type=="common"):
        ms=joblib.load('model_files/'+'ms_common.pkl')
    CAT_COUNTER=0
    if(all(x in svr_data1.columns for x in ['Point_cluster_Low','Point_cluster_Medium','Point_cluster_High'])):
        pcl_tr=svr_data1[['Point_cluster_Low','Point_cluster_Medium','Point_cluster_High']].copy()
        svr_data1.drop(['Point_cluster_Low','Point_cluster_Medium','Point_cluster_High'],axis=1,inplace=True)
        CAT_COUNTER=1
    sv_data=ms.transform(svr_data1)
    cols=svr_data1.columns
    sv_data=pd.DataFrame(sv_data, columns=cols, index=svr_data1.index)       
    if(CAT_COUNTER==1):
        scaled_data=pd.concat([sv_data,pcl_tr],axis=1)
    scaled_data.sort_index(inplace=True)
    return scaled_data

def min_max_scale_no_cluster_full(svr_data1,type):
    if(type=="lr"):
        ms=joblib.load('model_files/'+'ms_lr.pkl')
    if(type=="nn"):
        ms=joblib.load('model_files/'+'ms_nn.pkl')
    if(type=="common"):
        ms=joblib.load('model_files/'+'ms_common.pkl')
    sv_data=ms.transform(svr_data1)
    cols=svr_data1.columns
    sv_data=pd.DataFrame(sv_data, columns=cols, index=svr_data1.index)
    return sv_data

def robust_scale_cluster_full(svr_data1,type):
    if(type=="lr"):
        rs=joblib.load('model_files/'+'rs_lr.pkl')
    if(type=="nn"):
        rs=joblib.load('model_files/'+'rs_nn.pkl')
    if(type=="common"):
        rs=joblib.load('model_files/'+'rs_common.pkl')
    CAT_COUNTER=0
    if(all(x in svr_data1.columns for x in ['Point_cluster_Low','Point_cluster_Medium','Point_cluster_High'])):
        pcl_tr=svr_data1[['Point_cluster_Low','Point_cluster_Medium','Point_cluster_High']].copy()
        svr_data1.drop(['Point_cluster_Low','Point_cluster_Medium','Point_cluster_High'],axis=1,inplace=True)
        CAT_COUNTER=1
    sv_data=rs.transform(svr_data1)
    cols=svr_data1.columns
    sv_data=pd.DataFrame(sv_data, columns=cols, index=svr_data1.index)
    if(CAT_COUNTER==1):
        scaled_data=pd.concat([sv_data,pcl_tr],axis=1)
    scaled_data.sort_index(inplace=True)
    return scaled_data

def robust_scale_no_cluster_full(svr_data1,type):
    if(type=="lr"):
        rs=joblib.load('model_files/'+'rs_lr.pkl')
    if(type=="nn"):
        rs=joblib.load('model_files/'+'rs_nn.pkl')
    if(type=="common"):
        rs=joblib.load('model_files/'+'rs_common.pkl')
    sv_data=rs.transform(svr_data1)
    cols=svr_data1.columns
    sv_data=pd.DataFrame(sv_data, columns=cols, index=svr_data1.index)
    return sv_data

def power_scale_cluster_full(svr_data1,type):
    if(type=="lr"):
        pt=joblib.load('model_files/'+'pt_lr.pkl')
    if(type=="nn"):
        pt=joblib.load('model_files/'+'pt_nn.pkl')
    if(type=="common"):
        pt=joblib.load('model_files/'+'pt_common.pkl')
    CAT_COUNTER=0
    if(all(x in svr_data1.columns for x in ['Point_cluster_Low','Point_cluster_Medium','Point_cluster_High'])):
        pcl_tr=svr_data1[['Point_cluster_Low','Point_cluster_Medium','Point_cluster_High']].copy()
        svr_data1.drop(['Point_cluster_Low','Point_cluster_Medium','Point_cluster_High'],axis=1,inplace=True)
        CAT_COUNTER=1
    sv_data=pt.transform(svr_data1)
    cols=svr_data1.columns
    sv_data=pd.DataFrame(sv_data, columns=cols, index=svr_data1.index)
    if(CAT_COUNTER==1):
        scaled_data=pd.concat([sv_data,pcl_tr],axis=1)
    scaled_data.sort_index(inplace=True)
    return scaled_data

def power_scale_no_cluster_full(svr_data1,type):
    if(type=="lr"):
        pt=joblib.load('model_files/'+'pt_lr.pkl')
    if(type=="nn"):
        pt=joblib.load('model_files/'+'pt_nn.pkl')
    if(type=="common"):
        pt=joblib.load('model_files/'+'pt_common.pkl')
    sv_data=pt.transform(svr_data1)
    cols=svr_data1.columns
    sv_data=pd.DataFrame(sv_data, columns=cols, index=svr_data1.index)
    return sv_data




################################################################ MAIN FUNCTIONS FROM HERE ########################################

###Part 1----

@app.route('/')
@app.route('/home')
def home():
    return render_template('home.html',title='Bathymetry Colour Toolkit',dropdown_options="home")

@app.route('/full_workflow')
def full_workflow():
    return render_template('full_workflow.html',title='Full Workflow')


@app.route('/store_local',methods=['GET','POST'])
def store_file():
    #   f = request.files['file']
    #   f.save(secure_filename(f.filename))
    #   return "done "
    # only reuqest.form works
    data = request.form['akey']
    data_ujson = ujson.loads( data )
    #data = str(data[0:20])
    df=pd.io.json.json_normalize(data_ujson)
    print(df.head())
  
    if(True in list(df.iloc[-1].isnull()[:])):
        drop_ind=df.index[-1]
        print(drop_ind)
        df.drop(drop_ind,axis=0,inplace=True)  
    
    #create a dir for all temp files
    if os.path.exists('model_files'):
        shutil.rmtree('model_files')
        os.mkdir('model_files')
    df.to_csv('model_files/'+'temp_pipeline.csv',index=False)
    del df
    return ("Success from store_file")

@app.route('/calculate_distance',methods=['GET','POST'])
def calculate_distance():
    df=load_data('temp_pipeline.csv')
    yorn=request.form['yorn']
    sel_feats=request.form['sel_feat']
    #print(" Selected feats from ajax: ",sel_feats)
    print(yorn,type(yorn))
    #sel_feats is already in json format
    feats  = json.loads(sel_feats)
    print(feats)
    df=df[feats].copy()
    df=df.sort_values(by=feats[0])
    df=df.reset_index().drop(['index'],axis=1)
    if(yorn=="no_distance"):
        east=feats[1]
        north=feats[2]
        agg=0
        df['Distance_new']=0
        for i in range(1,len(df)):
            diff=distance_cal(df[east].iloc[i],df[north].iloc[i],df[east].iloc[i-1],df[north].iloc[i-1])
            agg=agg+diff
            df['Distance_new'].iloc[i]=agg   
        jsonfiles = json.loads(df.head().to_json(orient='records'))
        
        if(True in list(df.iloc[-1].isnull()[:])):
            drop_ind=df.index[-1]
            print(drop_ind)
            df.drop(drop_ind,axis=0,inplace=True)
        df.sort_index(inplace=True)
        df.to_csv('model_files/'+'temp_pipeline_2.csv',index=False)
        del df
        return (jsonify(jsonfiles))
        #return jsonify("parsed")
        
    else:
        return jsonify("Could not parse")

@app.route('/pre_distance',methods=['GET','POST'])
def pre_calc_distance():
    df=load_data('temp_pipeline.csv')
    sel_feats=request.form['sel_feat']
    feats  = json.loads(sel_feats)
    print(feats)
    cdist_get=request.form['cdist_send']
    cdist = json.loads(cdist_get)
    df=df.sort_values(by=feats[0])
    df=df.reset_index().drop(['index'],axis=1)
    df.rename(columns={cdist: "Distance_new"},inplace=True)
    new_df = pd.concat([df[feats].copy(),df['Distance_new']],axis=1)  
    new_df=new_df.reset_index().drop(['index'],axis=1)
    if(True in list(new_df.iloc[-1].isnull()[:])):
        drop_ind=new_df.index[-1]
        new_df.drop(drop_ind,axis=0,inplace=True)
    
    new_df.sort_index(inplace=True)
    new_df.to_csv('model_files/'+'temp_pipeline_2.csv',index=False)
    jsonfiles = json.loads(new_df.head().to_json(orient='records'))
    del new_df
    return (jsonify(jsonfiles))

###Part 2----

@app.route('/indep_data',methods=['GET','POST'])
def independent_feats():
    data=load_data('temp_pipeline_2.csv')#contains only selected columns data + distance
    feats=request.form['sel_feat']
    sel_cols1  = json.loads(feats)
    print(sel_cols1)
    r=sel_cols1[3] #3 because of east north corrd input 
    g=sel_cols1[4]
    b=sel_cols1[5]
    x=data[sel_cols1[3:]].copy()   

    # check if 0 value is present in r,g,b
    if(x.describe()[r]['min']==0):                   
        x[r]=np.where(x[r]==0,0.1,x[r])
    if(x.describe()[g]['min']==0):                   
        x[g]=np.where(x[g]==0,0.1,x[g])
    if(x.describe()[b]['min']==0):                   
        x[b]=np.where(x[b]==0,0.1,x[b])
   
                                                  
    print('You selected:', sel_cols1[3:-1],' as the independent features')
    print('You selected:', sel_cols1[-1],' as the dependent feature')
    # check if last row has Nan values
    if(True in list(x.iloc[-1].isnull()[:])):
        drop_ind=x.index[-1]
        x.drop(drop_ind,axis=0,inplace=True)

    x.to_csv('model_files/'+'x_y.csv',index=False)
    x.drop(sel_cols1[-1],axis=1,inplace=True)
    jsonfiles_x = json.loads(x.head().to_json(orient='records'))
    return (jsonify(jsonfiles_x))

@app.route('/dep_data',methods=['GET','POST'])
def dependent_feats():
    x=load_data('x_y.csv')#contains only selected columns data
    feats=request.form['sel_feat']
    sel_cols1  = json.loads(feats)
    y=pd.DataFrame(columns={sel_cols1[-1]})
    y[sel_cols1[-1]]=x[sel_cols1[-1]].values
    print(y.head())
    jsonfiles_y = json.loads(y.head().to_json(orient='records'))
    return (jsonify(jsonfiles_y))

#https://medium.com/@rrfd/standardize-or-normalize-examples-in-python-e3f174b65dfc
# above is standardizing vs normalization
@app.route('/process_train_data',methods=['GET','POST'])
def process_training_data():
    #for all options in the list, i will call separate functions -define them outsde - in an order.
    #the order of the if blocks are V.IMP
    process_options_obj=request.form['process_options']
    process_options=json.loads(process_options_obj)
    feats=request.form['sel_feat']
    sel_cols1  = json.loads(feats)
    r=sel_cols1[3]
    g=sel_cols1[4]
    b=sel_cols1[5]
    data=load_data('x_y.csv')
    data.sort_index(inplace=True)
    x=data[sel_cols1[3:-1]].copy()
    y=data[sel_cols1[-1]].copy()
    processed_data=x.copy()
    print(processed_data.dtypes)
    all_default=0

    if("default_options_all" in process_options):
        #lr data
        lr_processed_data=logperm_gen(processed_data,r,g,b)
        lr_processed_data=greyscale_gen(lr_processed_data,r,g,b)
        lr_processed_data=cluster_data(lr_processed_data,r,g,b)
        lr_processed_data=standard_scale_cluster(lr_processed_data,y,"lr")
        lr_processed_data=pca_reduction_cluster(lr_processed_data,y,"lr")
        lr_x=lr_processed_data.copy()
        lr_x.to_csv('model_files/'+'lr_svr_x.csv',index=False)
        print("LR HEAD",lr_x.head())
        #svr data
        svr_x=lr_processed_data.copy()
        print("SVR HEAD",svr_x.head())

        #rf data
        processed_data=x.copy()
        rf_processed_data=logperm_gen(processed_data,r,g,b)
        rf_processed_data=greyscale_gen(rf_processed_data,r,g,b)
        rf_processed_data=cluster_data(rf_processed_data,r,g,b)
        rf_processed_data=poly_creation_cluster(rf_processed_data,y)
        rf_processed_data=correl_cluster(rf_processed_data,y,"rf")
        rf_x=rf_processed_data.copy()
        rf_x.to_csv('model_files/'+'rf_xgb_x.csv',index=False)
        print("RF HEAD",rf_x.head())
        #xgb data
        xgb_x=rf_processed_data.copy()
        print("XGB HEAD",xgb_x.head())
        
        #nn data
        processed_data=x.copy()
        nn_processed_data=logperm_gen(processed_data,r,g,b)
        nn_processed_data=greyscale_gen(nn_processed_data,r,g,b)
        nn_processed_data=cluster_data(nn_processed_data,r,g,b)
        nn_processed_data=poly_creation_cluster(nn_processed_data,y)
        nn_processed_data=standard_scale_cluster(nn_processed_data,y,"nn")
        nn_processed_data=correl_cluster(nn_processed_data,y,"nn")

        nn_x=nn_processed_data.copy()
        nn_x.to_csv('model_files/'+'nn_x.csv',index=False)
        print("NN HEAD",nn_x.head())

        all_default=1
        process_options=[]

    if(all_default==1):
        print("All default",xgb_x.head())
        jsonfiles_processed_data = json.loads(xgb_x.head().to_json(orient='records'))
        return (jsonify(jsonfiles_processed_data))

    if("logperm_gen" in process_options):
        processed_data=logperm_gen(processed_data,r,g,b)

    if("greyscale_gen" in process_options):
        processed_data=greyscale_gen(processed_data,r,g,b)

    if("cluster_gen" in process_options):
        processed_data=cluster_data(processed_data,r,g,b)

    if(("poly_corr_gen" in process_options) & ("cluster_gen" in process_options)):
        processed_data=poly_creation_cluster(processed_data,y)

    if(("poly_corr_gen" in process_options) & ("cluster_gen" not in process_options)):
        processed_data=poly_creation_no_cluster(processed_data,y)

    #SCALING PART

    if(("ss_scale" in process_options) & ("cluster_gen" in process_options)):
        processed_data=standard_scale_cluster(processed_data,y,"common")

    if(("ss_scale" in process_options) & ("cluster_gen" not in process_options)):
        processed_data=standard_scale_no_cluster(processed_data,y,"common")

    if(("minmax_scale" in process_options) & ("cluster_gen" in process_options)):
        processed_data=min_max_scale_cluster(processed_data,y,"common")

    if(("minmax_scale" in process_options) & ("cluster_gen" not in process_options)):
        processed_data=min_max_scale_no_cluster(processed_data,y,"common")

    if(("robust_scale" in process_options) & ("cluster_gen" in process_options)):
        processed_data=robust_scale_cluster(processed_data,y,"common")

    if(("robust_scale" in process_options) & ("cluster_gen" not in process_options)):
        processed_data=robust_scale_no_cluster(processed_data,y,"common")

    if(("power_scale" in process_options) & ("cluster_gen" in process_options)):
        processed_data=power_scale_cluster(processed_data,y,"common")

    if(("power_scale" in process_options) & ("cluster_gen" not in process_options)):
        processed_data=power_scale_no_cluster(processed_data,y,"common")

    #FEAT REDUCTION PART
    if(("pearson_correl" in process_options) & ("cluster_gen" in process_options)):
        processed_data=correl_cluster(processed_data,y,"common")

    if(("pearson_correl" in process_options) & ("cluster_gen" not in process_options)):
        processed_data=correl_no_cluster(processed_data,y,"common")

    if("poly_lasso_trans" in process_options):
        processed_data=lasso_reg(processed_data,y)

    if(("pca_trans" in process_options) & ("cluster_gen" in process_options)):
        processed_data=pca_reduction_cluster(processed_data,y,"common")    

    if(("pca_trans" in process_options) & ("cluster_gen" not in process_options)):
        processed_data=pca_reduction_no_cluster(processed_data,y,"common")

    if(("pls_trans" in process_options) & ("cluster_gen" in process_options)):
        processed_data=pls_reduction_cluster(processed_data,y)

    if(("pls_trans" in process_options) & ("cluster_gen" not in process_options)):
        processed_data=pls_reduction_no_cluster(processed_data,y)

    else:           
        print(processed_data.head())
        processed_data.to_csv('model_files/'+'data_for_all_models.csv',index=False)
        jsonfiles_processed_data = json.loads(processed_data.head().to_json(orient='records'))
        return (jsonify(jsonfiles_processed_data))


@app.route('/ml_algorithms',methods=['GET','POST'])
def ml_algorithms():
    process_options_obj=request.form['process_options']
    process_options=json.loads(process_options_obj)
    print(process_options)
    ml_algo_obj=request.form['ml_algo']
    algo_choice=json.loads(ml_algo_obj)
    print(algo_choice)

    tts_value_obj=request.form['tts_value']
    tr_split=json.loads(tts_value_obj)
    print(tr_split)

    feats=request.form['sel_feat']
    sel_cols1  = json.loads(feats)

    test_split=(100-int(tr_split))/100
    data=load_data('x_y.csv')
    data.sort_index(inplace=True)
    y=data[sel_cols1[-1]].copy()
    if("default_options_all" in process_options):
        sub_x=load_data('lr_svr_x.csv')
        x_train2,x_test2,y_train2,y_test2=tts(sub_x,y,test_split)
        # for rf,xgb
        rf_x=load_data('rf_xgb_x.csv')
        totrain=load_data('rf_xgb_x.csv')
        x_train,x_test,y_train,y_test=tts(rf_x,y,test_split)
        # for nn
        nn_x=load_data('nn_x.csv')
        x_train3,x_test3,y_train3,y_test3=tts(nn_x,y,test_split)
        predictions_all=pd.DataFrame(y)

    else:
        data=load_data('data_for_all_models.csv')
        sub_x=data.copy()
        rf_x=data.copy()
        nn_x=data.copy()
        x_train2,x_test2,y_train2,y_test2=tts(data,y,test_split)
        # for rf,xgb
        x_train,x_test,y_train,y_test=tts(data,y,test_split)
        # for nn
        x_train3,x_test3,y_train3,y_test3=tts(data,y,test_split)
        predictions_all=pd.DataFrame(y)   
        
    if(algo_choice=="Multi-Linear Regression (LR)"):
        lr,predictions_all,fts,valid_results=LR_reg(sub_x,x_train2,x_test2,y_train2,y_test2,predictions_all)
        print("Training dataset Evaluation Metrics")
        print(fts)
        print("Validation dataset Evaluation Metrics :")
        print(valid_results)
        print("Predictions")
        print(predictions_all.head())
        predictions_all.to_csv('model_files/'+'lr_pred_temp.csv',index=False)
        joblib.dump(lr,'model_files/'+'lr.pkl')

    if(algo_choice=="Random Forest Regressor (RF)"):
        rf,predictions_all,fts,valid_results=RF_reg(rf_x,x_train,x_test,y_train,y_test,predictions_all)
        print("Training dataset Evaluation Metrics")
        print(fts)
        print("Validation dataset Evaluation Metrics :")
        print(valid_results)
        print("Predictions")
        print(predictions_all.head())
        predictions_all.to_csv('model_files/'+'rf_pred_temp.csv',index=False) 
        joblib.dump(rf,'model_files/'+'rf.pkl') 

    if(algo_choice=="Extreme Gradient Boosting (XGBoost)"):
        xgb,predictions_all,fts,valid_results=XGB_reg(rf_x,x_train,x_test,y_train,y_test,predictions_all)
        print("Training dataset Evaluation Metrics")
        print(fts)
        print("Validation dataset Evaluation Metrics :")
        print(valid_results)
        print("Predictions")
        print(predictions_all.head())
        predictions_all.to_csv('model_files/'+'xgb_pred_temp.csv',index=False)
        joblib.dump(xgb,'model_files/'+'xgb.pkl')

    if(algo_choice=="Multi-layer Perceptron (Neural Network)"):
        nn,predictions_all,fts,valid_results=NN_reg(nn_x,x_train3,x_test3,y_train3,y_test3,predictions_all)
        print("Training dataset Evaluation Metrics")
        print(fts)
        print("Validation dataset Evaluation Metrics :")
        print(valid_results)
        print("Predictions")
        print(predictions_all.head())
        predictions_all.to_csv('model_files/'+'nn_pred_temp.csv',index=False)
        joblib.dump(nn,'model_files/'+'nn.pkl')

    if(algo_choice=="Support Vector Regressor (SVR)"):
        svr,predictions_all,fts,valid_results=SVR_reg(sub_x,x_train2,x_test2,y_train2,y_test2,predictions_all)
        print("Training dataset Evaluation Metrics")
        print(fts)
        print("Validation dataset Evaluation Metrics :")
        print(valid_results)
        print("Predictions")
        print(predictions_all.head())
        predictions_all.to_csv('model_files/'+'svr_pred_temp.csv',index=False)
        joblib.dump(svr,'model_files/'+'svr.pkl')

    if(algo_choice=='Try all Models'):
        lr,nn,rf,xgb,svr,predictions_all5,final_scores_test,valid_results=All_reg(sub_x,nn_x,rf_x,x_train,x_test,y_train,y_test,x_train2,x_test2,y_train2,y_test2,x_train3,x_test3,y_train3,y_test3,predictions_all)
        print(x_train.shape)
        print(x_test.shape)
        print("Training dataset Evaluation Metrics")
        print(final_scores_test)
        print("Validation dataset Evaluation Metrics :")
        print(valid_results)
        print("Predictions")
        print(predictions_all5.head())
        fts=final_scores_test.copy()
        predictions_all_new=predictions_all5.copy()
        predictions_all_new.to_csv('model_files/'+'all_models_preds.csv',index=False)
        joblib.dump(lr,'model_files/'+'lr.pkl')
        joblib.dump(svr,'model_files/'+'svr.pkl')
        joblib.dump(nn,'model_files/'+'nn.pkl')
        joblib.dump(rf,'model_files/'+'rf.pkl')
        joblib.dump(xgb,'model_files/'+'xgb.pkl')

        jsonfiles_fts = json.loads(fts.to_json(orient='records'))
        jsonfiles_valid_results = json.loads(valid_results.to_json(orient='records'))
        jsonfiles_predictions_all = json.loads(predictions_all_new.head().to_json(orient='records'))
        return (jsonify(jsonfiles_fts,jsonfiles_valid_results,jsonfiles_predictions_all)) 

    final_results_train=pd.concat([fts,valid_results],axis=0)
    final_results_test=pd.concat([valid_results,fts],axis=0)
    # print("fin res ++++",final_results[valid_results.columns])

    jsonfiles_fts = json.loads(final_results_train[fts.columns].to_json(orient='records'))
    jsonfiles_valid_results = json.loads(final_results_test[valid_results.columns].to_json(orient='records'))
    jsonfiles_predictions_all = json.loads(predictions_all.head().to_json(orient='records'))
    return (jsonify(jsonfiles_fts,jsonfiles_valid_results,jsonfiles_predictions_all))


#CREATING GRAPHS - PASSING PREDICTIONS
@app.route('/graphs_predictions_single_model', methods=['POST','GET'])
def graphs_predictions_sm():
    show_graphs_check_obj=request.form['show_graphs_check']
    show_graphs_check=json.loads(show_graphs_check_obj)
    ml_algo_obj=request.form['ml_algo_name']
    algo_choice=json.loads(ml_algo_obj)
    preds=pd.DataFrame()
    data=load_data('temp_pipeline_2.csv')
    if(show_graphs_check=="Yes"):
        if(algo_choice=="Multi-Linear Regression (LR)"):
            preds=load_data('lr_pred_temp.csv')
        if(algo_choice=="Random Forest Regressor (RF)"):
            preds=load_data('rf_pred_temp.csv')
        if(algo_choice=="Extreme Gradient Boosting (XGBoost)"):
            preds=load_data('xgb_pred_temp.csv')
        if(algo_choice=="Multi-layer Perceptron (Neural Network)"):
            preds=load_data('nn_pred_temp.csv')
        if(algo_choice=="Support Vector Regressor (SVR)"):
            preds=load_data('svr_pred_temp.csv')                
       
        print(preds.head())
        return jsonify({'preds': list(preds[preds.columns[-1]]), 'actual':list(preds[preds.columns[-2]]), 'distance':list(data['Distance_new']) })

#CREATING GRAPHS - PASSING PREDICTIONS
@app.route('/graphs_predictions_all_models', methods=['POST','GET'])
def graphs_predictions_am():
    show_graphs_check_obj=request.form['show_graphs_check']
    show_graphs_check=json.loads(show_graphs_check_obj)
    if(show_graphs_check=="Yes"):
        preds=load_data('all_models_preds.csv')
        data=load_data('temp_pipeline_2.csv')
        print(preds.head())
        print(list(preds['NN_full_Pred'].head()))
        print(list(preds['RF_full_Pred'].head()))
        return jsonify({'lr_preds': list(preds['LR_full_Pred']), 'rf_preds':list(preds['RF_full_Pred']), 'xgb_preds':list(preds['XGB_full_Pred']), 'nn_preds':list(preds['NN_full_Pred']), 'svr_preds':list(preds['SVR_full_Pred']),'actual':list(preds[preds.columns[0]]), 'distance':list(data['Distance_new']) })


#surface plot
@app.route('/surface_plot_data', methods=['POST','GET'])
def surface_plot_data():
    show_surface_plot_check_obj=request.form['show_surface_plot_check']
    show_surface_plot_check=json.loads(show_surface_plot_check_obj)
    sel_feats=request.form['feats']
    feats  = json.loads(sel_feats)
    print(feats)
    east=feats[1]
    north=feats[2]
    if(show_surface_plot_check=="Yes"):
        data=load_data('temp_pipeline_2.csv')
        preds=load_data('all_models_preds.csv')
        
        x=list(data[east].iloc[:500].values)
        y=list(data[north].iloc[:500].values)
        z=[list(data[preds.columns[0]].iloc[:500].values)]*len(data[east].iloc[:500])
        print(len(x))
        #df3 = {'x':[1, 2, 3, 4, 5],'y':[10, 20, 30, 40, 50],'z': [[5, 4, 3, 2, 1]]*5}
        #z=np.array(data['Depth'].head()).reshape((len(data['Y_NZTM'].head()), len(data['X_NZTM'].head()) ))
        #return jsonify({'x': list(data['X_NZTM'].head()), 'y':list(data['Y_NZTM'].head()), 'z':list(z) })
        return jsonify({'x': x, 'y': y, 'z':z})

@app.route('/check_if_file_present',methods=['POST','GET'])
def check_if_file_present():
    downloads_path=get_download_path()
    downloads_path=str(downloads_path)+ "/full_river_data_from_bctk_software.csv"
    print(downloads_path)
    if(os.path.exists(downloads_path)):
        os.remove(downloads_path)
        return jsonify(" Exisiting file deleted - Great")
    else:
        return jsonify(" No path present - Great")    


# full river input and process
@app.route('/full_river_input',methods=['POST'])
def full_river_input():
    downloads_path=get_download_path()
    file_path=str(downloads_path)+ "/full_river_data_from_bctk_software.csv"
    df=pd.read_csv(file_path, chunksize=100000)
    full_river=pd.DataFrame()
    for jun in df:
        full_river=pd.concat([full_river,jun],axis=0)
        full_river=full_river.reset_index().drop(['index'],axis=1) 

    #remove null values at end if present
    print("Full river sample" , full_river.head())
    if(True in list(full_river.iloc[-1].isnull()[:])):
        drop_ind=full_river.index[-1]
        print(drop_ind)
        full_river.drop(drop_ind,axis=0,inplace=True)
    full_columns=full_river.columns
    print(list(full_columns))    
    full_river.to_csv('model_files/'+'full_river_data_software_generated.csv',index=False) 
    df_jsonfiles = json.loads(full_river.head().to_json(orient='records'))
    return (jsonify({'data':df_jsonfiles,'columns':list(full_columns)}))


@app.route('/full_river_predictions',methods=['POST','GET'])
def full_river_predictions():
    ml_algo_obj=request.form['full_ml_algo']
    ml_algo_name=json.loads(ml_algo_obj)
    model=''
    sel_feats=request.form['feats']
    sel_cols3 = json.loads(sel_feats)

    actual_status=request.form['actual_status']
    # actual_status=json.loads(actual_status_obj)
    
    target_json=request.form['actual_variable']
    target=json.loads(target_json)
    print(target)
    process_options_obj=request.form['process_options']
    process_options=json.loads(process_options_obj)

    full_data=pd.read_csv('model_files/'+'full_river_data_software_generated.csv', chunksize=1000000)
    pid=sel_cols3[0]
    east=sel_cols3[1]
    north=sel_cols3[2]
    
    r=sel_cols3[3]
    g=sel_cols3[4]
    b=sel_cols3[5]
    sel_cols2=sel_cols3[3:]
    
    if(actual_status=="Yes"):
        sel_tar=str(target)
        print("sel tar done")
    if(actual_status=="No"):
        sel_tar="none" 

    print("Process options",process_options)
    print("Actual status",actual_status)
    print(" Model name",ml_algo_name)
    print("features",sel_cols3)
    if("default_options_all" in process_options):
        print("in default")
        lr_svr_x=load_data('lr_svr_x.csv')
        rf_xgb_x=load_data('rf_xgb_x.csv')
        nn_x=load_data('nn_x.csv')
        print("searching for algos")
        if(ml_algo_name=="Multi-Linear Regression (LR)"):
            model=joblib.load('model_files/'+'lr.pkl')
            col_name="LR"
            full_river=pd.DataFrame()
            for jun in full_data:
                to_merge=pd.DataFrame()
                merged=pd.DataFrame()
                to_merge=pd.DataFrame(jun[[pid,east,north]])
                hutt_full=jun[sel_cols2].copy()
                lr_processed_data=logperm_gen_full(hutt_full,r,g,b)
                lr_processed_data=greyscale_gen_full(lr_processed_data,r,g,b)
                lr_processed_data=cluster_data_full(lr_processed_data,r,g,b)
                lr_processed_data=standard_scale_cluster_full(lr_processed_data,"lr")
                lr_processed_data=pca_reduction_cluster_full(lr_processed_data,"lr")
                if(actual_status=="Yes"):
                    predi=pd.DataFrame()
                    predi[sel_tar]=jun[sel_tar].copy()
                elif(actual_status=="No"):
                    predi=pd.DataFrame()   
                predi[str("LR")+'_Pred']=model.predict(lr_processed_data)
                df_preds2=pd.DataFrame(predi.copy())
                merged=pd.concat([to_merge,df_preds2],axis=1)
                merged=merged.reset_index().drop(['index'],axis=1) 
                full_river=pd.concat([full_river,merged],axis=0)
                full_river=full_river.reset_index().drop(['index'],axis=1) 
                del merged
                del df_preds2
                del predi
                import gc
                gc.collect()

            full_river=full_river.sort_values(by=pid)
            full_river=full_river.reset_index().drop(['index'],axis=1) 
            print("Sample of the output")
            print(full_river.head())
            full_river.to_csv('model_files/'+'Predictions_lr.csv',index=False)
            del lr_processed_data


        if(ml_algo_name=="Support Vector Regressor (SVR)"):
            model=joblib.load('model_files/'+'svr.pkl')
            col_name="SVR"
            full_river=pd.DataFrame()
            for jun in full_data:
                to_merge=pd.DataFrame()
                merged=pd.DataFrame()
                to_merge=pd.DataFrame(jun[[pid,east,north]])
                hutt_full=jun[sel_cols2].copy()
                lr_processed_data=logperm_gen_full(hutt_full,r,g,b)
                lr_processed_data=greyscale_gen_full(lr_processed_data,r,g,b)
                lr_processed_data=cluster_data_full(lr_processed_data,r,g,b)
                lr_processed_data=standard_scale_cluster_full(lr_processed_data,"lr")
                lr_processed_data=pca_reduction_cluster_full(lr_processed_data,"lr")
                if(actual_status=="Yes"):
                    predi=pd.DataFrame()
                    predi[sel_tar]=jun[sel_tar].copy()
                elif(actual_status=="No"):
                    predi=pd.DataFrame()
                predi[str("SVR")+'_Pred']=model.predict(lr_processed_data)
                df_preds2=pd.DataFrame(predi.copy())
                merged=pd.concat([to_merge,df_preds2],axis=1)
                merged=merged.reset_index().drop(['index'],axis=1) 
                full_river=pd.concat([full_river,merged],axis=0)
                full_river=full_river.reset_index().drop(['index'],axis=1) 
                del merged
                del df_preds2
                del predi
                import gc
                gc.collect()

            full_river=full_river.sort_values(by=pid)
            full_river=full_river.reset_index().drop(['index'],axis=1) 
            print("Sample of the output")
            del lr_processed_data
            print(full_river.head())
            full_river.to_csv('model_files/'+'Predictions_svr.csv',index=False)    

        if(ml_algo_name=="Random Forest Regressor (RF)"):
            model=joblib.load('model_files/'+'rf.pkl')
            col_name="RF"
            full_river=pd.DataFrame()
            for jun in full_data:
                to_merge=pd.DataFrame()
                merged=pd.DataFrame()
                to_merge=pd.DataFrame(jun[[pid,east,north]])
                hutt_full=jun[sel_cols2].copy()
                rf_processed_data=logperm_gen_full(hutt_full,r,g,b)
                rf_processed_data=greyscale_gen_full(rf_processed_data,r,g,b)
                rf_processed_data=cluster_data_full(rf_processed_data,r,g,b)
                rf_processed_data=poly_creation_cluster_full(rf_processed_data)
                rf_processed_data=correl_full(rf_processed_data,"rf")
                if(actual_status=="Yes"):
                    predi=pd.DataFrame()
                    predi[sel_tar]=jun[sel_tar].copy()
                elif(actual_status=="No"):
                    predi=pd.DataFrame()
                predi[str("RF")+'_Pred']=model.predict(rf_processed_data)
                df_preds2=pd.DataFrame(predi.copy())
                merged=pd.concat([to_merge,df_preds2],axis=1)
                merged=merged.reset_index().drop(['index'],axis=1) 
                full_river=pd.concat([full_river,merged],axis=0)
                full_river=full_river.reset_index().drop(['index'],axis=1) 
                del merged
                del df_preds2
                del predi
                import gc
                gc.collect()

            full_river=full_river.sort_values(by=pid)
            full_river=full_river.reset_index().drop(['index'],axis=1) 
            print("Sample of the output")
            del rf_processed_data
            print(full_river.head())
            full_river.to_csv('model_files/'+'Predictions_rf.csv',index=False)

        if(ml_algo_name=="Extreme Gradient Boosting (XGBoost)"):
            model=joblib.load('model_files/'+'xgb.pkl')
            col_name="XGB"
            full_river=pd.DataFrame()
            for jun in full_data:
                to_merge=pd.DataFrame()
                merged=pd.DataFrame()
                to_merge=pd.DataFrame(jun[[pid,east,north]])
                print(sel_cols2)
                hutt_full=jun[sel_cols2].copy()
                print("start",hutt_full.shape)
                xgb_processed_data=logperm_gen_full(hutt_full,r,g,b)
                print("middle 1",xgb_processed_data.shape)
                xgb_processed_data=greyscale_gen_full(xgb_processed_data,r,g,b)
                print("middle 2",xgb_processed_data.shape)
                xgb_processed_data=cluster_data_full(xgb_processed_data,r,g,b)
                print("middle 3",xgb_processed_data.shape)
                print(xgb_processed_data.columns)
                xgb_processed_data=poly_creation_cluster_full(xgb_processed_data)
                print("middle 4",xgb_processed_data.shape)
                xgb_processed_data=correl_full(xgb_processed_data,"rf")
                print("middle 5",xgb_processed_data.shape)
                if(actual_status=="Yes"):
                    predi=pd.DataFrame()
                    predi[sel_tar]=jun[sel_tar].copy()
                elif(actual_status=="No"):
                    predi=pd.DataFrame()
                predi[str("XGB")+'_Pred']=model.predict(xgb_processed_data)
                df_preds2=pd.DataFrame(predi.copy())
                merged=pd.concat([to_merge,df_preds2],axis=1)
                merged=merged.reset_index().drop(['index'],axis=1) 
                full_river=pd.concat([full_river,merged],axis=0)
                full_river=full_river.reset_index().drop(['index'],axis=1) 
                del merged
                del df_preds2
                del predi
                import gc
                gc.collect()

            full_river=full_river.sort_values(by=pid)
            full_river=full_river.reset_index().drop(['index'],axis=1) 
            print("Sample of the output")
            del xgb_processed_data
            print(full_river.head())
            full_river.to_csv('model_files/'+'Predictions_xgb.csv',index=False)    

        if(ml_algo_name=="Multi-layer Perceptron (Neural Network)"):
            model=joblib.load('model_files/'+'nn.pkl')
            col_name="NN"
            full_river=pd.DataFrame()
            for jun in full_data:
                to_merge=pd.DataFrame()
                merged=pd.DataFrame()
                to_merge=pd.DataFrame(jun[[pid,east,north]])
                hutt_full=jun[sel_cols2].copy()
                print("start",hutt_full.shape)
                nn_processed_data=logperm_gen_full(hutt_full,r,g,b)
                print("start 1",nn_processed_data.shape)
                nn_processed_data=greyscale_gen_full(nn_processed_data,r,g,b)
                print("start 2",nn_processed_data.shape)
                nn_processed_data=cluster_data_full(nn_processed_data,r,g,b)
                print("start 3",nn_processed_data.shape)
                nn_processed_data=poly_creation_cluster_full(nn_processed_data)
                print("start 4",nn_processed_data.shape)
                nn_processed_data=standard_scale_cluster_full(nn_processed_data,"nn")
                print("start 5",nn_processed_data.shape)
                nn_processed_data=correl_full(nn_processed_data,"nn")
                print("start 6",nn_processed_data.shape)
                if(actual_status=="Yes"):
                    predi=pd.DataFrame()
                    predi[sel_tar]=jun[sel_tar].copy()
                elif(actual_status=="No"):
                    predi=pd.DataFrame()
                predi[str("NN")+'_Pred']=model.predict(nn_processed_data)
                df_preds2=pd.DataFrame(predi.copy())
                merged=pd.concat([to_merge,df_preds2],axis=1)
                merged=merged.reset_index().drop(['index'],axis=1) 
                full_river=pd.concat([full_river,merged],axis=0)
                full_river=full_river.reset_index().drop(['index'],axis=1) 
                del merged
                del df_preds2
                del predi
                import gc
                gc.collect()

            full_river=full_river.sort_values(by=pid)
            full_river=full_river.reset_index().drop(['index'],axis=1) 
            print("Sample of the output")
            del nn_processed_data
            print(full_river.head())
            full_river.to_csv('model_files/'+'Predictions_nn.csv',index=False)
        
        print("done")
        gc.collect()
        jsonfiles_full_river_preds = json.loads(full_river.head().to_json(orient='records'))
        if(actual_status=='Yes'):
            full_river_preds_metrics=calculate_metrics(ml_algo_name, full_river,sel_tar, str(col_name))
            print(full_river_preds_metrics)
            full_river_preds_metrics_temp=calculate_metrics_temp(full_river, sel_tar, str(col_name))
            final_preds_full_concat=pd.concat([full_river_preds_metrics,full_river_preds_metrics_temp],axis=0)

            jsonfiles_full_river_preds_metrics = json.loads(final_preds_full_concat[full_river_preds_metrics.columns].to_json(orient='records'))
            del full_river
            return (jsonify({'preds':jsonfiles_full_river_preds,'metrics':jsonfiles_full_river_preds_metrics}))    
        if(actual_status=="No"):
            del full_river
            return (jsonify(jsonfiles_full_river_preds))
    
    elif("default_options_all" not in process_options):
        print("in not default")
        full_river=pd.DataFrame()
        data_reference=load_data('data_for_all_models.csv')
        if(ml_algo_name=="Multi-Linear Regression (LR)"):
            model=joblib.load('model_files/'+'lr.pkl')
            col_name="LR"

        if(ml_algo_name=="Support Vector Regressor (SVR)"):
            model=joblib.load('model_files/'+'svr.pkl')
            col_name="SVR"

        if(ml_algo_name=="Random Forest Regressor (RF)"):
            model=joblib.load('model_files/'+'rf.pkl')
            col_name="RF"

        if(ml_algo_name=="Extreme Gradient Boosting (XGBoost)"):
            model=joblib.load('model_files/'+'xgb.pkl')
            col_name="XGB"

        if(ml_algo_name=="Multi-layer Perceptron (Neural Network)"):
            model=joblib.load('model_files/'+'nn.pkl')
            col_name="NN"

        for processed_data in full_data:
            if(actual_status=="Yes"):
                predi=pd.DataFrame()
                predi[sel_tar]=processed_data[sel_tar].copy()
            elif(actual_status=="No"):
                predi=pd.DataFrame()

            to_merge=pd.DataFrame()
            merged=pd.DataFrame()
            to_merge=pd.DataFrame(processed_data[[pid,east,north]])
            processed_data=processed_data[sel_cols2].copy()
            print("start",processed_data.shape)
            if("logperm_gen" in process_options):
                processed_data=logperm_gen_full(processed_data,r,g,b)
            print("start1",processed_data.shape)

            if("greyscale_gen" in process_options):
                processed_data=greyscale_gen_full(processed_data,r,g,b)
            print("start2",processed_data.shape)

            if("cluster_gen" in process_options):
                processed_data=cluster_data_full(processed_data,r,g,b)  
            print("start3",processed_data.shape)

            if(("poly_corr_gen" in process_options) & ("cluster_gen" in process_options)):
                processed_data=poly_creation_cluster_full(processed_data)    
            print("start4",processed_data.shape)

            if(("poly_corr_gen" in process_options) & ("cluster_gen" not in process_options)):
                processed_data=poly_creation_no_cluster_full(processed_data)     
            print("start4",processed_data.shape)

            if(("ss_scale" in process_options) & ("cluster_gen" in process_options)):
                processed_data=standard_scale_cluster_full(processed_data,"common")
                print("start5",processed_data.shape)

            if(("ss_scale" in process_options) & ("cluster_gen" not in process_options)):
                processed_data=standard_scale_no_cluster_full(processed_data,"common")    
            print("start5",processed_data.shape)
            if(("minmax_scale" in process_options) & ("cluster_gen" in process_options)):
                processed_data=min_max_scale_cluster_full(processed_data,"common")

            if(("minmax_scale" in process_options) & ("cluster_gen" not in process_options)):
                processed_data=min_max_scale_no_cluster_full(processed_data,"common")    

            if(("robust_scale" in process_options) & ("cluster_gen" in process_options)):
                processed_data=robust_scale_cluster_full(processed_data,"common")

            if(("robust_scale" in process_options) & ("cluster_gen" not in process_options)):
                processed_data=robust_scale_no_cluster_full(processed_data,"common")    

            if(("power_scale" in process_options) & ("cluster_gen" in process_options)):
                processed_data=power_scale_cluster_full(processed_data,"common")

            if(("power_scale" in process_options) & ("cluster_gen" not in process_options)):
                processed_data=power_scale_no_cluster_full(processed_data,"common")  

            if("pearson_correl" in process_options):
                processed_data=correl_full(processed_data,"common")
                print("start6",processed_data.shape)

            if("poly_lasso_trans" in process_options):
                lasso_result=pd.read_csv('model_files/'+'lasso_reg.csv')
                processed_data=lasso_reg_full(processed_data,lasso_result)

            if(("pca_trans" in process_options) & ("cluster_gen" in process_options)):
                processed_data=pca_reduction_cluster_full(processed_data,"common")    

            if(("pca_trans" in process_options) & ("cluster_gen" not in process_options)):
                processed_data=pca_reduction_no_cluster_full(processed_data,"common")

            predi[str(col_name)+'_Pred']=model.predict(processed_data)
            df_preds2=pd.DataFrame(predi.copy())
            merged=pd.concat([to_merge,df_preds2],axis=1)
            merged=merged.reset_index().drop(['index'],axis=1) 
            full_river=pd.concat([full_river,merged],axis=0)
            full_river=full_river.reset_index().drop(['index'],axis=1) 
            del merged
            del df_preds2
            del predi
            import gc
            gc.collect()

        full_river=full_river.sort_values(by=pid)
        full_river=full_river.reset_index().drop(['index'],axis=1) 
        print("Sample of the output")
        print(full_river.head())
        preds_file_name='Predictions_'+str(col_name).lower()+'.csv'
        full_river.to_csv('model_files/'+preds_file_name,index=False)

        jsonfiles_full_river_preds = json.loads(full_river.head().to_json(orient='records'))
        if(actual_status=='Yes'):
            full_river_preds_metrics=calculate_metrics(ml_algo_name, full_river, sel_tar, str(col_name))
            full_river_preds_metrics_temp=calculate_metrics_temp(full_river, sel_tar, str(col_name))
            final_preds_full_concat=pd.concat([full_river_preds_metrics,full_river_preds_metrics_temp],axis=0)

            jsonfiles_full_river_preds_metrics = json.loads(final_preds_full_concat[full_river_preds_metrics.columns].to_json(orient='records'))
            del full_river
            return (jsonify({'preds':jsonfiles_full_river_preds,'metrics':jsonfiles_full_river_preds_metrics}))    
        if(actual_status=="No"):
            del full_river
            return (jsonify(jsonfiles_full_river_preds))

@app.route('/save_model_files', methods=['POST','GET'])
def save_model_files():
    model_loc_obj= request.form['model_save_loc']
    model_loc=json.loads(model_loc_obj)
    print(model_loc)

    algo_choice_obj= request.form['ml_algo_name']
    algo_choice=json.loads(algo_choice_obj)
    print(algo_choice)
    if os.path.exists(model_loc):
        shutil.rmtree(model_loc)
    shutil.copytree('model_files',model_loc)
    return jsonify("Done")





#to read https://towardsdatascience.com/data-science-you-need-to-know-a-b-testing-f2f12aff619
#  in case we dont use flask run and instead opt to use the python run command, then we have to use __name__ to run # to run using flask run - do export env things
if __name__=='__main__':
   # Timer(1.5, open_browser).start()
    app.run(debug=True)



# charts:
#https://blog.ruanbekker.com/blog/2017/12/14/graphing-pretty-charts-with-python-flask-and-chartjs/
#https://mdbootstrap.com/docs/jquery/javascript/charts/

# PLS AND PCA:
#https://medium.com/analytics-vidhya/dimensionality-reduction-in-supervised-framework-and-partial-least-square-regression-b557a4c6c049#:~:text=As%20discussed%20in%20the%20above,the%20data%20in%20a%20latent