#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Mar 27 12:14:23 2020

@author: lego
"""



import streamlit as st
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
import scipy as sc
import plotly_express as px
import plotly
import time
import plotly.graph_objects as go
from sklearn.ensemble import IsolationForest
import random
from sklearn.preprocessing import PolynomialFeatures
from sklearn.pipeline import make_pipeline
from sklearn.model_selection import train_test_split
from xgboost import XGBRegressor
from sklearn.linear_model import LinearRegression
from sklearn.ensemble import RandomForestRegressor
from sklearn.metrics import mean_squared_error as mse
from sklearn.metrics import r2_score
from sklearn.metrics import mean_absolute_error
from sklearn.metrics import mean_poisson_deviance
from sklearn.metrics import max_error
from sklearn.metrics import explained_variance_score
from sklearn.svm import SVR
from sklearn.tree import DecisionTreeRegressor
from sklearn.externals.six import StringIO  
from IPython.display import Image  
from sklearn.tree import export_graphviz                                        
from subprocess import call
import matplotlib.pyplot as plt
from sklearn.inspection import PartialDependenceDisplay
from sklearn.inspection import plot_partial_dependence
from rfpimp import permutation_importances
import shap
from sklearn.cluster import KMeans
from sklearn.cluster import MiniBatchKMeans
import os,sys
from sklearn.decomposition import PCA
import time
from sklearn.manifold import TSNE
from sklearn.preprocessing import StandardScaler
import joblib

#st.write("The final CSV file before entering the Machine learing workflow is ", data.head())

def load_data(data_path):
    #st.write("Loading Data...")
    rangi_01_orig=pd.read_csv(data_path)
    #st.write("Done!!")
    return rangi_01_orig


def main():
    
    st.subheader("Launching Stage 2 of our pipeline....")
    st.title("Pipeline Stage : Feature Engineering and Polynomial Creation")
    #data_path=st.text_input("Enter the path of the CSV data. If you have preprocessed and exported, type 'Cleaned_file.csv' . If you did not export or not preprocessed, type the original file name: ")#"/home/lego/Desktop/Job/Land River Sea Consulting Ltd./rangitata_01_new.csv
    #st.write(" \n \n")
    #if(data_path):
    data=load_data('temp_pipeline.csv')
    cor=load_data('Coordinates.csv')
    st.write("The final CSV file before entering the Machine learing workflow is ", data.head())
    
    if st.checkbox(" Enter into Machine learning workflow "):
        sel_cols1=st.multiselect("Select the features(in the order 'R','G','B' and other important ones) for training the Machine learning model and then select the Target column (to be predicted) at the end",data.columns.tolist())                
        st.write('You selected:', sel_cols1)  
        conf4=st.text_input("Type 'Yes'  and press enter to confirm the above as your selected features")
        if(conf4=='Yes'):
            r=sel_cols1[0]
            g=sel_cols1[1]
            b=sel_cols1[2]
            x=data[sel_cols1[:-1]].copy()
            y=data[sel_cols1[-1]].copy()
            x['ln(b/r)']=0
            x['ln(b/g)']=0
            x['ln(b/r)'] = x.apply(lambda row: np.log10((row[b]/row[r])), axis=1)
            x['ln(b/g)'] = x.apply(lambda row: np.log10((row[b]/row[g])), axis=1)
                                                                
            st.write('You selected:', sel_cols1[:-1],' as the independent features')
            st.write('You selected:', sel_cols1[-1],' as the dependent feature')
                        
            st.write(x.head())
            st.write(y.head())
            x[sel_cols1[-1]]=y.values
            x.to_csv('y.csv',index=False)
            x.drop(sel_cols1[-1],axis=1,inplace=True)
            conf3='No'
            conf3=st.text_input("Type 'Yes' and press enter to confirm and proceed")
            if(conf3=='Yes'):
                st.write("Please wait while this pipeline creates some important features.... ")
                                       
               # ''' Start of New stuffs'''
                ## We calculate the r*g*b value and store as rgb_combo column
                x['RGB_Combo']=x.apply(lambda row: row[r]*row[g]*row[b], axis=1)
                data_new=x.copy()
                data_new=data_new.reset_index().drop(['index'],axis=1)
                
                st.write("Clustering the RGB values..")
                data_new=cluster_data(data_new,r,g,b)#legor
                
                st.subheader("Plot showing the Depth of the river on every Cluster")
                show_cluster_depth(data_new,cor,y)
                
                x_c=cor.columns[0]
                y_c=cor.columns[1]
                
                st.subheader("Plot showing the the calibration river points on every Cluster")
                show_cluster_river(data_new,x_c,y_c,cor)
                st.write("Using this plot, we can see that the points closer to the edges have higher RGB cluster, and the deeper points(middle of the river) with medium RGB cluster")
                
                # peak rgb ratio
                data_new=peak_ratio_rgb(data_new,r,g,b)
                
                # Already W is there. Else: compute_weight()
                data_clean=data_new.copy()
                 #One hot encode cluster feature
                data_clean=pd.get_dummies(data_clean)
                
                # To maintain the order in low, med ,high....
                lowp=data_clean['Point_cluster_Low'].values
                medp=data_clean['Point_cluster_Medium'].values
                highp=data_clean['Point_cluster_High'].values
                clus_all=['Point_cluster_Low','Point_cluster_Medium','Point_cluster_High']
                data_clean.drop(clus_all,axis=1,inplace=True)
                data_clean['Point_cluster_Low']=lowp
                data_clean['Point_cluster_Medium']=medp
                data_clean['Point_cluster_High']=highp
                
                st.subheader("In the left hand side you can vizualize some plots describing the PCA(Principal component analysis) and t-SNE(T-test Distributed Schotastic neighbour embedding). NOTE : You will need some knowledge of eigen vectors to understand these plots")
                pca_tsne_plots(data_clean,y)
                
                con1=0
                con1=st.checkbox("Check this box to continue")
                if(con1):
                    # Greyscale
                    temp=data_clean.copy()
                    temp['Greyscale']=temp.apply(lambda row: (0.3*row[r] + 0.59*row[g] + 0.11*row[b]), axis=1)
                    # Drop one varaiable for dummy var
                    
                    st.write(" The dataframe before creating polynomial features of degree 2 :")
                    st.write(temp.head())
                    # feat imp graph
                    rf_imp=feature_imp(temp,y)
                    feat_imp=(rf_imp.feature_importances_)*100
                    #sort_index=np.argsort(-feat_imp)
                    fig=px.bar(x=feat_imp,y=temp.columns, orientation='h')#full
                    st.plotly_chart(fig)
                    st.write("Hint : The above bar chart represents the feature importances. On the Y-axis are the most important features out of",len(temp.columns.tolist())," polynomial features and on the X-axis is the weight assigned to each feature. The longer the weight-bar is, the more important that feature is to explain the data. "   )
    
                    st.header("Entering into Polynomial Feature Generation")
                    st.write(" Reason to create polynomial features : Machine learning models perform better with increasing number of features, as more features explain the data better. But it should be remembered that providing too many features is also not optimal and may lead to reduced algorithm performance as it promotes overfitting. Another reason for not selecting huge number of features is the algorithm's computational capability.")    
                    temp.drop(['RGB_Combo'],axis=1,inplace=True)
                    
                    new_data=poly_creation(temp,y)
                    st.write(new_data.head())
                    
                    
                  #  '''End of new stuffs'''
        
                    #x.to_csv('chose_features.csv',index=False)
                    #poly.to_csv('temp_poly.csv',index=False)
                    
                    #totrain=poly_feat_numbers(poly,importance_model)
                    
                    ens=st.checkbox("Check this box to proceed to the next Stage")
                    if(ens):
                        st.write('Shape of x and y is : ',new_data.shape,' and ',y.shape,' respectively ')
                        # Next create different datasets and store as csv for each model ( with poly, with sc etc.)  
                    ##nn        
                        sc_nn=StandardScaler()
                        nn_x=sc_nn.fit_transform(new_data)
                        nn_x=pd.DataFrame(nn_x)
                        nn_x.to_csv('nn_x.csv',index=False)
                        joblib.dump(sc_nn,'sc_nn_full.pkl')
                    #rf,xgb
                        new_data.to_csv('rf_x.csv',index=False)
                        #svr,lr
                        pca1=PCA(n_components=3)
                        pca_result=pca1.fit_transform(temp)
                        sub=pd.DataFrame()
                        sub['pca-one']=0
                        sub['pca-two']=0
                        sub['pca-three']=0
                        sub['pca-one'] = pca_result[:,0]
                        sub['pca-two'] = pca_result[:,1] 
                        sub['pca-three'] = pca_result[:,2]
                        sc1=StandardScaler()
                        sub_x=sc1.fit_transform(sub)
                        sub_x=pd.DataFrame(sub_x,columns=sub.columns)
                        
                        sub_x.to_csv('sub_x.csv',index=False)
                        joblib.dump(pca1,'pca_full.pkl')
                        joblib.dump(sc1,'sc_full.pkl')
                                                
                        st.subheader(" Now kindly wait as we redirect you to the next stage - Machine Learning Stage ")
                        #st.subheader(" UNDER CONSTRUCTION!!")
                        time.sleep(2)
                        os.system('streamlit run ml_stage.py')

def pca_tsne_plots(data_clean,y):
    dataviz_choice=st.sidebar.selectbox("Choose the Vizualization",['None','PCA + t-SNE'],key='257')
    if dataviz_choice=='PCA + t-SNE':
        st.subheader("PCA(Principal component analysis) and t-SNE(T-test Distributed Schotastic neighbour embedding) Maps")
        pca_plot(data_clean,y)


def tsne_plot(sub,y):
    tsne_results=perform_tsne(sub)
    sub['tnse-one']=tsne_results[:,0]
    sub['tnse-two']=tsne_results[:,1]
 
    import plotly.graph_objects as go    
    fig1=go.Figure()
    fig1.add_trace(go.Scatter(
        x=sub['tnse-one'],
        y=sub['tnse-two'],
        name="TSNE 2-D Graph",
        mode='markers',
        showlegend=True,
        marker=dict(color=np.arange(min(y),max(y),0.01),size=10,line=dict(color='DarkSlateGrey',width=0.5))
    ))

    st.plotly_chart(fig1)
    
@st.cache(suppress_st_warning=True,allow_output_mutation=True)
def perform_tsne(sub):
    tsne = TSNE(n_components=2, verbose=1, perplexity=40, n_iter=300)
    tsne_results = tsne.fit_transform(sub)
    return tsne_results

def pca_plot(data_clean,y):
    pca_result=perform_pca(data_clean)
    sub=pd.DataFrame()
    sub['pca-one']=0
    sub['pca-two']=0
    sub['pca-three']=0
    
    sub['pca-one'] = pca_result[:,0]
    sub['pca-two'] = pca_result[:,1] 
    sub['pca-three'] = pca_result[:,2]
    
    import plotly.graph_objects as go

    fig = go.Figure()

    fig.add_trace(go.Scatter(
        x=sub['pca-one'],
        y=sub['pca-two'],
        name="PCA 2-D Graph",
        mode='markers',
        showlegend=True,
        marker=dict(color=np.arange(min(y),max(y),0.01),size=10,line=dict(color='DarkSlateGrey',width=0.5))
    ))
    
    st.plotly_chart(fig)
    
    tsne_plot(sub,y)
    
@st.cache(suppress_st_warning=True,allow_output_mutation=True)
def perform_pca(data_clean):
    pca=PCA(n_components=3)
    pca_result=pca.fit_transform(data_clean)
    st.write('Explained variation per principal component: {}'.format(pca.explained_variance_ratio_))
    return pca_result
    
    
@st.cache(suppress_st_warning=True,allow_output_mutation=True)
def peak_ratio_rgb(hutt_full,r,g,b):
    
        
    peak_r=hutt_full.groupby(['Point_cluster']).max()[r]
    peak_g=hutt_full.groupby(['Point_cluster']).max()[g]
    peak_b=hutt_full.groupby(['Point_cluster']).max()[b]
    peak_rgb=hutt_full.groupby(['Point_cluster']).max()['RGB_Combo']
    hutt_full[[r,g,b,'RGB_Combo','Point_cluster']].to_csv('groupby.csv',index=False)
    
    hutt_full['Ratio_peak_R']=0.0
    hutt_full['Ratio_peak_G']=0.0
    hutt_full['Ratio_peak_B']=0.0
    hutt_full['Ratio_peak_RGBCombo']=0.0
    
    #low
    hutt_full['Ratio_peak_R']=np.where(hutt_full['Point_cluster']=='Low',hutt_full[r]/peak_r['Low'],hutt_full['Ratio_peak_R'])
    hutt_full['Ratio_peak_G']=np.where(hutt_full['Point_cluster']=='Low',hutt_full[g]/peak_g['Low'],hutt_full['Ratio_peak_G'])
    hutt_full['Ratio_peak_B']=np.where(hutt_full['Point_cluster']=='Low',hutt_full[b]/peak_b['Low'],hutt_full['Ratio_peak_B'])
    hutt_full['Ratio_peak_RGBCombo']=np.where(hutt_full['Point_cluster']=='Low',hutt_full['RGB_Combo']/peak_rgb['Low'],hutt_full['Ratio_peak_RGBCombo'])
    
    #medium
    hutt_full['Ratio_peak_R']=np.where(hutt_full['Point_cluster']=='Medium',hutt_full[r]/peak_r['Medium'],hutt_full['Ratio_peak_R'])
    hutt_full['Ratio_peak_G']=np.where(hutt_full['Point_cluster']=='Medium',hutt_full[g]/peak_g['Medium'],hutt_full['Ratio_peak_G'])
    hutt_full['Ratio_peak_B']=np.where(hutt_full['Point_cluster']=='Medium',hutt_full[b]/peak_b['Medium'],hutt_full['Ratio_peak_B'])
    hutt_full['Ratio_peak_RGBCombo']=np.where(hutt_full['Point_cluster']=='Medium',hutt_full['RGB_Combo']/peak_rgb['Medium'],hutt_full['Ratio_peak_RGBCombo'])
    
    #high
    hutt_full['Ratio_peak_R']=np.where(hutt_full['Point_cluster']=='High',hutt_full[r]/peak_r['High'],hutt_full['Ratio_peak_R'])
    hutt_full['Ratio_peak_G']=np.where(hutt_full['Point_cluster']=='High',hutt_full[g]/peak_g['High'],hutt_full['Ratio_peak_G'])
    hutt_full['Ratio_peak_B']=np.where(hutt_full['Point_cluster']=='High',hutt_full[b]/peak_b['High'],hutt_full['Ratio_peak_B'])
    hutt_full['Ratio_peak_RGBCombo']=np.where(hutt_full['Point_cluster']=='High',hutt_full['RGB_Combo']/peak_rgb['High'],hutt_full['Ratio_peak_RGBCombo'])
    st.write(peak_r,peak_g,peak_b)
    
    return hutt_full

def show_cluster_river(data_new,x_c,y_c,cor):
    import plotly.graph_objects as go
    low_indices=data_new[data_new['Point_cluster']=='Low'].index
    med_indices=data_new[data_new['Point_cluster']=='Medium'].index
    high_indices=data_new[data_new['Point_cluster']=='High'].index
    
    layout = go.Layout(
        title=x_c + " vs " + y_c,
        xaxis=dict(
            title=x_c
        ),
        yaxis=dict(
            title=y_c
        ) )
    fig = go.Figure(layout=layout)
    
    fig.add_trace(go.Scatter(
        x=cor[x_c].iloc[low_indices],
        y=cor[y_c].iloc[low_indices],
        name="Low RGB cluster",
        mode='markers',
        marker=dict(size=10,line=dict(color='DarkSlateGrey',width=0.5))
    ))
    
    fig.add_trace(go.Scatter(
        x=cor[x_c].iloc[med_indices],
        y=cor[y_c].iloc[med_indices],
        name="Medium RGB cluster",
        mode='markers',
        marker=dict(size=10,line=dict(color='DarkSlateGrey',width=0.5))
    ))
    fig.add_trace(go.Scatter(
        x=cor[x_c].iloc[high_indices],
        y=cor[y_c].iloc[high_indices],
        name="High RGB cluster",
        mode='markers',
        marker=dict(size=10,line=dict(color='DarkSlateGrey',width=0.5))
    ))
    
    st.plotly_chart(fig)
    
def show_cluster_depth(data_new,cor,y):
    
    import plotly.graph_objects as go
    
    low_indices=data_new[data_new['Point_cluster']=='Low'].index
    med_indices=data_new[data_new['Point_cluster']=='Medium'].index
    high_indices=data_new[data_new['Point_cluster']=='High'].index
    layout = go.Layout(
        title='Distance' + " vs " + 'Depth' +' With coloured clusters',
        xaxis=dict(
            title='Distance'
        ),
        yaxis=dict(
            title='Depth'
        ) )
    fig = go.Figure(layout=layout)
    
    fig.add_trace(go.Scatter(
        x=cor['Distance_new'].iloc[low_indices],
        y=y.iloc[low_indices],
        name="Low RGB cluster",
        mode='markers',
        marker=dict(size=10,line=dict(color='DarkSlateGrey',width=0.5))
    ))
    
    fig.add_trace(go.Scatter(
        x=cor['Distance_new'].iloc[med_indices],
        y=y.iloc[med_indices],
        name="Medium RGB cluster",
        mode='markers',
        marker=dict(size=10,line=dict(color='DarkSlateGrey',width=0.5))
    ))
    fig.add_trace(go.Scatter(
        x=cor['Distance_new'].iloc[high_indices],
        y=y.iloc[high_indices],
        name="High RGB cluster",
        mode='markers',
        marker=dict(size=10,line=dict(color='DarkSlateGrey',width=0.5))
    ))

    st.plotly_chart(fig)
    

@st.cache(suppress_st_warning=True,allow_output_mutation=True)
def cluster_data(data_new,r,g,b):
    rgb=np.vstack((data_new[[r,g,b]].values))
    wcss=[]
    mapping={}
    for i in range(1,10):
      kmeans = MiniBatchKMeans(n_clusters=i,random_state=10).fit(rgb)
      wcss.append(kmeans.inertia_)
      mapping[i]=kmeans.inertia_

    optimal=0
    diff=[]
    for i in range(2,10):
      diff.append(-(mapping[i]-mapping[i-1]))
    diff_mean=np.mean(diff)
    for i in range(2,10):
      if((mapping[i-1]-mapping[i]>diff_mean)==False):
        optimal=i-1
        break
    if(optimal==3):
        st.write("Optimal clusters : " ,optimal)
    kmeans = MiniBatchKMeans(n_clusters=3 ,random_state=10).fit(rgb)
    data_new.loc[:, 'point_cluster'] = kmeans.predict(rgb)
    joblib.dump(kmeans,'kmeans.pkl')
    data_new['point_cluster']=data_new['point_cluster'].astype('int64')
    # store this csv to use for caluculating mean/med during full river predictions
    data_new.to_csv('min_med_max.csv',index=False)
    max_indx=data_new.groupby(['point_cluster']).mean()['RGB_Combo'][data_new.groupby(['point_cluster']).mean()['RGB_Combo']==np.max(data_new.groupby(['point_cluster']).mean()['RGB_Combo'])].index
    min_indx=data_new.groupby(['point_cluster']).mean()['RGB_Combo'][data_new.groupby(['point_cluster']).mean()['RGB_Combo']==np.min(data_new.groupby(['point_cluster']).mean()['RGB_Combo'])].index
    indexs=list(data_new.groupby(['point_cluster']).mean().index)
    indexs.remove(min_indx)
    indexs.remove(max_indx)
    med_indx=indexs[0]
    
    
    data_new['Point_cluster']=''
    data_new['Point_cluster']=np.where(data_new['point_cluster']==max_indx[0],'High',data_new['Point_cluster'])
    data_new['Point_cluster']=np.where(data_new['point_cluster']==med_indx,'Medium',data_new['Point_cluster'])
    data_new['Point_cluster']=np.where(data_new['point_cluster']==min_indx[0],'Low',data_new['Point_cluster'])
    
    data_new.drop(['point_cluster'],axis=1,inplace=True)
    
    return data_new
    

def tts(df,y,test_split):#0.15 for hutt
    x_train,x_test,y_train,y_test=train_test_split(df,y,test_size=test_split,random_state=0,shuffle=True)
    return x_train,x_test,y_train,y_test

@st.cache(suppress_st_warning=True,allow_output_mutation=True)
def poly_creation(df,y):
    st.write("Converting",df.shape[1]," features to multiple polynomial features.... ")
    pcl=df[['Point_cluster_Low','Point_cluster_Medium','Point_cluster_High']].copy()
    df.drop(['Point_cluster_Low','Point_cluster_Medium','Point_cluster_High'],axis=1,inplace=True)
    poly = PolynomialFeatures(2)
    x_poly=poly.fit_transform(df)
    poly_names=poly.get_feature_names(['Feature'+str(l) for l in range(1,len(np.array(poly.get_feature_names())))])
    df_poly=pd.DataFrame(x_poly,columns=poly_names)
    df_poly.drop(['1'],inplace=True,axis=1)
    
    #append the clusters
    df_poly=pd.concat([df_poly,pcl],axis=1)
    #st.write("done",df_poly.head())
    x_train,x_test,y_train,y_test=tts(df_poly,y,0.2)
    corr_features = set()
    #remove pcl again, pearsosn corr not appropriate for categorical variables
    pcl_tr=x_train[['Point_cluster_Low','Point_cluster_Medium','Point_cluster_High']].copy()
    x_train.drop(['Point_cluster_Low','Point_cluster_Medium','Point_cluster_High'],axis=1,inplace=True)
    
    # create the correlation matrix (default to pearson)
    corr_matrix = x_train.corr()
    from sklearn.linear_model import Lasso
    from sklearn.feature_selection import SelectFromModel
    for i in range(len(corr_matrix .columns)):
        for j in range(i):
            if abs(corr_matrix.iloc[i, j]) > 0.90:
                colname = corr_matrix.columns[i]
                corr_features.add(colname)
                
    x_train.drop(labels=corr_features, axis=1, inplace=True)
    
    #append back to use lasso on these columns...without any interactions among categorical variables by polynomial fc
    x_train=pd.concat([x_train,pcl_tr],axis=1)
    #st.write(x_train.shape)
    selected_feat=x_train.columns
    X_train_selected=df_poly[selected_feat]
    #st.write("done2",x_train.head())
   # scaler = StandardScaler()
   # scaler.fit(x_train)
    #sel_=Lasso(alpha=0.01, max_iter=10e5)
   # from sklearn.model_selection import GridSearchCV 
  
    # defining parameter range 
   # param_grid = {'alpha': [0.01,0.001, 0.0001,0.00001]}  
     
   # ls = GridSearchCV(Lasso(max_iter=10e5), param_grid, refit = True, verbose = 3)
    
   # ls.fit(scaler.transform(x_train), y_train)
   # best_coeff=ls.best_params_
   # st.write(ls.best_score_)
   # st.write(ls.best_params_)
 #   sel_=Lasso(alpha=best_coeff['alpha'],max_iter=10e5)
   # sel_.fit(scaler.transform(x_train), y_train)
  #  selected_feat = x_train.columns[sel_.coef_!=0]
   # st.write('total features: {}'.format((x_train.shape[1])))
  #  st.write('selected features: {}'.format(len(selected_feat)))
   # st.write('features with coefficients shrank to zero: {}'.format(np.sum(sel_.coef_ == 0)))
    
   # X_train_selected =df_poly[selected_feat]
    #st.write(X_train_selected)
    

    return X_train_selected
    # op_feat=0
    #op_feat=st.text_input("Enter the optimal number of feature combinations to intake for training models (default 25)")
   
@st.cache(suppress_st_warning=True,allow_output_mutation=True) 
def feature_imp(temp,y):
    rf_imp=RandomForestRegressor()
    rf_imp.fit(temp,y)
    return rf_imp

    
#def poly_feat_numbers(df_poly,rf_imp):
    
#    feat_imp=(rf_imp.feature_importances_)*100
  #  sort_index=np.argsort(-feat_imp)
   # fig=px.bar(x=feat_imp[sort_index[:40]],y=df_poly.columns[sort_index[:40]], orientation='h')#full
    #st.plotly_chart(fig)
    #st.write("Hint : The above bar chart represents the feature importances. On the Y-axis are the top 40 most important features out of",len(df_poly.columns.tolist())," polynomial features and on the X-axis is the weight assigned to each feature. The longer the weight-bar is, the more important that feature is to explain the data. "   )

    #st.subheader("Created polynomial features")
    #op_feat=st.text_input("Enter the optimal number of polynomial features to intake for training models")
    #st.write(" Hint : Machine learning models perform better with increasing number of features, as more features explain the data better. But it should be remembered that providing too many features is also not optimal and may lead to reduced algorithm performance as it promotes overfitting. Another reason for not selecting huge number of features is the algorithm's computational capability. So, choose the right number of features from the above bar chart (neither too low nor too high), be smart and pick up the only those features that are really important")
    #if(op_feat):
     #   num=int(op_feat)
      #  st.write(num)
       # feat_index=sort_index[:num]
        #fig2=px.bar(x=feat_imp[feat_index],y=df_poly.columns[feat_index],orientation='h')
        #st.plotly_chart(fig2)
       # totrain=df_poly.iloc[:,feat_index].copy()
        #st.write(totrain.head())
#    num=int(op_feat)
#    except:
        #num=15
#        print(" ")
        #return df_poly.iloc[:,sort_index[:num]]
     
if __name__ == "__main__":
    main()

