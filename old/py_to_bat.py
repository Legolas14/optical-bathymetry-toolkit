
#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Mar 27 12:14:23 2020

@author: lego
"""

import os,sys

def main():
    print("\n")
    print("\n")
    print("WELCOME TO BATHYMETRY COLOUR TOOLKIT SOFTWARE !! ")
    z=0
    print("\n")
    fon=input("Are you installing or using this software for the first time on this machine?    (Y/N)  ")
    if(fon=='Y'):
        try:
            os.system("pip install virtualenv")
            print("\n")
            vname=input("Please type a name you wish to give for the environment  ")
            create_ven_cmd="virtualenv -p python3 "+str(vname)
            os.system(create_ven_cmd)
            #print(vname+"\Scripts\\activate")
            os.system(vname+"\Scripts\\activate")
            os.system("pip install -r requirements.txt")
            z=1
        except:
            print("Oh-uh something went wrong. Please close this session and try again")
    elif(fon=='N'):
        try:
            z=0
            print("\n")
            vn=input("Please type the name you had given to the virtual env while installing. If you are not sure of the name, check the folder names in the current directory  ")
            os.system(vn+"\Scripts\\activate")
            os.system("pip install -r requirements.txt")
            print("\n")
            start_app()
        except:
            print("Oh-uh something went wrong. Please close this session and try again")        
    if(z==1):
        print("Installed successfully!! ")
        print("\n")
        start_app()

def start_app():
    print(" Please Wait.. ")
    os.system("streamlit run landing.py")
    

if __name__ == "__main__":
    main()







