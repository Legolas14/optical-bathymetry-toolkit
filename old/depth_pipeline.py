#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Mar 27 12:14:23 2020

@author: lego
"""


import streamlit as st
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
import scipy as sc
import plotly_express as px
import plotly
import time
import plotly.graph_objects as go
from sklearn.ensemble import IsolationForest
import random
from sklearn.preprocessing import PolynomialFeatures
from sklearn.pipeline import make_pipeline
from sklearn.model_selection import train_test_split
from xgboost import XGBRegressor
from sklearn.linear_model import LinearRegression
from sklearn.ensemble import RandomForestRegressor
from sklearn.metrics import mean_squared_error as mse
from sklearn.metrics import r2_score
from sklearn.metrics import mean_absolute_error
from sklearn.metrics import mean_poisson_deviance
from sklearn.metrics import max_error
from sklearn.metrics import explained_variance_score
from sklearn.svm import SVR
from sklearn.tree import DecisionTreeRegressor
from sklearn.externals.six import StringIO  
from IPython.display import Image  
from sklearn.tree import export_graphviz                                        
from subprocess import call
import matplotlib.pyplot as plt
from sklearn.inspection import PartialDependenceDisplay
from sklearn.inspection import plot_partial_dependence
from rfpimp import permutation_importances
import shap
import os,sys

st.markdown(
        f"""
<style>
    .reportview-container .main .block-container{{
        max-width: {850}px;
    }}
    .reportview-container .main {{
        color: {'Black'};
        background-color: {'White'};
    }}
</style>
""",
        unsafe_allow_html=True,
    )

@st.cache(persist=True, suppress_st_warning=True)
def load_data(data_path):
    st.write("Loading Data...")
    rangi_01_orig=pd.read_csv(data_path)
    st.write("Done!!")
    return rangi_01_orig

#load_data, preprocessing, viz_before, viz_after

#@st.cache(persist=True, suppress_st_warning=True)
def main():
    st.title("Pipeline Stage : Prepocessing and Data Visualizations")
    global df_data
    data_path=st.text_input("Enter the path of the CSV data: ")#"/home/lego/Desktop/Job/Land River Sea Consulting Ltd./rangitata_01_new.csv
    if(data_path):
        data_path=str(data_path)
        df_data_orig=load_data(data_path)
        
        # Saving the file locally so that at the end I can access the same columns to export
        df_data_orig.to_csv('temp_input_data.csv',index=False)
        
        
        st.subheader("River")
        st.image("rangi.jpeg",width=500)
    
        if st.checkbox("Show Raw Data"):
            st.subheader("Showing raw data -> ")
            st.write(df_data_orig.head())
            st.write("Shape of the data is",df_data_orig.shape)
        
        
        #Multiselect widget
        sel_cols=st.multiselect("Select the Columns in the order of 'ID' , 'Longitude(X_coord)', 'Latitude(Y_coord)', 'Depth' ,'R', 'G', 'B' ,'weight','chainage', and other important features. Then enter the 'Elevation'(if present),'Distance'(if present))",df_data_orig.columns.tolist())
        df_data=df_data_orig[sel_cols]
        st.write('You selected:', sel_cols)  
        #idx=df_data.columns.tolist()[0]
        conf1=st.text_input("Type 'Yes' and press enter to confirm the above as your selected columns")
        if(conf1=='Yes'):
            st.subheader("Showing new data -> ")
            st.write(df_data.head())
           # st.write(df_data.dtypes)
           # st.write(list(sel_cols[4:7]))
            #sns.heatmap(df_data[sel_cols[4:7]].corr())
            #st.pyplot()
            #   st.plotly_chart(display_map(df_data[['R01m22','G01m22','B01m22','SounderDep','North','East']]))
            df_data=df_data.sort_values(by=sel_cols[0])
            df_data=df_data.reset_index().drop(['index'],axis=1)
 
            orig=df_data.copy()
                        
            st.subheader("Shape of the river extracted for calibration dataset : ")
            show_river(orig,sel_cols[1],sel_cols[2])
        
            st.subheader("In the left hand side you can vizualize some plots describing the dataset before it is gonna get preprocessed")
            viz_before(orig)
            data=preprocessing(orig)# works
            
            #st.write(data.head())#works
            if st.checkbox('See Preprocessed Data head to proceed '):
                try:
                    st.subheader("Showing preprocessed data - >")	
                    st.write(data.head())
                except:
                    #print(e)  
                    st.write("No preprocessing done! So showing original data head",df_data.head())
                    data=df_data.copy()
            #works
                #storing x,y for further use
                df_data=df_data.reset_index().drop(['index'],axis=1)
                cor=pd.DataFrame()
                cor[sel_cols[1]]=df_data[sel_cols[1]].copy()
                cor[sel_cols[2]]=df_data[sel_cols[2]].copy()
                cor['Distance_new']=data['Distance_new'].copy()
                cor.to_csv('Coordinates.csv',index=False)            

                st.subheader("In the left hand side you can vizualize some plots describing the dataset after it got preprocessed")
                viz_after(data)
                expo=st.text_input(" Do you wish to export the cleaned dataset as CSV to your local storage? (Yes/No)" )
                if(expo=='Yes'):
                    data.to_csv('Cleaned_file.csv',index=False)
                    st.write("File downloaded!! Please check your local files location to access the Cleaned_file.csv")
                elif(expo=='No'):
                    
                    st.write("You chose not to download")
                data.to_csv("temp_pipeline.csv",index=False)
                ns=st.checkbox(" Check this box to proceed to next stage")
                if(ns):
                    st.subheader(" Now kindly wait as we redirect you to the next stage - Polynomial features creation Stage ")
                    time.sleep(2)
                    os.system('streamlit run pc_test.py')


def show_river(data_new,x,y):
    
    import plotly.graph_objects as go
    layout = go.Layout(
        title=x + " vs " + y,
        xaxis=dict(
            title=x
        ),
        yaxis=dict(
            title=y
        ) )
    fig = go.Figure(layout=layout)
    
    fig.add_trace(go.Scatter(
        x=data_new[x],
        y=data_new[y],
        name="River shape travelled by boat",
        mode='markers',
        marker=dict(size=10,line=dict(color='DarkSlateGrey',width=0.5))
    ))
    
    st.plotly_chart(fig)

def preprocessing(df):
    st.header("Preprocessing Stage - >")
    st.subheader(" If you want to skip this stage, then you can check the box below that says 'See the Prepocessed data head to proceed'")
    #st.write(len(df.columns.tolist()))
    ix_col=df.columns.tolist()[0]
    east=df.columns.tolist()[1]
    north=df.columns.tolist()[2]
    depth=df.columns.tolist()[3]
    r=df.columns.tolist()[4]
    g=df.columns.tolist()[5]
    b=df.columns.tolist()[6]
    if(len(df.columns.tolist())>-2):
        elev=df.columns.tolist()[7]
    if(len(df.columns.tolist())>-1):
        cdist=df.columns.tolist()[8]
        #st.write(cdist)
    
    # Distance calculation
    agg=0
    dist_c=st.text_input(" Does this dataset already have the distance calculated? (Yes/No) Note: It is highly recommended to use distance calculated by the software")
    if(dist_c=='No'):
        df['Distance_new']=0
        for i in range(1,len(df)):
            diff=distance_cal(df[east].iloc[i],df[north].iloc[i],df[east].iloc[i-1],df[north].iloc[i-1])
            agg=agg+diff
            df['Distance_new'].iloc[i]=agg
        st.write("The Distance has been calculated by the software. Please use the 'Distance_new' as your new distance feature")                                
    elif(dist_c=='Yes'):
#        clast=df.columns.tolist()
        df.rename(columns={cdist: "Distance_new"},inplace=True)
        st.write("The default Distance calculation taken into dataset. It has been renamed to Distance_New")                    
    
    st.header("Basic Plots for two variables : ")
    # Selecting variables to visualize
    max_df=pd.DataFrame()
    max_df=df.copy()
    new_df=plot_clean(df,'45','65','1','2','3','6','7','8','9','10','11','12','13','14','15','16','17','18','19','20','21','22')
    loo=st.text_input("Want to preprocess any more variables? (Type 'Yes' or 'No')")
    if(loo=="Yes"):
        new_df_1=plot_clean(new_df,'451','651','11','21','31','61','71','81','91','101','111','121','131','141','151','161','171','181','191','1001','1002','221')
        
        loo1=st.text_input("Want to preprocess any more variables ? (Type 'Yes' or 'No')")
        if(loo1=="Yes"):
            new_df_2=plot_clean(new_df_1,'452','652','12','22','32','62','72','82','92','102','112','122','132','142','152','162','172','182','192','1102','1103','222')
            
            loo2=st.text_input(" Want to preprocess any more variables?  (Type 'Yes' or 'No')")
            if(loo2=="Yes"):
                new_df_3=plot_clean(new_df_2,'453','653','13','23','33','63','73','83','93','103','113','123','133','143','153','163','173','183','193','1203','1202','223')                 
                max_df=new_df_3.copy()
                
                loo3=st.text_input("Want to preprocess any more  variables?  (Type 'Yes' or 'No' )")
                if(loo3=="Yes"):
                    new_df_4=plot_clean(new_df_3,'454','654','14','24','34','64','74','84','94','104','114','124','134','144','154','164','174','184','194','1084','1004','224')  
                    max_df=new_df_4.copy()
                    
                elif(loo3=="No"):
                    max_df=new_df_3.copy()
                    st.write("Preprocessing done!!")
                    return max_df 

            elif(loo2=="No"):
                max_df=new_df_2.copy()
                st.write("Preprocessing done!!")
                return max_df 
            
        elif(loo1=="No"):
            max_df=new_df_1.copy()
            st.write("Preprocessing done!!")
            return max_df 
        
    elif(loo=="No"):
        max_df=new_df.copy()
        st.write("Preprocessing done!!")#works
        #st.write(max_df)#works
       # st.write(" OUT OF IF LOOP")
        return max_df

#@st.cache(suppress_st_warning=True) ..DO NOT USE HERE, AS IT WONT OUTPUT OR RUN AGAIN
def plot_clean(df,v1,v2,v3,v4,v5,v6,v7,v8,v9,v10,v11,v12,v13,v14,v15,v16,v17,v18,v19,v20,v21,v22):
    cols=['None']
    for i in df.columns:
        cols.append(i)    
    var1=st.selectbox("Select the first feature for X axis in scatter plot",cols,key=v1)
    var2=st.selectbox("Select the second feature for Y axis in scatter plot",cols,key=v2)
    to_send=df
    if st.checkbox("Tick this box to show the output plot",key=v3):
        
        st.write("Showing ",var1 ," and ",var2," Lineplot -> ")
        radio_ops=['Lines','Markers','Lines + Markers']
        radio_val=st.radio("Select any one of the following plotting modes",radio_ops,key=v4)
        if(radio_val=='Lines'):
            layout = go.Layout(
                title=var1 + " vs " + var2,
                xaxis=dict(
                    title=var1
                ),
                yaxis=dict(
                    title=var2
                ) ) 
            
            fig = go.Figure(layout=layout)
                    
            fig.add_trace(go.Scatter(x=df[var1], y=df[var2],
                    mode='lines',
                    name='Original data lineplot'))        
            st.plotly_chart(fig)
        if(radio_val=='Markers'):
            layout = go.Layout(
                title=var1 + " vs " + var2,
                xaxis=dict(
                    title=var1
                ),
                yaxis=dict(
                    title=var2
                ) )
            fig = go.Figure(layout=layout)        
            fig.add_trace(go.Scatter(x=df[var1], y=df[var2],
                    mode='markers',
                    name='Original data lineplot'))        
            st.plotly_chart(fig)
        if(radio_val=='Lines + Markers'):
            layout = go.Layout(
                title=var1 + " vs " + var2,
                xaxis=dict(
                    title=var1
                ),
                yaxis=dict(
                    title=var2
                ) )
            fig = go.Figure(layout=layout)      
            fig.add_trace(go.Scatter(x=df[var1], y=df[var2],
                    mode='lines+markers',
                    name='Original data lineplot'))        
            st.plotly_chart(fig)    
#variable selection
        variable_option = st.selectbox("Select appropriate data variable to clean",cols,key=v5)
        st.write('You selected:', variable_option)
        if(variable_option!='None'):
            cl_options=['None','Isolation Forest Algorithm','Manual-Threshold Spatial Algorithm','Moving Average Algorithm','Manual-Threshold Spatial Algorithm + Isolation Forest Algorithm']
            clean_option = st.selectbox("Select appropriate algorithm to clean the choosen Variable",cl_options,key=v6)    
            st.write('You selected:', clean_option)
    #Algo 1       
            if(clean_option=='Manual-Threshold Spatial Algorithm'):
                thres=st.text_input("Enter a threshold difference value between two neighbouring points, after carefully inspecting the graph",key=v7)            
                if(thres):
                    filt=find_outlier_rangi_mth(df,thres,variable_option)
                    yes_in=filt[filt['Outlier_filter']=='Yes'].index
                    print(yes_in)
                    st.write(len(yes_in))
                    drop_data=filt.drop(yes_in,axis=0)
                   
                    #if st.checkbox("Check this box to visualize the Lineplot Graph with same variables as the unfiltered graph, after filtering",key=v8):
                    st.write("Showing ",var1 ," and ",var2," Lineplot -> ")
                    radio_ops=['Lines','Markers','Lines + Markers']
                    radio_val1=st.radio("Select any one of the following plotting modes",radio_ops,key=v9)
                    if(radio_val1=='Lines'):
                        layout = go.Layout(
                            title=var1 + " vs " + var2,
                            xaxis=dict(
                                title=var1
                            ),
                            yaxis=dict(
                                title=var2
                            ) )
                        fig = go.Figure(layout=layout)         
                        fig.add_trace(go.Scatter(x=drop_data[var1], y=drop_data[var2],
                                mode='lines',
                                name='Filtered data lineplot'))        
                        st.plotly_chart(fig)
                    if(radio_val1=='Markers'):
                        layout = go.Layout(
                            title=var1 + " vs " + var2,
                            xaxis=dict(
                                title=var1
                            ),
                            yaxis=dict(
                                title=var2
                            ) )
                        fig = go.Figure(layout=layout)        
                        fig.add_trace(go.Scatter(x=drop_data[var1], y=drop_data[var2],
                                mode='markers',
                                name='Filtered data lineplot'))        
                        st.plotly_chart(fig)
                    if(radio_val1=='Lines + Markers'):
                        layout = go.Layout(
                            title=var1 + " vs " + var2,
                            xaxis=dict(
                                title=var1
                            ),
                            yaxis=dict(
                                title=var2
                            ) )
                        fig = go.Figure(layout=layout)        
                        fig.add_trace(go.Scatter(x=drop_data[var1], y=drop_data[var2],
                                mode='lines+markers',
                                name='Filtered data lineplot'))        
                        st.plotly_chart(fig)
                    
                    conf2=st.text_input("Type 'Yes' and press enter to confirm the changes if the graph looks noise-free. Type 'No' to continue or go back and change some values",key=v10)    
                    if(conf2=='Yes'):
                        df=drop_data
                        st.write("Changes made to original data!")
                       # look=st.text_input("Want to plot graphs for any more variables?")    
                        #if(look=='Yes'):
                        to_send=df
    
       #Algo 2      
            if(clean_option=='Isolation Forest Algorithm'):
                r=df.columns.tolist()[4]
                g=df.columns.tolist()[5]
                b=df.columns.tolist()[6]            
                filt=find_outlier_rangi_iso(df,0.10,variable_option,r,g,b)
                yes_in=filt[filt['ISO']==-1].index
                drop_data=filt.drop(yes_in,axis=0)
                
               # if st.checkbox("Check this box to visualize the Lineplot Graph with same variables as the unfiltered graph, after filtering",key=v11):   
                st.write("Showing ",var1 ," and ",var2," Lineplot -> ")
                radio_ops=['Lines','Markers','Lines + Markers']
                radio_val1=st.radio("Select any one of the following plotting modes",radio_ops,key=v12)
                if(radio_val1=='Lines'):
                    layout = go.Layout(
                            title=var1 + " vs " + var2,
                            xaxis=dict(
                                title=var1
                            ),
                            yaxis=dict(
                                title=var2
                            ) )
                    fig = go.Figure(layout=layout)       
                    fig.add_trace(go.Scatter(x=drop_data[var1], y=drop_data[var2],
                            mode='lines',
                            name='Filtered data lineplot'))        
                    st.plotly_chart(fig)
                if(radio_val1=='Markers'):
                    layout = go.Layout(
                            title=var1 + " vs " + var2,
                            xaxis=dict(
                                title=var1
                            ),
                            yaxis=dict(
                                title=var2
                            ) )
                    fig = go.Figure(layout=layout)        
                    fig.add_trace(go.Scatter(x=drop_data[var1], y=drop_data[var2],
                            mode='markers',
                            name='Filtered data lineplot'))        
                    st.plotly_chart(fig)
                if(radio_val1=='Lines + Markers'):
                    layout = go.Layout(
                            title=var1 + " vs " + var2,
                            xaxis=dict(
                                title=var1
                            ),
                            yaxis=dict(
                                title=var2
                            ) )
                    fig = go.Figure(layout=layout)        
                    fig.add_trace(go.Scatter(x=drop_data[var1], y=drop_data[var2],
                            mode='lines+markers',
                            name='Filtered data lineplot'))        
                    st.plotly_chart(fig)
                
                conf2=st.text_input("Type 'Yes' to confirm the changes if the graph looks noise-free. Type 'No' to continue or go back and change some values",key=v13)    
                if(conf2=='Yes'):
                    df=drop_data
                    st.write("Changes made to original data!")
                    #look=st.text_input("Want to plot graphs for any more variables?")    
                    #if(look=='Yes'):
                    to_send=df

#Algo 3
            #@st.cache(persist=True, suppress_st_warning=True)        
            if(clean_option=='Moving Average Algorithm'):                
               # thresh_dist=st.slider("Use this Slider to set an appropriate river Distance",100,1000,key=v20)
                thresh_dist=st.text_input("Enter an appropriate river Distance (ex:500), and press enter",key=v8)#0.3
                thresh_diff=st.text_input("Enter the appropriate theshold difference after inspecting the graph and press enter",key=v21)
                if(thresh_diff):
                    thresh_dist=float(thresh_dist)
                    drop_data=find_outlier_rangi_ma(df,thresh_dist,thresh_diff,variable_option)
                    #if st.checkbox("Check this box to visualize the Lineplot Graph with same variables as the unfiltered graph, after filtering",key=v19):
                    st.write("Showing ",var1 ," and ",var2," Lineplot -> ")
                    radio_ops=['Lines','Markers','Lines + Markers']
                    radio_val1=st.radio("Select any one of the following plotting modes",radio_ops,key=v18)
                    if(radio_val1=='Lines'):
                        layout = go.Layout(
                            title=var1 + " vs " + var2,
                            xaxis=dict(
                                title=var1
                            ),
                            yaxis=dict(
                                title=var2
                            ) )
                        fig = go.Figure(layout=layout)        
                        fig.add_trace(go.Scatter(x=drop_data[var1], y=drop_data[var2],
                                    mode='lines',
                                    name='Filtered data lineplot'))        
                        st.plotly_chart(fig)
                    if(radio_val1=='Markers'):
                        layout = go.Layout(
                            title=var1 + " vs " + var2,
                            xaxis=dict(
                                title=var1
                            ),
                            yaxis=dict(
                                title=var2
                            ) )
                        fig = go.Figure(layout=layout)        
                        fig.add_trace(go.Scatter(x=drop_data[var1], y=drop_data[var2],
                                    mode='markers',
                                    name='Filtered data lineplot'))        
                        st.plotly_chart(fig)
                    if(radio_val1=='Lines + Markers'):
                        layout = go.Layout(
                            title=var1 + " vs " + var2,
                            xaxis=dict(
                                title=var1
                            ),
                            yaxis=dict(
                                title=var2
                            ) )
                        fig = go.Figure(layout=layout)        
                        fig.add_trace(go.Scatter(x=drop_data[var1], y=drop_data[var2],
                                    mode='lines+markers',
                                    name='Filtered data lineplot'))        
                        st.plotly_chart(fig)

                    conf6=st.text_input("Type 'Yes' to confirm the changes if the graph looks noise-free. Type 'No' to continue or go back and change some values",key=v22)    
                    if(conf6=='Yes'):
                        df=drop_data
                        st.write("Changes made to original data!")
                    #look=st.text_input("Want to plot graphs for any more variables?")    
                    #if(look=='Yes'):
                    to_send=df        
    #Algo 4 
            if(clean_option=='Manual-Threshold Spatial Algorithm + Isolation Forest Algorithm'):
                
                thres=st.text_input("Enter a threshold difference value between two neighbouring points, after carefully inspecting the graph",key=v14)            
                if(thres):
                    r=df.columns.tolist()[4]
                    g=df.columns.tolist()[5]
                    b=df.columns.tolist()[6]               
                    filt=find_outlier_rangi_mth(df,thres,variable_option)
                    yes_in=filt[filt['Outlier_filter']=='Yes'].index
                    drop_data=filt.drop(yes_in,axis=0)
                    st.write(len(yes_in))
                   
                    filt1=find_outlier_rangi_iso(drop_data,0.15,variable_option,r,g,b)
                    yes_in1=filt1[filt1['ISO']==-1].index
                    drop_data1=filt1.drop(yes_in1,axis=0)
                    st.write(len(yes_in1))
                    
                    if st.checkbox("Check this box to visualize the Lineplot Graph with same variables as the unfiltered graph, after filtering",key=v15):
                        st.write("Showing ",var1 ," and ",var2," Lineplot -> ")
                        radio_ops=['Lines','Markers','Lines + Markers']
                        radio_val1=st.radio("Select any one of the following plotting modes",radio_ops,key=v16)
                        if(radio_val1=='Lines'):
                            layout = go.Layout(
                            title=var1 + " vs " + var2,
                            xaxis=dict(
                                title=var1
                            ),
                            yaxis=dict(
                                title=var2
                            ) )
                            fig = go.Figure(layout=layout)        
                            fig.add_trace(go.Scatter(x=drop_data1[var1], y=drop_data1[var2],
                                    mode='lines',
                                    name='Filtered data lineplot'))        
                            st.plotly_chart(fig)
                        if(radio_val1=='Markers'):
                            layout = go.Layout(
                            title=var1 + " vs " + var2,
                            xaxis=dict(
                                title=var1
                            ),
                            yaxis=dict(
                                title=var2
                            ) )
                            fig = go.Figure(layout=layout)        
                            fig.add_trace(go.Scatter(x=drop_data1[var1], y=drop_data1[var2],
                                    mode='markers',
                                    name='Filtered data lineplot'))        
                            st.plotly_chart(fig)
                        if(radio_val1=='Lines + Markers'):
                            layout = go.Layout(
                            title=var1 + " vs " + var2,
                            xaxis=dict(
                                title=var1
                            ),
                            yaxis=dict(
                                title=var2
                            ) )
                            fig = go.Figure(layout=layout)        
                            fig.add_trace(go.Scatter(x=drop_data1[var1], y=drop_data1[var2],
                                    mode='lines+markers',
                                    name='Filtered data lineplot'))        
                            st.plotly_chart(fig)
                        
                    conf2=st.text_input("Type 'Yes' to confirm the changes if the graph looks noise-free. Type 'No' to continue or go back and change some values",key=v17)    
                    if(conf2=='Yes'):
                        df=drop_data1
                        st.write("Changes made to original data!")
                        #look=st.text_input("Want to plot graphs for any more variables?")    
                        to_send=df                
           # st.write(to_send.head())#works
    return to_send

@st.cache(suppress_st_warning=True,allow_output_mutation=True)
def find_outlier_rangi_ma(rangi_01,thresh_dist,thresh_diff,variable_option):
    outliers=[]
    clus=max(rangi_01['Distance_new'])/thresh_dist
    prev_dist=0
    for l in range(0,int(clus)):
        print("Cluster",l)
        if(l==0):
            start_ind=0
            end_ind=rangi_01[rangi_01['Distance_new']>thresh_dist].index[0]
            while(start_ind==end_ind):
                new_dist=prev_dist+thresh_dist
                end_ind=rangi_01[rangi_01['Distance_new']>new_dist].index[0]
                prev_dist=new_dist
            for i in range(start_ind,end_ind):
                dist_current=rangi_01.iloc[i]['Distance_new']
                gr_dc_in=rangi_01[rangi_01['Distance_new']>(dist_current+thresh_dist)].index[0]
                for_avg=np.mean(rangi_01[variable_option].iloc[i:gr_dc_in])
                if(abs(rangi_01.iloc[i][variable_option]-for_avg)>float(thresh_diff)):
                    outliers.append(i) 
                    
        elif(l>0 and l<int(clus)-1):
            if(prev_dist+thresh_dist>rangi_01['Distance_new'].iloc[-1]):
                end_ind=len(rangi_01)-1
            else:      
                end_ind=rangi_01[rangi_01['Distance_new']>prev_dist+thresh_dist].index[0]
        
            while(start_ind==end_ind):
                new_dist=prev_dist+thresh_dist
                if(new_dist>rangi_01['Distance_new'].iloc[-1]):
                    end_ind=len(rangi_01)-1
                else:
                    end_ind=rangi_01[rangi_01['Distance_new']>new_dist].index[0]
                prev_dist=new_dist
            if(end_ind==len(rangi_01)-1):
                l=int(clus)-2
            else:
                for i in range(start_ind,end_ind):
                    dist_current=rangi_01.iloc[i]['Distance_new']
                    gr_dc_in=rangi_01[rangi_01['Distance_new']>(dist_current+thresh_dist)].index[0]
                    for_avg=np.mean(rangi_01[variable_option].iloc[i:gr_dc_in])  
                    rev_avg_pts=[]
                    rev_dc_in=rangi_01[rangi_01['Distance_new']<(abs(dist_current-thresh_dist))].index[-1]
                    for j in range(rev_dc_in,i):
                        h=j
                        while(h in outliers):
                            h=h-1  
                        rev_avg_pts.append(rangi_01[variable_option].iloc[h])
                    rev_avg=np.mean(rev_avg_pts)
                    if((abs(rangi_01.iloc[i][variable_option]-for_avg)>float(thresh_diff))|(abs(rangi_01.iloc[i][variable_option]-rev_avg)>float(thresh_diff))):
                        outliers.append(i) 

        elif(l==int(clus)-1): 
           end_ind=len(rangi_01)-1
           for i in range(start_ind,end_ind):
         #   print("Index",i)
             dist_current=rangi_01.iloc[i]['Distance_new']
             rev_avg_pts=[]
             rev_dc_in=rangi_01[rangi_01['Distance_new']<(abs(dist_current-thresh_dist))].index[-1]
             for j in range(rev_dc_in,i):
               h=j
               while(h in outliers):
                 h=h-1  
               rev_avg_pts.append(rangi_01[variable_option].iloc[h])
             rev_avg=np.mean(rev_avg_pts)  
             if((abs(rangi_01.iloc[i][variable_option]-for_avg)>float(thresh_diff))|(abs(rangi_01.iloc[i][variable_option]-rev_avg)>float(thresh_diff))):
               outliers.append(i)
        
        prev_dist=rangi_01.iloc[i]['Distance_new']
        print(prev_dist)
        start_ind=end_ind                    
        
    st.write("Detected number of outliers :",len(outliers))    
    drop_data=rangi_01.drop(outliers,axis=0)    
    drop_data=drop_data.reset_index().drop(['index'],axis=1)    
    
    return drop_data                    
#@st.cache(suppress_st_warning=True,allow_output_mutation=True) 
def find_outlier_rangi_iso(sample,fraction,variable_option,r,g,b):
    
    lof_data=np.array(sample[[r,g,b,str(variable_option)]])
    clf=IsolationForest(contamination=fraction,n_estimators=1000)
    sample['ISO']=clf.fit_predict(lof_data)
    return sample

#@st.cache(suppress_st_warning=True,allow_output_mutation=True)
def find_outlier_rangi_mth(sample,threshold,variable_option):
    outlier_index=[]
    sample['Outlier_filter']=''
    for i in range(1,len(sample)):  
      h=i-1#0.065
#    j=i+1
      while(h in outlier_index):
          h=h-1
      neg_diff=abs(sample[str(variable_option)].iloc[i]-sample[str(variable_option)].iloc[h])
      if(neg_diff>float(threshold)):
          sample['Outlier_filter'].iloc[i]='Yes'
          outlier_index.append(i)
      else:
          sample['Outlier_filter'].iloc[i]='No'
    st.write("Detected number of outliers :",len(outlier_index))      
    return sample
            
def viz_after(df):
    dataviz_choice=st.sidebar.selectbox("Choose data Vizualization after preprocessing steps",['None','Correlation Map','Skewness Distribution Graph'],key='7997')
    if dataviz_choice=='Correlation Map':
        st.subheader("Corr Heat Map")
        cof=[]
        int_feat=df.dtypes[df.dtypes=='int64'].index
        float_feat=df.dtypes[df.dtypes=='float64'].index
        for i in int_feat:
            cof.append(i)
        for j in float_feat:
            cof.append(j)
        fig, ax = plt.subplots(figsize=(5,5))
        sns.heatmap(df[cof].corr())
        st.pyplot(plt)
        st.subheader("Correlation analysis is a statistical method used to evaluate the strength of relationship between two quantitative variables. A high correlation means that two or more variables have a strong relationship with each other, while a weak correlation means that the variables are hardly related. If the color is lighter, the two variables are positively correlated and if the color darker, the two variables are negatively correlated. The intensity of the color represents how high or low they are correlated.")
    if dataviz_choice=='Skewness Distribution Graph':
        cof=[]
        int_feat=df.dtypes[df.dtypes=='int64'].index
        float_feat=df.dtypes[df.dtypes=='float64'].index
        for i in int_feat:
            cof.append(i)
        for j in float_feat:
            cof.append(j)
        st.subheader("Skewness Distribution Graph")
        var_choice=st.radio("Choose variable",cof,key='646')
        sns.distplot(df[var_choice])
        st.pyplot()       
        st.subheader(" The above skewness graph explains how much skewed the variable is. Skewness refers to how much 'one-sided' the graph is. Say, if the graph has a longer tail on its right its called Right-skewed data, and if it has a longer tail on its left, its called a Left-Skewed data. Neither of them is good for our dataset, meaning, the graph should look more centered. One way to reduce skewness is to normalize our dataset. This can be done by guassian normalization, log-normalization, or by removing outliers/noises by some method.")
        from scipy.stats import skew
        sk_val=skew(df[var_choice])
        #st.write(sk_val)
        if((sk_val>1)|(sk_val<-1)):
            st.write("Looking at the graph I say the variable ",var_choice," is Highly Skewed")
        elif((sk_val<0.5 and sk_val>-1) | (sk_val>0.5 and sk_val<1)):
            st.write("Looking at the graph I say the variable ",var_choice," is Moderately Skewed")


def viz_before(df):
    dataviz_choice=st.sidebar.selectbox("Choose data Vizualization before preprocessing steps",['None','Correlation Map','Skewness Distribution Graph'],key='7996')
    if dataviz_choice=='Correlation Map':
        st.subheader("Corr Heat Map")
        cof=[]
        int_feat=df.dtypes[df.dtypes=='int64'].index
        float_feat=df.dtypes[df.dtypes=='float64'].index
        for i in int_feat:
            cof.append(i)
        for j in float_feat:
            cof.append(j)
        fig, ax = plt.subplots(figsize=(10,10))
        #sns.heatmap(df[int_feat].corr())
        sns.heatmap(df[cof].corr())
        st.pyplot(plt)
        st.subheader("Correlation analysis is a statistical method used to evaluate the strength of relationship between two quantitative variables. A high correlation means that two or more variables have a strong relationship with each other, while a weak correlation means that the variables are hardly related. If the color is lighter, the two variables are positively correlated and if the color darker, the two variables are negatively correlated. The intensity of the color represents how high or low they are correlated.")
    if dataviz_choice=='Skewness Distribution Graph':
        cof=[]
        int_feat=df.dtypes[df.dtypes=='int64'].index
        float_feat=df.dtypes[df.dtypes=='float64'].index
        for i in int_feat:
            cof.append(i)
        for j in float_feat:
            cof.append(j)
        st.subheader("Skewness Distribution Graph")
        var_choice=st.radio("Choose variable",cof,key='646')
        sns.distplot(df[var_choice])
        st.pyplot()       
        st.subheader(" The above skewness graph explains how much skewed the variable is. Skewness refers to how much 'one-sided' the graph is. Say, if the graph has a longer tail on its right its called Right-skewed data, and if it has a longer tail on its left, its called a Left-Skewed data. Neither of them is good for our dataset, meaning, the graph should look more centered. One way to reduce skewness is to normalize our dataset. This can be done by guassian normalization, log-normalization, or by removing outliers/noises by some method.")
        from scipy.stats import skew
        sk_val=skew(df[var_choice])
        if((sk_val>1)|(sk_val<-1)):
            st.write("Looking at the graph I say the variable ",var_choice," is Highly Skewed. Skewness value = ",sk_val)
        elif((sk_val<0.5 and sk_val>-1) | ((sk_val>0.5 and sk_val<1))):
            st.write("Looking at the graph I say the variable ",var_choice," is Moderately Skewed. Skewness value = ",sk_val)            

def distance_cal(x1,y1,x2,y2):
   return np.sqrt(((x2-x1)**2)+((y2-y1)**2)) 

global df

       
if __name__ == "__main__":
    main()







