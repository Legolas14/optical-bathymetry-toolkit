#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Mar 27 12:14:28 2020

@author: lego
"""

import streamlit as st
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
import scipy as sc
import plotly_express as px
import plotly
import time
import plotly.graph_objects as go
from sklearn.ensemble import IsolationForest
import random
from sklearn.preprocessing import PolynomialFeatures
from sklearn.pipeline import make_pipeline
from sklearn.model_selection import train_test_split
from xgboost import XGBRegressor
from sklearn.linear_model import LinearRegression
from sklearn.ensemble import RandomForestRegressor
from sklearn.metrics import mean_squared_error as mse
from sklearn.metrics import r2_score
from sklearn.metrics import mean_absolute_error
from sklearn.metrics import mean_poisson_deviance
from sklearn.metrics import max_error
from sklearn.metrics import explained_variance_score
from sklearn.svm import SVR
from sklearn.tree import DecisionTreeRegressor
from sklearn.externals.six import StringIO  
from IPython.display import Image  
from sklearn.tree import export_graphviz                                        
from subprocess import call
import matplotlib.pyplot as plt
from sklearn.inspection import PartialDependenceDisplay
from sklearn.inspection import plot_partial_dependence
from rfpimp import permutation_importances
import shap
import os,sys
import joblib
#import keras
#import tensorflow as tf
from sklearn.neural_network import MLPRegressor
from streamlit import caching


@st.cache(persist=True, suppress_st_warning=True)
def load_data(data_path):  
    rangi_01_orig=pd.read_csv(data_path)
    return rangi_01_orig

def local_css(file_name):
    with open(file_name) as f:
        st.markdown(f'<style>{f.read()}</style>', unsafe_allow_html=True)

def main():
    st.title("Bathymetry from Colour Toolkit")
    local_css("style.css")
    st.image("LRS_logo.jpeg",width=300)
    st.write("PLEASE WAIT WHILE WE SET UP PIPELINE ON YOUR PLATFORM :",sys.platform,"....")
    my_bar = st.progress(0)
    for percent_complete in range(100):
        time.sleep(0.0009)
        my_bar.progress(percent_complete + 1)
    caching.clear_cache()   
    b1 = st.button("Full Workflow ")
    if(b1):
        st.write("Please wait while we gear up the pipeline. If you wish to select any other option, please refresh the page to do that")
        os.system("streamlit run depth_pipeline.py")

    b2 = st.button("Simplified Workflow")
    if(b2):
        st.write("Please wait while we gear up the pipeline. If you wish to select any other option, please refresh the page to do that")
        os.system("streamlit run simplified_workflow.py")
        
    b3= st.button("Apply Pre-trained model to New Dataset")
    if(b3):
        st.write("Please wait while we gear up the pipeline. If you wish to select any other option, please refresh the page to do that")
        os.system("streamlit run ml_stage_pre.py")
    
    b4= st.button("Apply smoothing algorithm to raw predictions")
    if(b4):
        st.write("Please wait while we gear up the pipeline. If you wish to select any other option, please refresh the page to do that")
        os.system("streamlit run smoothing_pred.py")
    
    b5 = st.button("River Trend Visualizations")
    if(b5):
        st.write("Please wait while we gear up the pipeline. If you wish to select any other option, please refresh the page to do that")
        os.system("streamlit run visuals.py")    
        
        
if __name__ == "__main__":
    main()

