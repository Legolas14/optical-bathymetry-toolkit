#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jun 10 13:05:17 2020

@author: lego
"""



import streamlit as st
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
import scipy as sc
import plotly_express as px
import plotly
import time
import plotly.graph_objects as go
from sklearn.ensemble import IsolationForest
import random
from sklearn.preprocessing import PolynomialFeatures
from sklearn.pipeline import make_pipeline
from sklearn.model_selection import train_test_split
from xgboost import XGBRegressor
from sklearn.linear_model import LinearRegression
from sklearn.ensemble import RandomForestRegressor
from sklearn.metrics import mean_squared_error as mse
from sklearn.metrics import r2_score
from sklearn.metrics import mean_absolute_error
from sklearn.metrics import mean_poisson_deviance
from sklearn.metrics import max_error
from sklearn.metrics import explained_variance_score
from sklearn.svm import SVR
from sklearn.tree import DecisionTreeRegressor
from sklearn.externals.six import StringIO  
from IPython.display import Image  
from sklearn.tree import export_graphviz                                        
from subprocess import call
import matplotlib.pyplot as plt
from sklearn.inspection import PartialDependenceDisplay
from sklearn.inspection import plot_partial_dependence
from rfpimp import permutation_importances
import shap
import os,sys

st.markdown(
        f"""
<style>
    .reportview-container .main .block-container{{
        max-width: {850}px;
    }}
    .reportview-container .main {{
        color: {'Black'};
        background-color: {'White'};
    }}
</style>
""",
        unsafe_allow_html=True,
    )


@st.cache(persist=True, suppress_st_warning=True)
def load_data(data_path):
    st.write("Loading Data...")
    rangi_01_orig=pd.read_csv(data_path)
    #st.write("Done!!")
    return rangi_01_orig


def distance_cal(x1,y1,x2,y2):
   return np.sqrt(((x2-x1)**2)+((y2-y1)**2)) 

def main():
    st.title(" River trend Visualizations")
    global df_data
    data_path=st.text_input("Enter the path of the CSV data: ")#"/home/lego/Desktop/Job/Land River Sea Consulting Ltd./rangitata_01_new.csv
    if(data_path):
        data_path=str(data_path)
        df_data=load_data(data_path)
        if st.checkbox("Show Raw Data"):
            st.subheader("Showing raw data -> ")
            st.write(df_data.head())
            st.write("Shape of the data is",df_data.shape)
        
        sel_cols=st.multiselect("Select the Columns in the order of 'ID' ,'R', 'G', 'B' ",df_data.columns.tolist())
        pro=st.checkbox(' Check this box after selecting appropriate columns ')
        if(pro):
            df_data=df_data.sort_values(by=sel_cols[0])
            df_data=df_data.reset_index().drop(['index'],axis=1)
            st.write('You selected:', sel_cols)
            #calculate all needed values (ln, r/g etc)
            r=sel_cols[1]
            g=sel_cols[2]
            b=sel_cols[3]
            
            df_data=calc_perm(df_data,r,g,b)
            
            st.subheader(" New columns are added , please use the below options to visualize different variables ")
            
            #with group or no group filter
            gng=st.text_input(" Type 'Yes' if you want to visualize by filtering different groups (Note : You must have a column that uniquely identifies every row as different groups/chunks). Type 'No' to continue without group-based filtering ")
            
            if(gng=='No'):
                cols=[]
                for i in df_data.columns:
                    cols.append(i) 
                cols.insert(0,'None')    
                df=df_data.copy()  
                var1=st.selectbox("Select the first feature for X axis in scatter plot",cols,key='11')
                var2=st.selectbox("Select the second feature for Y axis in scatter plot",cols,key='42')
                if st.checkbox("Tick this box to show the output plot"):        
                    st.write("Showing ",var1 ," and ",var2," Lineplot -> ")
                    radio_ops=['Markers','Lines','Lines + Markers']
                    radio_val=st.radio("Select any one of the following plotting modes",radio_ops)
                    if(radio_val=='Lines'):
                        layout = go.Layout(
                            title=var1 + " vs " + var2,
                            xaxis=dict(
                                title=var1
                            ),
                            yaxis=dict(
                                title=var2
                            ) ) 
                        
                        fig = go.Figure(layout=layout)
                                
                        fig.add_trace(go.Scatter(x=df[var1], y=df[var2],
                                mode='lines',
                                name='Original data lineplot'))        
                        st.plotly_chart(fig)
                    if(radio_val=='Markers'):
                        layout = go.Layout(
                            title=var1 + " vs " + var2,
                            xaxis=dict(
                                title=var1
                            ),
                            yaxis=dict(
                                title=var2
                            ) )
                        fig = go.Figure(layout=layout)        
                        fig.add_trace(go.Scatter(x=df[var1], y=df[var2],
                                mode='markers',
                                name='Original data lineplot'))        
                        st.plotly_chart(fig)
                    if(radio_val=='Lines + Markers'):
                        layout = go.Layout(
                            title=var1 + " vs " + var2,
                            xaxis=dict(
                                title=var1
                            ),
                            yaxis=dict(
                                title=var2
                            ) )
                        fig = go.Figure(layout=layout)      
                        fig.add_trace(go.Scatter(x=df[var1], y=df[var2],
                                mode='lines+markers',
                                name='Original data lineplot'))        
                        st.plotly_chart(fig)    

            if(gng=='Yes'):
                cols=[]
                for i in df_data.columns:
                    cols.append(i)
                cols.insert(0,'None')    
                grp=st.selectbox(' Select the Grouping feature ',cols)
                ppx=st.checkbox(" Check to proceed ")
                if(ppx):
                    st.write('You selected:', grp)
                    uq=df_data[grp].unique()
                    st.write(" The unique groups present are : ",uq)
                    uq=list(uq)
                    uq.insert(0,'All of them')
                    grs=st.multiselect(" Select the individual or combinations of Groups to visualize the calibration dataset ",uq)
                    pox=0
                    pox=st.checkbox(" Check this box after selecting groups ")
                    if(pox):
                        
                        if(grs[0]=="All of them"):
                            #st.write(grs)
                            gre=df_data[grp].unique()
                            te=pd.DataFrame()
                            for i in gre:
                                #st.write(i)
                                nte=pd.DataFrame()
                                nte=df_data[df_data[grp]==i].copy()
                                nte=nte.reset_index().drop(['index'],axis=1)
                                te=pd.concat([te,nte],axis=0)
                                te=te.reset_index().drop(['index'],axis=1) 
                            
                            te=te.sort_values(by=sel_cols[0])
                            te=te.reset_index().drop(['index'],axis=1)
                            del nte
                            df=te.copy()  
                            #st.write(df.head())
                                                        
                        else:
                            te=pd.DataFrame()
                            sls=list(grs)
                            #st.write(sls)
                            for i in sls:
                                nte=pd.DataFrame()
                                nte=df_data[df_data[grp]==i].copy()
                                nte=nte.reset_index().drop(['index'],axis=1)
                                te=pd.concat([te,nte],axis=0)
                                te=te.reset_index().drop(['index'],axis=1) 
                            
                            te=te.sort_values(by=sel_cols[0])
                            te=te.reset_index().drop(['index'],axis=1)
                            del nte
                            df=te.copy()  
                       
                        #st.write(df.head())
                        var1=st.selectbox("Select the first feature for X axis in scatter plot",cols,key='121')
                        var2=st.selectbox("Select the second feature for Y axis in scatter plot",cols,key='422')
                        if st.checkbox("Tick this box to show the output plot"):        
                            st.write("Showing ",var1 ," and ",var2," Lineplot -> ")
                            radio_ops=['Markers','Lines','Lines + Markers']
                            radio_val=st.radio("Select any one of the following plotting modes",radio_ops)
                            if(radio_val=='Lines'):
                                layout = go.Layout(
                                    title=var1 + " vs " + var2,
                                    xaxis=dict(
                                        title=var1
                                    ),
                                    yaxis=dict(
                                        title=var2
                                    ) ) 
                                
                                fig = go.Figure(layout=layout)
                                        
                                fig.add_trace(go.Scatter(x=df[var1], y=df[var2],
                                        mode='lines',
                                        name='Original data lineplot'))        
                                st.plotly_chart(fig)
                            if(radio_val=='Markers'):
                                layout = go.Layout(
                                    title=var1 + " vs " + var2,
                                    xaxis=dict(
                                        title=var1
                                    ),
                                    yaxis=dict(
                                        title=var2
                                    ) )
                                fig = go.Figure(layout=layout)        
                                fig.add_trace(go.Scatter(x=df[var1], y=df[var2],
                                        mode='markers',
                                        name='Original data lineplot'))        
                                st.plotly_chart(fig)
                            if(radio_val=='Lines + Markers'):
                                layout = go.Layout(
                                    title=var1 + " vs " + var2,
                                    xaxis=dict(
                                        title=var1
                                    ),
                                    yaxis=dict(
                                        title=var2
                                    ) )
                                fig = go.Figure(layout=layout)      
                                fig.add_trace(go.Scatter(x=df[var1], y=df[var2],
                                        mode='lines+markers',
                                        name='Original data lineplot'))        
                                st.plotly_chart(fig)
                
            
 
@st.cache(persist=True, suppress_st_warning=True,allow_output_mutation=True)       
def calc_perm(df_data,r,g,b):
    df_data['r/g']=0
    df_data['r/b']=0
    df_data['g/r']=0
    df_data['g/b']=0
    df_data['b/g']=0
    df_data['b/r']=0
    
    df_data['ln(r/g)']=0
    df_data['ln(r/b)']=0
    df_data['ln(g/r)']=0
    df_data['ln(g/b)']=0
    df_data['ln(b/g)']=0
    df_data['ln(b/r)']=0
    
    df_data['r/g'] = df_data.apply(lambda row: (row[r]/row[g]), axis=1)
    df_data['r/b'] = df_data.apply(lambda row: (row[r]/row[b]), axis=1)            
    df_data['g/r'] = df_data.apply(lambda row: (row[g]/row[r]), axis=1)
    df_data['g/b'] = df_data.apply(lambda row: (row[g]/row[b]), axis=1)            
    df_data['b/g'] = df_data.apply(lambda row: (row[b]/row[g]), axis=1)
    df_data['b/r'] = df_data.apply(lambda row: (row[b]/row[r]), axis=1)
    
    df_data['ln(r/g)'] = df_data.apply(lambda row: np.log10((row[r]/row[g])), axis=1)
    df_data['ln(r/b)'] = df_data.apply(lambda row: np.log10((row[r]/row[b])), axis=1)            
    df_data['ln(g/r)'] = df_data.apply(lambda row: np.log10((row[g]/row[r])), axis=1)
    df_data['ln(g/b)'] = df_data.apply(lambda row: np.log10((row[g]/row[b])), axis=1)            
    df_data['ln(b/g)'] = df_data.apply(lambda row: np.log10((row[b]/row[g])), axis=1)            
    df_data['ln(b/r)'] = df_data.apply(lambda row: np.log10((row[b]/row[r])), axis=1)        
    
    return df_data

global df

       
if __name__ == "__main__":
    main()









