#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Mar 27 12:14:28 2020

@author: lego
"""

import streamlit as st
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
import scipy as sc
import plotly_express as px
import plotly
import time
import plotly.graph_objects as go
from sklearn.ensemble import IsolationForest
import random
from sklearn.preprocessing import PolynomialFeatures
from sklearn.pipeline import make_pipeline
from sklearn.model_selection import train_test_split
from xgboost import XGBRegressor
from sklearn.linear_model import LinearRegression
from sklearn.ensemble import RandomForestRegressor
from sklearn.metrics import mean_squared_error as mse
from sklearn.metrics import r2_score
from sklearn.metrics import mean_absolute_error
from sklearn.metrics import mean_poisson_deviance
from sklearn.metrics import max_error
from sklearn.metrics import explained_variance_score
from sklearn.svm import SVR
from sklearn.tree import DecisionTreeRegressor
from sklearn.externals.six import StringIO  
from IPython.display import Image  
from sklearn.tree import export_graphviz                                        
from subprocess import call
import matplotlib.pyplot as plt
from sklearn.inspection import PartialDependenceDisplay
from sklearn.inspection import plot_partial_dependence
from rfpimp import permutation_importances
import shap
import os,sys
import joblib
#import keras
#import tensorflow as tf
from sklearn.neural_network import MLPRegressor


@st.cache(persist=True, suppress_st_warning=True)
def load_data(data_path):  
    rangi_01_orig=pd.read_csv(data_path)
    return rangi_01_orig


def main():    
    st.title("Pre-trained model apply stage")
    cof=st.checkbox("Please tick this box to confirm that you have the model pickel file(.pkl), the files which were stored temporarily during the training of that model and the new dataset, all in the same folder along with these python scripts")
    if(cof):
        fn_loc=st.text_input("Enter the name of the folder(try giving the full path if name doesn't work) containing the Pre-trained model and the required files. If the files are in the same directory as the python scripts are, then just type '.' ")
        if(fn_loc):
            st.write("Loading all the required files...")
            fn_loc=fn_loc+'/'
            st.write(fn_loc)
            totrain=load_data(fn_loc+'rf_x.csv')
            x_y=load_data(fn_loc+'y.csv')
            y=x_y[x_y.columns.tolist()[-1]].copy()
            #st.write(y.head())
            data=load_data(fn_loc+'temp_pipeline.csv')
            #data=load_data('Cleaned_file.csv')    
            # for lr,svr
            sub_x=load_data(fn_loc+'sub_x.csv')
            x_train2,x_test2,y_train2,y_test2=tts(sub_x,y)
            
            # for rf,xgb
            rf_x=load_data(fn_loc+'rf_x.csv')
            x_train,x_test,y_train,y_test=tts(rf_x,y)
            #st.write(rf_x.head())
            # for nn
            nn_x=load_data(fn_loc+'nn_x.csv')
            x_train3,x_test3,y_train3,y_test3=tts(nn_x,y)   
                    
            pmo=st.text_input("Please input the name of the Pre-trained model present within the above folder - along with the .(pkl/h5/w5) extension",key='538')
            #time.sleep(1)
            if(pmo):
                pmo=fn_loc+pmo
                model=joblib.load(pmo)
                predictions_all=pd.DataFrame(y)
                col0=predictions_all.columns.tolist()[0]
                var1='None'
                var1=st.selectbox("Select the parent Algorithm to which this model belongs to (This is important because each model has a different feature engineering method)",["None","Linear Regression","Random Forest Regressor","XGBoost Regressor","Neural Network","Support Vector Regressor"],key=89)
                
                if(var1=='Linear Regression'):
                    st.subheader("Dispalying the model performance over its training data")
                    predictions_all,fts=model_pred(model,sub_x,predictions_all)
                    st.write("Evaluation Metrics : ")
                    st.write(fts.head())
                    st.write(predictions_all.head())
                    st.subheader(" You can now see the visualizations about the insights that the model has learnt from the data, from the left-side bar ") 
                # works till here
                    viz_model_insights(model,sub_x,y)
                    gr=st.checkbox("Show actual depth vs prediction depth graph ")
                    if(gr):
                        layout = go.Layout(
                            title='Distance' + " vs " + 'Depth',
                            xaxis=dict(
                                title='Distance'
                            ),
                            yaxis=dict(
                                title='Depth'
                            ) )
                        fig = go.Figure(layout=layout)
                        fig.add_trace(go.Scatter(x=data['Distance_new'], y=y,
                                            mode='lines+markers',
                                            name='Original data lineplot'))
                                                                    
                        fig.add_trace(go.Scatter(x=data['Distance_new'], y=predictions_all['MODEL_full_Pred'],
                                            mode='lines+markers',
                                            name='LR Preds lineplot'))
                        st.plotly_chart(fig)
                        #Plotting sorted on depth vs pid
                        st.write(" Actual vs Predicted Depths, Sorted by actual depth")    
                        pre=predictions_all.copy()
                        col0=pre.columns.tolist()[0]
                        pre=pre.sort_values(by=col0)
                        pre=pre.reset_index()
                        
                        layout = go.Layout(
                            title='Distance' + " vs " + 'Depth',
                            xaxis=dict(
                                title='Distance'
                            ),
                            yaxis=dict(
                                title='Depth'
                            ) )
                        fig1 = go.Figure(layout=layout)
                        fig1.add_trace(go.Scatter(x=pre.index, y=pre[col0],
                                            mode='lines+markers',
                                            name='Original data lineplot'))
                                                                    
                        fig1.add_trace(go.Scatter(x=pre.index, y=pre['MODEL_full_Pred'],
                                            mode='lines+markers',
                                            name='MODEL Preds lineplot'))
                        st.plotly_chart(fig1)
                        
                        ms=st.checkbox("Do you wish to save the model statistics as text file? ")
                        if(ms):
                            fts.to_csv(fn_loc+'Model_statistics.txt',index=True)
                            st.write("File saved as Model_statistics.txt")
                        
                        dol=st.checkbox("Use this model for Predicting full River Points ")
                        if(dol):
                            new_data_path=st.text_input("Enter the path of the CSV file, over which the machine learning model has to Predict (It can be training data or full river points data): ",key='123')#"/home/lego/Desktop/Job/Land River Sea Consulting Ltd./rangitata_01_new.csv
                            if(new_data_path):
                                new_data_path=str(new_data_path)
                                full_df=load_data(new_data_path)
                                
                                sel_cols3=st.multiselect("Select the features for predicting (in the order 'Point Id','Longitude (X Coordinate)','Latitude (Y Coordinate),'R','G','B', and any other variables) ",full_df.columns.tolist())                
                                pro=st.checkbox('Check this box after selecting necessary columns')
                                if(pro):
                                    st.write('You selected:', sel_cols3)
            #                        conf5=st.text_input("Type 'Yes' to confirm the above as your selected features")
                                    pid=sel_cols3[0]
                                    east=sel_cols3[1]
                                    north=sel_cols3[2]
                                    
                                    r=sel_cols3[3]
                                    g=sel_cols3[4]
                                    b=sel_cols3[5]
                                    
                                    if(len(sel_cols3)>6):
                                        w=sel_cols3[6]
                                    sel_cols2=sel_cols3[3:]
                                    new_df1=pd.read_csv(new_data_path,chunksize=1000000)
                                    import gc
                                    gc.collect()
                                    to_som='No'
                                    radius=0
                                    threshold=0
                                    to_som=st.checkbox("Do you wish to smooth the end prediction results using cKDTree neighbours")
                                    if(to_som):
                                        to_som='Yes'
                                        rad=st.text_input("Enter the radius to find the nearest neighbors")
                                        if(rad):
                                            radius=float(rad)
                                            threshold=st.text_input("Enter the threshold value")
                                            if(threshold):
                                                threshold=float(threshold)
                                    tar=st.text_input(" Does this data set have the actual target values column? This is usually the case when you want to predict and export on the calibration data itself (Yes/No)",key='551')
                                    if(tar=='Yes'):
                                        sel_tar=st.multiselect("Select the target column ",full_df.columns.tolist())                
                                        if(sel_tar):
                                            st.write('You selected:', sel_tar)
                                            
                                            fn='none'
                                            st.write("Please wait while the full river prediction is running")
                                            full_river_process_lr(new_df1,r,g,b,sub_x,model,'LR',sel_tar,fn_loc,sel_cols2,pid,east,north,to_som,radius,threshold)
                                            del new_df1

                                    if(tar=='No'):
                                        sel_tar='none'
                                        fn='none'
                                        st.write("Please wait while the full river prediction is running")
                                        full_river_process_lr(new_df1,r,g,b,sub_x,model,'LR',sel_tar,fn_loc,sel_cols2,pid,east,north,to_som,radius,threshold)
                                        del new_df1
                
                if(var1=='Random Forest Regressor'):
                    
                    predictions_all,fts=model_pred(model,rf_x,predictions_all)
                    st.subheader("Dispalying the model performance over its training data")
                    st.write("Evaluation Metrics : ")
                    st.write(fts.head())
                    st.write(predictions_all.head())
                    st.subheader(" You can now see the visualizations about the insights that the model has learnt from the data, from the left-side bar ") 
                # works till here
                    viz_model_insights(model,rf_x,y)
                    gr=st.checkbox("Show actual depth vs prediction depth graph ")
                    if(gr):
                        layout = go.Layout(
                            title='Distance' + " vs " + 'Depth',
                            xaxis=dict(
                                title='Distance'
                            ),
                            yaxis=dict(
                                title='Depth'
                            ) )
                        fig = go.Figure(layout=layout)
                        fig.add_trace(go.Scatter(x=data['Distance_new'], y=y,
                                            mode='lines+markers',
                                            name='Original data lineplot'))
                                                                    
                        fig.add_trace(go.Scatter(x=data['Distance_new'], y=predictions_all['MODEL_full_Pred'],
                                            mode='lines+markers',
                                            name='MODEL Preds lineplot'))
                        st.plotly_chart(fig)
                        #Plotting sorted on depth vs pid
                        st.write(" Actual vs Predicted Depths, Sorted by actual depth")    
                        pre=predictions_all.copy()
                        col0=pre.columns.tolist()[0]
                        pre=pre.sort_values(by=col0)
                        pre=pre.reset_index()
                        
                        layout = go.Layout(
                            title='Distance' + " vs " + 'Depth',
                            xaxis=dict(
                                title='Distance'
                            ),
                            yaxis=dict(
                                title='Depth'
                            ) )
                        fig1 = go.Figure(layout=layout)
                        fig1.add_trace(go.Scatter(x=pre.index, y=pre[col0],
                                            mode='lines+markers',
                                            name='Original data lineplot'))
                                                                    
                        fig1.add_trace(go.Scatter(x=pre.index, y=pre['MODEL_full_Pred'],
                                            mode='lines+markers',
                                            name='MODEL Preds lineplot'))
                        st.plotly_chart(fig1)
    
                        ms=st.checkbox("Do you wish to save the model statistics as text file? ")
                        if(ms):
                            fts.to_csv(fn_loc+'Model_statistics.txt',index=True)
                            st.write("File saved as Model_statistics.txt")
                        
                        dol=st.checkbox("Use this model for Predicting full River Points ")
                        if(dol):
                            new_data_path=st.text_input("Enter the path of the CSV file, over which the machine learning model has to Predict (It can be training data or full river points data): ",key='123')#"/home/lego/Desktop/Job/Land River Sea Consulting Ltd./rangitata_01_new.csv
                            if(new_data_path):
                                new_data_path=str(new_data_path)
                                full_df=load_data(new_data_path)
                                sel_cols3=st.multiselect("Select the features for predicting (in the order 'Point Id','Longitude (X Coordinate)','Latitude (Y Coordinate),'R','G','B', and any other variables) ",full_df.columns.tolist())                
                                pro=st.checkbox('Check this box after selecting necessary columns')
                                if(pro):
                                    st.write('You selected:', sel_cols3)
            #                        conf5=st.text_input("Type 'Yes' to confirm the above as your selected features")
                                    pid=sel_cols3[0]
                                    east=sel_cols3[1]
                                    north=sel_cols3[2]
                                    
                                    r=sel_cols3[3]
                                    g=sel_cols3[4]
                                    b=sel_cols3[5]
                                    
                                    if(len(sel_cols3)>6):
                                        w=sel_cols3[6]
                                    sel_cols2=sel_cols3[3:]
                                    new_df1=pd.read_csv(new_data_path,chunksize=1000000)
                                    import gc
                                    gc.collect()
                                    to_som='No'
                                    radius=0
                                    threshold=0
                                    to_som=st.checkbox("Do you wish to smooth the end prediction results using cKDTree neighbours")
                                    if(to_som):
                                        to_som='Yes'
                                        rad=st.text_input("Enter the radius to find the nearest neighbors")
                                        if(rad):
                                            radius=float(rad)
                                            threshold=st.text_input("Enter the threshold value")
                                            if(threshold):
                                                threshold=float(threshold)
                                    tar=st.text_input(" Does this data set have the actual target values column? This is usually the case when you want to predict and export on the calibration data itself (Yes/No)",key='551')
                                    if(tar=='Yes'):
                                        sel_tar=st.multiselect("Select the target column ",full_df.columns.tolist())                
                                        if(sel_tar):
                                            st.write('You selected:', sel_tar)
                                            
                                            #st.write(sel_cols1[-1])                                        
                                            fn='none'
                                            st.write("Please wait while the full river prediction is running")
                                            full_river_process_rf(new_df1,r,g,b,rf_x,model,'RF',sel_tar,fn_loc,sel_cols2,pid,east,north,to_som,radius,threshold)
                                            del new_df1
                                            
                                    if(tar=='No'):
                                        sel_tar='none'                                      
                                        fn='none'
                                        st.write("Please wait while the full river prediction is running")
                                        full_river_process_rf(new_df1,r,g,b,rf_x,model,'RF',sel_tar,fn_loc,sel_cols2,pid,east,north,to_som,radius,threshold)
                                        del new_df1
                
                if(var1=='XGBoost Regressor'):
                   
                    predictions_all,fts=model_pred(model,rf_x,predictions_all)
                    st.subheader("Dispalying the model performance over its training data")
                    st.write("Evaluation Metrics : ")
                    st.write(fts.head())
                    st.write(predictions_all.head())
                    st.subheader(" You can now see the visualizations about the insights that the model has learnt from the data, from the left-side bar ") 
                # works till here
                    #viz_model_insights(model,rf_x,y)
                    gr=st.checkbox("Show actual depth vs prediction depth graph ")
                    if(gr):
                        layout = go.Layout(
                            title='Distance' + " vs " + 'Depth',
                            xaxis=dict(
                                title='Distance'
                            ),
                            yaxis=dict(
                                title='Depth'
                            ) )
                        fig = go.Figure(layout=layout)
                        fig.add_trace(go.Scatter(x=data['Distance_new'], y=y,
                                            mode='lines+markers',
                                            name='Original data lineplot'))
                                                                    
                        fig.add_trace(go.Scatter(x=data['Distance_new'], y=predictions_all['MODEL_full_Pred'],
                                            mode='lines+markers',
                                            name='MODEL Preds lineplot'))
                        st.plotly_chart(fig)
                        #Plotting sorted on depth vs pid
                        st.write(" Actual vs Predicted Depths, Sorted by actual depth")    
                        pre=predictions_all.copy()
                        col0=pre.columns.tolist()[0]
                        pre=pre.sort_values(by=col0)
                        pre=pre.reset_index()
                        
                        layout = go.Layout(
                            title='Distance' + " vs " + 'Depth',
                            xaxis=dict(
                                title='Distance'
                            ),
                            yaxis=dict(
                                title='Depth'
                            ) )
                        fig1 = go.Figure(layout=layout)
                        fig1.add_trace(go.Scatter(x=pre.index, y=pre[col0],
                                            mode='lines+markers',
                                            name='Original data lineplot'))
                                                                    
                        fig1.add_trace(go.Scatter(x=pre.index, y=pre['MODEL_full_Pred'],
                                            mode='lines+markers',
                                            name='MODEL Preds lineplot'))
                        st.plotly_chart(fig1)
                        ms=st.checkbox("Do you wish to save the model statistics as text file? ")
                        if(ms):
                            fts.to_csv(fn_loc+'Model_statistics.txt',index=True)
                            st.write("File saved as Model_statistics.txt")
                        dol=st.checkbox("Use this model for Predicting full River Points ")
                        if(dol):
                            new_data_path=st.text_input("Enter the path of the CSV file, over which the machine learning model has to Predict (It can be training data or full river points data): ",key='123')#"/home/lego/Desktop/Job/Land River Sea Consulting Ltd./rangitata_01_new.csv
                            if(new_data_path):
                                new_data_path=str(new_data_path)
                                full_df=load_data(new_data_path)
                                sel_cols3=st.multiselect("Select the features for predicting (in the order 'Point Id','Longitude (X Coordinate)','Latitude (Y Coordinate),'R','G','B', and any other variables) ",full_df.columns.tolist())                
                                pro=st.checkbox('Check this box after selecting necessary columns')
                                if(pro):
                                    st.write('You selected:', sel_cols3)
            #                        conf5=st.text_input("Type 'Yes' to confirm the above as your selected features")
                                    pid=sel_cols3[0]
                                    east=sel_cols3[1]
                                    north=sel_cols3[2]
                                    
                                    r=sel_cols3[3]
                                    g=sel_cols3[4]
                                    b=sel_cols3[5]
                                    
                                    if(len(sel_cols3)>6):
                                        w=sel_cols3[6]
                                    sel_cols2=sel_cols3[3:]
                                    new_df1=pd.read_csv(new_data_path,chunksize=1000000)
                                    import gc
                                    gc.collect()
                                    to_som='No'
                                    radius=0
                                    threshold=0
                                    to_som=st.checkbox("Do you wish to smooth the end prediction results using cKDTree neighbours")
                                    if(to_som):
                                        to_som='Yes'
                                        rad=st.text_input("Enter the radius to find the nearest neighbors")
                                        if(rad):
                                            radius=float(rad)
                                            threshold=st.text_input("Enter the threshold value")
                                            if(threshold):
                                                threshold=float(threshold)
                                    tar=st.text_input(" Does this data set have the actual target values column? This is usually the case when you want to predict and export on the calibration data itself (Yes/No)",key='551')
                                    if(tar=='Yes'):
                                        sel_tar=st.multiselect("Select the target column ",full_df.columns.tolist())                
                                        if(sel_tar):
                                            st.write('You selected:', sel_tar)
                                            
                                            #st.write(sel_cols1[-1])                                        
                                            fn='none'
                                            st.write("Please wait while the full river prediction is running")
                                            full_river_process_xgb(new_df1,r,g,b,rf_x,model,'XGB',sel_tar,fn_loc,sel_cols2,pid,east,north,to_som,radius,threshold)
                                            del new_df1

                                    if(tar=='No'):
                                        sel_tar='none'
                                        fn='none'
                                        st.write("Please wait while the full river prediction is running")
                                        full_river_process_xgb(new_df1,r,g,b,rf_x,model,'XGB',sel_tar,fn_loc,sel_cols2,pid,east,north,to_som,radius,threshold)
                                        del new_df1
                
                
                if(var1=='Neural Network'):
                    
                    predictions_all,fts=model_pred(model,nn_x,predictions_all)
                    st.subheader("Dispalying the model performance over its training data")
                    st.write("Evaluation Metrics : ")
                    st.write(fts.head())
                    st.write(predictions_all.head())
                    st.subheader(" You can now see the visualizations about the insights that the model has learnt from the data, from the left-side bar ") 
                # works till here
                    viz_model_insights(model,rf_x,y)
                    gr=st.checkbox("Show actual depth vs prediction depth graph ")
                    if(gr):
                        layout = go.Layout(
                            title='Distance' + " vs " + 'Depth',
                            xaxis=dict(
                                title='Distance'
                            ),
                            yaxis=dict(
                                title='Depth'
                            ) )
                        fig = go.Figure(layout=layout)
                        fig.add_trace(go.Scatter(x=data['Distance_new'], y=y,
                                            mode='lines+markers',
                                            name='Original data lineplot'))
                                                                    
                        fig.add_trace(go.Scatter(x=data['Distance_new'], y=predictions_all['MODEL_full_Pred'],
                                            mode='lines+markers',
                                            name='MODEL Preds lineplot'))
                        st.plotly_chart(fig)
                        #Plotting sorted on depth vs pid
                        st.write(" Actual vs Predicted Depths, Sorted by actual depth")    
                        pre=predictions_all.copy()
                        col0=pre.columns.tolist()[0]
                        pre=pre.sort_values(by=col0)
                        pre=pre.reset_index()
                        
                        layout = go.Layout(
                            title='Distance' + " vs " + 'Depth',
                            xaxis=dict(
                                title='Distance'
                            ),
                            yaxis=dict(
                                title='Depth'
                            ) )
                        fig1 = go.Figure(layout=layout)
                        fig1.add_trace(go.Scatter(x=pre.index, y=pre[col0],
                                            mode='lines+markers',
                                            name='Original data lineplot'))
                                                                    
                        fig1.add_trace(go.Scatter(x=pre.index, y=pre['MODEL_full_Pred'],
                                            mode='lines+markers',
                                            name='MODEL Preds lineplot'))
                        st.plotly_chart(fig1)
                        ms=st.checkbox("Do you wish to save the model statistics as text file? ")
                        if(ms):
                            fts.to_csv(fn_loc+'Model_statistics.txt',index=True)
                            st.write("File saved as Model_statistics.txt")
                        dol=st.checkbox("Use this model for Predicting full River Points ")
                        if(dol):
                            new_data_path=st.text_input("Enter the path of the CSV file, over which the machine learning model has to Predict (It can be training data or full river points data): ",key='123')#"/home/lego/Desktop/Job/Land River Sea Consulting Ltd./rangitata_01_new.csv
                            if(new_data_path):
                                new_data_path=str(new_data_path)
                                full_df=load_data(new_data_path)
                                sel_cols3=st.multiselect("Select the features for predicting (in the order 'Point Id','Longitude (X Coordinate)','Latitude (Y Coordinate),'R','G','B', and any other variables) ",full_df.columns.tolist())                
                                pro=st.checkbox('Check this box after selecting necessary columns')
                                if(pro):
                                    st.write('You selected:', sel_cols3)
            #                        conf5=st.text_input("Type 'Yes' to confirm the above as your selected features")
                                    pid=sel_cols3[0]
                                    east=sel_cols3[1]
                                    north=sel_cols3[2]
                                    
                                    r=sel_cols3[3]
                                    g=sel_cols3[4]
                                    b=sel_cols3[5]
                                    
                                    if(len(sel_cols3)>6):
                                        w=sel_cols3[6]
                                    sel_cols2=sel_cols3[3:]
                                    new_df1=pd.read_csv(new_data_path,chunksize=1000000)
                                    import gc
                                    gc.collect()
                                    to_som='No'
                                    radius=0
                                    threshold=0
                                    to_som=st.checkbox("Do you wish to smooth the end prediction results using cKDTree neighbours")
                                    if(to_som):
                                        to_som='Yes'
                                        rad=st.text_input("Enter the radius to find the nearest neighbors")
                                        if(rad):
                                            radius=float(rad)
                                            threshold=st.text_input("Enter the threshold value")
                                            if(threshold):
                                                threshold=float(threshold)
                                    tar=st.text_input(" Does this data set have the actual target values column? This is usually the case when you want to predict and export on the calibration data itself (Yes/No)",key='551')
                                    if(tar=='Yes'):
                                        sel_tar=st.multiselect("Select the target column ",full_df.columns.tolist())                
                                        if(sel_tar):
                                            st.write('You selected:', sel_tar)
                                            
                                            #st.write(sel_cols1[-1])                                        
                                            fn='none'
                                            st.write("Please wait while the full river prediction is running")
                                            full_river_process_nn(new_df1,r,g,b,nn_x,model,'NN',sel_tar,fn_loc,sel_cols2,pid,east,north,to_som,radius,threshold)
                                            del new_df1

                                    if(tar=='No'):
                                        sel_tar='none'
                                        fn='none'
                                        st.write("Please wait while the full river prediction is running")
                                        full_river_process_nn(new_df1,r,g,b,nn_x,model,'NN',sel_tar,fn_loc,sel_cols2,pid,east,north,to_som,radius,threshold)
                                        del new_df1
        
                if(var1=='Support Vector Regressor'):
                    predictions_all,fts=model_pred(model,sub_x,predictions_all)
                    st.subheader("Dispalying the model performance over its training data")
                    st.write("Evaluation Metrics : ")
                    st.write(fts.head())
                    st.write(predictions_all.head())
                    st.subheader(" You can now see the visualizations about the insights that the model has learnt from the data, from the left-side bar ") 
                # works till here
                    viz_model_insights(model,sub_x,y)
                    gr=st.checkbox("Show actual depth vs prediction depth graph ")
                    if(gr):
                        layout = go.Layout(
                        title='Distance' + " vs " + 'Depth',
                        xaxis=dict(
                            title='Distance'
                        ),
                        yaxis=dict(
                            title='Depth'
                        ) )
                        fig = go.Figure(layout=layout)
                        fig.add_trace(go.Scatter(x=data['Distance_new'], y=y,
                                            mode='lines+markers',
                                            name='Original data lineplot'))
                                                                    
                        fig.add_trace(go.Scatter(x=data['Distance_new'], y=predictions_all['MODEL_full_Pred'],
                                            mode='lines+markers',
                                            name='MODEL Preds lineplot'))
                        st.plotly_chart(fig)
                        #Plotting sorted on depth vs pid
                        st.write(" Actual vs Predicted Depths, Sorted by actual depth")    
                        pre=predictions_all.copy()
                        col0=pre.columns.tolist()[0]
                        pre=pre.sort_values(by=col0)
                        pre=pre.reset_index()
                        
                        layout = go.Layout(
                        title='Distance' + " vs " + 'Depth',
                        xaxis=dict(
                            title='Distance'
                        ),
                        yaxis=dict(
                            title='Depth'
                        ) )
                        fig1 = go.Figure(layout=layout)
                        fig1.add_trace(go.Scatter(x=pre.index, y=pre[col0],
                                            mode='lines+markers',
                                            name='Original data lineplot'))
                                                                    
                        fig1.add_trace(go.Scatter(x=pre.index, y=pre['MODEL_full_Pred'],
                                            mode='lines+markers',
                                            name='MODEL Preds lineplot'))
                        st.plotly_chart(fig1)
                        ms=st.checkbox("Do you wish to save the model statistics as text file? ")
                        if(ms):
                            fts.to_csv(fn_loc+'Model_statistics.txt',index=True)
                            st.write("File saved as Model_statistics.txt")
    
                        dol=st.checkbox("Use this model for Predicting full River Points ")
                        if(dol):
                            new_data_path=st.text_input("Enter the path of the CSV file, over which the machine learning model has to Predict (It can be training data or full river points data): ",key='123')#"/home/lego/Desktop/Job/Land River Sea Consulting Ltd./rangitata_01_new.csv
                            if(new_data_path):
                                new_data_path=str(new_data_path)
                                full_df=load_data(new_data_path)
                                sel_cols3=st.multiselect("Select the features for predicting (in the order 'Point Id','Longitude (X Coordinate)','Latitude (Y Coordinate),'R','G','B', and any other variables) ",full_df.columns.tolist())                
                                pro=st.checkbox('Check this box after selecting necessary columns')
                                if(pro):
                                    st.write('You selected:', sel_cols3)
            #                        conf5=st.text_input("Type 'Yes' to confirm the above as your selected features")
                                    pid=sel_cols3[0]
                                    east=sel_cols3[1]
                                    north=sel_cols3[2]
                                    
                                    r=sel_cols3[3]
                                    g=sel_cols3[4]
                                    b=sel_cols3[5]
                                    
                                    if(len(sel_cols3)>6):
                                        w=sel_cols3[6]
                                    sel_cols2=sel_cols3[3:]
                                    new_df1=pd.read_csv(new_data_path,chunksize=1000000)
                                    import gc
                                    gc.collect()
                                    to_som='No'
                                    radius=0
                                    threshold=0
                                    to_som=st.checkbox("Do you wish to smooth the end prediction results using cKDTree neighbours")
                                    if(to_som):
                                        to_som='Yes'
                                        rad=st.text_input("Enter the radius to find the nearest neighbors")
                                        if(rad):
                                            radius=float(rad)
                                            threshold=st.text_input("Enter the threshold value")
                                            if(threshold):
                                                threshold=float(threshold)
                                    tar=st.text_input(" Does this data set have the actual target values column? This is usually the case when you want to predict and export on the calibration data itself (Yes/No)",key='551')
                                    if(tar=='Yes'):
                                        sel_tar=st.multiselect("Select the target column ",full_df.columns.tolist())                
                                        if(sel_tar):
                                            st.write('You selected:', sel_tar)
                                            
                                            #st.write(sel_cols1[-1])                                        
                                            fn='none'
                                            st.write("Please wait while the full river prediction is running")
                                            full_river_process_svr(new_df1,r,g,b,sub_x,model,'SVR',sel_tar,fn_loc,sel_cols2,pid,east,north,to_som,radius,threshold)
                                            del new_df1
                                                         
                                    if(tar=='No'):
                                        sel_tar='none'
                                        fn='none'
                                        st.write("Please wait while the full river prediction is running")
                                        full_river_process_svr(new_df1,r,g,b,sub_x,model,'SVR',sel_tar,fn_loc,sel_cols2,pid,east,north,to_som,radius,threshold)
                                        del new_df1          
                                    
@st.cache(persist=True, suppress_st_warning=True,allow_output_mutation=True)
def only_xgb(x_train,y_train):
    xgb1=RandomForestRegressor(n_estimators=250,random_state=50)
    xgb1.fit(x_train,y_train)
    return xgb1



def full_river_process_lr(new_df1,r,g,b,sub_x,model,model_name,sel_tar,fn,sel_cols2,pid,east,north,to_som,radius,threshold):
    #st.write(hutt_full.head())
    full_river=pd.DataFrame()
    for jun in new_df1:
        to_merge=pd.DataFrame()
        merged=pd.DataFrame()
        st.write(" Current batch of shape ",jun.shape)
        jun=jun.reset_index().drop(['index'],axis=1) 
        hutt_full=pd.DataFrame()
        to_merge=pd.DataFrame(jun[[pid,east,north]])

        hutt_full=jun[sel_cols2].copy()
        lnbr=[]
        lnbg=[]
        #hut full has r,g,b,w only
        r_in=hutt_full.columns.get_loc(r)
        g_in=hutt_full.columns.get_loc(g)
        b_in=hutt_full.columns.get_loc(b)
        te=hutt_full.values
        for i in range(len(te)):
          val=np.log10(te[i,b_in]/te[i,r_in])
          lnbr.append(val)      
          val1=np.log10(te[i,b_in]/te[i,g_in])
          lnbg.append(val1)
              
        hutt_full['ln(b/r)']=np.array(lnbr)
        hutt_full['ln(b/g)']=np.array(lnbg)
        
        rgbc=[]
        for i in range(len(te)):
          combo=te[i,r_in]*te[i,g_in]*te[i,b_in]
          rgbc.append(combo)
        
        hutt_full['RGB_Combo']=0
        hutt_full['RGB_Combo']=np.array(rgbc)
        #load kmeans
        kmeans=joblib.load('kmeans.pkl')
        hutt_full.loc[:, 'point_cluster'] = kmeans.predict(hutt_full[[r,g,b]])
        hutt_full['point_cluster']=hutt_full['point_cluster'].astype('int64')
        mm=load_data('min_med_max.csv')
        
        max_indx=mm.groupby(['point_cluster']).mean()['RGB_Combo'][mm.groupby(['point_cluster']).mean()['RGB_Combo']==np.max(mm.groupby(['point_cluster']).mean()['RGB_Combo'])].index
        min_indx=mm.groupby(['point_cluster']).mean()['RGB_Combo'][mm.groupby(['point_cluster']).mean()['RGB_Combo']==np.min(mm.groupby(['point_cluster']).mean()['RGB_Combo'])].index
        indexs=list(mm.groupby(['point_cluster']).mean().index)
        indexs.remove(min_indx)
        indexs.remove(max_indx)
        med_indx=indexs[0]
        
        hutt_full['Point_cluster']=''
        hutt_full['Point_cluster']=np.where(hutt_full['point_cluster']==max_indx[0],'High',hutt_full['Point_cluster'])
        hutt_full['Point_cluster']=np.where(hutt_full['point_cluster']==med_indx,'Medium',hutt_full['Point_cluster'])
        hutt_full['Point_cluster']=np.where(hutt_full['point_cluster']==min_indx[0],'Low',hutt_full['Point_cluster'])
        hutt_full=hutt_full.reset_index().drop(['index'],axis=1)
        
        dn=load_data('groupby.csv')  
        #st.write(dn.head())
    
        gr=dn.columns[0]
        gg=dn.columns[1]
        gb=dn.columns[2]
        
        peak_r=dn.groupby(['Point_cluster']).max()[gr]
        peak_g=dn.groupby(['Point_cluster']).max()[gg]
        peak_b=dn.groupby(['Point_cluster']).max()[gb]
        peak_rgb=dn.groupby(['Point_cluster']).max()['RGB_Combo']
       # st.write(peak_r,peak_g,peak_b)
        
        hutt_full['Ratio_peak_R']=0.0
        hutt_full['Ratio_peak_G']=0.0
        hutt_full['Ratio_peak_B']=0.0
        hutt_full['Ratio_peak_RGBCombo']=0.0
        
        #low
        hutt_full['Ratio_peak_R']=np.where(hutt_full['Point_cluster']=='Low',hutt_full[r]/peak_r['Low'],hutt_full['Ratio_peak_R'])
        hutt_full['Ratio_peak_G']=np.where(hutt_full['Point_cluster']=='Low',hutt_full[g]/peak_g['Low'],hutt_full['Ratio_peak_G'])
        hutt_full['Ratio_peak_B']=np.where(hutt_full['Point_cluster']=='Low',hutt_full[b]/peak_b['Low'],hutt_full['Ratio_peak_B'])
        hutt_full['Ratio_peak_RGBCombo']=np.where(hutt_full['Point_cluster']=='Low',hutt_full['RGB_Combo']/peak_rgb['Low'],hutt_full['Ratio_peak_RGBCombo'])
        
        #medium
        hutt_full['Ratio_peak_R']=np.where(hutt_full['Point_cluster']=='Medium',hutt_full[r]/peak_r['Medium'],hutt_full['Ratio_peak_R'])
        hutt_full['Ratio_peak_G']=np.where(hutt_full['Point_cluster']=='Medium',hutt_full[g]/peak_g['Medium'],hutt_full['Ratio_peak_G'])
        hutt_full['Ratio_peak_B']=np.where(hutt_full['Point_cluster']=='Medium',hutt_full[b]/peak_b['Medium'],hutt_full['Ratio_peak_B'])
        hutt_full['Ratio_peak_RGBCombo']=np.where(hutt_full['Point_cluster']=='Medium',hutt_full['RGB_Combo']/peak_rgb['Medium'],hutt_full['Ratio_peak_RGBCombo'])
        
        #high
        hutt_full['Ratio_peak_R']=np.where(hutt_full['Point_cluster']=='High',hutt_full[r]/peak_r['High'],hutt_full['Ratio_peak_R'])
        hutt_full['Ratio_peak_G']=np.where(hutt_full['Point_cluster']=='High',hutt_full[g]/peak_g['High'],hutt_full['Ratio_peak_G'])
        hutt_full['Ratio_peak_B']=np.where(hutt_full['Point_cluster']=='High',hutt_full[b]/peak_b['High'],hutt_full['Ratio_peak_B'])
        hutt_full['Ratio_peak_RGBCombo']=np.where(hutt_full['Point_cluster']=='High',hutt_full['RGB_Combo']/peak_rgb['High'],hutt_full['Ratio_peak_RGBCombo'])
        
        hutt_full=pd.get_dummies(hutt_full)
        #st.write("Before",hutt_full.head())
        # to confirm if all 3 clusters are there..else add dummy column
        grs=[]
        clus_all=['Point_cluster_Low','Point_cluster_Medium','Point_cluster_High']
        fk=[]
        for c in clus_all:
            if(c not in hutt_full.columns):
                fk.append(c)
                
        for fkc in fk:
            hutt_full[fkc]=0
        # To maintain the order in low, med ,high....
        lowp=hutt_full['Point_cluster_Low'].values
        medp=hutt_full['Point_cluster_Medium'].values
        highp=hutt_full['Point_cluster_High'].values
        
        hutt_full.drop(clus_all,axis=1,inplace=True)
        hutt_full['Point_cluster_Low']=lowp
        hutt_full['Point_cluster_Medium']=medp
        hutt_full['Point_cluster_High']=highp
        #st.write("After",hutt_full.head())     
        tes=np.array(hutt_full)
        i=0
        for i in range(len(tes)):
          combo=0.3*tes[i,r_in]+ 0.59*tes[i,g_in]+ 0.11*tes[i,b_in]
          grs.append(combo)
        
        hutt_full['Greyscale']=np.array(grs)  
        hf=pd.DataFrame()
        hf=hutt_full.drop(['point_cluster','RGB_Combo'],axis=1).copy()
        #st.write(hf.head(),hf.shape)
        #start of next func
        pca=joblib.load('pca_full.pkl')
        pca_result1=pca.transform(hf)
       # st.write("did")
        sub1=pd.DataFrame()
        sub1['pca-one']=0
        sub1['pca-two']=0
        sub1['pca-three']=0
        
        sub1['pca-one'] = pca_result1[:,0]
        sub1['pca-two'] = pca_result1[:,1] 
        sub1['pca-three'] = pca_result1[:,2]
        svr_data1=sub1.copy()
        sc=joblib.load('sc_full.pkl')
        sv_data=sc.transform(svr_data1)
        predi=pd.DataFrame()
        predi[str(model_name)+'_Pred']=model.predict(sv_data)
        
        if(sel_tar=='none'):
            df_preds2=pd.DataFrame(predi.copy())
            merged=pd.concat([to_merge,df_preds2],axis=1)
            merged=merged.reset_index().drop(['index'],axis=1) 
            full_river=pd.concat([full_river,merged],axis=0)
            full_river=full_river.reset_index().drop(['index'],axis=1) 

        else:
            predi[sel_tar]=jun[sel_tar].copy()
            df_preds2=pd.DataFrame(predi.copy())
            merged=pd.concat([to_merge,df_preds2],axis=1)
            merged=merged.reset_index().drop(['index'],axis=1)
            full_river=pd.concat([full_river,merged],axis=0)
            full_river=full_river.reset_index().drop(['index'],axis=1) 
        
        del merged
       # del df_poly1
        del hf
        del hutt_full
        del df_preds2
        del predi
        del grs
        import gc
        gc.collect()
       # st.write("Merged",merged.tail())
       # st.write(full_river.tail())
        
# till here
  #  full_river=full_river.reset_index().drop(['index'],axis=1)
    full_river=full_river.sort_values(by=pid)
    full_river=full_river.reset_index().drop(['index'],axis=1) 
    #st.write(full_river.shape)
    st.write("Sample of the output")
    st.write(full_river.head())
    if(to_som=='Yes'):
        # to remove spikes..end smoothing algo , arguments needed= radius, threshold, to_som
        import scipy.spatial as spatial # use cKDTree to find neigbouring points 
        latlon=np.array(full_river[[east,north]].copy())
        point_tree=spatial.cKDTree(latlon)
        fr=np.array(full_river[str(model_name)+'_Pred'].copy())
        #st.write("entered into smoother")
        #now for each point find all those points withing radius from this tree
        out_ind=[]
        for i in range(len(latlon)):
            ixs=point_tree.query_ball_point(latlon[i], radius)
            avg_all=np.mean(fr[ixs])
            if(abs(fr[i]-avg_all)>threshold):
                out_ind.append(i)
    
        st.write(" Total dropped noise data points ",len(out_ind))
        not_smooth=full_river.drop(out_ind,axis=0)
        del fr
        del latlon
        
    file_loc=fn+'Predictions_lr_pretrained.csv'
    full_river.to_csv(file_loc,index=False)
    if(to_som=='Yes'):
        n_fl=fn+'Predictions_lr_pretrained_smoothed.csv'
        not_smooth.to_csv(n_fl,index=False)
        #required_files(fn)
    st.write("Prediction file downloaded!! Please check your local files location  - in the given folder name to access the 'Predictions_lr_pretrained.csv' or 'Predictions_lr_pretrained_smoothed.csv' ")
    del full_river
 #       st.write("done 4")

def full_river_process_svr(new_df1,r,g,b,sub_x,model,model_name,sel_tar,fn,sel_cols2,pid,east,north,to_som,radius,threshold):
    #st.write(hutt_full.head())
    full_river=pd.DataFrame()
    for jun in new_df1:
        to_merge=pd.DataFrame()
        merged=pd.DataFrame()
        st.write(" Current batch of shape ",jun.shape)
        jun=jun.reset_index().drop(['index'],axis=1) 
        hutt_full=pd.DataFrame()
        to_merge=pd.DataFrame(jun[[pid,east,north]])

        hutt_full=jun[sel_cols2].copy()
        lnbr=[]
        lnbg=[]
        #hut full has r,g,b,w only
        r_in=hutt_full.columns.get_loc(r)
        g_in=hutt_full.columns.get_loc(g)
        b_in=hutt_full.columns.get_loc(b)
        te=hutt_full.values
        for i in range(len(te)):
          val=np.log10(te[i,b_in]/te[i,r_in])
          lnbr.append(val)      
          val1=np.log10(te[i,b_in]/te[i,g_in])
          lnbg.append(val1)
              
        hutt_full['ln(b/r)']=np.array(lnbr)
        hutt_full['ln(b/g)']=np.array(lnbg)
        
        rgbc=[]
        for i in range(len(te)):
          combo=te[i,r_in]*te[i,g_in]*te[i,b_in]
          rgbc.append(combo)
        
        hutt_full['RGB_Combo']=0
        hutt_full['RGB_Combo']=np.array(rgbc)
        #load kmeans
        kmeans=joblib.load('kmeans.pkl')
        hutt_full.loc[:, 'point_cluster'] = kmeans.predict(hutt_full[[r,g,b]])
        hutt_full['point_cluster']=hutt_full['point_cluster'].astype('int64')
        mm=load_data('min_med_max.csv')
        
        max_indx=mm.groupby(['point_cluster']).mean()['RGB_Combo'][mm.groupby(['point_cluster']).mean()['RGB_Combo']==np.max(mm.groupby(['point_cluster']).mean()['RGB_Combo'])].index
        min_indx=mm.groupby(['point_cluster']).mean()['RGB_Combo'][mm.groupby(['point_cluster']).mean()['RGB_Combo']==np.min(mm.groupby(['point_cluster']).mean()['RGB_Combo'])].index
        indexs=list(mm.groupby(['point_cluster']).mean().index)
        indexs.remove(min_indx)
        indexs.remove(max_indx)
        med_indx=indexs[0]
        
        hutt_full['Point_cluster']=''
        hutt_full['Point_cluster']=np.where(hutt_full['point_cluster']==max_indx[0],'High',hutt_full['Point_cluster'])
        hutt_full['Point_cluster']=np.where(hutt_full['point_cluster']==med_indx,'Medium',hutt_full['Point_cluster'])
        hutt_full['Point_cluster']=np.where(hutt_full['point_cluster']==min_indx[0],'Low',hutt_full['Point_cluster'])
        hutt_full=hutt_full.reset_index().drop(['index'],axis=1)
        
        dn=load_data('groupby.csv')  
        #st.write(dn.head())
    
        gr=dn.columns[0]
        gg=dn.columns[1]
        gb=dn.columns[2]
        
        peak_r=dn.groupby(['Point_cluster']).max()[gr]
        peak_g=dn.groupby(['Point_cluster']).max()[gg]
        peak_b=dn.groupby(['Point_cluster']).max()[gb]
        peak_rgb=dn.groupby(['Point_cluster']).max()['RGB_Combo']
       # st.write(peak_r,peak_g,peak_b)
        
        hutt_full['Ratio_peak_R']=0.0
        hutt_full['Ratio_peak_G']=0.0
        hutt_full['Ratio_peak_B']=0.0
        hutt_full['Ratio_peak_RGBCombo']=0.0
        
        #low
        hutt_full['Ratio_peak_R']=np.where(hutt_full['Point_cluster']=='Low',hutt_full[r]/peak_r['Low'],hutt_full['Ratio_peak_R'])
        hutt_full['Ratio_peak_G']=np.where(hutt_full['Point_cluster']=='Low',hutt_full[g]/peak_g['Low'],hutt_full['Ratio_peak_G'])
        hutt_full['Ratio_peak_B']=np.where(hutt_full['Point_cluster']=='Low',hutt_full[b]/peak_b['Low'],hutt_full['Ratio_peak_B'])
        hutt_full['Ratio_peak_RGBCombo']=np.where(hutt_full['Point_cluster']=='Low',hutt_full['RGB_Combo']/peak_rgb['Low'],hutt_full['Ratio_peak_RGBCombo'])
        
        #medium
        hutt_full['Ratio_peak_R']=np.where(hutt_full['Point_cluster']=='Medium',hutt_full[r]/peak_r['Medium'],hutt_full['Ratio_peak_R'])
        hutt_full['Ratio_peak_G']=np.where(hutt_full['Point_cluster']=='Medium',hutt_full[g]/peak_g['Medium'],hutt_full['Ratio_peak_G'])
        hutt_full['Ratio_peak_B']=np.where(hutt_full['Point_cluster']=='Medium',hutt_full[b]/peak_b['Medium'],hutt_full['Ratio_peak_B'])
        hutt_full['Ratio_peak_RGBCombo']=np.where(hutt_full['Point_cluster']=='Medium',hutt_full['RGB_Combo']/peak_rgb['Medium'],hutt_full['Ratio_peak_RGBCombo'])
        
        #high
        hutt_full['Ratio_peak_R']=np.where(hutt_full['Point_cluster']=='High',hutt_full[r]/peak_r['High'],hutt_full['Ratio_peak_R'])
        hutt_full['Ratio_peak_G']=np.where(hutt_full['Point_cluster']=='High',hutt_full[g]/peak_g['High'],hutt_full['Ratio_peak_G'])
        hutt_full['Ratio_peak_B']=np.where(hutt_full['Point_cluster']=='High',hutt_full[b]/peak_b['High'],hutt_full['Ratio_peak_B'])
        hutt_full['Ratio_peak_RGBCombo']=np.where(hutt_full['Point_cluster']=='High',hutt_full['RGB_Combo']/peak_rgb['High'],hutt_full['Ratio_peak_RGBCombo'])
        
        hutt_full=pd.get_dummies(hutt_full)
        #st.write("Before",hutt_full.head())
        # to confirm if all 3 clusters are there..else add dummy column
        grs=[]
        clus_all=['Point_cluster_Low','Point_cluster_Medium','Point_cluster_High']
        fk=[]
        for c in clus_all:
            if(c not in hutt_full.columns):
                fk.append(c)
                
        for fkc in fk:
            hutt_full[fkc]=0
        # To maintain the order in low, med ,high....
        lowp=hutt_full['Point_cluster_Low'].values
        medp=hutt_full['Point_cluster_Medium'].values
        highp=hutt_full['Point_cluster_High'].values
        
        hutt_full.drop(clus_all,axis=1,inplace=True)
        hutt_full['Point_cluster_Low']=lowp
        hutt_full['Point_cluster_Medium']=medp
        hutt_full['Point_cluster_High']=highp
        #st.write("After",hutt_full.head())     
        tes=np.array(hutt_full)
        i=0
        for i in range(len(tes)):
          combo=0.3*tes[i,r_in]+ 0.59*tes[i,g_in]+ 0.11*tes[i,b_in]
          grs.append(combo)
        
        hutt_full['Greyscale']=np.array(grs)  
        hf=pd.DataFrame()
        hf=hutt_full.drop(['point_cluster','RGB_Combo'],axis=1).copy()
        #st.write(hf.head(),hf.shape)
        #start of next func
        pca=joblib.load('pca_full.pkl')
        pca_result1=pca.transform(hf)
       # st.write("did")
        sub1=pd.DataFrame()
        sub1['pca-one']=0
        sub1['pca-two']=0
        sub1['pca-three']=0
        
        sub1['pca-one'] = pca_result1[:,0]
        sub1['pca-two'] = pca_result1[:,1] 
        sub1['pca-three'] = pca_result1[:,2]
        svr_data1=sub1.copy()
        sc=joblib.load('sc_full.pkl')
        sv_data=sc.transform(svr_data1)
        predi=pd.DataFrame()
        predi[str(model_name)+'_Pred']=model.predict(sv_data)
        
        if(sel_tar=='none'):
            df_preds2=pd.DataFrame(predi.copy())
            merged=pd.concat([to_merge,df_preds2],axis=1)
            merged=merged.reset_index().drop(['index'],axis=1) 
            full_river=pd.concat([full_river,merged],axis=0)
            full_river=full_river.reset_index().drop(['index'],axis=1) 

        else:
            predi[sel_tar]=jun[sel_tar].copy()
            df_preds2=pd.DataFrame(predi.copy())
            merged=pd.concat([to_merge,df_preds2],axis=1)
            merged=merged.reset_index().drop(['index'],axis=1)
            full_river=pd.concat([full_river,merged],axis=0)
            full_river=full_river.reset_index().drop(['index'],axis=1) 
        
        del merged
        del svr_data1
        del sub1
        del hf
        del hutt_full
        del df_preds2
        del predi
        del grs
        import gc
        gc.collect()
       # st.write("Merged",merged.tail())
       # st.write(full_river.tail())
        
# till here
  #  full_river=full_river.reset_index().drop(['index'],axis=1)
    full_river=full_river.sort_values(by=pid)
    full_river=full_river.reset_index().drop(['index'],axis=1) 
    #st.write(full_river.shape)
    st.write("Sample of the output")
    st.write(full_river.head())
    if(to_som=='Yes'):
        # to remove spikes..end smoothing algo , arguments needed= radius, threshold, to_som
        import scipy.spatial as spatial # use cKDTree to find neigbouring points 
        latlon=np.array(full_river[[east,north]].copy())
        point_tree=spatial.cKDTree(latlon)
        fr=np.array(full_river[str(model_name)+'_Pred'].copy())
        #st.write("entered into smoother")
        #now for each point find all those points withing radius from this tree
        out_ind=[]
        for i in range(len(latlon)):
            ixs=point_tree.query_ball_point(latlon[i], radius)
            avg_all=np.mean(fr[ixs])
            if(abs(fr[i]-avg_all)>threshold):
                out_ind.append(i)
    
        st.write(" Total dropped noise data points ",len(out_ind))
        not_smooth=full_river.drop(out_ind,axis=0)
        del fr
        del latlon
        
    file_loc=fn+'Predictions_svr_pretrained.csv'
    full_river.to_csv(file_loc,index=False)
    if(to_som=='Yes'):
        n_fl=fn+'Predictions_svr_pretrained_smoothed.csv'
        not_smooth.to_csv(n_fl,index=False)
    st.write("Prediction file downloaded!! Please check your local files location  - in the given folder name to access the 'Predictions_svr_pretrained.csv' or 'Predictions_svr_pretrained_smoothed.csv' ")
    del full_river
#               st.write("done 4")
    

def full_river_process_rf(new_df1,r,g,b,rf_x,model,model_name,sel_tar,fn,sel_cols2,pid,east,north,to_som,radius,threshold):
    #st.write(hutt_full.head())
    full_river=pd.DataFrame()
    for jun in new_df1:
        to_merge=pd.DataFrame()
        merged=pd.DataFrame()
        st.write(" Current batch of shape ",jun.shape)
        jun=jun.reset_index().drop(['index'],axis=1) 
        hutt_full=pd.DataFrame()
        to_merge=pd.DataFrame(jun[[pid,east,north]])

        hutt_full=jun[sel_cols2].copy()
        lnbr=[]
        lnbg=[]
        #hut full has r,g,b,w only
        r_in=hutt_full.columns.get_loc(r)
        g_in=hutt_full.columns.get_loc(g)
        b_in=hutt_full.columns.get_loc(b)
        te=hutt_full.values
        for i in range(len(te)):
          val=np.log10(te[i,b_in]/te[i,r_in])
          lnbr.append(val)      
          val1=np.log10(te[i,b_in]/te[i,g_in])
          lnbg.append(val1)
              
        hutt_full['ln(b/r)']=np.array(lnbr)
        hutt_full['ln(b/g)']=np.array(lnbg)
        
        rgbc=[]
        for i in range(len(te)):
          combo=te[i,r_in]*te[i,g_in]*te[i,b_in]
          rgbc.append(combo)
        
        hutt_full['RGB_Combo']=0
        hutt_full['RGB_Combo']=np.array(rgbc)
        #load kmeans
        kmeans=joblib.load('kmeans.pkl')
        hutt_full.loc[:, 'point_cluster'] = kmeans.predict(hutt_full[[r,g,b]])
        hutt_full['point_cluster']=hutt_full['point_cluster'].astype('int64')
        mm=load_data('min_med_max.csv')
        
        max_indx=mm.groupby(['point_cluster']).mean()['RGB_Combo'][mm.groupby(['point_cluster']).mean()['RGB_Combo']==np.max(mm.groupby(['point_cluster']).mean()['RGB_Combo'])].index
        min_indx=mm.groupby(['point_cluster']).mean()['RGB_Combo'][mm.groupby(['point_cluster']).mean()['RGB_Combo']==np.min(mm.groupby(['point_cluster']).mean()['RGB_Combo'])].index
        indexs=list(mm.groupby(['point_cluster']).mean().index)
        indexs.remove(min_indx)
        indexs.remove(max_indx)
        med_indx=indexs[0]
        
        hutt_full['Point_cluster']=''
        hutt_full['Point_cluster']=np.where(hutt_full['point_cluster']==max_indx[0],'High',hutt_full['Point_cluster'])
        hutt_full['Point_cluster']=np.where(hutt_full['point_cluster']==med_indx,'Medium',hutt_full['Point_cluster'])
        hutt_full['Point_cluster']=np.where(hutt_full['point_cluster']==min_indx[0],'Low',hutt_full['Point_cluster'])
        hutt_full=hutt_full.reset_index().drop(['index'],axis=1)
        
        dn=load_data('groupby.csv')  
        #st.write(dn.head())
    
        gr=dn.columns[0]
        gg=dn.columns[1]
        gb=dn.columns[2]
        
        peak_r=dn.groupby(['Point_cluster']).max()[gr]
        peak_g=dn.groupby(['Point_cluster']).max()[gg]
        peak_b=dn.groupby(['Point_cluster']).max()[gb]
        peak_rgb=dn.groupby(['Point_cluster']).max()['RGB_Combo']
       # st.write(peak_r,peak_g,peak_b)
        
        hutt_full['Ratio_peak_R']=0.0
        hutt_full['Ratio_peak_G']=0.0
        hutt_full['Ratio_peak_B']=0.0
        hutt_full['Ratio_peak_RGBCombo']=0.0
        
        #low
        hutt_full['Ratio_peak_R']=np.where(hutt_full['Point_cluster']=='Low',hutt_full[r]/peak_r['Low'],hutt_full['Ratio_peak_R'])
        hutt_full['Ratio_peak_G']=np.where(hutt_full['Point_cluster']=='Low',hutt_full[g]/peak_g['Low'],hutt_full['Ratio_peak_G'])
        hutt_full['Ratio_peak_B']=np.where(hutt_full['Point_cluster']=='Low',hutt_full[b]/peak_b['Low'],hutt_full['Ratio_peak_B'])
        hutt_full['Ratio_peak_RGBCombo']=np.where(hutt_full['Point_cluster']=='Low',hutt_full['RGB_Combo']/peak_rgb['Low'],hutt_full['Ratio_peak_RGBCombo'])
        
        #medium
        hutt_full['Ratio_peak_R']=np.where(hutt_full['Point_cluster']=='Medium',hutt_full[r]/peak_r['Medium'],hutt_full['Ratio_peak_R'])
        hutt_full['Ratio_peak_G']=np.where(hutt_full['Point_cluster']=='Medium',hutt_full[g]/peak_g['Medium'],hutt_full['Ratio_peak_G'])
        hutt_full['Ratio_peak_B']=np.where(hutt_full['Point_cluster']=='Medium',hutt_full[b]/peak_b['Medium'],hutt_full['Ratio_peak_B'])
        hutt_full['Ratio_peak_RGBCombo']=np.where(hutt_full['Point_cluster']=='Medium',hutt_full['RGB_Combo']/peak_rgb['Medium'],hutt_full['Ratio_peak_RGBCombo'])
        
        #high
        hutt_full['Ratio_peak_R']=np.where(hutt_full['Point_cluster']=='High',hutt_full[r]/peak_r['High'],hutt_full['Ratio_peak_R'])
        hutt_full['Ratio_peak_G']=np.where(hutt_full['Point_cluster']=='High',hutt_full[g]/peak_g['High'],hutt_full['Ratio_peak_G'])
        hutt_full['Ratio_peak_B']=np.where(hutt_full['Point_cluster']=='High',hutt_full[b]/peak_b['High'],hutt_full['Ratio_peak_B'])
        hutt_full['Ratio_peak_RGBCombo']=np.where(hutt_full['Point_cluster']=='High',hutt_full['RGB_Combo']/peak_rgb['High'],hutt_full['Ratio_peak_RGBCombo'])
        
        hutt_full=pd.get_dummies(hutt_full)
        #st.write("Before",hutt_full.head())
        # to confirm if all 3 clusters are there..else add dummy column
        grs=[]
        clus_all=['Point_cluster_Low','Point_cluster_Medium','Point_cluster_High']
        fk=[]
        for c in clus_all:
            if(c not in hutt_full.columns):
                fk.append(c)
                
        for fkc in fk:
            hutt_full[fkc]=0
        # To maintain the order in low, med ,high....
        lowp=hutt_full['Point_cluster_Low'].values
        medp=hutt_full['Point_cluster_Medium'].values
        highp=hutt_full['Point_cluster_High'].values
        
        hutt_full.drop(clus_all,axis=1,inplace=True)
        hutt_full['Point_cluster_Low']=lowp
        hutt_full['Point_cluster_Medium']=medp
        hutt_full['Point_cluster_High']=highp
        #st.write("After",hutt_full.head())     
        tes=np.array(hutt_full)
        i=0
        for i in range(len(tes)):
          combo=0.3*tes[i,r_in]+ 0.59*tes[i,g_in]+ 0.11*tes[i,b_in]
          grs.append(combo)
        
        hutt_full['Greyscale']=np.array(grs)  
        hf=pd.DataFrame()
        hf=hutt_full.drop(['point_cluster','RGB_Combo'],axis=1).copy()
        #st.write(hf.head(),hf.shape)
        #start of next func
        df_poly1=get_poly(hf)
        #st.write("done1")
        spe_cols=list(rf_x.columns)
        rf_data=df_poly1[spe_cols].copy()
        #st.write("done")
        
        predi=pd.DataFrame()
        predi[str(model_name)+'_Pred']=model.predict(rf_data)
        
        if(sel_tar=='none'):
            df_preds2=pd.DataFrame(predi.copy())
            merged=pd.concat([to_merge,df_preds2],axis=1)
            merged=merged.reset_index().drop(['index'],axis=1) 
            full_river=pd.concat([full_river,merged],axis=0)
            full_river=full_river.reset_index().drop(['index'],axis=1) 

        else:
            predi[sel_tar]=jun[sel_tar].copy()
            df_preds2=pd.DataFrame(predi.copy())
            merged=pd.concat([to_merge,df_preds2],axis=1)
            merged=merged.reset_index().drop(['index'],axis=1)
            full_river=pd.concat([full_river,merged],axis=0)
            full_river=full_river.reset_index().drop(['index'],axis=1) 
       
        del merged
        del df_poly1
        del hf
        del hutt_full
        del df_preds2
        del predi
        del grs
        import gc
        gc.collect()
       # st.write("Merged",merged.tail())
       # st.write(full_river.tail())
        
# till here
  #  full_river=full_river.reset_index().drop(['index'],axis=1)
    full_river=full_river.sort_values(by=pid)
    full_river=full_river.reset_index().drop(['index'],axis=1) 
    #st.write(full_river.shape)
    st.write("Sample of the output")
    st.write(full_river.head())
    if(to_som=='Yes'):
        # to remove spikes..end smoothing algo , arguments needed= radius, threshold, to_som
        import scipy.spatial as spatial # use cKDTree to find neigbouring points 
        latlon=np.array(full_river[[east,north]].copy())
        point_tree=spatial.cKDTree(latlon)
        fr=np.array(full_river[str(model_name)+'_Pred'].copy())
        #st.write("entered into smoother")
        #now for each point find all those points withing radius from this tree
        out_ind=[]
        for i in range(len(latlon)):
            ixs=point_tree.query_ball_point(latlon[i], radius)
            avg_all=np.mean(fr[ixs])
            if(abs(fr[i]-avg_all)>threshold):
                out_ind.append(i)
    
        st.write(" Total dropped noise data points ",len(out_ind))
        not_smooth=full_river.drop(out_ind,axis=0)
        del fr
        del latlon
        
    file_loc=fn+'Predictions_rf_pretrained.csv'
    full_river.to_csv(file_loc,index=False)
    if(to_som=='Yes'):
        n_fl=fn+'Predictions_rf_pretrained_smoothed.csv'
        not_smooth.to_csv(n_fl,index=False)
    st.write("Prediction file downloaded!! Please check your local files location  - in the given folder name to access the 'Predictions_rf_pretrained.csv' or 'Predictions_rf_pretrained_smoothed.csv' ")
    del full_river
#               st.write("done 4")
    
 
def full_river_process_xgb(new_df1,r,g,b,rf_x,model,model_name,sel_tar,fn,sel_cols2,pid,east,north,to_som,radius,threshold):
    #st.write(hutt_full.head())
    full_river=pd.DataFrame()
    for jun in new_df1:
        to_merge=pd.DataFrame()
        merged=pd.DataFrame()
        st.write(" Current batch of shape ",jun.shape)
        jun=jun.reset_index().drop(['index'],axis=1) 
        hutt_full=pd.DataFrame()
        to_merge=pd.DataFrame(jun[[pid,east,north]])

        hutt_full=jun[sel_cols2].copy()
        lnbr=[]
        lnbg=[]
        #hut full has r,g,b,w only
        r_in=hutt_full.columns.get_loc(r)
        g_in=hutt_full.columns.get_loc(g)
        b_in=hutt_full.columns.get_loc(b)
        te=hutt_full.values
        for i in range(len(te)):
          val=np.log10(te[i,b_in]/te[i,r_in])
          lnbr.append(val)      
          val1=np.log10(te[i,b_in]/te[i,g_in])
          lnbg.append(val1)
              
        hutt_full['ln(b/r)']=np.array(lnbr)
        hutt_full['ln(b/g)']=np.array(lnbg)
        
        rgbc=[]
        for i in range(len(te)):
          combo=te[i,r_in]*te[i,g_in]*te[i,b_in]
          rgbc.append(combo)
        
        hutt_full['RGB_Combo']=0
        hutt_full['RGB_Combo']=np.array(rgbc)
        #load kmeans
        kmeans=joblib.load('kmeans.pkl')
        hutt_full.loc[:, 'point_cluster'] = kmeans.predict(hutt_full[[r,g,b]])
        hutt_full['point_cluster']=hutt_full['point_cluster'].astype('int64')
        mm=load_data('min_med_max.csv')
        
        max_indx=mm.groupby(['point_cluster']).mean()['RGB_Combo'][mm.groupby(['point_cluster']).mean()['RGB_Combo']==np.max(mm.groupby(['point_cluster']).mean()['RGB_Combo'])].index
        min_indx=mm.groupby(['point_cluster']).mean()['RGB_Combo'][mm.groupby(['point_cluster']).mean()['RGB_Combo']==np.min(mm.groupby(['point_cluster']).mean()['RGB_Combo'])].index
        indexs=list(mm.groupby(['point_cluster']).mean().index)
        indexs.remove(min_indx)
        indexs.remove(max_indx)
        med_indx=indexs[0]
        
        hutt_full['Point_cluster']=''
        hutt_full['Point_cluster']=np.where(hutt_full['point_cluster']==max_indx[0],'High',hutt_full['Point_cluster'])
        hutt_full['Point_cluster']=np.where(hutt_full['point_cluster']==med_indx,'Medium',hutt_full['Point_cluster'])
        hutt_full['Point_cluster']=np.where(hutt_full['point_cluster']==min_indx[0],'Low',hutt_full['Point_cluster'])
        hutt_full=hutt_full.reset_index().drop(['index'],axis=1)
        
        dn=load_data('groupby.csv')  
        #st.write(dn.head())
    
        gr=dn.columns[0]
        gg=dn.columns[1]
        gb=dn.columns[2]
        
        peak_r=dn.groupby(['Point_cluster']).max()[gr]
        peak_g=dn.groupby(['Point_cluster']).max()[gg]
        peak_b=dn.groupby(['Point_cluster']).max()[gb]
        peak_rgb=dn.groupby(['Point_cluster']).max()['RGB_Combo']
       # st.write(peak_r,peak_g,peak_b)
        
        hutt_full['Ratio_peak_R']=0.0
        hutt_full['Ratio_peak_G']=0.0
        hutt_full['Ratio_peak_B']=0.0
        hutt_full['Ratio_peak_RGBCombo']=0.0
        
        #low
        hutt_full['Ratio_peak_R']=np.where(hutt_full['Point_cluster']=='Low',hutt_full[r]/peak_r['Low'],hutt_full['Ratio_peak_R'])
        hutt_full['Ratio_peak_G']=np.where(hutt_full['Point_cluster']=='Low',hutt_full[g]/peak_g['Low'],hutt_full['Ratio_peak_G'])
        hutt_full['Ratio_peak_B']=np.where(hutt_full['Point_cluster']=='Low',hutt_full[b]/peak_b['Low'],hutt_full['Ratio_peak_B'])
        hutt_full['Ratio_peak_RGBCombo']=np.where(hutt_full['Point_cluster']=='Low',hutt_full['RGB_Combo']/peak_rgb['Low'],hutt_full['Ratio_peak_RGBCombo'])
        
        #medium
        hutt_full['Ratio_peak_R']=np.where(hutt_full['Point_cluster']=='Medium',hutt_full[r]/peak_r['Medium'],hutt_full['Ratio_peak_R'])
        hutt_full['Ratio_peak_G']=np.where(hutt_full['Point_cluster']=='Medium',hutt_full[g]/peak_g['Medium'],hutt_full['Ratio_peak_G'])
        hutt_full['Ratio_peak_B']=np.where(hutt_full['Point_cluster']=='Medium',hutt_full[b]/peak_b['Medium'],hutt_full['Ratio_peak_B'])
        hutt_full['Ratio_peak_RGBCombo']=np.where(hutt_full['Point_cluster']=='Medium',hutt_full['RGB_Combo']/peak_rgb['Medium'],hutt_full['Ratio_peak_RGBCombo'])
        
        #high
        hutt_full['Ratio_peak_R']=np.where(hutt_full['Point_cluster']=='High',hutt_full[r]/peak_r['High'],hutt_full['Ratio_peak_R'])
        hutt_full['Ratio_peak_G']=np.where(hutt_full['Point_cluster']=='High',hutt_full[g]/peak_g['High'],hutt_full['Ratio_peak_G'])
        hutt_full['Ratio_peak_B']=np.where(hutt_full['Point_cluster']=='High',hutt_full[b]/peak_b['High'],hutt_full['Ratio_peak_B'])
        hutt_full['Ratio_peak_RGBCombo']=np.where(hutt_full['Point_cluster']=='High',hutt_full['RGB_Combo']/peak_rgb['High'],hutt_full['Ratio_peak_RGBCombo'])
        
        hutt_full=pd.get_dummies(hutt_full)
        #st.write("Before",hutt_full.head())
        # to confirm if all 3 clusters are there..else add dummy column
        grs=[]
        clus_all=['Point_cluster_Low','Point_cluster_Medium','Point_cluster_High']
        fk=[]
        for c in clus_all:
            if(c not in hutt_full.columns):
                fk.append(c)
                
        for fkc in fk:
            hutt_full[fkc]=0
        # To maintain the order in low, med ,high....
        lowp=hutt_full['Point_cluster_Low'].values
        medp=hutt_full['Point_cluster_Medium'].values
        highp=hutt_full['Point_cluster_High'].values
        
        hutt_full.drop(clus_all,axis=1,inplace=True)
        hutt_full['Point_cluster_Low']=lowp
        hutt_full['Point_cluster_Medium']=medp
        hutt_full['Point_cluster_High']=highp
        #st.write("After",hutt_full.head())     
        tes=np.array(hutt_full)
        i=0
        for i in range(len(tes)):
          combo=0.3*tes[i,r_in]+ 0.59*tes[i,g_in]+ 0.11*tes[i,b_in]
          grs.append(combo)
        
        hutt_full['Greyscale']=np.array(grs)  
        hf=pd.DataFrame()
        hf=hutt_full.drop(['point_cluster','RGB_Combo'],axis=1).copy()
        #st.write(hf.head(),hf.shape)
        #start of next func
        df_poly1=get_poly(hf)
        #st.write("done1")
        spe_cols=list(rf_x.columns)
        
        rf_data=df_poly1[spe_cols].copy()
        st.write("done")
        
        predi=pd.DataFrame()
        predi[str(model_name)+'_Pred']=model.predict(rf_data)
        
        if(sel_tar=='none'):
            df_preds2=pd.DataFrame(predi.copy())
            merged=pd.concat([to_merge,df_preds2],axis=1)
            merged=merged.reset_index().drop(['index'],axis=1) 
            full_river=pd.concat([full_river,merged],axis=0)
            full_river=full_river.reset_index().drop(['index'],axis=1) 

        else:
            predi[sel_tar]=jun[sel_tar].copy()
            df_preds2=pd.DataFrame(predi.copy())
            merged=pd.concat([to_merge,df_preds2],axis=1)
            merged=merged.reset_index().drop(['index'],axis=1)
            full_river=pd.concat([full_river,merged],axis=0)
            full_river=full_river.reset_index().drop(['index'],axis=1) 

        del merged
        del df_poly1
        del hf
        del hutt_full
        del df_preds2
        del predi
        del grs
        import gc
        gc.collect()
        #if(len(full_river)>2000000):
        #   break
       # st.write("Merged",merged.tail())
       # st.write(full_river.tail())
        
# till here
  #  full_river=full_river.reset_index().drop(['index'],axis=1)
    full_river=full_river.sort_values(by=pid)
    full_river=full_river.reset_index().drop(['index'],axis=1) 
    #st.write(full_river.shape)
    st.write("Sample of the output")
    st.write(full_river.head())
    #radius=2
    #threshold=0.2
    if(to_som=='Yes'):
        # to remove spikes..end smoothing algo , arguments needed= radius, threshold, to_som
        import scipy.spatial as spatial # use cKDTree to find neigbouring points 
        latlon=np.array(full_river[[east,north]].copy())
        point_tree=spatial.cKDTree(latlon)
        fr=np.array(full_river[str(model_name)+'_Pred'].copy())
        #st.write("entered into smoother")
        #now for each point find all those points withing radius from this tree
        out_ind=[]
        for i in range(len(latlon)):
            ixs=point_tree.query_ball_point(latlon[i], radius)
            avg_all=np.mean(fr[ixs])
            if(abs(fr[i]-avg_all)>threshold):
                out_ind.append(i)
    
        st.write(" Total dropped noise data points ",len(out_ind))
        not_smooth=full_river.drop(out_ind,axis=0)
        del fr
        del latlon
    
    file_loc=fn+'Predictions_xgb_pretrained.csv'
    full_river.to_csv(file_loc,index=False)
    if(to_som=='Yes'):
        n_fl=fn+'Predictions_xgb_pretrained_smoothed.csv'
        not_smooth.to_csv(n_fl,index=False)
    st.write("Prediction file downloaded!! Please check your local files location  - in the given folder name to access the 'Predictions_xgb_pretrained.csv' or 'Predictions_xgb_pretrained_smoothed.csv' ")
    del full_river

def full_river_process_nn(new_df1,r,g,b,nn_x,model,model_name,sel_tar,fn,sel_cols2,pid,east,north,to_som,radius,threshold):
    full_river=pd.DataFrame()
    for jun in new_df1:
        to_merge=pd.DataFrame()
        merged=pd.DataFrame()
        st.write(" Current batch of shape ",jun.shape)
        jun=jun.reset_index().drop(['index'],axis=1) 
        hutt_full=pd.DataFrame()
        to_merge=pd.DataFrame(jun[[pid,east,north]])

        hutt_full=jun[sel_cols2].copy()
        lnbr=[]
        lnbg=[]
        #hut full has r,g,b,w only
        r_in=hutt_full.columns.get_loc(r)
        g_in=hutt_full.columns.get_loc(g)
        b_in=hutt_full.columns.get_loc(b)
        te=hutt_full.values
        for i in range(len(te)):
          val=np.log10(te[i,b_in]/te[i,r_in])
          lnbr.append(val)      
          val1=np.log10(te[i,b_in]/te[i,g_in])
          lnbg.append(val1)
              
        hutt_full['ln(b/r)']=np.array(lnbr)
        hutt_full['ln(b/g)']=np.array(lnbg)
        
        rgbc=[]
        for i in range(len(te)):
          combo=te[i,r_in]*te[i,g_in]*te[i,b_in]
          rgbc.append(combo)
        
        hutt_full['RGB_Combo']=0
        hutt_full['RGB_Combo']=np.array(rgbc)
        #load kmeans
        kmeans=joblib.load('kmeans.pkl')
        hutt_full.loc[:, 'point_cluster'] = kmeans.predict(hutt_full[[r,g,b]])
        hutt_full['point_cluster']=hutt_full['point_cluster'].astype('int64')
        mm=load_data('min_med_max.csv')
        
        max_indx=mm.groupby(['point_cluster']).mean()['RGB_Combo'][mm.groupby(['point_cluster']).mean()['RGB_Combo']==np.max(mm.groupby(['point_cluster']).mean()['RGB_Combo'])].index
        min_indx=mm.groupby(['point_cluster']).mean()['RGB_Combo'][mm.groupby(['point_cluster']).mean()['RGB_Combo']==np.min(mm.groupby(['point_cluster']).mean()['RGB_Combo'])].index
        indexs=list(mm.groupby(['point_cluster']).mean().index)
        indexs.remove(min_indx)
        indexs.remove(max_indx)
        med_indx=indexs[0]
        
        hutt_full['Point_cluster']=''
        hutt_full['Point_cluster']=np.where(hutt_full['point_cluster']==max_indx[0],'High',hutt_full['Point_cluster'])
        hutt_full['Point_cluster']=np.where(hutt_full['point_cluster']==med_indx,'Medium',hutt_full['Point_cluster'])
        hutt_full['Point_cluster']=np.where(hutt_full['point_cluster']==min_indx[0],'Low',hutt_full['Point_cluster'])
        hutt_full=hutt_full.reset_index().drop(['index'],axis=1)
        
        dn=load_data('groupby.csv')  
        #st.write(dn.head())
    
        gr=dn.columns[0]
        gg=dn.columns[1]
        gb=dn.columns[2]
        
        peak_r=dn.groupby(['Point_cluster']).max()[gr]
        peak_g=dn.groupby(['Point_cluster']).max()[gg]
        peak_b=dn.groupby(['Point_cluster']).max()[gb]
        peak_rgb=dn.groupby(['Point_cluster']).max()['RGB_Combo']
       # st.write(peak_r,peak_g,peak_b)
        
        hutt_full['Ratio_peak_R']=0.0
        hutt_full['Ratio_peak_G']=0.0
        hutt_full['Ratio_peak_B']=0.0
        hutt_full['Ratio_peak_RGBCombo']=0.0
        
        #low
        hutt_full['Ratio_peak_R']=np.where(hutt_full['Point_cluster']=='Low',hutt_full[r]/peak_r['Low'],hutt_full['Ratio_peak_R'])
        hutt_full['Ratio_peak_G']=np.where(hutt_full['Point_cluster']=='Low',hutt_full[g]/peak_g['Low'],hutt_full['Ratio_peak_G'])
        hutt_full['Ratio_peak_B']=np.where(hutt_full['Point_cluster']=='Low',hutt_full[b]/peak_b['Low'],hutt_full['Ratio_peak_B'])
        hutt_full['Ratio_peak_RGBCombo']=np.where(hutt_full['Point_cluster']=='Low',hutt_full['RGB_Combo']/peak_rgb['Low'],hutt_full['Ratio_peak_RGBCombo'])
        
        #medium
        hutt_full['Ratio_peak_R']=np.where(hutt_full['Point_cluster']=='Medium',hutt_full[r]/peak_r['Medium'],hutt_full['Ratio_peak_R'])
        hutt_full['Ratio_peak_G']=np.where(hutt_full['Point_cluster']=='Medium',hutt_full[g]/peak_g['Medium'],hutt_full['Ratio_peak_G'])
        hutt_full['Ratio_peak_B']=np.where(hutt_full['Point_cluster']=='Medium',hutt_full[b]/peak_b['Medium'],hutt_full['Ratio_peak_B'])
        hutt_full['Ratio_peak_RGBCombo']=np.where(hutt_full['Point_cluster']=='Medium',hutt_full['RGB_Combo']/peak_rgb['Medium'],hutt_full['Ratio_peak_RGBCombo'])
        
        #high
        hutt_full['Ratio_peak_R']=np.where(hutt_full['Point_cluster']=='High',hutt_full[r]/peak_r['High'],hutt_full['Ratio_peak_R'])
        hutt_full['Ratio_peak_G']=np.where(hutt_full['Point_cluster']=='High',hutt_full[g]/peak_g['High'],hutt_full['Ratio_peak_G'])
        hutt_full['Ratio_peak_B']=np.where(hutt_full['Point_cluster']=='High',hutt_full[b]/peak_b['High'],hutt_full['Ratio_peak_B'])
        hutt_full['Ratio_peak_RGBCombo']=np.where(hutt_full['Point_cluster']=='High',hutt_full['RGB_Combo']/peak_rgb['High'],hutt_full['Ratio_peak_RGBCombo'])
        
        hutt_full=pd.get_dummies(hutt_full)
        #st.write("Before",hutt_full.head())
        # to confirm if all 3 clusters are there..else add dummy column
        grs=[]
        clus_all=['Point_cluster_Low','Point_cluster_Medium','Point_cluster_High']
        fk=[]
        for c in clus_all:
            if(c not in hutt_full.columns):
                fk.append(c)
                
        for fkc in fk:
            hutt_full[fkc]=0
        # To maintain the order in low, med ,high....
        lowp=hutt_full['Point_cluster_Low'].values
        medp=hutt_full['Point_cluster_Medium'].values
        highp=hutt_full['Point_cluster_High'].values
        
        hutt_full.drop(clus_all,axis=1,inplace=True)
        hutt_full['Point_cluster_Low']=lowp
        hutt_full['Point_cluster_Medium']=medp
        hutt_full['Point_cluster_High']=highp
        #st.write("After",hutt_full.head())     
        tes=np.array(hutt_full)
        i=0
        for i in range(len(tes)):
          combo=0.3*tes[i,r_in]+ 0.59*tes[i,g_in]+ 0.11*tes[i,b_in]
          grs.append(combo)
        
        hutt_full['Greyscale']=np.array(grs)  
        hf=pd.DataFrame()
        hf=hutt_full.drop(['point_cluster','RGB_Combo'],axis=1).copy()
        #st.write(hf.head(),hf.shape)
        
        #start of next func
        df_poly1=get_poly(hf)
        
        # give rf_x coz, sc array wont have feature names
        spe_cols=list(nn_x.columns)
        #st.write(spe_cols)
        #st.write(df_poly1.head())
        
        pred_data=df_poly1[spe_cols].copy()
        #st.write("one")
        sc=joblib.load('sc_nn_full.pkl')
        
        nn_data=sc.transform(pred_data)
        predi=pd.DataFrame()
        predi[str(model_name)+'_Pred']=model.predict(nn_data)
        
        if(sel_tar=='none'):
            df_preds2=pd.DataFrame(predi.copy())
            merged=pd.concat([to_merge,df_preds2],axis=1)
            merged=merged.reset_index().drop(['index'],axis=1) 
            full_river=pd.concat([full_river,merged],axis=0)
            full_river=full_river.reset_index().drop(['index'],axis=1) 

        else:
            predi[sel_tar]=jun[sel_tar].copy()
            df_preds2=pd.DataFrame(predi.copy())
            merged=pd.concat([to_merge,df_preds2],axis=1)
            merged=merged.reset_index().drop(['index'],axis=1)
            full_river=pd.concat([full_river,merged],axis=0)
            full_river=full_river.reset_index().drop(['index'],axis=1) 
        
        del merged
        del df_poly1
        del nn_data
        del pred_data
        del hf
        del hutt_full
        del df_preds2
        del predi
        del grs
        import gc
        gc.collect()
       # st.write("Merged",merged.tail())
       # st.write(full_river.tail())
        
# till here
  #  full_river=full_river.reset_index().drop(['index'],axis=1)
    full_river=full_river.sort_values(by=pid)
    full_river=full_river.reset_index().drop(['index'],axis=1) 
    #st.write(full_river.shape)
    st.write("Sample of the output")
    st.write(full_river.head())
    if(to_som=='Yes'):
        # to remove spikes..end smoothing algo , arguments needed= radius, threshold, to_som
        import scipy.spatial as spatial # use cKDTree to find neigbouring points 
        latlon=np.array(full_river[[east,north]].copy())
        point_tree=spatial.cKDTree(latlon)
        fr=np.array(full_river[str(model_name)+'_Pred'].copy())
        #st.write("entered into smoother")
        #now for each point find all those points withing radius from this tree
        out_ind=[]
        for i in range(len(latlon)):
            ixs=point_tree.query_ball_point(latlon[i], radius)
            avg_all=np.mean(fr[ixs])
            if(abs(fr[i]-avg_all)>threshold):
                out_ind.append(i)
    
        st.write(" Total dropped noise data points ",len(out_ind))
        not_smooth=full_river.drop(out_ind,axis=0)
        del fr
        del latlon
        
    file_loc=fn+'Predictions_nn_pretrained.csv'
    full_river.to_csv(file_loc,index=False)
    if(to_som=='Yes'):
        n_fl=fn+'Predictions_nn_pretrained_smoothed.csv'
        not_smooth.to_csv(n_fl,index=False)
    st.write("Prediction file downloaded!! Please check your local files location  - in the given folder name to access the 'Predictions_nn_pretrained.csv' or 'Predictions_nn_pretrained_smoothed.csv' ")
    del full_river
    
              
#@st.cache(persist=True, suppress_st_warning=True,allow_output_mutation=True)
def get_poly(new_data):
    #st.write('done')
    pcl=new_data[['Point_cluster_Low','Point_cluster_Medium','Point_cluster_High']].copy()
    new_data.drop(['Point_cluster_Low','Point_cluster_Medium','Point_cluster_High'],axis=1,inplace=True)
    poly = PolynomialFeatures(2)
    x_poly=poly.fit_transform(new_data)
    poly_names=poly.get_feature_names(['Feature'+str(l) for l in range(1,len(np.array(poly.get_feature_names())))])
    df_poly1=pd.DataFrame(x_poly,columns=poly_names)
    to_send=df_poly1.drop(['1'],axis=1)
    #append the clusters
    to_send=pd.concat([to_send,pcl],axis=1)
    
    return to_send 
                                         
############################ ML ALGO DEFS####################################################

#@st.cache(persist=True, suppress_st_warning=True)
def model_pred(model,totrain,predictions_all):
    y_pred_lr_all=model.predict(totrain)
    col1=predictions_all.columns.tolist()[0]
  #  predictions_test['LR_test_Pred']=y_pred_lr
    predictions_all['MODEL_full_Pred']=y_pred_lr_all
  #  st.write("RMSE of linear regression on TEST dataset : ",np.sqrt(mse(predictions_test[col1],predictions_test['LR_test_Pred'])))
   # st.write("R2 Score of linear regression on TEST dataset :",r2(predictions_test[col1],predictions_test['LR_test_Pred']))    
    r2s=r2_score(predictions_all[col1],predictions_all['MODEL_full_Pred'])
    rmses=np.sqrt(mse(predictions_all[col1],predictions_all['MODEL_full_Pred']))
    maes=mean_absolute_error(predictions_all[col1],predictions_all['MODEL_full_Pred'])
    evars=explained_variance_score(predictions_all[col1],predictions_all['MODEL_full_Pred'])
    maxs=max_error(predictions_all[col1],predictions_all['MODEL_full_Pred'])
   # psds=mean_poisson_deviance(predictions_all[col1],predictions_all['LR_full_Pred'])

    tmp=[]
    tmp.append([rmses,maes,r2s,maxs,evars])
    final_scores_test=pd.DataFrame(tmp,columns=['Root Mean Squared Error(RMSE)','Mean Absolute Error(MAE)','R2 Spearman Coefficient of Determination','Maximum Residual Error','Explained variance Score'],index=['MODEL'])    

    return predictions_all,final_scores_test

@st.cache(persist=True, suppress_st_warning=True,allow_output_mutation=True)
def test_result(model,x_test,y_test,model_name):   
        #full dataset results
    predictions_all=pd.DataFrame(y_test)     
    y_pred_model=model.predict(x_test)
    col1=predictions_all.columns.tolist()[0]
  #  predictions_test['LR_test_Pred']=y_pred_lr
    predictions_all['Model_full_Pred']=y_pred_model
  #  st.write("RMSE of linear regression on TEST dataset : ",np.sqrt(mse(predictions_test[col1],predictions_test['LR_test_Pred'])))
   # st.write("R2 Score of linear regression on TEST dataset :",r2(predictions_test[col1],predictions_test['LR_test_Pred']))    
    r2s=r2_score(predictions_all[col1],predictions_all['Model_full_Pred'])
    rmses=np.sqrt(mse(predictions_all[col1],predictions_all['Model_full_Pred']))
    maes=mean_absolute_error(predictions_all[col1],predictions_all['Model_full_Pred'])
    evars=explained_variance_score(predictions_all[col1],predictions_all['Model_full_Pred'])
    maxs=max_error(predictions_all[col1],predictions_all['Model_full_Pred'])
   # psds=mean_poisson_deviance(predictions_all[col1],predictions_all['LR_full_Pred'])

    tmp=[]
    tmp.append([rmses,maes,r2s,maxs,evars])
    final_scores_test=pd.DataFrame(tmp,columns=['Validation_Root Mean Squared Error(RMSE)','Validation_Mean Absolute Error(MAE)','Validation_R2 Spearman Coefficient of Determination','Validation_Maximum Residual Error','Validation_Explained variance Score'],index=[model_name])    

    return final_scores_test

@st.cache(persist=True, suppress_st_warning=True,allow_output_mutation=True)
def LR_reg(totrain,x_train,x_test,y_train,y_test,predictions_all):
    lr=LinearRegression()
    lr.fit(x_train,y_train)    
    #y_pred_lr=lr.predict(x_test)    
    #full dataset results
    y_pred_lr_all=lr.predict(totrain)
    col1=predictions_all.columns.tolist()[0]
  #  predictions_test['LR_test_Pred']=y_pred_lr
    predictions_all['LR_full_Pred']=y_pred_lr_all
  #  st.write("RMSE of linear regression on TEST dataset : ",np.sqrt(mse(predictions_test[col1],predictions_test['LR_test_Pred'])))
   # st.write("R2 Score of linear regression on TEST dataset :",r2(predictions_test[col1],predictions_test['LR_test_Pred']))    
    r2s=r2_score(predictions_all[col1],predictions_all['LR_full_Pred'])
    rmses=np.sqrt(mse(predictions_all[col1],predictions_all['LR_full_Pred']))
    maes=mean_absolute_error(predictions_all[col1],predictions_all['LR_full_Pred'])
    evars=explained_variance_score(predictions_all[col1],predictions_all['LR_full_Pred'])
    maxs=max_error(predictions_all[col1],predictions_all['LR_full_Pred'])
   # psds=mean_poisson_deviance(predictions_all[col1],predictions_all['LR_full_Pred'])

    tmp=[]
    tmp.append([rmses,maes,r2s,maxs,evars])
    final_scores_test=pd.DataFrame(tmp,columns=['Root Mean Squared Error(RMSE)','Mean Absolute Error(MAE)','R2 Spearman Coefficient of Determination','Maximum Residual Error','Explained variance Score'],index=['Linear Regression'])    
    
    final_test=test_result(lr,x_test,y_test,'Linear Regression')
    return lr,predictions_all,final_scores_test,final_test

@st.cache(persist=True, suppress_st_warning=True,allow_output_mutation=True)                    
def XGB_reg(totrain,x_train,x_test,y_train,y_test,predictions_all):
    eval_set = [(x_test, y_test)]
    # st.write("s")
    # A parameter grid for XGBoost
    params = {
            'min_child_weight': [6,7,8],
            'subsample': [0.7,0.5],
            'max_depth': [5,7,8],
            'eta':[0.05 , 0.03 ]
            
            }
    
    # magic with cv
    from sklearn.model_selection import GridSearchCV 

    xgb = GridSearchCV(XGBRegressor(n_estimators=500,early_stopping_rounds=10, eval_metric='mae', eval_set=eval_set,random_state=10), params, refit = True, verbose = 3) 
    #st.write(xgb.best_estimators_)
    #st.write(xgb.best_params_)
    #st.write(xgb.best_score_)
    #xgb=XGBRegressor(n_estimators=500,early_stopping_rounds=10, eval_metric="mae", eval_set=eval_set,random_state=10,max_depth=7, min_child_weight=6,subsample=0.7,eta=0.03)
    xgb.fit(x_train,y_train)    
    # y_pred_xgb=xgb.predict(x_test)       
    y_pred_xgb_all=xgb.predict(totrain)
   # predictions_test['XGB_test_Pred']=y_pred_xgb
    predictions_all['XGB_full_Pred']=y_pred_xgb_all
    col1=predictions_all.columns.tolist()[0]

    r2s=r2_score(predictions_all[col1],predictions_all['XGB_full_Pred'])
    rmses=np.sqrt(mse(predictions_all[col1],predictions_all['XGB_full_Pred']))
    maes=mean_absolute_error(predictions_all[col1],predictions_all['XGB_full_Pred'])
    evars=explained_variance_score(predictions_all[col1],predictions_all['XGB_full_Pred'])
    maxs=max_error(predictions_all[col1],predictions_all['XGB_full_Pred'])
   # psds=mean_poisson_deviance(predictions_all[col1],predictions_all['XGB_full_Pred'])

    tmp=[]
    tmp.append([rmses,maes,r2s,maxs,evars])
    final_scores_test=pd.DataFrame(tmp,columns=['Root Mean Squared Error(RMSE)','Mean Absolute Error(MAE)','R2 Spearman Coefficient of Determination','Maximum Residual Error','Explained variance Score'],index=['XGB Regressor'])    
    
   # final_test=test_result(xgb,x_test,y_test,'XGBoost')
    
    predictions_all2=pd.DataFrame(y_test)     
    y_pred_model=xgb.predict(x_test)
    col1=predictions_all2.columns.tolist()[0]
  #  predictions_test['LR_test_Pred']=y_pred_lr
    predictions_all2['Model_full_Pred']=y_pred_model
  #  st.write("RMSE of linear regression on TEST dataset : ",np.sqrt(mse(predictions_test[col1],predictions_test['LR_test_Pred'])))
   # st.write("R2 Score of linear regression on TEST dataset :",r2(predictions_test[col1],predictions_test['LR_test_Pred']))    
    r2s=r2_score(predictions_all2[col1],predictions_all2['Model_full_Pred'])
    rmses=np.sqrt(mse(predictions_all2[col1],predictions_all2['Model_full_Pred']))
    maes=mean_absolute_error(predictions_all2[col1],predictions_all2['Model_full_Pred'])
    evars=explained_variance_score(predictions_all2[col1],predictions_all2['Model_full_Pred'])
    maxs=max_error(predictions_all2[col1],predictions_all2['Model_full_Pred'])
   # psds=mean_poisson_deviance(predictions_all[col1],predictions_all['LR_full_Pred'])

    tmp1=[]
    tmp1.append([rmses,maes,r2s,maxs,evars])
    final_test=pd.DataFrame(tmp1,columns=['Validation_Root Mean Squared Error(RMSE)','Validation_Mean Absolute Error(MAE)','Validation_R2 Spearman Coefficient of Determination','Validation_Maximum Residual Error','Validation_Explained variance Score'],index=['XGB Regressor'])    
    
    return xgb,predictions_all,final_scores_test,final_test

@st.cache(persist=True, suppress_st_warning=True,allow_output_mutation=True)
def RF_reg(totrain,x_train,x_test,y_train,y_test,predictions_all):
    rf=RandomForestRegressor(random_state=10)
    rf.fit(x_train,y_train)   
   # y_pred_rf=rf.predict(x_test)       
    y_pred_rf_all=rf.predict(totrain)
   # predictions_test['RF_test_Pred']=y_pred_rf
    predictions_all['RF_full_Pred']=y_pred_rf_all
    col1=predictions_all.columns.tolist()[0]

    r2s=r2_score(predictions_all[col1],predictions_all['RF_full_Pred'])
    rmses=np.sqrt(mse(predictions_all[col1],predictions_all['RF_full_Pred']))
    maes=mean_absolute_error(predictions_all[col1],predictions_all['RF_full_Pred'])
    evars=explained_variance_score(predictions_all[col1],predictions_all['RF_full_Pred'])
    maxs=max_error(predictions_all[col1],predictions_all['RF_full_Pred'])
  #  psds=mean_poisson_deviance(predictions_all[col1],predictions_all['RF_full_Pred'])
    
    tmp=[]
    tmp.append([rmses,maes,r2s,maxs,evars])
    final_scores_test=pd.DataFrame(tmp,columns=['Root Mean Squared Error(RMSE)','Mean Absolute Error(MAE)','Spearman Coefficient of Determination','Maximum Residual Error','Explained variance Score'],index=['Random Forest Regression'])    
    final_test=test_result(rf,x_test,y_test,'Random Forest Regression')
 
    return rf,predictions_all,final_scores_test,final_test
                    
@st.cache(persist=True, suppress_st_warning=True,allow_output_mutation=True)
def NN_reg(totrain,x_train,x_test,y_train,y_test,predictions_all):
    ann=MLPRegressor(hidden_layer_sizes=(500,),activation='relu', solver='adam', alpha=0.001, batch_size='auto',learning_rate='constant', learning_rate_init=0.01, power_t=0.5, max_iter=1000, shuffle=True, random_state=9, tol=0.0001, verbose=False, warm_start=False, momentum=0.9, nesterovs_momentum=True,early_stopping=True, validation_fraction=0.1, beta_1=0.9, beta_2=0.999, epsilon=1e-08)
    ann.fit(x_train,y_train)
    # y_pred_rf=rf.predict(x_test)       
    y_pred_dt_all=ann.predict(totrain)
   # predictions_test['RF_test_Pred']=y_pred_rf
    predictions_all['NN_full_Pred']=y_pred_dt_all
    col1=predictions_all.columns.tolist()[0]

    r2s=r2_score(predictions_all[col1],predictions_all['NN_full_Pred'])
    rmses=np.sqrt(mse(predictions_all[col1],predictions_all['NN_full_Pred']))
    maes=mean_absolute_error(predictions_all[col1],predictions_all['NN_full_Pred'])
    evars=explained_variance_score(predictions_all[col1],predictions_all['NN_full_Pred'])
    maxs=max_error(predictions_all[col1],predictions_all['NN_full_Pred'])
   # psds=mean_poisson_deviance(predictions_all[col1],predictions_all['NN_full_Pred'])
  
    tmp=[]
    tmp.append([rmses,maes,r2s,maxs,evars])
    final_scores_test=pd.DataFrame(tmp,columns=['Root Mean Squared Error(RMSE)','Mean Absolute Error(MAE)','R2 Spearman Coefficient of Determination','Maximum Residual Error','Explained variance Score'],index=['Neural Network'])    
    final_test=test_result(ann,x_test,y_test,'Neural Network')
    
    return ann,predictions_all,final_scores_test,final_test
                    
@st.cache(persist=True, suppress_st_warning=True,allow_output_mutation=True)                    
def SVR_reg(totrain,x_train,x_test,y_train,y_test,predictions_all):
    svr=SVR(kernel='rbf',C=10)
    #grid.fit(x_train,y_train)   
    #from sklearn.model_selection import GridSearchCV 
  
    # defining parameter range 
    #param_grid = {'C': [0.1, 1, 10, 100],  
    #              'gamma': [1, 0.1, 0.01, 0.001], 
    #              'kernel': ['rbf']}  
      
     
    # fitting the model for grid search 
    svr.fit(x_train, y_train) 
    
   # y_pred_rf=rf.predict(x_test)       
    y_pred_svr_all=svr.predict(totrain)
   
   # predictions_test['RF_test_Pred']=y_pred_rf
    predictions_all['SVR_full_Pred']=y_pred_svr_all
    
    col1=predictions_all.columns.tolist()[0]

    r2s=r2_score(predictions_all[col1],predictions_all['SVR_full_Pred'])
    rmses=np.sqrt(mse(predictions_all[col1],predictions_all['SVR_full_Pred']))
    maes=mean_absolute_error(predictions_all[col1],predictions_all['SVR_full_Pred'])
    evars=explained_variance_score(predictions_all[col1],predictions_all['SVR_full_Pred'])
    maxs=max_error(predictions_all[col1],predictions_all['SVR_full_Pred'])
    #psds=mean_poisson_deviance(predictions_all[col1],predictions_all['SVR_full_Pred'])
    
    tmp=[]
    tmp.append([rmses,maes,r2s,maxs,evars])
    final_scores_test=pd.DataFrame(tmp,columns=['Root Mean Squared Error(RMSE)','Mean Absolute Error(MAE)','R2 Spearman Coefficient of Determination','Maximum Residual Error','Explained variance Score'],index=['Support Vector Regression'])    
    final_test=test_result(svr,x_test,y_test,'Support Vector Regression')
 
    return svr,predictions_all,final_scores_test,final_test
     

def viz_model_insights(model,train,y):
    dataviz_choice1=st.sidebar.selectbox("Choose Vizualization for Model-Explainability ",['None','Permutation Importance','Partial Dependency Plots'],key='7999')
    if dataviz_choice1=='Permutation Importance':
        st.subheader("Permutation Importance")
        
        def r2(model, x_train, y_train):
            return r2_score(y_train, model.predict(x_train))
        
        perm_imp_rfpimp=permutation_importances(model, train, y, r2) 
        vals=perm_imp_rfpimp.values.reshape(1,-1)[0]
        feas=perm_imp_rfpimp.index.values.reshape(1,-1)[0]
        figs=px.bar(x=vals,y=feas, orientation='h')#full
        st.plotly_chart(figs)
        st.subheader("This approach directly measures feature importance by observing how random re-shuffling (thus preserving the distribution of the variable) of different rows influences the model performance. Longer the bar, higher is its importance in deciding the Depth, calculated through permutation")
        
    if dataviz_choice1=='Partial Dependency Plots':
        fes=[]
        for i in train.columns.tolist():
            fes.append(i)
     
        #var_choice1=st.radio("Choose feature ",fes,key='601')
       # var_choice1=st.selectbox("Choose most important feature for PDP plot",train.columns.tolist(),key='6103')
        my_plots= plot_partial_dependence(model,features=[fes[0]],X=train,feature_names=train.columns.tolist(),grid_resolution=10)
        fig=plt.show(my_plots)
        st.pyplot(fig)
        st.subheader("The partial dependence plot (short PDP) shows the marginal effect of one or two features have on the predicted outcome of a machine learning model. One way to interpret this plot is, if the trend in Y axis rises,for every change in the values along X-axis, the odds that the depth value is high, also increases. ")
                
   # if dataviz_choice1=='SHAP Advanced feature-insights':
    #    if(model==dt | model==rf | model==xgb):
     #       explainer=shap.TreeExplainer(model)
      #      sv=explainer.shap_values(train)
            #long computation
       #     fgg=shap.summary_plot(sv,train)
        #    pfs=plt.show(fgg)
         #   st.pyplot(pfs)
          #  st.write("Interpretation info: The goal of SHAP is to explain the prediction of an instance x by computing the contribution of each feature to the prediction. SHAP dependence plots are an alternative to partial dependence plots and accumulated local effects. While PDP and ALE plot show average effects, SHAP dependence also shows the variance on the y-axis. Especially in case of interactions, the SHAP dependence plot will be much more dispersed in the y-axis. Each position on the x-axis is an instance of the data. Red SHAP values increase the prediction, blue values decrease it.")
        #else:
         #   st.subheader("This advanced feature insight is only applicable for Random forest, Decision Tree and XGBoost and other tree based machine learning algotihms")
        
def viz_model_insights_all(lr,dt,rf,xgb,svr,train,y):
    dataviz_choice1=st.sidebar.selectbox("Choose Vizualization for Model-Explainability ",['None','Permutation Importance','Partial Dependency Plots','SHAP Advanced feature-insights'],key='7999')
    if dataviz_choice1=='Permutation Importance':
        alg=st.selectbox("Select a ML model: ",['None','Linear Regression','Decision Tree','Random Forest','XGBoost','Support Vector Machines(SVR)'])
        if(alg=='Linear Regression'):                   
            st.subheader("Permutation Importance")
            def r2(lr, x_train, y_train):
                return r2_score(y_train, lr.predict(x_train))
    
            perm_imp_rfpimp=permutation_importances(lr, train, y, r2) 
            vals=perm_imp_rfpimp.values.reshape(1,-1)[0]
            feas=perm_imp_rfpimp.index.values.reshape(1,-1)[0]
            figs=px.bar(x=vals,y=feas, orientation='h')#full
            st.plotly_chart(figs)
            st.subheader("Interpretation info: The above graph shows an approach that directly measures feature importance by observing how random re-shuffling (thus preserving the distribution of the variable) of different rows influences the model performance. Longer the bar, higher is its importance in deciding the Depth, calculated through permutation")

        if(alg=='Decision Tree'):                   
            st.subheader("Permutation Importance")
            def r2(dt, x_train, y_train):
                return r2_score(y_train, dt.predict(x_train))
    
            perm_imp_rfpimp=permutation_importances(dt, train, y, r2) 
            vals=perm_imp_rfpimp.values.reshape(1,-1)[0]
            feas=perm_imp_rfpimp.index.values.reshape(1,-1)[0]
            figs=px.bar(x=vals,y=feas, orientation='h')#full
            st.plotly_chart(figs)
            st.subheader("Interpretation info: The above graph shows an approach that directly measures feature importance by observing how random re-shuffling (thus preserving the distribution of the variable) of different rows influences the model performance. Longer the bar, higher is its importance in deciding the Depth, calculated through permutation")

        if(alg=='Random Forest'):                   
            st.subheader("Permutation Importance")
            def r2(rf, x_train, y_train):
                return r2_score(y_train, rf.predict(x_train))
    
            perm_imp_rfpimp=permutation_importances(rf, train, y, r2) 
            vals=perm_imp_rfpimp.values.reshape(1,-1)[0]
            feas=perm_imp_rfpimp.index.values.reshape(1,-1)[0]
            figs=px.bar(x=vals,y=feas, orientation='h')#full
            st.plotly_chart(figs)
            st.subheader("Interpretation info: The above graph shows an approach that directly measures feature importance by observing how random re-shuffling (thus preserving the distribution of the variable) of different rows influences the model performance. Longer the bar, higher is its importance in deciding the Depth, calculated through permutation")

        if(alg=='XGBoost'):                   
            st.subheader("Permutation Importance")
            def r2(xgb, x_train, y_train):
                return r2_score(y_train, xgb.predict(x_train))
    
            perm_imp_rfpimp=permutation_importances(xgb, train, y, r2) 
            vals=perm_imp_rfpimp.values.reshape(1,-1)[0]
            feas=perm_imp_rfpimp.index.values.reshape(1,-1)[0]
            figs=px.bar(x=vals,y=feas, orientation='h')#full
            st.plotly_chart(figs)
            st.subheader("Interpretation info: The above graph shows an approach that directly measures feature importance by observing how random re-shuffling (thus preserving the distribution of the variable) of different rows influences the model performance. Longer the bar, higher is its importance in deciding the Depth, calculated through permutation")

        if(alg=='Support Vector Machines(SVR)'):                   
            st.subheader("Permutation Importance")
            def r2(svr, x_train, y_train):
                return r2_score(y_train, svr.predict(x_train))
    
            perm_imp_rfpimp=permutation_importances(svr, train, y, r2) 
            vals=perm_imp_rfpimp.values.reshape(1,-1)[0]
            feas=perm_imp_rfpimp.index.values.reshape(1,-1)[0]
            figs=px.bar(x=vals,y=feas, orientation='h')#full
            st.plotly_chart(figs)
            st.subheader("Interpretation info: The above graph shows an approach that directly measures feature importance by observing how random re-shuffling (thus preserving the distribution of the variable) of different rows influences the model performance. Longer the bar, higher is its importance in deciding the Depth, calculated through permutation")

            
    if dataviz_choice1=='Partial Dependency Plots':
        fes=[]
        for i in train.columns.tolist():
            fes.append(i)  
        alg=st.selectbox("Select a ML model: ",['None','Linear Regression','Decision Tree','Random Forest','XGBoost','Support Vector Machines(SVR)'])
        if(alg=='Linear Regression'):                           
#            var_choice1=st.selectbox("Choose most important feature for PDP plot",train.columns.tolist(),key='6103')
            my_plots= plot_partial_dependence(lr,features=[fes[0]],X=train,feature_names=train.columns.tolist(),grid_resolution=10)
            fig=plt.show(my_plots)
            st.pyplot(fig)
            st.subheader("Interpretation info: The partial dependence plot (short PDP) shows the marginal effect of some features have on the predicted outcome of a machine learning model. One way to interpret this plot is, if the trend in Y axis rises,for every change in the values along X-axis, the odds that the depth value is high, also increases. ")

        if(alg=='Decision Tree'):                           
  #          var_choice1=st.selectbox("Choose most important feature for PDP plot",train.columns.tolist(),key='6103')

            my_plots= plot_partial_dependence(dt,features=[fes[0]],X=train,feature_names=train.columns.tolist(),grid_resolution=10)
            fig=plt.show(my_plots)
            st.pyplot(fig)
            st.subheader("Interpretation info: The partial dependence plot (short PDP) shows the marginal effect of some features have on the predicted outcome of a machine learning model. One way to interpret this plot is, if the trend in Y axis rises,for every change in the values along X-axis, the odds that the depth value is high, also increases. ")

        if(alg=='Random Forest'):                           
            my_plots= plot_partial_dependence(rf,features=[fes[0]],X=train,feature_names=train.columns.tolist(),grid_resolution=10)
            fig=plt.show(my_plots)
            st.pyplot(fig)
            st.subheader("Interpretation info: The partial dependence plot (short PDP) shows the marginal effect of some features have on the predicted outcome of a machine learning model. One way to interpret this plot is, if the trend in Y axis rises,for every change in the values along X-axis, the odds that the depth value is high, also increases. ")
 
        if(alg=='XGBoost'):                           
            my_plots= plot_partial_dependence(rf,features=[fes[0]],X=train,feature_names=train.columns.tolist(),grid_resolution=10)
            fig=plt.show(my_plots)
            st.pyplot(fig)
            st.subheader("Interpretation info: The partial dependence plot (short PDP) shows the marginal effect of some features have on the predicted outcome of a machine learning model. One way to interpret this plot is, if the trend in Y axis rises,for every change in the values along X-axis, the odds that the depth value is high, also increases. ")

        if(alg=='Support Vector Machines(SVR)'):                           
            my_plots= plot_partial_dependence(svr,features=[fes[0]],X=train,feature_names=train.columns.tolist(),grid_resolution=10)
            fig=plt.show(my_plots)
            st.pyplot(fig)
            st.subheader("Interpretation info: The partial dependence plot (short PDP) shows the marginal effect of some features have on the predicted outcome of a machine learning model. One way to interpret this plot is, if the trend in Y axis rises,for every change in the values along X-axis, the odds that the depth value is high, also increases. ")
               
    if dataviz_choice1=='SHAP Advanced feature-insights':
        alg=st.selectbox("Select a ML model: ",['None','Decision Tree','Random Forest','XGBoost'])
        if(alg=='Decision Tree'):        
            explainer=shap.TreeExplainer(dt)
            sv=explainer.shap_values(train)
            #long computation
            fgg=shap.summary_plot(sv,train)
            pfs=plt.show(fgg)
            st.pyplot(pfs)
            st.write("Interpretation info: The goal of SHAP is to explain the prediction of an instance x by computing the contribution of each feature to the prediction. SHAP dependence plots are an alternative to partial dependence plots and accumulated local effects. While PDP and ALE plot show average effects, SHAP dependence also shows the variance on the y-axis. Especially in case of interactions, the SHAP dependence plot will be much more dispersed in the y-axis. Each position on the x-axis is an instance of the data. Red SHAP values increase the prediction, blue values decrease it.")
        if(alg=='Random Forest'):        
            explainer=shap.TreeExplainer(rf)
            sv=explainer.shap_values(train)
            #long computation
            fgg=shap.summary_plot(sv,train)
            pfs=plt.show(fgg)
            st.pyplot(pfs)
            st.write("Interpretation info: The goal of SHAP is to explain the prediction of an instance x by computing the contribution of each feature to the prediction. SHAP dependence plots are an alternative to partial dependence plots and accumulated local effects. While PDP and ALE plot show average effects, SHAP dependence also shows the variance on the y-axis. Especially in case of interactions, the SHAP dependence plot will be much more dispersed in the y-axis. Each position on the x-axis is an instance of the data. Red SHAP values increase the prediction, blue values decrease it.")
        if(alg=='XGBoost'):        
            explainer=shap.TreeExplainer(xgb)
            sv=explainer.shap_values(train)
            #long computation
            fgg=shap.summary_plot(sv,train)
            pfs=plt.show(fgg)
            st.pyplot(pfs)
            st.write("Interpretation info: The goal of SHAP is to explain the prediction of an instance x by computing the contribution of each feature to the prediction. SHAP dependence plots are an alternative to partial dependence plots and accumulated local effects. While PDP and ALE plot show average effects, SHAP dependence also shows the variance on the y-axis. Especially in case of interactions, the SHAP dependence plot will be much more dispersed in the y-axis. Each position on the x-axis is an instance of the data. Red SHAP values increase the prediction, blue values decrease it.")
        

def tts(df,y):
    x_train,x_test,y_train,y_test=train_test_split(df,y,test_size=0.15,random_state=0,shuffle=True)
    return x_train,x_test,y_train,y_test
   
if __name__ == "__main__":
    main()

