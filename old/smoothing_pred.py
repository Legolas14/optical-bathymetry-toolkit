#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jun  8 12:23:59 2020

@author: lego
"""


import streamlit as st
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
import scipy as sc
import plotly_express as px
import plotly
import time
import plotly.graph_objects as go
from sklearn.ensemble import IsolationForest
import random
from sklearn.preprocessing import PolynomialFeatures
from sklearn.pipeline import make_pipeline
from sklearn.model_selection import train_test_split
import os,sys
import scipy.spatial as spatial

#@st.cache(persist=True, suppress_st_warning=True)
def load_data(data_path):  
    rangi_01_orig=pd.read_csv(data_path)
    return rangi_01_orig


def main():    
    st.title("Apply cKDTree neighbours algorithm to smoothen predictions")
    fn_loc=st.text_input("Enter the name of the folder(try giving the full path if name doesn't work) containing the prediction files. If the file is in the same directory as the python scripts are, then just type '.' ")
    if(fn_loc):
        data_path=st.text_input("Enter the name of the prediction file within the above folder")
        if(data_path):
            fn_loc=fn_loc+'/'
            full_river=load_data(fn_loc+data_path)
            st.write(full_river.head())
            cols=full_river.columns
            cols=cols.insert(0,'None')
            variable_option = st.multiselect("Select columns in the order of : 'X coordinate' , 'Y coordinate' , 'Prediction column' ",cols,key='8955')
            st.write('You selected:', variable_option)
            pro=st.checkbox('Check this box after selecting necessary columns')
            if(pro):
                east=variable_option[0]
                north=variable_option[1]
                pres=variable_option[2]
                rad=st.text_input("Enter the radius to find the nearest neighbors")
                if(rad):
                    radius=float(rad)
                    threshold=st.text_input("Enter the threshold value")
                    if(threshold):
                        threshold=float(threshold)
                        sta=st.checkbox(' Check this box to start computation. It might take a long time for a large dataset')        
                        if(sta):
                            latlon=np.array(full_river[[east,north]].copy())
                            point_tree=spatial.cKDTree(latlon)
                            fr=full_river[pres]
                            #st.write("entered into smoother")
                            #now for each point find all those points withing radius from this tree
                            out_ind=[]
                            for i in range(len(latlon)):
                                ixs=point_tree.query_ball_point(latlon[i], radius)
                                avg_all=np.mean(fr[ixs])
                                if(abs(fr[i]-avg_all)>threshold):
                                    out_ind.append(i)
                        
                            st.write(" Total dropped noise data points ",len(out_ind))
                            full_river.drop(out_ind,axis=0,inplace=True)
                            del fr
                            del latlon
                            del out_ind
                            import gc
                            gc.collect()
                            
                            fn_loc=str(fn_loc)+'/'
                            dp=data_path.split('.csv')[0]
                            new_data_path=fn_loc+str(dp)+'_smoothed'+'.csv'
                            full_river.to_csv(new_data_path,index=False)
                            st.write("Smoothed prediction file downloaded!! Please check your local files - stored in the location ",new_data_path)
                                       
                         
if __name__ == "__main__":
    main()


        
        
        