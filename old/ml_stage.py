#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Mar 27 12:14:28 2020

@author: lego
"""

import streamlit as st
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
import scipy as sc
import plotly_express as px
import plotly
import time
import plotly.graph_objects as go
from sklearn.ensemble import IsolationForest
import random
from sklearn.preprocessing import PolynomialFeatures
from sklearn.pipeline import make_pipeline
from sklearn.model_selection import train_test_split
from xgboost import XGBRegressor
from sklearn.linear_model import LinearRegression
from sklearn.ensemble import RandomForestRegressor
from sklearn.metrics import mean_squared_error as mse
from sklearn.metrics import r2_score
from sklearn.metrics import mean_absolute_error
from sklearn.metrics import mean_poisson_deviance
from sklearn.metrics import max_error
from sklearn.metrics import explained_variance_score
from sklearn.svm import SVR
from sklearn.tree import DecisionTreeRegressor
from sklearn.externals.six import StringIO  
from IPython.display import Image  
from sklearn.tree import export_graphviz                                        
from subprocess import call
import matplotlib.pyplot as plt
from sklearn.inspection import PartialDependenceDisplay
from sklearn.inspection import plot_partial_dependence
from rfpimp import permutation_importances
import shap
import os,sys
import joblib
#import keras
#import tensorflow as tf
from sklearn.neural_network import MLPRegressor
import shutil

@st.cache(persist=True, suppress_st_warning=True)
def load_data(data_path):  
    rangi_01_orig=pd.read_csv(data_path)
    return rangi_01_orig

def required_files(fn):
    shutil.copy('rf_x.csv',fn+'rf_x.csv')
    shutil.copy('nn_x.csv',fn+'nn_x.csv')
    shutil.copy('sub_x.csv',fn+'sub_x.csv')
    shutil.copy('groupby.csv',fn+'groupby.csv')
    shutil.copy('Coordinates.csv',fn+'Coordinates.csv')
    shutil.copy('temp_pipeline.csv',fn+'temp_pipeline.csv')
    shutil.copy('min_med_max.csv',fn+'min_med_max.csv')
    shutil.copy('y.csv',fn+'y.csv')
    shutil.copy('sc_full.pkl',fn+'sc_full.pkl')
    shutil.copy('pca_full.pkl',fn+'pca_full.pkl')
    shutil.copy('sc_nn_full.pkl',fn+'sc_nn_full.pkl')
    shutil.copy('kmeans.pkl',fn+'kmeans.pkl')
 

def main():    
    st.subheader("Launching Stage 3 of our pipeline....")
    st.title("Pipeline Stage : Machine Learning Algorithms and Results")
    totrain=load_data('rf_x.csv')
    x_y=load_data('y.csv')
    y=x_y[x_y.columns.tolist()[-1]].copy()
    #st.write(y.head())
    data=load_data('temp_pipeline.csv')
    #data=load_data('Cleaned_file.csv')
    
    tr_split=st.text_input("Enter the percentage (% from 5-95) of data to be split into Train set, the rest would be allotted as Validation set  ")
    if(tr_split):
        test_split=(100-int(tr_split))/100   
        st.write("Splitting the dataset into train and validation sets..")#works
        
        # for lr,svr
        sub_x=load_data('sub_x.csv')
        x_train2,x_test2,y_train2,y_test2=tts(sub_x,y,test_split)
        
        # for rf,xgb
        rf_x=load_data('rf_x.csv')
        x_train,x_test,y_train,y_test=tts(rf_x,y,test_split)
        st.write(rf_x.head())
        # for nn
        nn_x=load_data('nn_x.csv')
        x_train3,x_test3,y_train3,y_test3=tts(nn_x,y,test_split)      
        
        st.write(" The shape of train set is ",x_train.shape, " and the shape of test set is ",x_test.shape)
        pn=st.text_input("Do you wish to use an existing pre-trained model or wish to train a new model (Please give the input as 'Pretrained' or 'New' in these exact words)")
        # WRITE FOR OLD ONE
        
        if(pn=='Pretrained'):
            pmo=st.text_input("Please input the path of the Pre-trained model along with the .(pkl/h5/w5) extension",key='538')
            time.sleep(1)
            if(pmo):
                model=joblib.load(pmo)
                predictions_all=pd.DataFrame(y)
                col0=predictions_all.columns.tolist()[0]
                var1='None'
                var1=st.selectbox("Select the parent Algorithm to which this model belongs to (This is important because each model has a different feature engineering method)",["None","Linear Regression","Random Forest Regressor","XGBoost Regressor","Neural Network","Support Vector Regressor"],key=89)
                
                if(var1=='Linear Regression'):
                    predictions_all,fts=model_pred(model,sub_x,predictions_all)
                    st.write("Evaluation Metrics : ")
                    st.write(fts.head())
                    st.write(predictions_all.head())
                    st.subheader(" You can now see the visualizations about the insights that the model has learnt from the data, from the left-side bar ") 
                # works till here
                    viz_model_insights(model,sub_x,y)
                    gr=st.checkbox("Show actual depth vs prediction depth graph ")
                    if(gr):
                        layout = go.Layout(
                            title='Distance' + " vs " + 'Depth',
                            xaxis=dict(
                                title='Distance'
                            ),
                            yaxis=dict(
                                title='Depth'
                            ) )
                        fig = go.Figure(layout=layout)
                        fig.add_trace(go.Scatter(x=data['Distance_new'], y=y,
                                            mode='lines+markers',
                                            name='Original data lineplot'))
                                                                    
                        fig.add_trace(go.Scatter(x=data['Distance_new'], y=predictions_all['MODEL_full_Pred'],
                                            mode='lines+markers',
                                            name='LR Preds lineplot'))
                        st.plotly_chart(fig)
                        #Plotting sorted on depth vs pid
                        st.write(" Actual vs Predicted Depths, Sorted by actual depth")    
                        pre=predictions_all.copy()
                        col0=pre.columns.tolist()[0]
                        pre=pre.sort_values(by=col0)
                        pre=pre.reset_index()
                        
                        layout = go.Layout(
                            title='Distance' + " vs " + 'Depth',
                            xaxis=dict(
                                title='Distance'
                            ),
                            yaxis=dict(
                                title='Depth'
                            ) )
                        fig1 = go.Figure(layout=layout)
                        fig1.add_trace(go.Scatter(x=pre.index, y=pre[col0],
                                            mode='lines+markers',
                                            name='Original data lineplot'))
                                                                    
                        fig1.add_trace(go.Scatter(x=pre.index, y=pre['MODEL_full_Pred'],
                                            mode='lines+markers',
                                            name='MODEL Preds lineplot'))
                        st.plotly_chart(fig1)
                        
                        ms=st.checkbox("Do you wish to save the model statistics as text file? ")
                        if(ms):
                            fts.to_csv('Model_statistics.txt',index=True)
                            st.write("File saved as Model_statistics.txt")
                        
                        dol=st.checkbox("Use this model for Predicting full River Points ")
                        if(dol):
                            new_data_path=st.text_input("Enter the path of the CSV file, over which the machine learning model has to Predict (It can be training data or full river points data): ",key='123')#"/home/lego/Desktop/Job/Land River Sea Consulting Ltd./rangitata_01_new.csv
                            if(new_data_path):
                                new_data_path=str(new_data_path)    
                                new_df1=pd.read_csv(new_data_path)
                                
            #                                                    st.write(" Selecting the columns , ",sel_cols1, "...")
                                # selecting only rgb
                            
                                # I specifically ask for the feature to be selected because there is a possibility that the feature names might be different
                                sel_cols3=st.multiselect("Select the features for predicting (in the order 'Point Id','Longitude (X Coordinate)','Latitude (Y Coordinate),'R','G','B', and any other variables) ",new_df1.columns.tolist())                
                                if(sel_cols3):
                                    st.write('You selected:', sel_cols3)
            #                        conf5=st.text_input("Type 'Yes' to confirm the above as your selected features")
                                    tar=st.text_input(" Does this data set have the actual target values column? This is usually the case when you want to predict and export on the calibration data itself (Yes/No)",key='551')
                                    if(tar=='Yes'):
                                        pid=sel_cols3[0]
                                        east=sel_cols3[1]
                                        north=sel_cols3[2]
                                        
                                        r=sel_cols3[3]
                                        g=sel_cols3[4]
                                        b=sel_cols3[5]
                                        
                                        if(len(sel_cols3)>6):
                                            w=sel_cols3[6]
                                        
                                        new_df1=new_df1.sort_values(by=pid)
                                        new_df1=new_df1.reset_index().drop(['index'],axis=1)
        
                                        #STORE 4 AND PASS IN 3 
                                        to_merge=pd.DataFrame(new_df1[[pid,east,north]])
                                        sel_cols2=sel_cols3[3:]
                                        new_df=new_df1[sel_cols2].copy()
                                        
                                        to_pred=full_river_process(new_df,r,g,b)
                                        st.write(to_pred.head())
                                        
                                        df_preds=full_river_predict_lr(to_pred,sub_x,model,'MODEL')
                                        
                                        sel_tar=st.multiselect("Select the target column ",new_df1.columns.tolist())                
                                        if(sel_tar):
                                            st.write('You selected:', sel_tar)
                                            #st.write(sel_cols1[-1])
                                            df_preds[sel_tar]=new_df1[sel_tar].copy()
                                            st.write("over")
                                            #added
                                            df_preds=pd.DataFrame(df_preds)
                                            merged=pd.concat([to_merge,df_preds],axis=1)
                                            
                                            st.write(merged.head())
                                            merged.to_csv('Predictions_model.csv',index=False)
                                            st.write("Prediction file downloaded!! Please check your local files location to access the 'Predictions_model.csv'")
                            #               st.write("done 4")
                                        #tar=st.text_input(" Does this data set have the actual target values column? (Yes/No)" )
                                        
                                    if(tar=='No'):
                                        pid=sel_cols3[0]
                                        east=sel_cols3[1]
                                        north=sel_cols3[2]
                                        
                                        r=sel_cols3[3]
                                        g=sel_cols3[4]
                                        b=sel_cols3[5]
                                        
                                        if(len(sel_cols3)>6):
                                            w=sel_cols3[6]
                                        
                                        new_df1=new_df1.sort_values(by=pid)
                                        new_df1=new_df1.reset_index().drop(['index'],axis=1)
        
                                        #STORE 4 AND PASS IN 3 
                                        to_merge=pd.DataFrame(new_df1[[pid,east,north]])
                                        sel_cols2=sel_cols3[3:]
                                        new_df=new_df1[sel_cols2].copy()
                                        
                                        to_pred=full_river_process(new_df,r,g,b)
                                        st.write(to_pred.head())
                                        
                                        df_preds=full_river_predict_lr(to_pred,sub_x,model,'MODEL')
                                        
                                        #added
                                        df_preds=pd.DataFrame(df_preds)
                                        merged=pd.concat([to_merge,df_preds],axis=1)
                                        st.write(merged.head())
                                        merged.to_csv('Predictions_model.csv',index=False)
                                        st.write("Prediction file downloaded!! Please check your local files location to access the 'Predictions_model.csv'")
                
                if(var1=='Random Forest Regressor'):
                    
                    predictions_all,fts=model_pred(model,rf_x,predictions_all)
                    st.write("Evaluation Metrics : ")
                    st.write(fts.head())
                    st.write(predictions_all.head())
                    st.subheader(" You can now see the visualizations about the insights that the model has learnt from the data, from the left-side bar ") 
                # works till here
                    viz_model_insights(model,rf_x,y)
                    gr=st.checkbox("Show actual depth vs prediction depth graph ")
                    if(gr):
                        layout = go.Layout(
                            title='Distance' + " vs " + 'Depth',
                            xaxis=dict(
                                title='Distance'
                            ),
                            yaxis=dict(
                                title='Depth'
                            ) )
                        fig = go.Figure(layout=layout)
                        fig.add_trace(go.Scatter(x=data['Distance_new'], y=y,
                                            mode='lines+markers',
                                            name='Original data lineplot'))
                                                                    
                        fig.add_trace(go.Scatter(x=data['Distance_new'], y=predictions_all['MODEL_full_Pred'],
                                            mode='lines+markers',
                                            name='MODEL Preds lineplot'))
                        st.plotly_chart(fig)
                        #Plotting sorted on depth vs pid
                        st.write(" Actual vs Predicted Depths, Sorted by actual depth")    
                        pre=predictions_all.copy()
                        col0=pre.columns.tolist()[0]
                        pre=pre.sort_values(by=col0)
                        pre=pre.reset_index()
                        
                        layout = go.Layout(
                            title='Distance' + " vs " + 'Depth',
                            xaxis=dict(
                                title='Distance'
                            ),
                            yaxis=dict(
                                title='Depth'
                            ) )
                        fig1 = go.Figure(layout=layout)
                        fig1.add_trace(go.Scatter(x=pre.index, y=pre[col0],
                                            mode='lines+markers',
                                            name='Original data lineplot'))
                                                                    
                        fig1.add_trace(go.Scatter(x=pre.index, y=pre['MODEL_full_Pred'],
                                            mode='lines+markers',
                                            name='MODEL Preds lineplot'))
                        st.plotly_chart(fig1)
    
                        ms=st.checkbox("Do you wish to save the model statistics as text file? ")
                        if(ms):
                            fts.to_csv('Model_statistics.txt',index=True)
                            st.write("File saved as Model_statistics.txt")
                        
                        dol=st.checkbox("Use this model for Predicting full River Points ")
                        if(dol):
                            new_data_path=st.text_input("Enter the path of the CSV file, over which the machine learning model has to Predict (It can be training data or full river points data): ",key='123')#"/home/lego/Desktop/Job/Land River Sea Consulting Ltd./rangitata_01_new.csv
                            if(new_data_path):
                                new_data_path=str(new_data_path)    
                                new_df1=pd.read_csv(new_data_path)
                                #new_df1=new_df1.iloc[:1000]
            #                                                    st.write(" Selecting the columns , ",sel_cols1, "...")
                                # selecting only rgb
                            
                                # I specifically ask for the feature to be selected because there is a possibility that the feature names might be different
                                sel_cols3=st.multiselect("Select the features for predicting (in the order 'Point Id','Longitude (X Coordinate)','Latitude (Y Coordinate),'R','G','B', and any other variables) ",new_df1.columns.tolist())                
                                if(sel_cols3):
                                    st.write('You selected:', sel_cols3)
            #                        conf5=st.text_input("Type 'Yes' to confirm the above as your selected features")
                                    tar=st.text_input(" Does this data set have the actual target values column? This is usually the case when you want to predict and export on the calibration data itself (Yes/No)",key='551')
                                    if(tar=='Yes'):
                                        pid=sel_cols3[0]
                                        east=sel_cols3[1]
                                        north=sel_cols3[2]
                                        
                                        r=sel_cols3[3]
                                        g=sel_cols3[4]
                                        b=sel_cols3[5]
                                        
                                        if(len(sel_cols3)>6):
                                            w=sel_cols3[6]
                                        
                                        new_df1=new_df1.sort_values(by=pid)
                                        new_df1=new_df1.reset_index().drop(['index'],axis=1)
        
                                        #STORE 4 AND PASS IN 3 
                                        to_merge=pd.DataFrame(new_df1[[pid,east,north]])
                                        sel_cols2=sel_cols3[3:]
                                        new_df=new_df1[sel_cols2].copy()
                                        
                                        to_pred=full_river_process(new_df,r,g,b)
                                        st.write(to_pred.head())
                                        
                                        df_preds=full_river_predict_rf(to_pred,rf_x,model,'MODEL')
                                        
                                        sel_tar=st.multiselect("Select the target column ",new_df1.columns.tolist())                
                                        if(sel_tar):
                                            st.write('You selected:', sel_tar)
                                            #st.write(sel_cols1[-1])
                                            df_preds[sel_tar]=new_df1[sel_tar].copy()
                                            st.write("over")
                                            #added
                                            df_preds=pd.DataFrame(df_preds)
                                            merged=pd.concat([to_merge,df_preds],axis=1)
                                            
                                            st.write(merged.head())
                                            merged.to_csv('Predictions_model.csv',index=False)
                                            st.write("Prediction file downloaded!! Please check your local files location to access the 'Predictions_model.csv'")
                            #               st.write("done 4")
                                        #tar=st.text_input(" Does this data set have the actual target values column? (Yes/No)" )
                                        
                                    if(tar=='No'):
                                        pid=sel_cols3[0]
                                        east=sel_cols3[1]
                                        north=sel_cols3[2]
                                        
                                        r=sel_cols3[3]
                                        g=sel_cols3[4]
                                        b=sel_cols3[5]
                                        
                                        if(len(sel_cols3)>6):
                                            w=sel_cols3[6]
                                        
                                        new_df1=new_df1.sort_values(by=pid)
                                        new_df1=new_df1.reset_index().drop(['index'],axis=1)
        
                                        #STORE 4 AND PASS IN 3 
                                        to_merge=pd.DataFrame(new_df1[[pid,east,north]])
                                        sel_cols2=sel_cols3[3:]
                                        new_df=new_df1[sel_cols2].copy()
                                        
                                        to_pred=full_river_process(new_df,r,g,b)
                                        st.write(to_pred.head())
                                        
                                        df_preds=full_river_predict_rf(to_pred,rf_x,model,'MODEL')
                                        
                                        #added
                                        df_preds=pd.DataFrame(df_preds)
                                        merged=pd.concat([to_merge,df_preds],axis=1)
                                        st.write(merged.head())
                                        merged.to_csv('Predictions_model.csv',index=False)
                                        st.write("Prediction file downloaded!! Please check your local files location to access the 'Predictions_model.csv'")
                
                if(var1=='XGBoost Regressor'):
                    
                    
                    predictions_all,fts=model_pred(model,rf_x,predictions_all)
                    st.write("Evaluation Metrics : ")
                    st.write(fts.head())
                    st.write(predictions_all.head())
                    st.subheader(" You can now see the visualizations about the insights that the model has learnt from the data, from the left-side bar ") 
                # works till here
                    #viz_model_insights(model,rf_x,y)
                    gr=st.checkbox("Show actual depth vs prediction depth graph ")
                    if(gr):
                        layout = go.Layout(
                            title='Distance' + " vs " + 'Depth',
                            xaxis=dict(
                                title='Distance'
                            ),
                            yaxis=dict(
                                title='Depth'
                            ) )
                        fig = go.Figure(layout=layout)
                        fig.add_trace(go.Scatter(x=data['Distance_new'], y=y,
                                            mode='lines+markers',
                                            name='Original data lineplot'))
                                                                    
                        fig.add_trace(go.Scatter(x=data['Distance_new'], y=predictions_all['MODEL_full_Pred'],
                                            mode='lines+markers',
                                            name='MODEL Preds lineplot'))
                        st.plotly_chart(fig)
                        #Plotting sorted on depth vs pid
                        st.write(" Actual vs Predicted Depths, Sorted by actual depth")    
                        pre=predictions_all.copy()
                        col0=pre.columns.tolist()[0]
                        pre=pre.sort_values(by=col0)
                        pre=pre.reset_index()
                        
                        layout = go.Layout(
                            title='Distance' + " vs " + 'Depth',
                            xaxis=dict(
                                title='Distance'
                            ),
                            yaxis=dict(
                                title='Depth'
                            ) )
                        fig1 = go.Figure(layout=layout)
                        fig1.add_trace(go.Scatter(x=pre.index, y=pre[col0],
                                            mode='lines+markers',
                                            name='Original data lineplot'))
                                                                    
                        fig1.add_trace(go.Scatter(x=pre.index, y=pre['MODEL_full_Pred'],
                                            mode='lines+markers',
                                            name='MODEL Preds lineplot'))
                        st.plotly_chart(fig1)
                        ms=st.checkbox("Do you wish to save the model statistics as text file? ")
                        if(ms):
                            fts.to_csv('Model_statistics.txt',index=True)
                            st.write("File saved as Model_statistics.txt")
                        dol=st.checkbox("Use this model for Predicting full River Points ")
                        if(dol):
                            new_data_path=st.text_input("Enter the path of the CSV file, over which the machine learning model has to Predict (It can be training data or full river points data): ",key='123')#"/home/lego/Desktop/Job/Land River Sea Consulting Ltd./rangitata_01_new.csv
                            if(new_data_path):
                                new_data_path=str(new_data_path)    
                                new_df1=pd.read_csv(new_data_path)
                                #new_df1=new_df1.iloc[:1000]
            #                                                    st.write(" Selecting the columns , ",sel_cols1, "...")
                                # selecting only rgb
                                #new_df1=new_df1.iloc[:1000000]
                                # I specifically ask for the feature to be selected because there is a possibility that the feature names might be different
                                sel_cols3=st.multiselect("Select the features for predicting (in the order 'Point Id','Longitude (X Coordinate)','Latitude (Y Coordinate),'R','G','B', and any other variables) ",new_df1.columns.tolist())                
                                if(sel_cols3):
                                    st.write('You selected:', sel_cols3)
            #                        conf5=st.text_input("Type 'Yes' to confirm the above as your selected features")
                                    tar=st.text_input(" Does this data set have the actual target values column? This is usually the case when you want to predict and export on the calibration data itself (Yes/No)",key='551')
                                    if(tar=='Yes'):
                                        pid=sel_cols3[0]
                                        east=sel_cols3[1]
                                        north=sel_cols3[2]
                                        
                                        r=sel_cols3[3]
                                        g=sel_cols3[4]
                                        b=sel_cols3[5]
                                        
                                        if(len(sel_cols3)>6):
                                            w=sel_cols3[6]
                                        
                                        new_df1=new_df1.sort_values(by=pid)
                                        new_df1=new_df1.reset_index().drop(['index'],axis=1)
        
                                        #STORE 4 AND PASS IN 3 
                                        to_merge=pd.DataFrame(new_df1[[pid,east,north]])
                                        sel_cols2=sel_cols3[3:]
                                        new_df=new_df1[sel_cols2].copy()
                                        
                                        to_pred=full_river_process(new_df,r,g,b)
                                        st.write(to_pred.head())
                                        
                                        df_preds2=full_river_predict_xgb(to_pred,rf_x)
                                        df_preds=predict_only(model,df_preds2,'MODEL')
                                        
                                        sel_tar=st.multiselect("Select the target column ",new_df1.columns.tolist())                
                                        if(sel_tar):
                                            st.write('You selected:', sel_tar)
                                            #st.write(sel_cols1[-1])
                                            df_preds[sel_tar]=new_df1[sel_tar].copy()
                                            st.write("over")
                                            #added
                                            df_preds=pd.DataFrame(df_preds)
                                            merged=pd.concat([to_merge,df_preds],axis=1)
                                            
                                            st.write(merged.head())
                                            merged.to_csv('Predictions_model.csv',index=False)
                                            st.write("Prediction file downloaded!! Please check your local files location to access the 'Predictions_model.csv'")
                            #               st.write("done 4")
                                        #tar=st.text_input(" Does this data set have the actual target values column? (Yes/No)" )
                                        
                                    if(tar=='No'):
                                        pid=sel_cols3[0]
                                        east=sel_cols3[1]
                                        north=sel_cols3[2]
                                        
                                        r=sel_cols3[3]
                                        g=sel_cols3[4]
                                        b=sel_cols3[5]
                                        
                                        if(len(sel_cols3)>6):
                                            w=sel_cols3[6]
                                        
                                        new_df1=new_df1.sort_values(by=pid)
                                        new_df1=new_df1.reset_index().drop(['index'],axis=1)
        
                                        #STORE 4 AND PASS IN 3 
                                        to_merge=pd.DataFrame(new_df1[[pid,east,north]])
                                        sel_cols2=sel_cols3[3:]
                                        new_df=new_df1[sel_cols2].copy()
                                        
                                        to_pred=full_river_process(new_df,r,g,b)
                                        st.write(to_pred.head())
                                        
                                        df_preds2=full_river_predict_xgb(to_pred,rf_x)
                                        df_preds=predict_only(model,df_preds2,'MODEL')
                                        
                                        #added
                                        df_preds=pd.DataFrame(df_preds)
                                        merged=pd.concat([to_merge,df_preds],axis=1)
                                        st.write(merged.head())
                                        merged.to_csv('Predictions_model.csv',index=False)
                                        st.write("Prediction file downloaded!! Please check your local files location to access the 'Predictions_model.csv'")
                
                
                if(var1=='Neural Network'):
                    
                    predictions_all,fts=model_pred(model,nn_x,predictions_all)
                    st.write("Evaluation Metrics : ")
                    st.write(fts.head())
                    st.write(predictions_all.head())
                    st.subheader(" You can now see the visualizations about the insights that the model has learnt from the data, from the left-side bar ") 
                # works till here
                    viz_model_insights(model,rf_x,y)
                    gr=st.checkbox("Show actual depth vs prediction depth graph ")
                    if(gr):
                        layout = go.Layout(
                            title='Distance' + " vs " + 'Depth',
                            xaxis=dict(
                                title='Distance'
                            ),
                            yaxis=dict(
                                title='Depth'
                            ) )
                        fig = go.Figure(layout=layout)
                        fig.add_trace(go.Scatter(x=data['Distance_new'], y=y,
                                            mode='lines+markers',
                                            name='Original data lineplot'))
                                                                    
                        fig.add_trace(go.Scatter(x=data['Distance_new'], y=predictions_all['MODEL_full_Pred'],
                                            mode='lines+markers',
                                            name='MODEL Preds lineplot'))
                        st.plotly_chart(fig)
                        #Plotting sorted on depth vs pid
                        st.write(" Actual vs Predicted Depths, Sorted by actual depth")    
                        pre=predictions_all.copy()
                        col0=pre.columns.tolist()[0]
                        pre=pre.sort_values(by=col0)
                        pre=pre.reset_index()
                        
                        layout = go.Layout(
                            title='Distance' + " vs " + 'Depth',
                            xaxis=dict(
                                title='Distance'
                            ),
                            yaxis=dict(
                                title='Depth'
                            ) )
                        fig1 = go.Figure(layout=layout)
                        fig1.add_trace(go.Scatter(x=pre.index, y=pre[col0],
                                            mode='lines+markers',
                                            name='Original data lineplot'))
                                                                    
                        fig1.add_trace(go.Scatter(x=pre.index, y=pre['MODEL_full_Pred'],
                                            mode='lines+markers',
                                            name='MODEL Preds lineplot'))
                        st.plotly_chart(fig1)
                        ms=st.checkbox("Do you wish to save the model statistics as text file? ")
                        if(ms):
                            fts.to_csv('Model_statistics.txt',index=True)
                            st.write("File saved as Model_statistics.txt")
                        dol=st.checkbox("Use this model for Predicting full River Points ")
                        if(dol):
                            new_data_path=st.text_input("Enter the path of the CSV file, over which the machine learning model has to Predict (It can be training data or full river points data): ",key='123')#"/home/lego/Desktop/Job/Land River Sea Consulting Ltd./rangitata_01_new.csv
                            if(new_data_path):
                                new_data_path=str(new_data_path)    
                                new_df1=pd.read_csv(new_data_path)
                                
            #                                                    st.write(" Selecting the columns , ",sel_cols1, "...")
                                # selecting only rgb
                            
                                # I specifically ask for the feature to be selected because there is a possibility that the feature names might be different
                                sel_cols3=st.multiselect("Select the features for predicting (in the order 'Point Id','Longitude (X Coordinate)','Latitude (Y Coordinate),'R','G','B', and any other variables) ",new_df1.columns.tolist())                
                                if(sel_cols3):
                                    st.write('You selected:', sel_cols3)
            #                        conf5=st.text_input("Type 'Yes' to confirm the above as your selected features")
                                    tar=st.text_input(" Does this data set have the actual target values column? This is usually the case when you want to predict and export on the calibration data itself (Yes/No)",key='551')
                                    if(tar=='Yes'):
                                        pid=sel_cols3[0]
                                        east=sel_cols3[1]
                                        north=sel_cols3[2]
                                        
                                        r=sel_cols3[3]
                                        g=sel_cols3[4]
                                        b=sel_cols3[5]
                                        
                                        if(len(sel_cols3)>6):
                                            w=sel_cols3[6]
                                        
                                        new_df1=new_df1.sort_values(by=pid)
                                        new_df1=new_df1.reset_index().drop(['index'],axis=1)
        
                                        #STORE 4 AND PASS IN 3 
                                        to_merge=pd.DataFrame(new_df1[[pid,east,north]])
                                        sel_cols2=sel_cols3[3:]
                                        new_df=new_df1[sel_cols2].copy()
                                        
                                        to_pred=full_river_process(new_df,r,g,b)
                                        st.write(to_pred.head())
                                        
                                        df_preds=full_river_predict_nn(to_pred,rf_x,model,'MODEL')
                                        
                                        sel_tar=st.multiselect("Select the target column ",new_df1.columns.tolist())                
                                        if(sel_tar):
                                            st.write('You selected:', sel_tar)
                                            #st.write(sel_cols1[-1])
                                            df_preds[sel_tar]=new_df1[sel_tar].copy()
                                            st.write("over")
                                            #added
                                            df_preds=pd.DataFrame(df_preds)
                                            merged=pd.concat([to_merge,df_preds],axis=1)
                                            
                                            st.write(merged.head())
                                            merged.to_csv('Predictions_model.csv',index=False)
                                            st.write("Prediction file downloaded!! Please check your local files location to access the 'Predictions_model.csv'")
                            #               st.write("done 4")
                                        #tar=st.text_input(" Does this data set have the actual target values column? (Yes/No)" )
                                        
                                    if(tar=='No'):
                                        pid=sel_cols3[0]
                                        east=sel_cols3[1]
                                        north=sel_cols3[2]
                                        
                                        r=sel_cols3[3]
                                        g=sel_cols3[4]
                                        b=sel_cols3[5]
                                        
                                        if(len(sel_cols3)>6):
                                            w=sel_cols3[6]
                                        
                                        new_df1=new_df1.sort_values(by=pid)
                                        new_df1=new_df1.reset_index().drop(['index'],axis=1)
        
                                        #STORE 4 AND PASS IN 3 
                                        to_merge=pd.DataFrame(new_df1[[pid,east,north]])
                                        sel_cols2=sel_cols3[3:]
                                        new_df=new_df1[sel_cols2].copy()
                                        
                                        to_pred=full_river_process(new_df,r,g,b)
                                        st.write(to_pred.head())
                                        
                                        df_preds=full_river_predict_nn(to_pred,rf_x,model,'MODEL')
                                        
                                        #added
                                        df_preds=pd.DataFrame(df_preds)
                                        merged=pd.concat([to_merge,df_preds],axis=1)
                                        st.write(merged.head())
                                        merged.to_csv('Predictions_model.csv',index=False)
                                        st.write("Prediction file downloaded!! Please check your local files location to access the 'Predictions_model.csv'")
        
                if(var1=='Support Vector Regressor'):
                    predictions_all,fts=model_pred(model,sub_x,predictions_all)
                    st.write("Evaluation Metrics : ")
                    st.write(fts.head())
                    st.write(predictions_all.head())
                    st.subheader(" You can now see the visualizations about the insights that the model has learnt from the data, from the left-side bar ") 
                # works till here
                    viz_model_insights(model,sub_x,y)
                    gr=st.checkbox("Show actual depth vs prediction depth graph ")
                    if(gr):
                        layout = go.Layout(
                        title='Distance' + " vs " + 'Depth',
                        xaxis=dict(
                            title='Distance'
                        ),
                        yaxis=dict(
                            title='Depth'
                        ) )
                        fig = go.Figure(layout=layout)
                        fig.add_trace(go.Scatter(x=data['Distance_new'], y=y,
                                            mode='lines+markers',
                                            name='Original data lineplot'))
                                                                    
                        fig.add_trace(go.Scatter(x=data['Distance_new'], y=predictions_all['MODEL_full_Pred'],
                                            mode='lines+markers',
                                            name='MODEL Preds lineplot'))
                        st.plotly_chart(fig)
                        #Plotting sorted on depth vs pid
                        st.write(" Actual vs Predicted Depths, Sorted by actual depth")    
                        pre=predictions_all.copy()
                        col0=pre.columns.tolist()[0]
                        pre=pre.sort_values(by=col0)
                        pre=pre.reset_index()
                        
                        layout = go.Layout(
                        title='Distance' + " vs " + 'Depth',
                        xaxis=dict(
                            title='Distance'
                        ),
                        yaxis=dict(
                            title='Depth'
                        ) )
                        fig1 = go.Figure(layout=layout)
                        fig1.add_trace(go.Scatter(x=pre.index, y=pre[col0],
                                            mode='lines+markers',
                                            name='Original data lineplot'))
                                                                    
                        fig1.add_trace(go.Scatter(x=pre.index, y=pre['MODEL_full_Pred'],
                                            mode='lines+markers',
                                            name='MODEL Preds lineplot'))
                        st.plotly_chart(fig1)
                        ms=st.checkbox("Do you wish to save the model statistics as text file? ")
                        if(ms):
                            fts.to_csv('Model_statistics.txt',index=True)
                            st.write("File saved as Model_statistics.txt")
    
                        dol=st.checkbox("Use this model for Predicting full River Points ")
                        if(dol):
                            new_data_path=st.text_input("Enter the path of the CSV file, over which the machine learning model has to Predict (It can be training data or full river points data): ",key='123')#"/home/lego/Desktop/Job/Land River Sea Consulting Ltd./rangitata_01_new.csv
                            if(new_data_path):
                                new_data_path=str(new_data_path)    
                                new_df1=pd.read_csv(new_data_path)
                                #new_df1=new_df1.iloc[:1000]
            #                                                    st.write(" Selecting the columns , ",sel_cols1, "...")
                                # selecting only rgb
                            
                                # I specifically ask for the feature to be selected because there is a possibility that the feature names might be different
                                sel_cols3=st.multiselect("Select the features for predicting (in the order 'Point Id','Longitude (X Coordinate)','Latitude (Y Coordinate),'R','G','B', and any other variables) ",new_df1.columns.tolist())                
                                if(sel_cols3):
                                    st.write('You selected:', sel_cols3)
            #                        conf5=st.text_input("Type 'Yes' to confirm the above as your selected features")
                                    tar=st.text_input(" Does this data set have the actual target values column? This is usually the case when you want to predict and export on the calibration data itself (Yes/No)",key='551')
                                    if(tar=='Yes'):
                                        pid=sel_cols3[0]
                                        east=sel_cols3[1]
                                        north=sel_cols3[2]
                                        
                                        r=sel_cols3[3]
                                        g=sel_cols3[4]
                                        b=sel_cols3[5]
                                        
                                        if(len(sel_cols3)>6):
                                            w=sel_cols3[6]
                                        
                                        new_df1=new_df1.sort_values(by=pid)
                                        new_df1=new_df1.reset_index().drop(['index'],axis=1)
        
                                        #STORE 4 AND PASS IN 3 
                                        to_merge=pd.DataFrame(new_df1[[pid,east,north]])
                                        sel_cols2=sel_cols3[3:]
                                        new_df=new_df1[sel_cols2].copy()
                                        
                                        to_pred=full_river_process(new_df,r,g,b)
                                        st.write(to_pred.head())
                                        
                                        df_preds=full_river_predict_svr(to_pred,sub_x,model,'MODEL')
                                        
                                        sel_tar=st.multiselect("Select the target column ",new_df1.columns.tolist())                
                                        if(sel_tar):
                                            st.write('You selected:', sel_tar)
                                            #st.write(sel_cols1[-1])
                                            df_preds[sel_tar]=new_df1[sel_tar].copy()
                                            st.write("over")
                                            #added
                                            df_preds=pd.DataFrame(df_preds)
                                            merged=pd.concat([to_merge,df_preds],axis=1)
                                            
                                            st.write(merged.head())
                                            merged.to_csv('Predictions_model.csv',index=False)
                                            st.write("Prediction file downloaded!! Please check your local files location to access the 'Predictions_model.csv'")
                            #               st.write("done 4")
                                        #tar=st.text_input(" Does this data set have the actual target values column? (Yes/No)" )
                                        
                                    if(tar=='No'):
                                        pid=sel_cols3[0]
                                        east=sel_cols3[1]
                                        north=sel_cols3[2]
                                        
                                        r=sel_cols3[3]
                                        g=sel_cols3[4]
                                        b=sel_cols3[5]
                                        
                                        if(len(sel_cols3)>6):
                                            w=sel_cols3[6]
                                        
                                        new_df1=new_df1.sort_values(by=pid)
                                        new_df1=new_df1.reset_index().drop(['index'],axis=1)
        
                                        #STORE 4 AND PASS IN 3 
                                        to_merge=pd.DataFrame(new_df1[[pid,east,north]])
                                        sel_cols2=sel_cols3[3:]
                                        new_df=new_df1[sel_cols2].copy()
                                        
                                        to_pred=full_river_process(new_df,r,g,b)
                                        st.write(to_pred.head())
                                        
                                        df_preds=full_river_predict_svr(to_pred,sub_x,model,'MODEL')
                                        
                                        #added
                                        df_preds=pd.DataFrame(df_preds)
                                        merged=pd.concat([to_merge,df_preds],axis=1)
                                        st.write(merged.head())
                                        merged.to_csv('Predictions_model.csv',index=False)
                                        st.write("Prediction file downloaded!! Please check your local files location to access the 'Predictions_model.csv'")
            
        
        if(pn=='New'):
            st.subheader(" Please select a Regression algorithm from the left-side bar ")
            algo_choice=st.sidebar.selectbox("Choose the ML model",["None","Linear Regression","Random Forest Regressor","XGBoost Regressor","Neural Network","Support Vector Regressor","Try all models"])
        #proceed if i get a choice here.....
           # predictions_test=pd.DataFrame(y_test)
            predictions_all=pd.DataFrame(y)
            col0=predictions_all.columns.tolist()[0]
            
            if(algo_choice=='Linear Regression'):
                st.write("Training ",algo_choice,"...")
                lr,predictions_all,fts,valid_results=LR_reg(sub_x,x_train2,x_test2,y_train2,y_test2,predictions_all)
                st.write("Training dataset Evaluation Metrics : ")
                st.write(fts.head())
                st.subheader("Validation dataset Evaluation Metrics : ")
                st.write(valid_results)
                
                st.write(predictions_all.head())
              
                
                #vals=predictions_all[col0].values
                st.subheader(" You can now see the visualizations about the insights that the model has learnt from the data, from the left-side bar ") 
                # works till here
                viz_model_insights(lr,sub_x,y)
                gr=st.checkbox("Show actual depth vs prediction depth graph ")
                if(gr):
                    layout = go.Layout(
                        title='Distance' + " vs " + 'Depth',
                        xaxis=dict(
                            title='Distance'
                        ),
                        yaxis=dict(
                            title='Depth'
                        ) )
                    fig = go.Figure(layout=layout)
                    fig.add_trace(go.Scatter(x=data['Distance_new'], y=y,
                                        mode='lines+markers',
                                        name='Original data lineplot'))
                                                                
                    fig.add_trace(go.Scatter(x=data['Distance_new'], y=predictions_all['LR_full_Pred'],
                                        mode='lines+markers',
                                        name='LR Preds lineplot'))
                    st.plotly_chart(fig)
                    #Plotting sorted on depth vs pid
                    st.write(" Actual vs Predicted Depths, Sorted by actual depth")    
                    pre=predictions_all.copy()
                    col0=pre.columns.tolist()[0]
                    pre=pre.sort_values(by=col0)
                    pre=pre.reset_index()
                    
                    layout = go.Layout(
                        title='Distance' + " vs " + 'Depth',
                        xaxis=dict(
                            title='Distance'
                        ),
                        yaxis=dict(
                            title='Depth'
                        ) )
                    fig1 = go.Figure(layout=layout)
                    fig1.add_trace(go.Scatter(x=pre.index, y=pre[col0],
                                        mode='lines+markers',
                                        name='Original data lineplot'))
                                                                
                    fig1.add_trace(go.Scatter(x=pre.index, y=pre['LR_full_Pred'],
                                        mode='lines+markers',
                                        name='LR Preds lineplot'))
                    st.plotly_chart(fig1)
                    ms=st.checkbox("Do you wish to save the model statistics as text file? ")
                    if(ms):
                        stat=pd.concat([fts,valid_results],axis=1)
                        st.write(stat)
                        stat.to_csv('Model_statistics.txt',index=True)
                        st.write("File saved as Model_statistics.txt")                                            
                    dol=st.checkbox("Use this model for Predicting full River Points ")
                    if(dol):
                        new_data_path=st.text_input("Enter the path of the CSV file, over which the machine learning model has to Predict (It can be training data or full river points data): ",key='123')#"/home/lego/Desktop/Job/Land River Sea Consulting Ltd./rangitata_01_new.csv
                        if(new_data_path):
                            new_data_path=str(new_data_path)    
                            
                            full_df=load_data(new_data_path)
                                                    
                            # I specifically ask for the feature to be selected because there is a possibility that the feature names might be different
                            sel_cols3=st.multiselect("Select the features for predicting (in the order 'Point Id','Longitude (X Coordinate)','Latitude (Y Coordinate),'R','G','B', and any other variables) ",full_df.columns.tolist())                
                            pro=st.checkbox('Check this box after selecting necessary columns')
                            if(pro):
                                st.write('You selected:', sel_cols3)
        #                        conf5=st.text_input("Type 'Yes' to confirm the above as your selected features")
                                pid=sel_cols3[0]
                                east=sel_cols3[1]
                                north=sel_cols3[2]
                                
                                r=sel_cols3[3]
                                g=sel_cols3[4]
                                b=sel_cols3[5]
                                
                                if(len(sel_cols3)>6):
                                    w=sel_cols3[6]
                                
                                #new_df1=new_df1.sort_values(by=pid)
                                #new_df1=new_df1.reset_index().drop(['index'],axis=1)

                                #STORE 4 AND PASS IN 3 
                                #to_merge=pd.DataFrame(new_df1[[pid,east,north]])
                                sel_cols2=sel_cols3[3:]
                                new_df1=pd.read_csv(new_data_path,chunksize=1000000)
                                import gc
                                gc.collect()
                                to_som='No'
                                radius=0
                                threshold=0
                                to_som=st.checkbox("Do you wish to smooth the end prediction results using cKDTree neighbours")
                                if(to_som):
                                    to_som='Yes'
                                    rad=st.text_input("Enter the radius to find the nearest neighbors")
                                    if(rad):
                                        radius=float(rad)
                                        threshold=st.text_input("Enter the threshold value")
                                        if(threshold):
                                            threshold=float(threshold)
                                tar=st.text_input(" Does this data set have the actual target values column? This is usually the case when you want to predict and export on the calibration data itself (Yes/No)",key='551')
                                if(tar=='Yes'):
                                    sel_tar=st.multiselect("Select the target column ",full_df.columns.tolist())                
                                    if(sel_tar):
                                        st.write('You selected:', sel_tar)

                                        #st.write(sel_cols1[-1])                                        
                                        sav=st.text_input("Do you wish to save this model to local storage for further use? 'Yes' to download both, the model and prediction file / 'No' to download just the prediction file ")
                                        if(sav=='No'):
                                            fn='none'
                                            st.write("Please wait while the full river prediction is running")
                                            #full_river_process_lr(new_df1,r,g,b,sub_x,model,model_name,sel_tar,sav,fn,sel_cols2,pid,east,north):
                                            full_river_process_lr(new_df1,r,g,b,sub_x,lr,'LR',sel_tar,sav,fn,sel_cols2,pid,east,north,to_som,radius,threshold)
                                            del new_df1
                                        if(sav=='Yes'):
                                            fn=st.text_input('Enter the name you wish to give for the folder ',key='10865')
                                            if(fn):
                                                fn=str(fn)+'/'
                                                os.makedirs(os.path.dirname(fn), exist_ok=True)
                                                full_river_process_lr(new_df1,r,g,b,sub_x,lr,'LR',sel_tar,sav,fn,sel_cols2,pid,east,north,to_som,radius,threshold)
                                                del new_df1     
                                if(tar=='No'):
                                    sel_tar='none'
                                    sav=st.text_input("Do you wish to save this model to local storage for further use? 'Yes' to download both, the model and prediction file / 'No' to download just the prediction file ")
                                    if(sav=='No'):
                                        fn='none'
                                        st.write("Please wait while the full river prediction is running")
                                        full_river_process_lr(new_df1,r,g,b,sub_x,lr,'LR',sel_tar,sav,fn,sel_cols2,pid,east,north,to_som,radius,threshold)
                                        del new_df1
                                    if(sav=='Yes'):
                                        fn=st.text_input('Enter the name you wish to give for the folder ',key='10865')
                                        if(fn):
                                            fn=str(fn)+'/'
                                            os.makedirs(os.path.dirname(fn), exist_ok=True)
                                            full_river_process_lr(new_df1,r,g,b,sub_x,lr,'LR',sel_tar,sav,fn,sel_cols2,pid,east,north,to_som,radius,threshold)
                                            del new_df1
                    
        
            if(algo_choice=='Random Forest Regressor'):
                st.write("Training ",algo_choice,"...")
                rf,predictions_all,fts,valid_results=RF_reg(rf_x,x_train,x_test,y_train,y_test,predictions_all)
                st.write("Training dataset Evaluation Metrics : ")
                st.write(fts.head())
                st.subheader("Validation dataset Evaluation Metrics : ")
                st.write(valid_results)
                st.write(predictions_all.head())
                st.subheader(" You can now see the visualizations about the insights that the model has learnt from the data, from the left-side bar ") 
                viz_model_insights(rf,rf_x,y)
                gr=st.checkbox("Show actual depth vs prediction depth graph ")
                if(gr):
                    layout = go.Layout(
                        title='Distance' + " vs " + 'Depth',
                        xaxis=dict(
                            title='Distance'
                        ),
                        yaxis=dict(
                            title='Depth'
                        ) )
                    fig = go.Figure(layout=layout)
                    fig.add_trace(go.Scatter(x=data['Distance_new'], y=y,
                                        mode='lines+markers',
                                        name='Original data lineplot'))
                                                                
                    fig.add_trace(go.Scatter(x=data['Distance_new'], y=predictions_all['RF_full_Pred'],
                                        mode='lines+markers',
                                        name='Preds lineplot'))                                       
                    st.plotly_chart(fig)
                                                                #Plotting sorted on depth vs pid
                    st.write(" Actual vs Predicted Depths, Sorted by actual depth")    
                    pre=predictions_all.copy()
                    col0=pre.columns.tolist()[0]
                    pre=pre.sort_values(by=col0)
                    pre=pre.reset_index()
                    
                    layout = go.Layout(
                        title='Distance' + " vs " + 'Depth',
                        xaxis=dict(
                            title='Distance'
                        ),
                        yaxis=dict(
                            title='Depth'
                        ) )
                    fig1 = go.Figure(layout=layout)
                    fig1.add_trace(go.Scatter(x=pre.index, y=pre[col0],
                                        mode='lines+markers',
                                        name='Original data lineplot'))
                                                                
                    fig1.add_trace(go.Scatter(x=pre.index, y=pre['RF_full_Pred'],
                                        mode='lines+markers',
                                        name='Preds lineplot'))
                    st.plotly_chart(fig1)
                    ms=st.checkbox("Do you wish to save the model statistics as text file? ")
                    if(ms):
                        stat=pd.concat([fts,valid_results],axis=1)
                        st.write(stat)
                        stat.to_csv('Model_statistics.txt',index=True)
                        st.write("File saved as Model_statistics.txt")   
                    dol=st.checkbox("Use this model for Predicting full River Points ")
                    if(dol):
                        new_data_path=st.text_input("Enter the path of the CSV file, over which the machine learning model has to Predict (It can be training data or full river points data): ",key='123')#"/home/lego/Desktop/Job/Land River Sea Consulting Ltd./rangitata_01_new.csv
                        if(new_data_path):
                            new_data_path=str(new_data_path)
                            full_df=load_data(new_data_path)                        
        
                            sel_cols3=st.multiselect("Select the features for predicting (in the order 'Point Id','Longitude (X Coordinate)','Latitude (Y Coordinate),'R','G','B', and any other variables) ",full_df.columns.tolist())                
                            pro=st.checkbox('Check this box after selecting necessary columns')
                            if(pro):
                                st.write('You selected:', sel_cols3)
        #                        conf5=st.text_input("Type 'Yes' to confirm the above as your selected features")
                                pid=sel_cols3[0]
                                east=sel_cols3[1]
                                north=sel_cols3[2]
                                
                                r=sel_cols3[3]
                                g=sel_cols3[4]
                                b=sel_cols3[5]
                                
                                if(len(sel_cols3)>6):
                                    w=sel_cols3[6]
                                sel_cols2=sel_cols3[3:]
                                new_df1=pd.read_csv(new_data_path,chunksize=1000000)
                                import gc
                                gc.collect()
                                to_som='No'
                                radius=0
                                threshold=0
                                to_som=st.checkbox("Do you wish to smooth the end prediction results using cKDTree neighbours")
                                if(to_som):
                                    to_som='Yes'
                                    rad=st.text_input("Enter the radius to find the nearest neighbors")
                                    if(rad):
                                        radius=float(rad)
                                        threshold=st.text_input("Enter the threshold value")
                                        if(threshold):
                                            threshold=float(threshold)

                                tar=st.text_input(" Does this data set have the actual target values column? This is usually the case when you want to predict and export on the calibration data itself (Yes/No)",key='551')
                                if(tar=='Yes'):
                                    sel_tar=st.multiselect("Select the target column ",full_df.columns.tolist())                
                                    if(sel_tar):
                                        st.write('You selected:', sel_tar)
                                        #st.write(sel_cols1[-1])                                        
                                        sav=st.text_input("Do you wish to save this model to local storage for further use? 'Yes' to download both, the model and prediction file / 'No' to download just the prediction file ")
                                        if(sav=='No'):
                                            fn='none'
                                            st.write("Please wait while the full river prediction is running")
                                            full_river_process_rf(new_df1,r,g,b,rf_x,rf,'RF',sel_tar,sav,fn,sel_cols2,pid,east,north,to_som,radius,threshold)
                                            del new_df1
                                        if(sav=='Yes'):
                                            fn=st.text_input('Enter the name you wish to give for the folder ',key='10865')
                                            if(fn):
                                                fn=str(fn)+'/'
                                                os.makedirs(os.path.dirname(fn), exist_ok=True)
                                                full_river_process_rf(new_df1,r,g,b,rf_x,rf,'RF',sel_tar,sav,fn,sel_cols2,pid,east,north,to_som,radius,threshold)
                                                del new_df1     
                                if(tar=='No'):
                                    sel_tar='none'
                                    sav=st.text_input("Do you wish to save this model to local storage for further use? 'Yes' to download both, the model and prediction file / 'No' to download just the prediction file ")
                                    if(sav=='No'):
                                        fn='none'
                                        st.write("Please wait while the full river prediction is running")
                                        full_river_process_rf(new_df1,r,g,b,rf_x,rf,'RF',sel_tar,sav,fn,sel_cols2,pid,east,north,to_som,radius,threshold)
                                        del new_df1
                                    if(sav=='Yes'):
                                        fn=st.text_input('Enter the name you wish to give for the folder ',key='10865')
                                        if(fn):
                                            fn=str(fn)+'/'
                                            os.makedirs(os.path.dirname(fn), exist_ok=True)
                                            full_river_process_rf(new_df1,r,g,b,rf_x,rf,'RF',sel_tar,sav,fn,sel_cols2,pid,east,north,to_som,radius,threshold)
                                            del new_df1
                                                                                 
            if(algo_choice=='XGBoost Regressor'):
                st.write("Training ",algo_choice,"...")
                xgb,predictions_all,fts,valid_results=XGB_reg(rf_x,x_train,x_test,y_train,y_test,predictions_all)
                st.write("Training dataset Evaluation Metrics : ")
                st.write(fts.head())
                st.subheader("Validation dataset Evaluation Metrics : ")
                st.write(valid_results)
                
                st.write(predictions_all.head())
              
                st.subheader(" You can now see the visualizations about the insights that the model has learnt from the data, from the left-side bar ") 
                xgb1=only_xgb(x_train,y_train)
                
                viz_model_insights(xgb1,rf_x,y)
                st.write(xgb.best_estimator_)
                st.write(xgb.best_score_)
                st.write(xgb.best_params_)
                
                gr=st.checkbox("Show actual depth vs prediction depth graph ")
                if(gr):
                    layout = go.Layout(
                        title='Distance' + " vs " + 'Depth',
                        xaxis=dict(
                            title='Distance'
                        ),
                        yaxis=dict(
                            title='Depth'
                        ) )
                    fig = go.Figure(layout=layout)
                    fig.add_trace(go.Scatter(x=data['Distance_new'], y=y,
                                        mode='lines+markers',
                                        name='Original data lineplot'))
                                                                
                    fig.add_trace(go.Scatter(x=data['Distance_new'], y=predictions_all['XGB_full_Pred'],
                                        mode='lines+markers',
                                        name='Preds lineplot'))
                    st.plotly_chart(fig)
                                                                #Plotting sorted on depth vs pid
                    st.write(" Actual vs Predicted Depths, Sorted by actual depth")    
                    pre=predictions_all.copy()
                    col0=pre.columns.tolist()[0]
                    pre=pre.sort_values(by=col0)
                    pre=pre.reset_index()
                    
                    layout = go.Layout(
                        title='Distance' + " vs " + 'Depth',
                        xaxis=dict(
                            title='Distance'
                        ),
                        yaxis=dict(
                            title='Depth'
                        ) )
                    fig1 = go.Figure(layout=layout)
                    fig1.add_trace(go.Scatter(x=pre.index, y=pre[col0],
                                        mode='lines+markers',
                                        name='Original data lineplot'))
                                                                
                    fig1.add_trace(go.Scatter(x=pre.index, y=pre['XGB_full_Pred'],
                                        mode='lines+markers',
                                        name='Preds lineplot'))
                    st.plotly_chart(fig1)
                    ms=st.checkbox("Do you wish to save the model statistics as text file? ")
                    if(ms):
                        stat=pd.concat([fts,valid_results],axis=1)
                        st.write(stat)
                        stat.to_csv('Model_statistics.txt',index=True)
                        st.write("File saved as Model_statistics.txt")
                    dol=st.checkbox("Use this model for Predicting full River Points ")
                    if(dol):
                        new_data_path=st.text_input("Enter the path of the CSV file, over which the machine learning model has to Predict (It can be training data or full river points data): ",key='123')#"/home/lego/Desktop/Job/Land River Sea Consulting Ltd./rangitata_01_new.csv
                        if(new_data_path):
                            new_data_path=str(new_data_path)
                            full_df=load_data(new_data_path)
                            
                            sel_cols3=st.multiselect("Select the features for predicting (in the order 'Point Id','Longitude (X Coordinate)','Latitude (Y Coordinate),'R','G','B', and any other variables) ",full_df.columns.tolist())                
                            pro=st.checkbox('Check this box after selecting necessary columns')
                            if(pro):
                                st.write('You selected:', sel_cols3)
        #                        conf5=st.text_input("Type 'Yes' to confirm the above as your selected features")
                                pid=sel_cols3[0]
                                east=sel_cols3[1]
                                north=sel_cols3[2]
                                
                                r=sel_cols3[3]
                                g=sel_cols3[4]
                                b=sel_cols3[5]
                                
                                if(len(sel_cols3)>6):
                                    w=sel_cols3[6]
                                sel_cols2=sel_cols3[3:]
                                new_df1=pd.read_csv(new_data_path,chunksize=1000000)
                                import gc
                                gc.collect()
                                to_som='No'
                                radius=0
                                threshold=0
                                to_som=st.checkbox("Do you wish to smooth the end prediction results using cKDTree neighbours")
                                if(to_som):
                                    to_som='Yes'
                                    rad=st.text_input("Enter the radius to find the nearest neighbors")
                                    if(rad):
                                        radius=float(rad)
                                        threshold=st.text_input("Enter the threshold value")
                                        if(threshold):
                                            threshold=float(threshold)
                                #st.write(new_df.columns)
                                tar=st.text_input(" Does this data set have the actual target values column? This is usually the case when you want to predict and export on the calibration data itself (Yes/No)",key='551')
                                if(tar=='Yes'):
                                    sel_tar=st.multiselect("Select the target column ",full_df.columns.tolist())                
                                    if(sel_tar):
                                        st.write('You selected:', sel_tar)
                                        #st.write(sel_cols1[-1])                                        
                                        
                                        sav=st.text_input("Do you wish to save this model to local storage for further use? 'Yes' to download both, the model and prediction file / 'No' to download just the prediction file ")
                                        if(sav=='No'):
                                            fn='none'
                                            st.write("Please wait while the full river prediction is running")
                                            #st.write(rf_x.columns)
                                            full_river_process_xgb(new_df1,r,g,b,rf_x,xgb,'XGB',sel_tar,sav,fn,sel_cols2,pid,east,north,to_som,radius,threshold)
                                            del new_df1
                                        if(sav=='Yes'):
                                            fn=st.text_input('Enter the name you wish to give for the folder ',key='10865')
                                            if(fn):
                                                fn=str(fn)+'/'
                                                os.makedirs(os.path.dirname(fn), exist_ok=True)
                                                full_river_process_xgb(new_df1,r,g,b,rf_x,xgb,'XGB',sel_tar,sav,fn,sel_cols2,pid,east,north,to_som,radius,threshold)
                                                del new_df1    
                                if(tar=='No'):
                                    sel_tar='none'
                                    sav=st.text_input("Do you wish to save this model to local storage for further use? 'Yes' to download both, the model and prediction file / 'No' to download just the prediction file ")
                                    if(sav=='No'):
                                        fn='none'
                                        st.write(rf_x.columns)
                                        st.write("Please wait while the full river prediction is running")
                                        full_river_process_xgb(new_df1,r,g,b,rf_x,xgb,'XGB',sel_tar,sav,fn,sel_cols2,pid,east,north,to_som,radius,threshold)
                                        del new_df1
                                    if(sav=='Yes'):
                                        fn=st.text_input('Enter the name you wish to give for the folder ',key='10865')
                                        if(fn):
                                            fn=str(fn)+'/'
                                            os.makedirs(os.path.dirname(fn), exist_ok=True)
                                            full_river_process_xgb(new_df1,r,g,b,rf_x,xgb,'XGB',sel_tar,sav,fn,sel_cols2,pid,east,north,to_som,radius,threshold)
                                            del new_df1
                                    
            if(algo_choice=='Neural Network'):
                st.write("Training ",algo_choice,"...")
                nn,predictions_all,fts,valid_results=NN_reg(nn_x,x_train3,x_test3,y_train3,y_test3,predictions_all)
                st.write("Training dataset Evaluation Metrics : ")
                st.write(fts.head())
                st.subheader("Validation dataset Evaluation Metrics : ")
                st.write(valid_results)
                st.write(predictions_all.head())
                
                st.subheader(" You can now see the visualizations about the insights that the model has learnt from the data, from the left-side bar ") 
                viz_model_insights(nn,nn_x,y)
                
                gr=st.checkbox("Show actual depth vs prediction depth graph ")
                if(gr):
                    layout = go.Layout(
                        title='Distance' + " vs " + 'Depth',
                        xaxis=dict(
                            title='Distance'
                        ),
                        yaxis=dict(
                            title='Depth'
                        ) )
                    fig = go.Figure(layout=layout)
                    fig.add_trace(go.Scatter(x=data['Distance_new'], y=y,
                                        mode='lines+markers',
                                        name='Original data lineplot'))
                                                                
                    fig.add_trace(go.Scatter(x=data['Distance_new'], y=predictions_all['NN_full_Pred'],
                                        mode='lines+markers',
                                        name='Preds lineplot'))
                    st.plotly_chart(fig)
                    #Plotting sorted on depth vs pid
                    st.write(" Actual vs Predicted Depths, Sorted by actual depth")    
                    pre=predictions_all.copy()
                    col0=pre.columns.tolist()[0]
                    pre=pre.sort_values(by=col0)
                    pre=pre.reset_index()
                    
                    layout = go.Layout(
                        title='Distance' + " vs " + 'Depth',
                        xaxis=dict(
                            title='Distance'
                        ),
                        yaxis=dict(
                            title='Depth'
                        ) )
                    fig1 = go.Figure(layout=layout)
                    fig1.add_trace(go.Scatter(x=pre.index, y=pre[col0],
                                        mode='lines+markers',
                                        name='Original data lineplot'))
                                                                
                    fig1.add_trace(go.Scatter(x=pre.index, y=pre['NN_full_Pred'],
                                        mode='lines+markers',
                                        name='Preds lineplot'))
                    st.plotly_chart(fig1)
                    ms=st.checkbox("Do you wish to save the model statistics as text file? ")
                    if(ms):
                        stat=pd.concat([fts,valid_results],axis=1)
                        st.write(stat)
                        stat.to_csv('Model_statistics.txt',index=True)
                        st.write("File saved as Model_statistics.txt")                                            
                    dol=st.checkbox("Use this model for Predicting full River Points ")
                    if(dol):
                        new_data_path=st.text_input("Enter the path of the CSV file, over which the machine learning model has to Predict (It can be training data or full river points data):",key='123')#"/home/lego/Desktop/Job/Land River Sea Consulting Ltd./rangitata_01_new.csv
                        if(new_data_path):
                            new_data_path=str(new_data_path)
                            full_df=load_data(new_data_path)
                        
                            sel_cols3=st.multiselect("Select the features for predicting (in the order 'Point Id','Longitude (X Coordinate)','Latitude (Y Coordinate),'R','G','B', and any other variables) ",full_df.columns.tolist())                
                            pro=st.checkbox('Check this box after selecting necessary columns')
                            if(pro):
                                st.write('You selected:', sel_cols3)
        #                        conf5=st.text_input("Type 'Yes' to confirm the above as your selected features")
                                pid=sel_cols3[0]
                                east=sel_cols3[1]
                                north=sel_cols3[2]
                                
                                r=sel_cols3[3]
                                g=sel_cols3[4]
                                b=sel_cols3[5]
                                
                                if(len(sel_cols3)>6):
                                    w=sel_cols3[6]
                                sel_cols2=sel_cols3[3:]
                                new_df1=pd.read_csv(new_data_path,chunksize=1000000)
                                import gc
                                gc.collect()
                                to_som='No'
                                radius=0
                                threshold=0
                                to_som=st.checkbox("Do you wish to smooth the end prediction results using cKDTree neighbours")
                                if(to_som):
                                    to_som='Yes'
                                    rad=st.text_input("Enter the radius to find the nearest neighbors")
                                    if(rad):
                                        radius=float(rad)
                                        threshold=st.text_input("Enter the threshold value")
                                        if(threshold):
                                            threshold=float(threshold)
                                tar=st.text_input(" Does this data set have the actual target values column? This is usually the case when you want to predict and export on the calibration data itself (Yes/No)",key='551')
                                if(tar=='Yes'):
                                    sel_tar=st.multiselect("Select the target column ",full_df.columns.tolist())                
                                    if(sel_tar):
                                        st.write('You selected:', sel_tar)
                                        
                                        #st.write(sel_cols1[-1])                                        
                                        sav=st.text_input("Do you wish to save this model to local storage for further use? 'Yes' to download both, the model and prediction file / 'No' to download just the prediction file ")
                                        if(sav=='No'):
                                            fn='none'
                                            st.write("Please wait while the full river prediction is running")
                                            full_river_process_nn(new_df1,r,g,b,nn_x,nn,'NN',sel_tar,sav,fn,sel_cols2,pid,east,north,to_som,radius,threshold)
                                            del new_df1
                                        if(sav=='Yes'):
                                            fn=st.text_input('Enter the name you wish to give for the folder ',key='10865')
                                            if(fn):
                                                fn=str(fn)+'/'
                                                os.makedirs(os.path.dirname(fn), exist_ok=True)
                                                full_river_process_nn(new_df1,r,g,b,nn_x,nn,'NN',sel_tar,sav,fn,sel_cols2,pid,east,north,to_som,radius,threshold)
                                                del new_df1    
                                if(tar=='No'):
                                    sel_tar='none'
                                    sav=st.text_input("Do you wish to save this model to local storage for further use? 'Yes' to download both, the model and prediction file / 'No' to download just the prediction file ")
                                    if(sav=='No'):
                                        fn='none'
                                        st.write("Please wait while the full river prediction is running")
                                        full_river_process_nn(new_df1,r,g,b,nn_x,nn,'NN',sel_tar,sav,fn,sel_cols2,pid,east,north,to_som,radius,threshold)
                                        del new_df1
                                    if(sav=='Yes'):
                                        fn=st.text_input('Enter the name you wish to give for the folder ',key='10865')
                                        if(fn):
                                            fn=str(fn)+'/'
                                            os.makedirs(os.path.dirname(fn), exist_ok=True)
                                            full_river_process_nn(new_df1,r,g,b,nn_x,nn,'NN',sel_tar,sav,fn,sel_cols2,pid,east,north,to_som,radius,threshold)
                                            del new_df1
        
            if(algo_choice=='Support Vector Regressor'):
                st.write("Training ",algo_choice,"...")
                svr,predictions_all,fts,valid_results=SVR_reg(sub_x,x_train2,x_test2,y_train2,y_test2,predictions_all)
                st.write("Training dataset Evaluation Metrics : ")
                st.write(fts.head())
                st.subheader("Validation dataset Evaluation Metrics : ")
                st.write(valid_results)
                
                st.write(predictions_all.head())
                
                st.subheader(" You can now see the visualizations about the insights that the model has learnt from the data, from the left-side bar ") 
                viz_model_insights(svr,totrain,y)
                gr=st.checkbox("Show actual depth vs prediction depth graph ")
                if(gr):
                    layout = go.Layout(
                        title='Distance' + " vs " + 'Depth',
                        xaxis=dict(
                            title='Distance'
                        ),
                        yaxis=dict(
                            title='Depth'
                        ) )
                    fig = go.Figure(layout=layout)
                    fig.add_trace(go.Scatter(x=data['Distance_new'], y=y,
                                        mode='lines+markers',
                                        name='Original data lineplot'))
                                                                
                    fig.add_trace(go.Scatter(x=data['Distance_new'], y=predictions_all['SVR_full_Pred'],
                                        mode='lines+markers',
                                        name='Preds lineplot'))
                    st.plotly_chart(fig)
                                                                #Plotting sorted on depth vs pid
                    st.write(" Actual vs Predicted Depths, Sorted by actual depth")    
                    pre=predictions_all.copy()
                    col0=pre.columns.tolist()[0]
                    pre=pre.sort_values(by=col0)
                    pre=pre.reset_index()
                    
                    layout = go.Layout(
                        title='Distance' + " vs " + 'Depth',
                        xaxis=dict(
                            title='Distance'
                        ),
                        yaxis=dict(
                            title='Depth'
                        ) )
                    fig1 = go.Figure(layout=layout)
                    fig1.add_trace(go.Scatter(x=pre.index, y=pre[col0],
                                        mode='lines+markers',
                                        name='Original data lineplot'))
                                                                
                    fig1.add_trace(go.Scatter(x=pre.index, y=pre['SVR_full_Pred'],
                                        mode='lines+markers',
                                        name='Preds lineplot'))
                    st.plotly_chart(fig1)
                    ms=st.checkbox("Do you wish to save the model statistics as text file? ")
                    if(ms):
                        stat=pd.concat([fts,valid_results],axis=1)
                        st.write(stat)
                        stat.to_csv('Model_statistics.txt',index=True)
                        st.write("File saved as Model_statistics.txt")
                    dol=st.checkbox("Use this model for Predicting full River Points ")
                    if(dol):
                        new_data_path=st.text_input("Enter the path of the CSV file, over which the machine learning model has to Predict (It can be training data or full river points data): ",key='123')#"/home/lego/Desktop/Job/Land River Sea Consulting Ltd./rangitata_01_new.csv
                        if(new_data_path):
                            new_data_path=str(new_data_path)
                            full_df=load_data(new_data_path)
                        
                            sel_cols3=st.multiselect("Select the features for predicting (in the order 'Point Id','Longitude (X Coordinate)','Latitude (Y Coordinate),'R','G','B', and any other variables) ",full_df.columns.tolist())                
                            pro=st.checkbox('Check this box after selecting necessary columns')
                            if(pro):
                                st.write('You selected:', sel_cols3)
        #                        conf5=st.text_input("Type 'Yes' to confirm the above as your selected features")
                                pid=sel_cols3[0]
                                east=sel_cols3[1]
                                north=sel_cols3[2]
                                
                                r=sel_cols3[3]
                                g=sel_cols3[4]
                                b=sel_cols3[5]
                                
                                if(len(sel_cols3)>6):
                                    w=sel_cols3[6]
                                sel_cols2=sel_cols3[3:]
                                new_df1=pd.read_csv(new_data_path,chunksize=1000000)
                                import gc
                                gc.collect()
                                to_som='No'
                                radius=0
                                threshold=0
                                to_som=st.checkbox("Do you wish to smooth the end prediction results using cKDTree neighbours")
                                if(to_som):
                                    to_som='Yes'
                                    rad=st.text_input("Enter the radius to find the nearest neighbors")
                                    if(rad):
                                        radius=float(rad)
                                        threshold=st.text_input("Enter the threshold value")
                                        if(threshold):
                                            threshold=float(threshold)
                                
                                tar=st.text_input(" Does this data set have the actual target values column? This is usually the case when you want to predict and export on the calibration data itself (Yes/No)",key='551')
                                if(tar=='Yes'):
                                    sel_tar=st.multiselect("Select the target column ",full_df.columns.tolist())                
                                    if(sel_tar):
                                        st.write('You selected:', sel_tar)
                                        
                                        #st.write(sel_cols1[-1])                                        
                                        sav=st.text_input("Do you wish to save this model to local storage for further use? 'Yes' to download both, the model and prediction file / 'No' to download just the prediction file ")
                                        if(sav=='No'):
                                            fn='none'
                                            st.write("Please wait while the full river prediction is running")
                                            full_river_process_svr(new_df1,r,g,b,sub_x,svr,'SVR',sel_tar,sav,fn,sel_cols2,pid,east,north,to_som,radius,threshold)
                                            del new_df1
                                        if(sav=='Yes'):
                                            fn=st.text_input('Enter the name you wish to give for the folder ',key='10865')
                                            if(fn):
                                                fn=str(fn)+'/'
                                                os.makedirs(os.path.dirname(fn), exist_ok=True)
                                                full_river_process_svr(new_df1,r,g,b,sub_x,svr,'SVR',sel_tar,sav,fn,sel_cols2,pid,east,north,to_som,radius,threshold)
                                                del new_df1     
                                if(tar=='No'):
                                    sel_tar='none'
                                    sav=st.text_input("Do you wish to save this model to local storage for further use? 'Yes' to download both, the model and prediction file / 'No' to download just the prediction file ")
                                    if(sav=='No'):
                                        fn='none'
                                        st.write("Please wait while the full river prediction is running")
                                        full_river_process_svr(new_df1,r,g,b,sub_x,svr,'SVR',sel_tar,sav,fn,sel_cols2,pid,east,north,to_som,radius,threshold)
                                        del new_df1
                                    if(sav=='Yes'):
                                        fn=st.text_input('Enter the name you wish to give for the folder ',key='10865')
                                        if(fn):
                                            fn=str(fn)+'/'
                                            os.makedirs(os.path.dirname(fn), exist_ok=True)
                                            full_river_process_svr(new_df1,r,g,b,sub_x,svr,'SVR',sel_tar,sav,fn,sel_cols2,pid,east,north,to_som,radius,threshold)
                                            del new_df1
        
            if(algo_choice=='Try all models'):
                st.subheader("Training all 6 models....")
                lr,nn,rf,xgb,svr,predictions_all5,final_scores_test,valid_results=All_reg(sub_x,nn_x,rf_x,x_train,x_test,y_train,y_test,x_train2,x_test2,y_train2,y_test2,x_train3,x_test3,y_train3,y_test3,predictions_all)
                st.subheader("Full dataset statistics : ")
                st.write(final_scores_test)
                st.subheader("Validation dataset statistics : ")
                st.write(valid_results)
                
                st.subheader(" A few actual vs predicted results :")
                st.write(predictions_all5.head())
                
                #st.subheader(" You can now see the visualizations about the insights that the model has learnt from the data, from the left-side bar . ########IN PROGRESS######### ") 
                #viz_model_insights(xgb,totrain,y)
                
                gr1=st.checkbox("Show actual depth vs prediction depth graph ")
                if(gr1):
                    st.write(".")
                    mods=['Linear Regression','Neural Network','Random Forest','XGBoost','Support Vector Machines(SVR)','All models']
                    radio_vals=st.radio("Select any of the following model(s)",mods,key='7051')
        
                    if(radio_vals=='Linear Regression'):
                        layout = go.Layout(
                            title='Distance' + " vs " + 'Depth',
                            xaxis=dict(
                                title='Distance'
                            ),
                            yaxis=dict(
                                title='Depth'
                            ) )
                        fig = go.Figure(layout=layout)
                        fig.add_trace(go.Scatter(x=data['Distance_new'], y=y,
                                            mode='lines+markers',
                                            name='Original data lineplot'))
                        fig.add_trace(go.Scatter(x=data['Distance_new'], y=predictions_all5['LR_full_Pred'],
                                       mode='lines+markers',
                                       name='Linear Regression lineplot'))       
                        st.plotly_chart(fig)
                    if(radio_vals=='Neural Network'):
                        layout = go.Layout(
                            title='Distance' + " vs " + 'Depth',
                            xaxis=dict(
                                title='Distance'
                            ),
                            yaxis=dict(
                                title='Depth'
                            ) )
                        fig = go.Figure(layout=layout)        
                        fig.add_trace(go.Scatter(x=data['Distance_new'], y=y,
                                            mode='lines+markers',
                                            name='Original data lineplot'))
                        fig.add_trace(go.Scatter(x=data['Distance_new'], y=predictions_all5['NN_full_Pred'],
                                       mode='lines+markers',
                                       name='Neural Network lineplot'))       
                        st.plotly_chart(fig)
                    if(radio_vals=='Random Forest'):
                        layout = go.Layout(
                            title='Distance' + " vs " + 'Depth',
                            xaxis=dict(
                                title='Distance'
                            ),
                            yaxis=dict(
                                title='Depth'
                            ) )
                        fig = go.Figure(layout=layout)       
                        fig.add_trace(go.Scatter(x=data['Distance_new'], y=y,
                                            mode='lines+markers',
                                            name='Original data lineplot'))
                        fig.add_trace(go.Scatter(x=data['Distance_new'], y=predictions_all5['RF_full_Pred'],
                                       mode='lines+markers',
                                       name='Random Forest lineplot'))       
                        st.plotly_chart(fig)
                    if(radio_vals=='XGBoost'):
                        layout = go.Layout(
                            title='Distance' + " vs " + 'Depth',
                            xaxis=dict(
                                title='Distance'
                            ),
                            yaxis=dict(
                                title='Depth'
                            ) )
                        fig = go.Figure(layout=layout)        
                        fig.add_trace(go.Scatter(x=data['Distance_new'], y=y,
                                            mode='lines+markers',
                                            name='Original data lineplot'))
                        fig.add_trace(go.Scatter(x=data['Distance_new'], y=predictions_all5['XGB_full_Pred'],
                                       mode='lines+markers',
                                       name='XGBoost lineplot'))       
                        st.plotly_chart(fig)    
                    if(radio_vals=='Support Vector Machines(SVR)'):
                        layout = go.Layout(
                            title='Distance' + " vs " + 'Depth',
                            xaxis=dict(
                                title='Distance'
                            ),
                            yaxis=dict(
                                title='Depth'
                            ) )
                        fig = go.Figure(layout=layout)        
                        fig.add_trace(go.Scatter(x=data['Distance_new'], y=y,
                                            mode='lines+markers',
                                            name='Original data lineplot'))
                        fig.add_trace(go.Scatter(x=data['Distance_new'], y=predictions_all5['SVR_full_Pred'],
                                       mode='lines+markers',
                                       name='Support Vector Machines(SVR) lineplot'))       
                        st.plotly_chart(fig)    
                    if(radio_vals=='All models'):    
                        layout = go.Layout(
                            title='Distance' + " vs " + 'Depth',
                            xaxis=dict(
                                title='Distance'
                            ),
                            yaxis=dict(
                                title='Depth'
                            ) )
                        fig = go.Figure(layout=layout)
                        fig.add_trace(go.Scatter(x=data['Distance_new'], y=y,
                                                 mode='lines+markers',
                                                name='Original data lineplot'))
                                                                    
                        fig.add_trace(go.Scatter(x=data['Distance_new'], y=predictions_all5['LR_full_Pred'],
                                                 mode='lines',
                                                 name='Linear Regression lineplot'))
                        fig.add_trace(go.Scatter(x=data['Distance_new'], y=predictions_all5['NN_full_Pred'],
                                                 mode='lines',
                                                 name='Neural Network lineplot'))
                        fig.add_trace(go.Scatter(x=data['Distance_new'], y=predictions_all5['RF_full_Pred'],
                                                 mode='lines',
                                                 name='Random Forest lineplot'))
                        fig.add_trace(go.Scatter(x=data['Distance_new'], y=predictions_all5['XGB_full_Pred'],
                                                 mode='lines',
                                                 name='XGBoost lineplot'))
                        fig.add_trace(go.Scatter(x=data['Distance_new'], y=predictions_all5['SVR_full_Pred'],
                                                 mode='lines',
                                                 name='Support Vector Machines(SVR) lineplot'))
                        st.plotly_chart(fig)
                        
                                                                    #Plotting sorted on depth vs pid
                    st.write("Actual vs Predicted Depths, Sorted by actual depth")    
                    pre=predictions_all5.copy()
                    col0=pre.columns.tolist()[0]
                    pre=pre.sort_values(by=col0)
                    pre=pre.reset_index()
            
                    mods=['Linear Regression','Neural Network','Random Forest','XGBoost','Support Vector Machines(SVR)','All models']
                    radio_vals=st.radio("Select any of the following model(s)",mods)
        
                    if(radio_vals=='Linear Regression'):
                        layout = go.Layout(
                            title='Distance' + " vs " + 'Depth',
                            xaxis=dict(
                                title='Distance'
                            ),
                            yaxis=dict(
                                title='Depth'
                            ) )
                        fig59 = go.Figure(layout=layout)
                        fig59.add_trace(go.Scatter(x=pre.index, y=pre[col0],
                                        mode='lines+markers',
                                        name='Original data lineplot'))
                        fig59.add_trace(go.Scatter(x=pre.index, y=pre['LR_full_Pred'],
                                       mode='lines+markers',
                                       name='Linear Regression lineplot'))       
                        st.plotly_chart(fig59)
                        
                    if(radio_vals=='Neural Network'):
                        layout = go.Layout(
                            title='Distance' + " vs " + 'Depth',
                            xaxis=dict(
                                title='Distance'
                            ),
                            yaxis=dict(
                                title='Depth'
                            ) )
                        fig23 = go.Figure(layout=layout)
                        fig23.add_trace(go.Scatter(x=pre.index, y=pre[col0],
                                        mode='lines+markers',
                                        name='Original data lineplot'))
                        fig23.add_trace(go.Scatter(x=pre.index, y=pre['NN_full_Pred'],
                                       mode='lines+markers',
                                       name='Neural Network lineplot'))       
                        st.plotly_chart(fig23)
                        
                    if(radio_vals=='Random Forest'):
                        layout = go.Layout(
                            title='Distance' + " vs " + 'Depth',
                            xaxis=dict(
                                title='Distance'
                            ),
                            yaxis=dict(
                                title='Depth'
                            ) )
                        fig22 = go.Figure(layout=layout)
                        fig22.add_trace(go.Scatter(x=pre.index, y=pre[col0],
                                        mode='lines+markers',
                                        name='Original data lineplot'))
                        fig22.add_trace(go.Scatter(x=pre.index, y=pre['RF_full_Pred'],
                                       mode='lines+markers',
                                       name='Random Forest lineplot'))       
                        st.plotly_chart(fig22)
                        
                    if(radio_vals=='XGBoost'):
                        layout = go.Layout(
                            title='Distance' + " vs " + 'Depth',
                            xaxis=dict(
                                title='Distance'
                            ),
                            yaxis=dict(
                                title='Depth'
                            ) )
                        fig19 = go.Figure(layout=layout)
                        fig19.add_trace(go.Scatter(x=pre.index, y=pre[col0],
                                        mode='lines+markers',
                                        name='Original data lineplot'))
                        fig19.add_trace(go.Scatter(x=pre.index, y=pre['XGB_full_Pred'],
                                       mode='lines+markers',
                                       name='XGBoost lineplot'))       
                        st.plotly_chart(fig19)    
                        
                    if(radio_vals=='Support Vector Machines(SVR)'):
                        layout = go.Layout(
                            title='Distance' + " vs " + 'Depth',
                            xaxis=dict(
                                title='Distance'
                            ),
                            yaxis=dict(
                                title='Depth'
                            ) )
                        fig20 = go.Figure(layout=layout)
                        fig20.add_trace(go.Scatter(x=pre.index, y=pre[col0],
                                        mode='lines+markers',
                                        name='Original data lineplot'))
                        fig20.add_trace(go.Scatter(x=pre.index, y=pre['SVR_full_Pred'],
                                       mode='lines+markers',
                                       name='Support Vector Machines(SVR) lineplot'))       
                        st.plotly_chart(fig20)    
                        
                    if(radio_vals=='All models'):    
                        layout = go.Layout(
                            title='Distance' + " vs " + 'Depth',
                            xaxis=dict(
                                title='Distance'
                            ),
                            yaxis=dict(
                                title='Depth'
                            ) )
                        fig9 = go.Figure(layout=layout)
                        fig9.add_trace(go.Scatter(x=pre.index, y=pre[col0],
                                        mode='lines+markers',
                                        name='Original data lineplot'))
                                                                    
                        fig9.add_trace(go.Scatter(x=pre.index, y=pre['LR_full_Pred'],
                                            mode='lines',
                                            name='Linear Regression lineplot'))
                        fig9.add_trace(go.Scatter(x=pre.index, y=pre['NN_full_Pred'],
                                                 mode='lines',
                                                 name='Neural Network lineplot'))
                        fig9.add_trace(go.Scatter(x=pre.index, y=pre['RF_full_Pred'],
                                                 mode='lines',
                                                 name='Random Forest lineplot'))
                        fig9.add_trace(go.Scatter(x=pre.index, y=pre['XGB_full_Pred'],
                                                 mode='lines',
                                                 name='XGBoost lineplot'))
                        fig9.add_trace(go.Scatter(x=pre.index, y=pre['SVR_full_Pred'],
                                                 mode='lines',
                                                 name='Support Vector Machines(SVR) lineplot'))
                        st.plotly_chart(fig9)
                    
                    #showing some more insights
                    st.subheader("Check out the 'Model Explainability' bar on the left-side, to interpret some advanced insights about the models")
                    viz_model_insights_all(lr,nn,rf,xgb,svr,totrain,y)
                    
                    ms=st.checkbox("Do you wish to save all models' statistic as text file? ")
                    if(ms):
                        stat=pd.concat([final_scores_test,valid_results],axis=1)
                        st.write(stat)
                        stat.to_csv('All_Model_statistics.txt',index=True)
                        st.write("File saved as All_Model_statistics.txt")
                        
                    selecb=['None']
                    for j in mods:
                        selecb.append(j)
                    mod=st.selectbox("Select the best model for Predicting full River Points ",selecb[:-1])                                   
                    
                    if(mod=='Linear Regression'):
                        new_data_path=st.text_input("Enter the path of the CSV file, over which the machine learning model has to Predict (It can be training data or full river points data): ",key='167')#"/home/lego/Desktop/Job/Land River Sea Consulting Ltd./rangitata_01_new.csv
                        if(new_data_path):
                            new_data_path=str(new_data_path)
                            full_df=load_data(new_data_path)
                            
                            sel_cols3=st.multiselect("Select the features for predicting (in the order 'Point Id','Longitude (X Coordinate)','Latitude (Y Coordinate),'R','G','B', and any other variables) ",full_df.columns.tolist())                
                            pro=st.checkbox('Check this box after selecting necessary columns')
                            if(pro):
                                st.write('You selected:', sel_cols3)
        #                        conf5=st.text_input("Type 'Yes' to confirm the above as your selected features")
                                pid=sel_cols3[0]
                                east=sel_cols3[1]
                                north=sel_cols3[2]
                                
                                r=sel_cols3[3]
                                g=sel_cols3[4]
                                b=sel_cols3[5]
                                
                                if(len(sel_cols3)>6):
                                    w=sel_cols3[6]
                                sel_cols2=sel_cols3[3:]
                                new_df1=pd.read_csv(new_data_path,chunksize=1000000)
                                import gc
                                gc.collect()
                                to_som='No'
                                radius=0
                                threshold=0
                                to_som=st.checkbox("Do you wish to smooth the end prediction results using cKDTree neighbours")
                                if(to_som):
                                    to_som='Yes'
                                    rad=st.text_input("Enter the radius to find the nearest neighbors")
                                    if(rad):
                                        radius=float(rad)
                                        threshold=st.text_input("Enter the threshold value")
                                        if(threshold):
                                            threshold=float(threshold)
                                tar=st.text_input(" Does this data set have the actual target values column? This is usually the case when you want to predict and export on the calibration data itself (Yes/No)",key='551')
                                if(tar=='Yes'):
                                    sel_tar=st.multiselect("Select the target column ",full_df.columns.tolist())                
                                    if(sel_tar):
                                        st.write('You selected:', sel_tar)
                                        
                                        #st.write(sel_cols1[-1])                                        
                                        sav=st.text_input("Do you wish to save this model to local storage for further use? 'Yes' to download both, the model and prediction file / 'No' to download just the prediction file ")
                                        if(sav=='No'):
                                            fn='none'
                                            st.write("Please wait while the full river prediction is running")
                                            full_river_process_lr(new_df1,r,g,b,sub_x,lr,'LR',sel_tar,sav,fn,sel_cols2,pid,east,north,to_som,radius,threshold)
                                            del new_df1
                                        if(sav=='Yes'):
                                            fn=st.text_input('Enter the name you wish to give for the folder ',key='10865')
                                            if(fn):
                                                fn=str(fn)+'/'
                                                os.makedirs(os.path.dirname(fn), exist_ok=True)
                                                full_river_process_lr(new_df1,r,g,b,sub_x,lr,'LR',sel_tar,sav,fn,sel_cols2,pid,east,north,to_som,radius,threshold)
                                                del new_df1     
                                if(tar=='No'):
                                    sel_tar='none'
                                    sav=st.text_input("Do you wish to save this model to local storage for further use? 'Yes' to download both, the model and prediction file / 'No' to download just the prediction file ")
                                    if(sav=='No'):
                                        fn='none'
                                        st.write("Please wait while the full river prediction is running")
                                        full_river_process_lr(new_df1,r,g,b,sub_x,lr,'LR',sel_tar,sav,fn,sel_cols2,pid,east,north,to_som,radius,threshold)
                                        del new_df1
                                    if(sav=='Yes'):
                                        fn=st.text_input('Enter the name you wish to give for the folder ',key='10865')
                                        if(fn):
                                            fn=str(fn)+'/'
                                            os.makedirs(os.path.dirname(fn), exist_ok=True)
                                            full_river_process_lr(new_df1,r,g,b,sub_x,lr,'LR',sel_tar,sav,fn,sel_cols2,pid,east,north,to_som,radius,threshold)
                                            del new_df1
                    if(mod=='Neural Network'):
                        new_data_path=st.text_input("Enter the path of the CSV file, over which the machine learning model has to Predict (It can be training data or full river points data): ",key='1117')#"/home/lego/Desktop/Job/Land River Sea Consulting Ltd./rangitata_01_new.csv
                        if(new_data_path):
                            new_data_path=str(new_data_path)
                            full_df=load_data(new_data_path)
                            sel_cols3=st.multiselect("Select the features for predicting (in the order 'Point Id','Longitude (X Coordinate)','Latitude (Y Coordinate),'R','G','B', and any other variables) ",full_df.columns.tolist())                
                            pro=st.checkbox('Check this box after selecting necessary columns')
                            if(pro):
                                st.write('You selected:', sel_cols3)
        #                        conf5=st.text_input("Type 'Yes' to confirm the above as your selected features")
                                pid=sel_cols3[0]
                                east=sel_cols3[1]
                                north=sel_cols3[2]
                                
                                r=sel_cols3[3]
                                g=sel_cols3[4]
                                b=sel_cols3[5]
                                
                                if(len(sel_cols3)>6):
                                    w=sel_cols3[6]
                                sel_cols2=sel_cols3[3:]
                                new_df1=pd.read_csv(new_data_path,chunksize=1000000)
                                import gc
                                gc.collect()
                                to_som='No'
                                radius=0
                                threshold=0
                                to_som=st.checkbox("Do you wish to smooth the end prediction results using cKDTree neighbours")
                                if(to_som):
                                    to_som='Yes'
                                    rad=st.text_input("Enter the radius to find the nearest neighbors")
                                    if(rad):
                                        radius=float(rad)
                                        threshold=st.text_input("Enter the threshold value")
                                        if(threshold):
                                            threshold=float(threshold)
                                tar=st.text_input(" Does this data set have the actual target values column? This is usually the case when you want to predict and export on the calibration data itself (Yes/No)",key='551')
                                if(tar=='Yes'):
                                    sel_tar=st.multiselect("Select the target column ",full_df.columns.tolist())                
                                    if(sel_tar):
                                        st.write('You selected:', sel_tar)
                                        
                                        #st.write(sel_cols1[-1])                                        
                                        sav=st.text_input("Do you wish to save this model to local storage for further use? 'Yes' to download both, the model and prediction file / 'No' to download just the prediction file ")
                                        if(sav=='No'):
                                            fn='none'
                                            st.write("Please wait while the full river prediction is running")
                                            full_river_process_nn(new_df1,r,g,b,nn_x,nn,'NN',sel_tar,sav,fn,sel_cols2,pid,east,north,to_som,radius,threshold)
                                            del new_df1
                                        if(sav=='Yes'):
                                            fn=st.text_input('Enter the name you wish to give for the folder ',key='10865')
                                            if(fn):
                                                fn=str(fn)+'/'
                                                os.makedirs(os.path.dirname(fn), exist_ok=True)
                                                full_river_process_nn(new_df1,r,g,b,nn_x,nn,'NN',sel_tar,sav,fn,sel_cols2,pid,east,north,to_som,radius,threshold)
                                                del new_df1    
                                if(tar=='No'):
                                    sel_tar='none'
                                    sav=st.text_input("Do you wish to save this model to local storage for further use? 'Yes' to download both, the model and prediction file / 'No' to download just the prediction file ")
                                    if(sav=='No'):
                                        fn='none'
                                        st.write("Please wait while the full river prediction is running")
                                        full_river_process_nn(new_df1,r,g,b,nn_x,nn,'NN',sel_tar,sav,fn,sel_cols2,pid,east,north,to_som,radius,threshold)
                                        del new_df1
                                    if(sav=='Yes'):
                                        fn=st.text_input('Enter the name you wish to give for the folder ',key='10865')
                                        if(fn):
                                            fn=str(fn)+'/'
                                            os.makedirs(os.path.dirname(fn), exist_ok=True)
                                            full_river_process_nn(new_df1,r,g,b,nn_x,nn,'NN',sel_tar,sav,fn,sel_cols2,pid,east,north,to_som,radius,threshold)
                                            del new_df1
        
                    
                    if(mod=='Random Forest'):
                        new_data_path=st.text_input("Enter the path of the CSV file, over which the machine learning model has to Predict (It can be training data or full river points data): ",key='1117')#"/home/lego/Desktop/Job/Land River Sea Consulting Ltd./rangitata_01_new.csv
                        if(new_data_path):
                            new_data_path=str(new_data_path)
                            full_df=load_data(new_data_path)
                            sel_cols3=st.multiselect("Select the features for predicting (in the order 'Point Id','Longitude (X Coordinate)','Latitude (Y Coordinate),'R','G','B', and any other variables) ",full_df.columns.tolist())                
                            pro=st.checkbox('Check this box after selecting necessary columns')
                            if(pro):
                                st.write('You selected:', sel_cols3)
        #                        conf5=st.text_input("Type 'Yes' to confirm the above as your selected features")
                                pid=sel_cols3[0]
                                east=sel_cols3[1]
                                north=sel_cols3[2]
                                
                                r=sel_cols3[3]
                                g=sel_cols3[4]
                                b=sel_cols3[5]
                                
                                if(len(sel_cols3)>6):
                                    w=sel_cols3[6]
                                sel_cols2=sel_cols3[3:]
                                new_df1=pd.read_csv(new_data_path,chunksize=1000000)
                                import gc
                                gc.collect()
                                to_som='No'
                                radius=0
                                threshold=0
                                to_som=st.checkbox("Do you wish to smooth the end prediction results using cKDTree neighbours")
                                if(to_som):
                                    to_som='Yes'
                                    rad=st.text_input("Enter the radius to find the nearest neighbors")
                                    if(rad):
                                        radius=float(rad)
                                        threshold=st.text_input("Enter the threshold value")
                                        if(threshold):
                                            threshold=float(threshold)
                                tar=st.text_input(" Does this data set have the actual target values column? This is usually the case when you want to predict and export on the calibration data itself (Yes/No)",key='551')
                                if(tar=='Yes'):
                                    sel_tar=st.multiselect("Select the target column ",full_df.columns.tolist())                
                                    if(sel_tar):
                                        st.write('You selected:', sel_tar)
                                        
                                        #st.write(sel_cols1[-1])                                        
                                        sav=st.text_input("Do you wish to save this model to local storage for further use? 'Yes' to download both, the model and prediction file / 'No' to download just the prediction file ")
                                        if(sav=='No'):
                                            fn='none'
                                            st.write("Please wait while the full river prediction is running")
                                            full_river_process_rf(new_df1,r,g,b,rf_x,rf,'RF',sel_tar,sav,fn,sel_cols2,pid,east,north,to_som,radius,threshold)
                                            del new_df1
                                        if(sav=='Yes'):
                                            fn=st.text_input('Enter the name you wish to give for the folder ',key='10865')
                                            if(fn):
                                                fn=str(fn)+'/'
                                                os.makedirs(os.path.dirname(fn), exist_ok=True)
                                                full_river_process_rf(new_df1,r,g,b,rf_x,rf,'RF',sel_tar,sav,fn,sel_cols2,pid,east,north,to_som,radius,threshold)
                                                del new_df1    
                                if(tar=='No'):
                                    sel_tar='none'
                                    sav=st.text_input("Do you wish to save this model to local storage for further use? 'Yes' to download both, the model and prediction file / 'No' to download just the prediction file ")
                                    if(sav=='No'):
                                        fn='none'
                                        st.write("Please wait while the full river prediction is running")
                                        full_river_process_rf(new_df1,r,g,b,rf_x,rf,'RF',sel_tar,sav,fn,sel_cols2,pid,east,north,to_som,radius,threshold)
                                        del new_df1
                                    if(sav=='Yes'):
                                        fn=st.text_input('Enter the name you wish to give for the folder ',key='10865')
                                        if(fn):
                                            fn=str(fn)+'/'
                                            os.makedirs(os.path.dirname(fn), exist_ok=True)
                                            full_river_process_rf(new_df1,r,g,b,rf_x,rf,'RF',sel_tar,sav,fn,sel_cols2,pid,east,north,to_som,radius,threshold)
                                            del new_df1
                    
                    if(mod=='XGBoost'):
                        new_data_path=st.text_input("Enter the path of the CSV file, over which the machine learning model has to Predict (It can be training data or full river points data):",key='1117')#"/home/lego/Desktop/Job/Land River Sea Consulting Ltd./rangitata_01_new.csv
                        if(new_data_path):
                            new_data_path=str(new_data_path)
                            full_df=load_data(new_data_path)
                            sel_cols3=st.multiselect("Select the features for predicting (in the order 'Point Id','Longitude (X Coordinate)','Latitude (Y Coordinate),'R','G','B', and any other variables) ",full_df.columns.tolist())                
                            pro=st.checkbox('Check this box after selecting necessary columns')
                            if(pro):
                                st.write('You selected:', sel_cols3)
        #                        conf5=st.text_input("Type 'Yes' to confirm the above as your selected features")
                                pid=sel_cols3[0]
                                east=sel_cols3[1]
                                north=sel_cols3[2]
                                
                                r=sel_cols3[3]
                                g=sel_cols3[4]
                                b=sel_cols3[5]
                                
                                if(len(sel_cols3)>6):
                                    w=sel_cols3[6]
                                sel_cols2=sel_cols3[3:]
                                new_df1=pd.read_csv(new_data_path,chunksize=1000000)
                                import gc
                                gc.collect()
                                to_som='No'
                                radius=0
                                threshold=0
                                to_som=st.checkbox("Do you wish to smooth the end prediction results using cKDTree neighbours")
                                if(to_som):
                                    to_som='Yes'
                                    rad=st.text_input("Enter the radius to find the nearest neighbors")
                                    if(rad):
                                        radius=float(rad)
                                        threshold=st.text_input("Enter the threshold value")
                                        if(threshold):
                                            threshold=float(threshold)
                                tar=st.text_input(" Does this data set have the actual target values column? This is usually the case when you want to predict and export on the calibration data itself (Yes/No)",key='551')
                                if(tar=='Yes'):
                                    sel_tar=st.multiselect("Select the target column ",full_df.columns.tolist())                
                                    if(sel_tar):
                                        st.write('You selected:', sel_tar)
                                        
                                        #st.write(sel_cols1[-1])                                        
                                        sav=st.text_input("Do you wish to save this model to local storage for further use? 'Yes' to download both, the model and prediction file / 'No' to download just the prediction file ")
                                        if(sav=='No'):
                                            fn='none'
                                            st.write("Please wait while the full river prediction is running")
                                            full_river_process_xgb(new_df1,r,g,b,rf_x,xgb,'XGB',sel_tar,sav,fn,sel_cols2,pid,east,north,to_som,radius,threshold)
                                            del new_df1
                                        if(sav=='Yes'):
                                            fn=st.text_input('Enter the name you wish to give for the folder ',key='10865')
                                            if(fn):
                                                fn=str(fn)+'/'
                                                os.makedirs(os.path.dirname(fn), exist_ok=True)
                                                full_river_process_xgb(new_df1,r,g,b,rf_x,xgb,'XGB',sel_tar,sav,fn,sel_cols2,pid,east,north,to_som,radius,threshold)
                                                del new_df1   
                                if(tar=='No'):
                                    sel_tar='none'
                                    sav=st.text_input("Do you wish to save this model to local storage for further use? 'Yes' to download both, the model and prediction file / 'No' to download just the prediction file ")
                                    if(sav=='No'):
                                        fn='none'
                                        st.write("Please wait while the full river prediction is running")
                                        full_river_process_xgb(new_df1,r,g,b,rf_x,xgb,'XGB',sel_tar,sav,fn,sel_cols2,pid,east,north,to_som,radius,threshold)
                                        del new_df1
                                    if(sav=='Yes'):
                                        fn=st.text_input('Enter the name you wish to give for the folder ',key='10865')
                                        if(fn):
                                            fn=str(fn)+'/'
                                            os.makedirs(os.path.dirname(fn), exist_ok=True)
                                            full_river_process_xgb(new_df1,r,g,b,rf_x,xgb,'XGB',sel_tar,sav,fn,sel_cols2,pid,east,north,to_som,radius,threshold)
                                            del new_df1
                                            
                    if(mod=='Support Vector Machines(SVR)'):
                        new_data_path=st.text_input("Enter the path of the CSV file, over which the machine learning model has to Predict (It can be training data or full river points data): ",key='1117')#"/home/lego/Desktop/Job/Land River Sea Consulting Ltd./rangitata_01_new.csv
                        if(new_data_path):
                            new_data_path=str(new_data_path)
                            full_df=load_data(new_data_path)
                            sel_cols3=st.multiselect("Select the features for predicting (in the order 'Point Id','Longitude (X Coordinate)','Latitude (Y Coordinate),'R','G','B', and any other variables) ",full_df.columns.tolist())                
                            pro=st.checkbox('Check this box after selecting necessary columns')
                            if(pro):
                                st.write('You selected:', sel_cols3)
        #                        conf5=st.text_input("Type 'Yes' to confirm the above as your selected features")
                                pid=sel_cols3[0]
                                east=sel_cols3[1]
                                north=sel_cols3[2]
                                
                                r=sel_cols3[3]
                                g=sel_cols3[4]
                                b=sel_cols3[5]
                                
                                if(len(sel_cols3)>6):
                                    w=sel_cols3[6]
                                sel_cols2=sel_cols3[3:]
                                new_df1=pd.read_csv(new_data_path,chunksize=1000000)
                                import gc
                                gc.collect()
                                to_som='No'
                                radius=0
                                threshold=0
                                to_som=st.checkbox("Do you wish to smooth the end prediction results using cKDTree neighbours")
                                if(to_som):
                                    to_som='Yes'
                                    rad=st.text_input("Enter the radius to find the nearest neighbors")
                                    if(rad):
                                        radius=float(rad)
                                        threshold=st.text_input("Enter the threshold value")
                                        if(threshold):
                                            threshold=float(threshold)
                                tar=st.text_input(" Does this data set have the actual target values column? This is usually the case when you want to predict and export on the calibration data itself (Yes/No)",key='551')
                                if(tar=='Yes'):
                                    sel_tar=st.multiselect("Select the target column ",full_df.columns.tolist())                
                                    if(sel_tar):
                                        st.write('You selected:', sel_tar)
                                        
                                        #st.write(sel_cols1[-1])                                        
                                        sav=st.text_input("Do you wish to save this model to local storage for further use? 'Yes' to download both, the model and prediction file / 'No' to download just the prediction file ")
                                        if(sav=='No'):
                                            fn='none'
                                            st.write("Please wait while the full river prediction is running")
                                            full_river_process_svr(new_df1,r,g,b,sub_x,svr,'SVR',sel_tar,sav,fn,sel_cols2,pid,east,north,to_som,radius,threshold)
                                            del new_df1
                                        if(sav=='Yes'):
                                            fn=st.text_input('Enter the name you wish to give for the folder ',key='10865')
                                            if(fn):
                                                fn=str(fn)+'/'
                                                os.makedirs(os.path.dirname(fn), exist_ok=True)
                                                full_river_process_svr(new_df1,r,g,b,sub_x,svr,'SVR',sel_tar,sav,fn,sel_cols2,pid,east,north,to_som,radius,threshold)
                                                del new_df1
                                                     
                                if(tar=='No'):
                                    sel_tar='none'
                                    sav=st.text_input("Do you wish to save this model to local storage for further use? 'Yes' to download both, the model and prediction file / 'No' to download just the prediction file ")
                                    if(sav=='No'):
                                        fn='none'
                                        st.write("Please wait while the full river prediction is running")
                                        full_river_process_svr(new_df1,r,g,b,sub_x,svr,'SVR',sel_tar,sav,fn,sel_cols2,pid,east,north,to_som,radius,threshold)
                                        del new_df1
                                    if(sav=='Yes'):
                                        fn=st.text_input('Enter the name you wish to give for the folder ',key='10865')
                                        if(fn):
                                            fn=str(fn)+'/'
                                            os.makedirs(os.path.dirname(fn), exist_ok=True)
                                            full_river_process_svr(new_df1,r,g,b,sub_x,svr,'SVR',sel_tar,sav,fn,sel_cols2,pid,east,north,to_som,radius,threshold)
                                            del new_df1
                                    
@st.cache(persist=True, suppress_st_warning=True,allow_output_mutation=True)
def only_xgb(x_train,y_train):
    xgb1=RandomForestRegressor(n_estimators=250,random_state=50)
    xgb1.fit(x_train,y_train)
    return xgb1


def full_river_process_lr(new_df1,r,g,b,sub_x,model,model_name,sel_tar,sav,fn,sel_cols2,pid,east,north,to_som,radius,threshold):
    #st.write(hutt_full.head())
    full_river=pd.DataFrame()
    for jun in new_df1:
        to_merge=pd.DataFrame()
        merged=pd.DataFrame()
        st.write(" Current batch of shape ",jun.shape)
        jun=jun.reset_index().drop(['index'],axis=1) 
        hutt_full=pd.DataFrame()
        to_merge=pd.DataFrame(jun[[pid,east,north]])

        hutt_full=jun[sel_cols2].copy()
        lnbr=[]
        lnbg=[]
        #hut full has r,g,b,w only
        r_in=hutt_full.columns.get_loc(r)
        g_in=hutt_full.columns.get_loc(g)
        b_in=hutt_full.columns.get_loc(b)
        te=hutt_full.values
        for i in range(len(te)):
          val=np.log10(te[i,b_in]/te[i,r_in])
          lnbr.append(val)      
          val1=np.log10(te[i,b_in]/te[i,g_in])
          lnbg.append(val1)
              
        hutt_full['ln(b/r)']=np.array(lnbr)
        hutt_full['ln(b/g)']=np.array(lnbg)
        
        rgbc=[]
        for i in range(len(te)):
          combo=te[i,r_in]*te[i,g_in]*te[i,b_in]
          rgbc.append(combo)
        
        hutt_full['RGB_Combo']=0
        hutt_full['RGB_Combo']=np.array(rgbc)
        #load kmeans
        kmeans=joblib.load('kmeans.pkl')
        hutt_full.loc[:, 'point_cluster'] = kmeans.predict(hutt_full[[r,g,b]])
        hutt_full['point_cluster']=hutt_full['point_cluster'].astype('int64')
        mm=load_data('min_med_max.csv')
        
        max_indx=mm.groupby(['point_cluster']).mean()['RGB_Combo'][mm.groupby(['point_cluster']).mean()['RGB_Combo']==np.max(mm.groupby(['point_cluster']).mean()['RGB_Combo'])].index
        min_indx=mm.groupby(['point_cluster']).mean()['RGB_Combo'][mm.groupby(['point_cluster']).mean()['RGB_Combo']==np.min(mm.groupby(['point_cluster']).mean()['RGB_Combo'])].index
        indexs=list(mm.groupby(['point_cluster']).mean().index)
        indexs.remove(min_indx)
        indexs.remove(max_indx)
        med_indx=indexs[0]
        
        hutt_full['Point_cluster']=''
        hutt_full['Point_cluster']=np.where(hutt_full['point_cluster']==max_indx[0],'High',hutt_full['Point_cluster'])
        hutt_full['Point_cluster']=np.where(hutt_full['point_cluster']==med_indx,'Medium',hutt_full['Point_cluster'])
        hutt_full['Point_cluster']=np.where(hutt_full['point_cluster']==min_indx[0],'Low',hutt_full['Point_cluster'])
        hutt_full=hutt_full.reset_index().drop(['index'],axis=1)
        
        dn=load_data('groupby.csv')  
        #st.write(dn.head())
    
        gr=dn.columns[0]
        gg=dn.columns[1]
        gb=dn.columns[2]
        
        peak_r=dn.groupby(['Point_cluster']).max()[gr]
        peak_g=dn.groupby(['Point_cluster']).max()[gg]
        peak_b=dn.groupby(['Point_cluster']).max()[gb]
        peak_rgb=dn.groupby(['Point_cluster']).max()['RGB_Combo']
       # st.write(peak_r,peak_g,peak_b)
        
        hutt_full['Ratio_peak_R']=0.0
        hutt_full['Ratio_peak_G']=0.0
        hutt_full['Ratio_peak_B']=0.0
        hutt_full['Ratio_peak_RGBCombo']=0.0
        
        #low
        hutt_full['Ratio_peak_R']=np.where(hutt_full['Point_cluster']=='Low',hutt_full[r]/peak_r['Low'],hutt_full['Ratio_peak_R'])
        hutt_full['Ratio_peak_G']=np.where(hutt_full['Point_cluster']=='Low',hutt_full[g]/peak_g['Low'],hutt_full['Ratio_peak_G'])
        hutt_full['Ratio_peak_B']=np.where(hutt_full['Point_cluster']=='Low',hutt_full[b]/peak_b['Low'],hutt_full['Ratio_peak_B'])
        hutt_full['Ratio_peak_RGBCombo']=np.where(hutt_full['Point_cluster']=='Low',hutt_full['RGB_Combo']/peak_rgb['Low'],hutt_full['Ratio_peak_RGBCombo'])
        
        #medium
        hutt_full['Ratio_peak_R']=np.where(hutt_full['Point_cluster']=='Medium',hutt_full[r]/peak_r['Medium'],hutt_full['Ratio_peak_R'])
        hutt_full['Ratio_peak_G']=np.where(hutt_full['Point_cluster']=='Medium',hutt_full[g]/peak_g['Medium'],hutt_full['Ratio_peak_G'])
        hutt_full['Ratio_peak_B']=np.where(hutt_full['Point_cluster']=='Medium',hutt_full[b]/peak_b['Medium'],hutt_full['Ratio_peak_B'])
        hutt_full['Ratio_peak_RGBCombo']=np.where(hutt_full['Point_cluster']=='Medium',hutt_full['RGB_Combo']/peak_rgb['Medium'],hutt_full['Ratio_peak_RGBCombo'])
        
        #high
        hutt_full['Ratio_peak_R']=np.where(hutt_full['Point_cluster']=='High',hutt_full[r]/peak_r['High'],hutt_full['Ratio_peak_R'])
        hutt_full['Ratio_peak_G']=np.where(hutt_full['Point_cluster']=='High',hutt_full[g]/peak_g['High'],hutt_full['Ratio_peak_G'])
        hutt_full['Ratio_peak_B']=np.where(hutt_full['Point_cluster']=='High',hutt_full[b]/peak_b['High'],hutt_full['Ratio_peak_B'])
        hutt_full['Ratio_peak_RGBCombo']=np.where(hutt_full['Point_cluster']=='High',hutt_full['RGB_Combo']/peak_rgb['High'],hutt_full['Ratio_peak_RGBCombo'])
        
        hutt_full=pd.get_dummies(hutt_full)
        #st.write("Before",hutt_full.head())
        # to confirm if all 3 clusters are there..else add dummy column
        grs=[]
        clus_all=['Point_cluster_Low','Point_cluster_Medium','Point_cluster_High']
        fk=[]
        for c in clus_all:
            if(c not in hutt_full.columns):
                fk.append(c)
                
        for fkc in fk:
            hutt_full[fkc]=0
        # To maintain the order in low, med ,high....
        lowp=hutt_full['Point_cluster_Low'].values
        medp=hutt_full['Point_cluster_Medium'].values
        highp=hutt_full['Point_cluster_High'].values
        
        hutt_full.drop(clus_all,axis=1,inplace=True)
        hutt_full['Point_cluster_Low']=lowp
        hutt_full['Point_cluster_Medium']=medp
        hutt_full['Point_cluster_High']=highp
        #st.write("After",hutt_full.head())     
        tes=np.array(hutt_full)
        i=0
        for i in range(len(tes)):
          combo=0.3*tes[i,r_in]+ 0.59*tes[i,g_in]+ 0.11*tes[i,b_in]
          grs.append(combo)
        
        hutt_full['Greyscale']=np.array(grs)  
        hf=pd.DataFrame()
        hf=hutt_full.drop(['point_cluster','RGB_Combo'],axis=1).copy()
        #st.write(hf.head(),hf.shape)
        #start of next func
        pca=joblib.load('pca_full.pkl')
        pca_result1=pca.transform(hf)
       # st.write("did")
        sub1=pd.DataFrame()
        sub1['pca-one']=0
        sub1['pca-two']=0
        sub1['pca-three']=0
        
        sub1['pca-one'] = pca_result1[:,0]
        sub1['pca-two'] = pca_result1[:,1] 
        sub1['pca-three'] = pca_result1[:,2]
        svr_data1=sub1.copy()
        sc=joblib.load('sc_full.pkl')
        sv_data=sc.transform(svr_data1)
        predi=pd.DataFrame()
        predi[str(model_name)+'_Pred']=model.predict(sv_data)
        
        if(sel_tar=='none'):
            df_preds2=pd.DataFrame(predi.copy())
            merged=pd.concat([to_merge,df_preds2],axis=1)
            merged=merged.reset_index().drop(['index'],axis=1) 
            full_river=pd.concat([full_river,merged],axis=0)
            full_river=full_river.reset_index().drop(['index'],axis=1) 

        else:
            predi[sel_tar]=jun[sel_tar].copy()
            df_preds2=pd.DataFrame(predi.copy())
            merged=pd.concat([to_merge,df_preds2],axis=1)
            merged=merged.reset_index().drop(['index'],axis=1)
            full_river=pd.concat([full_river,merged],axis=0)
            full_river=full_river.reset_index().drop(['index'],axis=1) 
        
        del merged
        del df_poly1
        del hf
        del hutt_full
        del df_preds2
        del predi
        del grs
        import gc
        gc.collect()
       # st.write("Merged",merged.tail())
       # st.write(full_river.tail())
        
# till here
  #  full_river=full_river.reset_index().drop(['index'],axis=1)
    full_river=full_river.sort_values(by=pid)
    full_river=full_river.reset_index().drop(['index'],axis=1) 
    #st.write(full_river.shape)
    st.write("Sample of the output")
    st.write(full_river.head())
    if(to_som=='Yes'):
        # to remove spikes..end smoothing algo , arguments needed= radius, threshold, to_som
        import scipy.spatial as spatial # use cKDTree to find neigbouring points 
        latlon=np.array(full_river[[east,north]].copy())
        point_tree=spatial.cKDTree(latlon)
        fr=np.array(full_river[str(model_name)+'_Pred'].copy())
        #st.write("entered into smoother")
        #now for each point find all those points withing radius from this tree
        out_ind=[]
        for i in range(len(latlon)):
            ixs=point_tree.query_ball_point(latlon[i], radius)
            avg_all=np.mean(fr[ixs])
            if(abs(fr[i]-avg_all)>threshold):
                out_ind.append(i)
    
        st.write(" Total dropped noise data points ",len(out_ind))
        not_smooth=full_river.drop(out_ind,axis=0)
        del fr
        del latlon
        
    if(sav=='No'):
        if(to_som=='Yes'):
            not_smooth.to_csv('Predictions_lr_smoothed.csv',index=False)
        full_river.to_csv('Predictions_lr.csv',index=False)
        st.write("Prediction file downloaded!! Please check your local files location to access the 'Predictions_lr.csv'")
        del full_river
    if(sav=='Yes'):        
        model_loc=fn+'LR.pkl'
        joblib.dump(model,model_loc)
        file_loc=fn+'Predictions_lr.csv'
        full_river.to_csv(file_loc,index=False)
        if(to_som=='Yes'):
            n_fl=fn+'Predictions_lr_smoothed.csv'
            not_smooth.to_csv(n_fl,index=False)
        required_files(fn)
        st.write("Prediction file and model downloaded!! Please check your local files location  - in the given folder name to access the 'Predictions_lr.csv' and 'LR.pkl' for model reference")
        del full_river
 #       st.write("done 4")

def full_river_process_svr(new_df1,r,g,b,sub_x,model,model_name,sel_tar,sav,fn,sel_cols2,pid,east,north,to_som,radius,threshold):
    #st.write(hutt_full.head())
    full_river=pd.DataFrame()
    for jun in new_df1:
        to_merge=pd.DataFrame()
        merged=pd.DataFrame()
        st.write(" Current batch of shape ",jun.shape)
        jun=jun.reset_index().drop(['index'],axis=1) 
        hutt_full=pd.DataFrame()
        to_merge=pd.DataFrame(jun[[pid,east,north]])

        hutt_full=jun[sel_cols2].copy()
        lnbr=[]
        lnbg=[]
        #hut full has r,g,b,w only
        r_in=hutt_full.columns.get_loc(r)
        g_in=hutt_full.columns.get_loc(g)
        b_in=hutt_full.columns.get_loc(b)
        te=hutt_full.values
        for i in range(len(te)):
          val=np.log10(te[i,b_in]/te[i,r_in])
          lnbr.append(val)      
          val1=np.log10(te[i,b_in]/te[i,g_in])
          lnbg.append(val1)
              
        hutt_full['ln(b/r)']=np.array(lnbr)
        hutt_full['ln(b/g)']=np.array(lnbg)
        
        rgbc=[]
        for i in range(len(te)):
          combo=te[i,r_in]*te[i,g_in]*te[i,b_in]
          rgbc.append(combo)
        
        hutt_full['RGB_Combo']=0
        hutt_full['RGB_Combo']=np.array(rgbc)
        #load kmeans
        kmeans=joblib.load('kmeans.pkl')
        hutt_full.loc[:, 'point_cluster'] = kmeans.predict(hutt_full[[r,g,b]])
        hutt_full['point_cluster']=hutt_full['point_cluster'].astype('int64')
        mm=load_data('min_med_max.csv')
        
        max_indx=mm.groupby(['point_cluster']).mean()['RGB_Combo'][mm.groupby(['point_cluster']).mean()['RGB_Combo']==np.max(mm.groupby(['point_cluster']).mean()['RGB_Combo'])].index
        min_indx=mm.groupby(['point_cluster']).mean()['RGB_Combo'][mm.groupby(['point_cluster']).mean()['RGB_Combo']==np.min(mm.groupby(['point_cluster']).mean()['RGB_Combo'])].index
        indexs=list(mm.groupby(['point_cluster']).mean().index)
        indexs.remove(min_indx)
        indexs.remove(max_indx)
        med_indx=indexs[0]
        
        hutt_full['Point_cluster']=''
        hutt_full['Point_cluster']=np.where(hutt_full['point_cluster']==max_indx[0],'High',hutt_full['Point_cluster'])
        hutt_full['Point_cluster']=np.where(hutt_full['point_cluster']==med_indx,'Medium',hutt_full['Point_cluster'])
        hutt_full['Point_cluster']=np.where(hutt_full['point_cluster']==min_indx[0],'Low',hutt_full['Point_cluster'])
        hutt_full=hutt_full.reset_index().drop(['index'],axis=1)
        
        dn=load_data('groupby.csv')  
        #st.write(dn.head())
    
        gr=dn.columns[0]
        gg=dn.columns[1]
        gb=dn.columns[2]
        
        peak_r=dn.groupby(['Point_cluster']).max()[gr]
        peak_g=dn.groupby(['Point_cluster']).max()[gg]
        peak_b=dn.groupby(['Point_cluster']).max()[gb]
        peak_rgb=dn.groupby(['Point_cluster']).max()['RGB_Combo']
       # st.write(peak_r,peak_g,peak_b)
        
        hutt_full['Ratio_peak_R']=0.0
        hutt_full['Ratio_peak_G']=0.0
        hutt_full['Ratio_peak_B']=0.0
        hutt_full['Ratio_peak_RGBCombo']=0.0
        
        #low
        hutt_full['Ratio_peak_R']=np.where(hutt_full['Point_cluster']=='Low',hutt_full[r]/peak_r['Low'],hutt_full['Ratio_peak_R'])
        hutt_full['Ratio_peak_G']=np.where(hutt_full['Point_cluster']=='Low',hutt_full[g]/peak_g['Low'],hutt_full['Ratio_peak_G'])
        hutt_full['Ratio_peak_B']=np.where(hutt_full['Point_cluster']=='Low',hutt_full[b]/peak_b['Low'],hutt_full['Ratio_peak_B'])
        hutt_full['Ratio_peak_RGBCombo']=np.where(hutt_full['Point_cluster']=='Low',hutt_full['RGB_Combo']/peak_rgb['Low'],hutt_full['Ratio_peak_RGBCombo'])
        
        #medium
        hutt_full['Ratio_peak_R']=np.where(hutt_full['Point_cluster']=='Medium',hutt_full[r]/peak_r['Medium'],hutt_full['Ratio_peak_R'])
        hutt_full['Ratio_peak_G']=np.where(hutt_full['Point_cluster']=='Medium',hutt_full[g]/peak_g['Medium'],hutt_full['Ratio_peak_G'])
        hutt_full['Ratio_peak_B']=np.where(hutt_full['Point_cluster']=='Medium',hutt_full[b]/peak_b['Medium'],hutt_full['Ratio_peak_B'])
        hutt_full['Ratio_peak_RGBCombo']=np.where(hutt_full['Point_cluster']=='Medium',hutt_full['RGB_Combo']/peak_rgb['Medium'],hutt_full['Ratio_peak_RGBCombo'])
        
        #high
        hutt_full['Ratio_peak_R']=np.where(hutt_full['Point_cluster']=='High',hutt_full[r]/peak_r['High'],hutt_full['Ratio_peak_R'])
        hutt_full['Ratio_peak_G']=np.where(hutt_full['Point_cluster']=='High',hutt_full[g]/peak_g['High'],hutt_full['Ratio_peak_G'])
        hutt_full['Ratio_peak_B']=np.where(hutt_full['Point_cluster']=='High',hutt_full[b]/peak_b['High'],hutt_full['Ratio_peak_B'])
        hutt_full['Ratio_peak_RGBCombo']=np.where(hutt_full['Point_cluster']=='High',hutt_full['RGB_Combo']/peak_rgb['High'],hutt_full['Ratio_peak_RGBCombo'])
        
        hutt_full=pd.get_dummies(hutt_full)
        #st.write("Before",hutt_full.head())
        # to confirm if all 3 clusters are there..else add dummy column
        grs=[]
        clus_all=['Point_cluster_Low','Point_cluster_Medium','Point_cluster_High']
        fk=[]
        for c in clus_all:
            if(c not in hutt_full.columns):
                fk.append(c)
                
        for fkc in fk:
            hutt_full[fkc]=0
        # To maintain the order in low, med ,high....
        lowp=hutt_full['Point_cluster_Low'].values
        medp=hutt_full['Point_cluster_Medium'].values
        highp=hutt_full['Point_cluster_High'].values
        
        hutt_full.drop(clus_all,axis=1,inplace=True)
        hutt_full['Point_cluster_Low']=lowp
        hutt_full['Point_cluster_Medium']=medp
        hutt_full['Point_cluster_High']=highp
        #st.write("After",hutt_full.head())     
        tes=np.array(hutt_full)
        i=0
        for i in range(len(tes)):
          combo=0.3*tes[i,r_in]+ 0.59*tes[i,g_in]+ 0.11*tes[i,b_in]
          grs.append(combo)
        
        hutt_full['Greyscale']=np.array(grs)  
        hf=pd.DataFrame()
        hf=hutt_full.drop(['point_cluster','RGB_Combo'],axis=1).copy()
        #st.write(hf.head(),hf.shape)
        #start of next func
        pca=joblib.load('pca_full.pkl')
        pca_result1=pca.transform(hf)
       # st.write("did")
        sub1=pd.DataFrame()
        sub1['pca-one']=0
        sub1['pca-two']=0
        sub1['pca-three']=0
        
        sub1['pca-one'] = pca_result1[:,0]
        sub1['pca-two'] = pca_result1[:,1] 
        sub1['pca-three'] = pca_result1[:,2]
        svr_data1=sub1.copy()
        sc=joblib.load('sc_full.pkl')
        sv_data=sc.transform(svr_data1)
        predi=pd.DataFrame()
        predi[str(model_name)+'_Pred']=model.predict(sv_data)
        
        if(sel_tar=='none'):
            df_preds2=pd.DataFrame(predi.copy())
            merged=pd.concat([to_merge,df_preds2],axis=1)
            merged=merged.reset_index().drop(['index'],axis=1) 
            full_river=pd.concat([full_river,merged],axis=0)
            full_river=full_river.reset_index().drop(['index'],axis=1) 

        else:
            predi[sel_tar]=jun[sel_tar].copy()
            df_preds2=pd.DataFrame(predi.copy())
            merged=pd.concat([to_merge,df_preds2],axis=1)
            merged=merged.reset_index().drop(['index'],axis=1)
            full_river=pd.concat([full_river,merged],axis=0)
            full_river=full_river.reset_index().drop(['index'],axis=1) 
        
        del merged
        del svr_data1
        del sub1
        del hf
        del hutt_full
        del df_preds2
        del predi
        del grs
        import gc
        gc.collect()
       # st.write("Merged",merged.tail())
       # st.write(full_river.tail())
        
# till here
  #  full_river=full_river.reset_index().drop(['index'],axis=1)
    full_river=full_river.sort_values(by=pid)
    full_river=full_river.reset_index().drop(['index'],axis=1) 
    #st.write(full_river.shape)
    st.write("Sample of the output")
    st.write(full_river.head())
    if(to_som=='Yes'):
        # to remove spikes..end smoothing algo , arguments needed= radius, threshold, to_som
        import scipy.spatial as spatial # use cKDTree to find neigbouring points 
        latlon=np.array(full_river[[east,north]].copy())
        point_tree=spatial.cKDTree(latlon)
        fr=np.array(full_river[str(model_name)+'_Pred'].copy())
        #st.write("entered into smoother")
        #now for each point find all those points withing radius from this tree
        out_ind=[]
        for i in range(len(latlon)):
            ixs=point_tree.query_ball_point(latlon[i], radius)
            avg_all=np.mean(fr[ixs])
            if(abs(fr[i]-avg_all)>threshold):
                out_ind.append(i)
    
        st.write(" Total dropped noise data points ",len(out_ind))
        not_smooth=full_river.drop(out_ind,axis=0)
        del fr
        del latlon
        
    if(sav=='No'):     
        if(to_som=='Yes'):
            not_smooth.to_csv('Predictions_svr_smoothed.csv',index=False)
        full_river.to_csv('Predictions_svr.csv',index=False)
        st.write("Prediction file downloaded!! Please check your local files location to access the 'Predictions_svr.csv'")
        del full_river
    if(sav=='Yes'):        
        model_loc=fn+'SVR.pkl'
        joblib.dump(model,model_loc)
        file_loc=fn+'Predictions_svr.csv'
        full_river.to_csv(file_loc,index=False)
        if(to_som=='Yes'):
            n_fl=fn+'Predictions_svr_smoothed.csv'
            not_smooth.to_csv(n_fl,index=False)
        required_files(fn)
        st.write("Prediction file and model downloaded!! Please check your local files location  - in the given folder name to access the 'Predictions_svr.csv' and 'SVR.pkl' for model reference")
        del full_river
#               st.write("done 4")
    

def full_river_process_rf(new_df1,r,g,b,rf_x,model,model_name,sel_tar,sav,fn,sel_cols2,pid,east,north,to_som,radius,threshold):
    #st.write(hutt_full.head())
    full_river=pd.DataFrame()
    for jun in new_df1:
        to_merge=pd.DataFrame()
        merged=pd.DataFrame()
        st.write(" Current batch of shape ",jun.shape)
        jun=jun.reset_index().drop(['index'],axis=1) 
        hutt_full=pd.DataFrame()
        to_merge=pd.DataFrame(jun[[pid,east,north]])

        hutt_full=jun[sel_cols2].copy()
        lnbr=[]
        lnbg=[]
        #hut full has r,g,b,w only
        r_in=hutt_full.columns.get_loc(r)
        g_in=hutt_full.columns.get_loc(g)
        b_in=hutt_full.columns.get_loc(b)
        te=hutt_full.values
        for i in range(len(te)):
          val=np.log10(te[i,b_in]/te[i,r_in])
          lnbr.append(val)      
          val1=np.log10(te[i,b_in]/te[i,g_in])
          lnbg.append(val1)
              
        hutt_full['ln(b/r)']=np.array(lnbr)
        hutt_full['ln(b/g)']=np.array(lnbg)
        
        rgbc=[]
        for i in range(len(te)):
          combo=te[i,r_in]*te[i,g_in]*te[i,b_in]
          rgbc.append(combo)
        
        hutt_full['RGB_Combo']=0
        hutt_full['RGB_Combo']=np.array(rgbc)
        #load kmeans
        kmeans=joblib.load('kmeans.pkl')
        hutt_full.loc[:, 'point_cluster'] = kmeans.predict(hutt_full[[r,g,b]])
        hutt_full['point_cluster']=hutt_full['point_cluster'].astype('int64')
        mm=load_data('min_med_max.csv')
        
        max_indx=mm.groupby(['point_cluster']).mean()['RGB_Combo'][mm.groupby(['point_cluster']).mean()['RGB_Combo']==np.max(mm.groupby(['point_cluster']).mean()['RGB_Combo'])].index
        min_indx=mm.groupby(['point_cluster']).mean()['RGB_Combo'][mm.groupby(['point_cluster']).mean()['RGB_Combo']==np.min(mm.groupby(['point_cluster']).mean()['RGB_Combo'])].index
        indexs=list(mm.groupby(['point_cluster']).mean().index)
        indexs.remove(min_indx)
        indexs.remove(max_indx)
        med_indx=indexs[0]
        
        hutt_full['Point_cluster']=''
        hutt_full['Point_cluster']=np.where(hutt_full['point_cluster']==max_indx[0],'High',hutt_full['Point_cluster'])
        hutt_full['Point_cluster']=np.where(hutt_full['point_cluster']==med_indx,'Medium',hutt_full['Point_cluster'])
        hutt_full['Point_cluster']=np.where(hutt_full['point_cluster']==min_indx[0],'Low',hutt_full['Point_cluster'])
        hutt_full=hutt_full.reset_index().drop(['index'],axis=1)
        
        dn=load_data('groupby.csv')  
        #st.write(dn.head())
    
        gr=dn.columns[0]
        gg=dn.columns[1]
        gb=dn.columns[2]
        
        peak_r=dn.groupby(['Point_cluster']).max()[gr]
        peak_g=dn.groupby(['Point_cluster']).max()[gg]
        peak_b=dn.groupby(['Point_cluster']).max()[gb]
        peak_rgb=dn.groupby(['Point_cluster']).max()['RGB_Combo']
       # st.write(peak_r,peak_g,peak_b)
        
        hutt_full['Ratio_peak_R']=0.0
        hutt_full['Ratio_peak_G']=0.0
        hutt_full['Ratio_peak_B']=0.0
        hutt_full['Ratio_peak_RGBCombo']=0.0
        
        #low
        hutt_full['Ratio_peak_R']=np.where(hutt_full['Point_cluster']=='Low',hutt_full[r]/peak_r['Low'],hutt_full['Ratio_peak_R'])
        hutt_full['Ratio_peak_G']=np.where(hutt_full['Point_cluster']=='Low',hutt_full[g]/peak_g['Low'],hutt_full['Ratio_peak_G'])
        hutt_full['Ratio_peak_B']=np.where(hutt_full['Point_cluster']=='Low',hutt_full[b]/peak_b['Low'],hutt_full['Ratio_peak_B'])
        hutt_full['Ratio_peak_RGBCombo']=np.where(hutt_full['Point_cluster']=='Low',hutt_full['RGB_Combo']/peak_rgb['Low'],hutt_full['Ratio_peak_RGBCombo'])
        
        #medium
        hutt_full['Ratio_peak_R']=np.where(hutt_full['Point_cluster']=='Medium',hutt_full[r]/peak_r['Medium'],hutt_full['Ratio_peak_R'])
        hutt_full['Ratio_peak_G']=np.where(hutt_full['Point_cluster']=='Medium',hutt_full[g]/peak_g['Medium'],hutt_full['Ratio_peak_G'])
        hutt_full['Ratio_peak_B']=np.where(hutt_full['Point_cluster']=='Medium',hutt_full[b]/peak_b['Medium'],hutt_full['Ratio_peak_B'])
        hutt_full['Ratio_peak_RGBCombo']=np.where(hutt_full['Point_cluster']=='Medium',hutt_full['RGB_Combo']/peak_rgb['Medium'],hutt_full['Ratio_peak_RGBCombo'])
        
        #high
        hutt_full['Ratio_peak_R']=np.where(hutt_full['Point_cluster']=='High',hutt_full[r]/peak_r['High'],hutt_full['Ratio_peak_R'])
        hutt_full['Ratio_peak_G']=np.where(hutt_full['Point_cluster']=='High',hutt_full[g]/peak_g['High'],hutt_full['Ratio_peak_G'])
        hutt_full['Ratio_peak_B']=np.where(hutt_full['Point_cluster']=='High',hutt_full[b]/peak_b['High'],hutt_full['Ratio_peak_B'])
        hutt_full['Ratio_peak_RGBCombo']=np.where(hutt_full['Point_cluster']=='High',hutt_full['RGB_Combo']/peak_rgb['High'],hutt_full['Ratio_peak_RGBCombo'])
        
        hutt_full=pd.get_dummies(hutt_full)
        #st.write("Before",hutt_full.head())
        # to confirm if all 3 clusters are there..else add dummy column
        grs=[]
        clus_all=['Point_cluster_Low','Point_cluster_Medium','Point_cluster_High']
        fk=[]
        for c in clus_all:
            if(c not in hutt_full.columns):
                fk.append(c)
                
        for fkc in fk:
            hutt_full[fkc]=0
        # To maintain the order in low, med ,high....
        lowp=hutt_full['Point_cluster_Low'].values
        medp=hutt_full['Point_cluster_Medium'].values
        highp=hutt_full['Point_cluster_High'].values
        
        hutt_full.drop(clus_all,axis=1,inplace=True)
        hutt_full['Point_cluster_Low']=lowp
        hutt_full['Point_cluster_Medium']=medp
        hutt_full['Point_cluster_High']=highp
        #st.write("After",hutt_full.head())     
        tes=np.array(hutt_full)
        i=0
        for i in range(len(tes)):
          combo=0.3*tes[i,r_in]+ 0.59*tes[i,g_in]+ 0.11*tes[i,b_in]
          grs.append(combo)
        
        hutt_full['Greyscale']=np.array(grs)  
        hf=pd.DataFrame()
        hf=hutt_full.drop(['point_cluster','RGB_Combo'],axis=1).copy()
        #st.write(hf.head(),hf.shape)
        #start of next func
        df_poly1=get_poly(hf)
        #st.write("done1")
        spe_cols=list(rf_x.columns)
        rf_data=df_poly1[spe_cols].copy()
        #st.write("done")
        
        predi=pd.DataFrame()
        predi[str(model_name)+'_Pred']=model.predict(rf_data)
        
        if(sel_tar=='none'):
            df_preds2=pd.DataFrame(predi.copy())
            merged=pd.concat([to_merge,df_preds2],axis=1)
            merged=merged.reset_index().drop(['index'],axis=1) 
            full_river=pd.concat([full_river,merged],axis=0)
            full_river=full_river.reset_index().drop(['index'],axis=1) 

        else:
            predi[sel_tar]=jun[sel_tar].copy()
            df_preds2=pd.DataFrame(predi.copy())
            merged=pd.concat([to_merge,df_preds2],axis=1)
            merged=merged.reset_index().drop(['index'],axis=1)
            full_river=pd.concat([full_river,merged],axis=0)
            full_river=full_river.reset_index().drop(['index'],axis=1) 
       
        del merged
        del df_poly1
        del hf
        del hutt_full
        del df_preds2
        del predi
        del grs
        import gc
        gc.collect()
       # st.write("Merged",merged.tail())
       # st.write(full_river.tail())
        
# till here
  #  full_river=full_river.reset_index().drop(['index'],axis=1)
    full_river=full_river.sort_values(by=pid)
    full_river=full_river.reset_index().drop(['index'],axis=1) 
    #st.write(full_river.shape)
    st.write("Sample of the output")
    st.write(full_river.head())
    if(to_som=='Yes'):
        # to remove spikes..end smoothing algo , arguments needed= radius, threshold, to_som
        import scipy.spatial as spatial # use cKDTree to find neigbouring points 
        latlon=np.array(full_river[[east,north]].copy())
        point_tree=spatial.cKDTree(latlon)
        fr=np.array(full_river[str(model_name)+'_Pred'].copy())
        #st.write("entered into smoother")
        #now for each point find all those points withing radius from this tree
        out_ind=[]
        for i in range(len(latlon)):
            ixs=point_tree.query_ball_point(latlon[i], radius)
            avg_all=np.mean(fr[ixs])
            if(abs(fr[i]-avg_all)>threshold):
                out_ind.append(i)
    
        st.write(" Total dropped noise data points ",len(out_ind))
        not_smooth=full_river.drop(out_ind,axis=0)
        del fr
        del latlon
        
    if(sav=='No'):     
        full_river.to_csv('Predictions_rf.csv',index=False)
        if(to_som=='Yes'):
            not_smooth.to_csv('Predictions_rf_smoothed.csv',index=False)
        st.write("Prediction file downloaded!! Please check your local files location to access the 'Predictions_rf.csv'")
        del full_river
    if(sav=='Yes'):        
        model_loc=fn+'RF.pkl'
        joblib.dump(model,model_loc)
        file_loc=fn+'Predictions_rf.csv'
        full_river.to_csv(file_loc,index=False)
        if(to_som=='Yes'):
            n_fl=fn+'Predictions_rf_smoothed.csv'
            not_smooth.to_csv(n_fl,index=False)
        required_files(fn)
        st.write("Prediction file and model downloaded!! Please check your local files location  - in the given folder name to access the 'Predictions_rf.csv' and 'RF.pkl' for model reference")
        del full_river
#               st.write("done 4")
    
 
def full_river_process_xgb(new_df1,r,g,b,rf_x,model,model_name,sel_tar,sav,fn,sel_cols2,pid,east,north,to_som,radius,threshold):
    #st.write(hutt_full.head())
    full_river=pd.DataFrame()
    for jun in new_df1:
        to_merge=pd.DataFrame()
        merged=pd.DataFrame()
        st.write(" Current batch of shape ",jun.shape)
        jun=jun.reset_index().drop(['index'],axis=1) 
        hutt_full=pd.DataFrame()
        to_merge=pd.DataFrame(jun[[pid,east,north]])

        hutt_full=jun[sel_cols2].copy()
        lnbr=[]
        lnbg=[]
        #hut full has r,g,b,w only
        r_in=hutt_full.columns.get_loc(r)
        g_in=hutt_full.columns.get_loc(g)
        b_in=hutt_full.columns.get_loc(b)
        te=hutt_full.values
        for i in range(len(te)):
          val=np.log10(te[i,b_in]/te[i,r_in])
          lnbr.append(val)      
          val1=np.log10(te[i,b_in]/te[i,g_in])
          lnbg.append(val1)
              
        hutt_full['ln(b/r)']=np.array(lnbr)
        hutt_full['ln(b/g)']=np.array(lnbg)
        
        rgbc=[]
        for i in range(len(te)):
          combo=te[i,r_in]*te[i,g_in]*te[i,b_in]
          rgbc.append(combo)
        
        hutt_full['RGB_Combo']=0
        hutt_full['RGB_Combo']=np.array(rgbc)
        #load kmeans
        kmeans=joblib.load('kmeans.pkl')
        hutt_full.loc[:, 'point_cluster'] = kmeans.predict(hutt_full[[r,g,b]])
        hutt_full['point_cluster']=hutt_full['point_cluster'].astype('int64')
        mm=load_data('min_med_max.csv')
        
        max_indx=mm.groupby(['point_cluster']).mean()['RGB_Combo'][mm.groupby(['point_cluster']).mean()['RGB_Combo']==np.max(mm.groupby(['point_cluster']).mean()['RGB_Combo'])].index
        min_indx=mm.groupby(['point_cluster']).mean()['RGB_Combo'][mm.groupby(['point_cluster']).mean()['RGB_Combo']==np.min(mm.groupby(['point_cluster']).mean()['RGB_Combo'])].index
        indexs=list(mm.groupby(['point_cluster']).mean().index)
        indexs.remove(min_indx)
        indexs.remove(max_indx)
        med_indx=indexs[0]
        
        hutt_full['Point_cluster']=''
        hutt_full['Point_cluster']=np.where(hutt_full['point_cluster']==max_indx[0],'High',hutt_full['Point_cluster'])
        hutt_full['Point_cluster']=np.where(hutt_full['point_cluster']==med_indx,'Medium',hutt_full['Point_cluster'])
        hutt_full['Point_cluster']=np.where(hutt_full['point_cluster']==min_indx[0],'Low',hutt_full['Point_cluster'])
        hutt_full=hutt_full.reset_index().drop(['index'],axis=1)
        
        dn=load_data('groupby.csv')  
        #st.write(dn.head())
    
        gr=dn.columns[0]
        gg=dn.columns[1]
        gb=dn.columns[2]
        
        peak_r=dn.groupby(['Point_cluster']).max()[gr]
        peak_g=dn.groupby(['Point_cluster']).max()[gg]
        peak_b=dn.groupby(['Point_cluster']).max()[gb]
        peak_rgb=dn.groupby(['Point_cluster']).max()['RGB_Combo']
       # st.write(peak_r,peak_g,peak_b)
        
        hutt_full['Ratio_peak_R']=0.0
        hutt_full['Ratio_peak_G']=0.0
        hutt_full['Ratio_peak_B']=0.0
        hutt_full['Ratio_peak_RGBCombo']=0.0
        
        #low
        hutt_full['Ratio_peak_R']=np.where(hutt_full['Point_cluster']=='Low',hutt_full[r]/peak_r['Low'],hutt_full['Ratio_peak_R'])
        hutt_full['Ratio_peak_G']=np.where(hutt_full['Point_cluster']=='Low',hutt_full[g]/peak_g['Low'],hutt_full['Ratio_peak_G'])
        hutt_full['Ratio_peak_B']=np.where(hutt_full['Point_cluster']=='Low',hutt_full[b]/peak_b['Low'],hutt_full['Ratio_peak_B'])
        hutt_full['Ratio_peak_RGBCombo']=np.where(hutt_full['Point_cluster']=='Low',hutt_full['RGB_Combo']/peak_rgb['Low'],hutt_full['Ratio_peak_RGBCombo'])
        
        #medium
        hutt_full['Ratio_peak_R']=np.where(hutt_full['Point_cluster']=='Medium',hutt_full[r]/peak_r['Medium'],hutt_full['Ratio_peak_R'])
        hutt_full['Ratio_peak_G']=np.where(hutt_full['Point_cluster']=='Medium',hutt_full[g]/peak_g['Medium'],hutt_full['Ratio_peak_G'])
        hutt_full['Ratio_peak_B']=np.where(hutt_full['Point_cluster']=='Medium',hutt_full[b]/peak_b['Medium'],hutt_full['Ratio_peak_B'])
        hutt_full['Ratio_peak_RGBCombo']=np.where(hutt_full['Point_cluster']=='Medium',hutt_full['RGB_Combo']/peak_rgb['Medium'],hutt_full['Ratio_peak_RGBCombo'])
        
        #high
        hutt_full['Ratio_peak_R']=np.where(hutt_full['Point_cluster']=='High',hutt_full[r]/peak_r['High'],hutt_full['Ratio_peak_R'])
        hutt_full['Ratio_peak_G']=np.where(hutt_full['Point_cluster']=='High',hutt_full[g]/peak_g['High'],hutt_full['Ratio_peak_G'])
        hutt_full['Ratio_peak_B']=np.where(hutt_full['Point_cluster']=='High',hutt_full[b]/peak_b['High'],hutt_full['Ratio_peak_B'])
        hutt_full['Ratio_peak_RGBCombo']=np.where(hutt_full['Point_cluster']=='High',hutt_full['RGB_Combo']/peak_rgb['High'],hutt_full['Ratio_peak_RGBCombo'])
        
        hutt_full=pd.get_dummies(hutt_full)
        #st.write("Before",hutt_full.head())
        # to confirm if all 3 clusters are there..else add dummy column
        grs=[]
        clus_all=['Point_cluster_Low','Point_cluster_Medium','Point_cluster_High']
        fk=[]
        for c in clus_all:
            if(c not in hutt_full.columns):
                fk.append(c)
                
        for fkc in fk:
            hutt_full[fkc]=0
        # To maintain the order in low, med ,high....
        lowp=hutt_full['Point_cluster_Low'].values
        medp=hutt_full['Point_cluster_Medium'].values
        highp=hutt_full['Point_cluster_High'].values
        
        hutt_full.drop(clus_all,axis=1,inplace=True)
        hutt_full['Point_cluster_Low']=lowp
        hutt_full['Point_cluster_Medium']=medp
        hutt_full['Point_cluster_High']=highp
        #st.write("After",hutt_full.head())     
        tes=np.array(hutt_full)
        i=0
        for i in range(len(tes)):
          combo=0.3*tes[i,r_in]+ 0.59*tes[i,g_in]+ 0.11*tes[i,b_in]
          grs.append(combo)
        
        hutt_full['Greyscale']=np.array(grs)  
        hf=pd.DataFrame()
        hf=hutt_full.drop(['point_cluster','RGB_Combo'],axis=1).copy()
        #st.write(hf.head(),hf.shape)
        #start of next func
        df_poly1=get_poly(hf)
        #st.write("done1")
        spe_cols=list(rf_x.columns)
        rf_data=df_poly1[spe_cols].copy()
        st.write("done")
        
        predi=pd.DataFrame()
        predi[str(model_name)+'_Pred']=model.predict(rf_data)
        
        if(sel_tar=='none'):
            df_preds2=pd.DataFrame(predi.copy())
            merged=pd.concat([to_merge,df_preds2],axis=1)
            merged=merged.reset_index().drop(['index'],axis=1) 
            full_river=pd.concat([full_river,merged],axis=0)
            full_river=full_river.reset_index().drop(['index'],axis=1) 

        else:
            predi[sel_tar]=jun[sel_tar].copy()
            df_preds2=pd.DataFrame(predi.copy())
            merged=pd.concat([to_merge,df_preds2],axis=1)
            merged=merged.reset_index().drop(['index'],axis=1)
            full_river=pd.concat([full_river,merged],axis=0)
            full_river=full_river.reset_index().drop(['index'],axis=1) 

        del merged
        del df_poly1
        del hf
        del hutt_full
        del df_preds2
        del predi
        del grs
        import gc
        gc.collect()
        #if(len(full_river)>2000000):
        #   break
       # st.write("Merged",merged.tail())
       # st.write(full_river.tail())
        
# till here
  #  full_river=full_river.reset_index().drop(['index'],axis=1)
    full_river=full_river.sort_values(by=pid)
    full_river=full_river.reset_index().drop(['index'],axis=1) 
    #st.write(full_river.shape)
    st.write("Sample of the output")
    st.write(full_river.head())
    #radius=2
    #threshold=0.2
    if(to_som=='Yes'):
        # to remove spikes..end smoothing algo , arguments needed= radius, threshold, to_som
        import scipy.spatial as spatial # use cKDTree to find neigbouring points 
        latlon=np.array(full_river[[east,north]].copy())
        point_tree=spatial.cKDTree(latlon)
        fr=np.array(full_river[str(model_name)+'_Pred'].copy())
        #st.write("entered into smoother")
        #now for each point find all those points withing radius from this tree
        out_ind=[]
        for i in range(len(latlon)):
            ixs=point_tree.query_ball_point(latlon[i], radius)
            avg_all=np.mean(fr[ixs])
            if(abs(fr[i]-avg_all)>threshold):
                out_ind.append(i)
    
        st.write(" Total dropped noise data points ",len(out_ind))
        not_smooth=full_river.drop(out_ind,axis=0)
        del fr
        del latlon
    
    if(sav=='No'):     
        full_river.to_csv('Predictions_xgb.csv',index=False)
        if(to_som=='Yes'):
            not_smooth.to_csv('Predictions_xgb_smoothed.csv',index=False)
        st.write("Prediction file downloaded!! Please check your local files location to access the 'Predictions_xgb.csv'")
        del full_river
    if(sav=='Yes'):        
        model_loc=fn+'XGB.pkl'
        joblib.dump(model,model_loc)
        file_loc=fn+'Predictions_xgb.csv'
        full_river.to_csv(file_loc,index=False)
        if(to_som=='Yes'):
            n_fl=fn+'Predictions_xgb_smoothed.csv'
            not_smooth.to_csv(n_fl,index=False)
        required_files(fn)
        st.write("Prediction file and model downloaded!! Please check your local files location  - in the given folder name to access the 'Predictions_xgb.csv' and 'XGB.pkl' for model reference")
#               st.write("done 4")
        del full_river

def full_river_process_nn(new_df1,r,g,b,nn_x,model,model_name,sel_tar,sav,fn,sel_cols2,pid,east,north,to_som,radius,threshold):
    full_river=pd.DataFrame()
    for jun in new_df1:
        to_merge=pd.DataFrame()
        merged=pd.DataFrame()
        st.write(" Current batch of shape ",jun.shape)
        jun=jun.reset_index().drop(['index'],axis=1) 
        hutt_full=pd.DataFrame()
        to_merge=pd.DataFrame(jun[[pid,east,north]])

        hutt_full=jun[sel_cols2].copy()
        lnbr=[]
        lnbg=[]
        #hut full has r,g,b,w only
        r_in=hutt_full.columns.get_loc(r)
        g_in=hutt_full.columns.get_loc(g)
        b_in=hutt_full.columns.get_loc(b)
        te=hutt_full.values
        for i in range(len(te)):
          val=np.log10(te[i,b_in]/te[i,r_in])
          lnbr.append(val)      
          val1=np.log10(te[i,b_in]/te[i,g_in])
          lnbg.append(val1)
              
        hutt_full['ln(b/r)']=np.array(lnbr)
        hutt_full['ln(b/g)']=np.array(lnbg)
        
        rgbc=[]
        for i in range(len(te)):
          combo=te[i,r_in]*te[i,g_in]*te[i,b_in]
          rgbc.append(combo)
        
        hutt_full['RGB_Combo']=0
        hutt_full['RGB_Combo']=np.array(rgbc)
        #load kmeans
        kmeans=joblib.load('kmeans.pkl')
        hutt_full.loc[:, 'point_cluster'] = kmeans.predict(hutt_full[[r,g,b]])
        hutt_full['point_cluster']=hutt_full['point_cluster'].astype('int64')
        mm=load_data('min_med_max.csv')
        
        max_indx=mm.groupby(['point_cluster']).mean()['RGB_Combo'][mm.groupby(['point_cluster']).mean()['RGB_Combo']==np.max(mm.groupby(['point_cluster']).mean()['RGB_Combo'])].index
        min_indx=mm.groupby(['point_cluster']).mean()['RGB_Combo'][mm.groupby(['point_cluster']).mean()['RGB_Combo']==np.min(mm.groupby(['point_cluster']).mean()['RGB_Combo'])].index
        indexs=list(mm.groupby(['point_cluster']).mean().index)
        indexs.remove(min_indx)
        indexs.remove(max_indx)
        med_indx=indexs[0]
        
        hutt_full['Point_cluster']=''
        hutt_full['Point_cluster']=np.where(hutt_full['point_cluster']==max_indx[0],'High',hutt_full['Point_cluster'])
        hutt_full['Point_cluster']=np.where(hutt_full['point_cluster']==med_indx,'Medium',hutt_full['Point_cluster'])
        hutt_full['Point_cluster']=np.where(hutt_full['point_cluster']==min_indx[0],'Low',hutt_full['Point_cluster'])
        hutt_full=hutt_full.reset_index().drop(['index'],axis=1)
        
        dn=load_data('groupby.csv')  
        #st.write(dn.head())
    
        gr=dn.columns[0]
        gg=dn.columns[1]
        gb=dn.columns[2]
        
        peak_r=dn.groupby(['Point_cluster']).max()[gr]
        peak_g=dn.groupby(['Point_cluster']).max()[gg]
        peak_b=dn.groupby(['Point_cluster']).max()[gb]
        peak_rgb=dn.groupby(['Point_cluster']).max()['RGB_Combo']
       # st.write(peak_r,peak_g,peak_b)
        
        hutt_full['Ratio_peak_R']=0.0
        hutt_full['Ratio_peak_G']=0.0
        hutt_full['Ratio_peak_B']=0.0
        hutt_full['Ratio_peak_RGBCombo']=0.0
        
        #low
        hutt_full['Ratio_peak_R']=np.where(hutt_full['Point_cluster']=='Low',hutt_full[r]/peak_r['Low'],hutt_full['Ratio_peak_R'])
        hutt_full['Ratio_peak_G']=np.where(hutt_full['Point_cluster']=='Low',hutt_full[g]/peak_g['Low'],hutt_full['Ratio_peak_G'])
        hutt_full['Ratio_peak_B']=np.where(hutt_full['Point_cluster']=='Low',hutt_full[b]/peak_b['Low'],hutt_full['Ratio_peak_B'])
        hutt_full['Ratio_peak_RGBCombo']=np.where(hutt_full['Point_cluster']=='Low',hutt_full['RGB_Combo']/peak_rgb['Low'],hutt_full['Ratio_peak_RGBCombo'])
        
        #medium
        hutt_full['Ratio_peak_R']=np.where(hutt_full['Point_cluster']=='Medium',hutt_full[r]/peak_r['Medium'],hutt_full['Ratio_peak_R'])
        hutt_full['Ratio_peak_G']=np.where(hutt_full['Point_cluster']=='Medium',hutt_full[g]/peak_g['Medium'],hutt_full['Ratio_peak_G'])
        hutt_full['Ratio_peak_B']=np.where(hutt_full['Point_cluster']=='Medium',hutt_full[b]/peak_b['Medium'],hutt_full['Ratio_peak_B'])
        hutt_full['Ratio_peak_RGBCombo']=np.where(hutt_full['Point_cluster']=='Medium',hutt_full['RGB_Combo']/peak_rgb['Medium'],hutt_full['Ratio_peak_RGBCombo'])
        
        #high
        hutt_full['Ratio_peak_R']=np.where(hutt_full['Point_cluster']=='High',hutt_full[r]/peak_r['High'],hutt_full['Ratio_peak_R'])
        hutt_full['Ratio_peak_G']=np.where(hutt_full['Point_cluster']=='High',hutt_full[g]/peak_g['High'],hutt_full['Ratio_peak_G'])
        hutt_full['Ratio_peak_B']=np.where(hutt_full['Point_cluster']=='High',hutt_full[b]/peak_b['High'],hutt_full['Ratio_peak_B'])
        hutt_full['Ratio_peak_RGBCombo']=np.where(hutt_full['Point_cluster']=='High',hutt_full['RGB_Combo']/peak_rgb['High'],hutt_full['Ratio_peak_RGBCombo'])
        
        hutt_full=pd.get_dummies(hutt_full)
        #st.write("Before",hutt_full.head())
        # to confirm if all 3 clusters are there..else add dummy column
        grs=[]
        clus_all=['Point_cluster_Low','Point_cluster_Medium','Point_cluster_High']
        fk=[]
        for c in clus_all:
            if(c not in hutt_full.columns):
                fk.append(c)
                
        for fkc in fk:
            hutt_full[fkc]=0
        # To maintain the order in low, med ,high....
        lowp=hutt_full['Point_cluster_Low'].values
        medp=hutt_full['Point_cluster_Medium'].values
        highp=hutt_full['Point_cluster_High'].values
        
        hutt_full.drop(clus_all,axis=1,inplace=True)
        hutt_full['Point_cluster_Low']=lowp
        hutt_full['Point_cluster_Medium']=medp
        hutt_full['Point_cluster_High']=highp
        #st.write("After",hutt_full.head())     
        tes=np.array(hutt_full)
        i=0
        for i in range(len(tes)):
          combo=0.3*tes[i,r_in]+ 0.59*tes[i,g_in]+ 0.11*tes[i,b_in]
          grs.append(combo)
        
        hutt_full['Greyscale']=np.array(grs)  
        hf=pd.DataFrame()
        hf=hutt_full.drop(['point_cluster','RGB_Combo'],axis=1).copy()
        #st.write(hf.head(),hf.shape)
        
        #start of next func
        df_poly1=get_poly(hf)
        
        # give rf_x coz, sc array wont have feature names
        spe_cols=list(nn_x.columns)
        #st.write(spe_cols)
        #st.write(df_poly1.head())
        
        pred_data=df_poly1[spe_cols].copy()
        #st.write("one")
        sc=joblib.load('sc_nn_full.pkl')
        
        nn_data=sc.transform(pred_data)
        predi=pd.DataFrame()
        predi[str(model_name)+'_Pred']=model.predict(nn_data)
        
        if(sel_tar=='none'):
            df_preds2=pd.DataFrame(predi.copy())
            merged=pd.concat([to_merge,df_preds2],axis=1)
            merged=merged.reset_index().drop(['index'],axis=1) 
            full_river=pd.concat([full_river,merged],axis=0)
            full_river=full_river.reset_index().drop(['index'],axis=1) 

        else:
            predi[sel_tar]=jun[sel_tar].copy()
            df_preds2=pd.DataFrame(predi.copy())
            merged=pd.concat([to_merge,df_preds2],axis=1)
            merged=merged.reset_index().drop(['index'],axis=1)
            full_river=pd.concat([full_river,merged],axis=0)
            full_river=full_river.reset_index().drop(['index'],axis=1) 
        
        del merged
        del df_poly1
        del nn_data
        del pred_data
        del hf
        del hutt_full
        del df_preds2
        del predi
        del grs
        import gc
        gc.collect()
       # st.write("Merged",merged.tail())
       # st.write(full_river.tail())
        
# till here
  #  full_river=full_river.reset_index().drop(['index'],axis=1)
    full_river=full_river.sort_values(by=pid)
    full_river=full_river.reset_index().drop(['index'],axis=1) 
    #st.write(full_river.shape)
    st.write("Sample of the output")
    st.write(full_river.head())
    if(to_som=='Yes'):
        # to remove spikes..end smoothing algo , arguments needed= radius, threshold, to_som
        import scipy.spatial as spatial # use cKDTree to find neigbouring points 
        latlon=np.array(full_river[[east,north]].copy())
        point_tree=spatial.cKDTree(latlon)
        fr=np.array(full_river[str(model_name)+'_Pred'].copy())
        #st.write("entered into smoother")
        #now for each point find all those points withing radius from this tree
        out_ind=[]
        for i in range(len(latlon)):
            ixs=point_tree.query_ball_point(latlon[i], radius)
            avg_all=np.mean(fr[ixs])
            if(abs(fr[i]-avg_all)>threshold):
                out_ind.append(i)
    
        st.write(" Total dropped noise data points ",len(out_ind))
        not_smooth=full_river.drop(out_ind,axis=0)
        del fr
        del latlon
        
    if(sav=='No'):     
        full_river.to_csv('Predictions_nn.csv',index=False)
        if(to_som=='Yes'):
            not_smooth.to_csv('Predictions_nn_smoothed.csv',index=False)
        st.write("Prediction file downloaded!! Please check your local files location to access the 'Predictions_nn.csv'")
        del full_river
    if(sav=='Yes'):        
        model_loc=fn+'NN.pkl'
        joblib.dump(model,model_loc)
        file_loc=fn+'Predictions_nn.csv'
        full_river.to_csv(file_loc,index=False)
        if(to_som=='Yes'):
            n_fl=fn+'Predictions_nn_smoothed.csv'
            not_smooth.to_csv(n_fl,index=False)
        required_files(fn)
        st.write("Prediction file and model downloaded!! Please check your local files location  - in the given folder name to access the 'Predictions_nn.csv' and 'NN.pkl' for model reference")
#               st.write("done 4")
        del full_river
    
        
#@st.cache(persist=True, suppress_st_warning=True,allow_output_mutation=True)
def get_poly(new_data):
    #st.write('done')
    pcl=new_data[['Point_cluster_Low','Point_cluster_Medium','Point_cluster_High']].copy()
    new_data.drop(['Point_cluster_Low','Point_cluster_Medium','Point_cluster_High'],axis=1,inplace=True)
    poly = PolynomialFeatures(2)
    x_poly=poly.fit_transform(new_data)
    poly_names=poly.get_feature_names(['Feature'+str(l) for l in range(1,len(np.array(poly.get_feature_names())))])
    df_poly1=pd.DataFrame(x_poly,columns=poly_names)
    to_send=df_poly1.drop(['1'],axis=1)
    #append the clusters
    to_send=pd.concat([to_send,pcl],axis=1)
    
    return to_send    
                                         
############################ ML ALGO DEFS####################################################

#@st.cache(persist=True, suppress_st_warning=True)
def model_pred(model,totrain,predictions_all):
    y_pred_lr_all=model.predict(totrain)
    col1=predictions_all.columns.tolist()[0]
  #  predictions_test['LR_test_Pred']=y_pred_lr
    predictions_all['MODEL_full_Pred']=y_pred_lr_all
  #  st.write("RMSE of linear regression on TEST dataset : ",np.sqrt(mse(predictions_test[col1],predictions_test['LR_test_Pred'])))
   # st.write("R2 Score of linear regression on TEST dataset :",r2(predictions_test[col1],predictions_test['LR_test_Pred']))    
    r2s=r2_score(predictions_all[col1],predictions_all['MODEL_full_Pred'])
    rmses=np.sqrt(mse(predictions_all[col1],predictions_all['MODEL_full_Pred']))
    maes=mean_absolute_error(predictions_all[col1],predictions_all['MODEL_full_Pred'])
    evars=explained_variance_score(predictions_all[col1],predictions_all['MODEL_full_Pred'])
    maxs=max_error(predictions_all[col1],predictions_all['MODEL_full_Pred'])
   # psds=mean_poisson_deviance(predictions_all[col1],predictions_all['LR_full_Pred'])

    tmp=[]
    tmp.append([rmses,maes,r2s,maxs,evars])
    final_scores_test=pd.DataFrame(tmp,columns=['Root Mean Squared Error(RMSE)','Mean Absolute Error(MAE)','R2 Spearman Coefficient of Determination','Maximum Residual Error','Explained variance Score'],index=['MODEL'])    

    return predictions_all,final_scores_test

@st.cache(persist=True, suppress_st_warning=True,allow_output_mutation=True)
def test_result(model,x_test,y_test,model_name):   
        #full dataset results
    predictions_all=pd.DataFrame(y_test)     
    y_pred_model=model.predict(x_test)
    col1=predictions_all.columns.tolist()[0]
  #  predictions_test['LR_test_Pred']=y_pred_lr
    predictions_all['Model_full_Pred']=y_pred_model
  #  st.write("RMSE of linear regression on TEST dataset : ",np.sqrt(mse(predictions_test[col1],predictions_test['LR_test_Pred'])))
   # st.write("R2 Score of linear regression on TEST dataset :",r2(predictions_test[col1],predictions_test['LR_test_Pred']))    
    r2s=r2_score(predictions_all[col1],predictions_all['Model_full_Pred'])
    rmses=np.sqrt(mse(predictions_all[col1],predictions_all['Model_full_Pred']))
    maes=mean_absolute_error(predictions_all[col1],predictions_all['Model_full_Pred'])
    evars=explained_variance_score(predictions_all[col1],predictions_all['Model_full_Pred'])
    maxs=max_error(predictions_all[col1],predictions_all['Model_full_Pred'])
   # psds=mean_poisson_deviance(predictions_all[col1],predictions_all['LR_full_Pred'])

    tmp=[]
    tmp.append([rmses,maes,r2s,maxs,evars])
    final_scores_test=pd.DataFrame(tmp,columns=['Validation_Root Mean Squared Error(RMSE)','Validation_Mean Absolute Error(MAE)','Validation_R2 Spearman Coefficient of Determination','Validation_Maximum Residual Error','Validation_Explained variance Score'],index=[model_name])    

    return final_scores_test

@st.cache(persist=True, suppress_st_warning=True,allow_output_mutation=True)
def LR_reg(totrain,x_train,x_test,y_train,y_test,predictions_all):
    lr=LinearRegression()
    lr.fit(x_train,y_train)    
    #y_pred_lr=lr.predict(x_test)    
    #full dataset results
    y_pred_lr_all=lr.predict(totrain)
    col1=predictions_all.columns.tolist()[0]
  #  predictions_test['LR_test_Pred']=y_pred_lr
    predictions_all['LR_full_Pred']=y_pred_lr_all
  #  st.write("RMSE of linear regression on TEST dataset : ",np.sqrt(mse(predictions_test[col1],predictions_test['LR_test_Pred'])))
   # st.write("R2 Score of linear regression on TEST dataset :",r2(predictions_test[col1],predictions_test['LR_test_Pred']))    
    r2s=r2_score(predictions_all[col1],predictions_all['LR_full_Pred'])
    rmses=np.sqrt(mse(predictions_all[col1],predictions_all['LR_full_Pred']))
    maes=mean_absolute_error(predictions_all[col1],predictions_all['LR_full_Pred'])
    evars=explained_variance_score(predictions_all[col1],predictions_all['LR_full_Pred'])
    maxs=max_error(predictions_all[col1],predictions_all['LR_full_Pred'])
   # psds=mean_poisson_deviance(predictions_all[col1],predictions_all['LR_full_Pred'])

    tmp=[]
    tmp.append([rmses,maes,r2s,maxs,evars])
    final_scores_test=pd.DataFrame(tmp,columns=['Root Mean Squared Error(RMSE)','Mean Absolute Error(MAE)','R2 Spearman Coefficient of Determination','Maximum Residual Error','Explained variance Score'],index=['Linear Regression'])    
    
    final_test=test_result(lr,x_test,y_test,'Linear Regression')
    return lr,predictions_all,final_scores_test,final_test

@st.cache(persist=True, suppress_st_warning=True,allow_output_mutation=True)                    
def XGB_reg(totrain,x_train,x_test,y_train,y_test,predictions_all):
    eval_set = [(x_test, y_test)]
    # st.write("s")
    # A parameter grid for XGBoost
    params = {
            'min_child_weight': [6,7,8],
            'subsample': [0.7,0.5],
            'max_depth': [5,7,8],
            'eta':[0.05 , 0.03 ]
            
            }
    
    # magic with cv
    from sklearn.model_selection import GridSearchCV 

    xgb = GridSearchCV(XGBRegressor(n_estimators=500,early_stopping_rounds=10, eval_metric='mae', eval_set=eval_set,random_state=10), params, refit = True, verbose = 3) 
    #st.write(xgb.best_estimators_)
    #st.write(xgb.best_params_)
    #st.write(xgb.best_score_)
    #xgb=XGBRegressor(n_estimators=500,early_stopping_rounds=10, eval_metric="mae", eval_set=eval_set,random_state=10,max_depth=7, min_child_weight=6,subsample=0.7,eta=0.03)
    xgb.fit(x_train,y_train)    
    # y_pred_xgb=xgb.predict(x_test)       
    y_pred_xgb_all=xgb.predict(totrain)
   # predictions_test['XGB_test_Pred']=y_pred_xgb
    predictions_all['XGB_full_Pred']=y_pred_xgb_all
    col1=predictions_all.columns.tolist()[0]

    r2s=r2_score(predictions_all[col1],predictions_all['XGB_full_Pred'])
    rmses=np.sqrt(mse(predictions_all[col1],predictions_all['XGB_full_Pred']))
    maes=mean_absolute_error(predictions_all[col1],predictions_all['XGB_full_Pred'])
    evars=explained_variance_score(predictions_all[col1],predictions_all['XGB_full_Pred'])
    maxs=max_error(predictions_all[col1],predictions_all['XGB_full_Pred'])
   # psds=mean_poisson_deviance(predictions_all[col1],predictions_all['XGB_full_Pred'])

    tmp=[]
    tmp.append([rmses,maes,r2s,maxs,evars])
    final_scores_test=pd.DataFrame(tmp,columns=['Root Mean Squared Error(RMSE)','Mean Absolute Error(MAE)','R2 Spearman Coefficient of Determination','Maximum Residual Error','Explained variance Score'],index=['XGB Regressor'])    
    
   # final_test=test_result(xgb,x_test,y_test,'XGBoost')
    
    predictions_all2=pd.DataFrame(y_test)     
    y_pred_model=xgb.predict(x_test)
    col1=predictions_all2.columns.tolist()[0]
  #  predictions_test['LR_test_Pred']=y_pred_lr
    predictions_all2['Model_full_Pred']=y_pred_model
  #  st.write("RMSE of linear regression on TEST dataset : ",np.sqrt(mse(predictions_test[col1],predictions_test['LR_test_Pred'])))
   # st.write("R2 Score of linear regression on TEST dataset :",r2(predictions_test[col1],predictions_test['LR_test_Pred']))    
    r2s=r2_score(predictions_all2[col1],predictions_all2['Model_full_Pred'])
    rmses=np.sqrt(mse(predictions_all2[col1],predictions_all2['Model_full_Pred']))
    maes=mean_absolute_error(predictions_all2[col1],predictions_all2['Model_full_Pred'])
    evars=explained_variance_score(predictions_all2[col1],predictions_all2['Model_full_Pred'])
    maxs=max_error(predictions_all2[col1],predictions_all2['Model_full_Pred'])
   # psds=mean_poisson_deviance(predictions_all[col1],predictions_all['LR_full_Pred'])

    tmp1=[]
    tmp1.append([rmses,maes,r2s,maxs,evars])
    final_test=pd.DataFrame(tmp1,columns=['Validation_Root Mean Squared Error(RMSE)','Validation_Mean Absolute Error(MAE)','Validation_R2 Spearman Coefficient of Determination','Validation_Maximum Residual Error','Validation_Explained variance Score'],index=['XGB Regressor'])    
    
    return xgb,predictions_all,final_scores_test,final_test

@st.cache(persist=True, suppress_st_warning=True,allow_output_mutation=True)
def RF_reg(totrain,x_train,x_test,y_train,y_test,predictions_all):
    rf=RandomForestRegressor(random_state=10)
    rf.fit(x_train,y_train)   
   # y_pred_rf=rf.predict(x_test)       
    y_pred_rf_all=rf.predict(totrain)
   # predictions_test['RF_test_Pred']=y_pred_rf
    predictions_all['RF_full_Pred']=y_pred_rf_all
    col1=predictions_all.columns.tolist()[0]

    r2s=r2_score(predictions_all[col1],predictions_all['RF_full_Pred'])
    rmses=np.sqrt(mse(predictions_all[col1],predictions_all['RF_full_Pred']))
    maes=mean_absolute_error(predictions_all[col1],predictions_all['RF_full_Pred'])
    evars=explained_variance_score(predictions_all[col1],predictions_all['RF_full_Pred'])
    maxs=max_error(predictions_all[col1],predictions_all['RF_full_Pred'])
  #  psds=mean_poisson_deviance(predictions_all[col1],predictions_all['RF_full_Pred'])
    
    tmp=[]
    tmp.append([rmses,maes,r2s,maxs,evars])
    final_scores_test=pd.DataFrame(tmp,columns=['Root Mean Squared Error(RMSE)','Mean Absolute Error(MAE)','Spearman Coefficient of Determination','Maximum Residual Error','Explained variance Score'],index=['Random Forest Regression'])    
    final_test=test_result(rf,x_test,y_test,'Random Forest Regression')
 
    return rf,predictions_all,final_scores_test,final_test
                    
@st.cache(persist=True, suppress_st_warning=True,allow_output_mutation=True)
def NN_reg(totrain,x_train,x_test,y_train,y_test,predictions_all):
    ann=MLPRegressor(hidden_layer_sizes=(500,),activation='relu', solver='adam', alpha=0.001, batch_size='auto',learning_rate='constant', learning_rate_init=0.01, power_t=0.5, max_iter=1000, shuffle=True, random_state=9, tol=0.0001, verbose=False, warm_start=False, momentum=0.9, nesterovs_momentum=True,early_stopping=True, validation_fraction=0.1, beta_1=0.9, beta_2=0.999, epsilon=1e-08)
    ann.fit(x_train,y_train)
    # y_pred_rf=rf.predict(x_test)       
    y_pred_dt_all=ann.predict(totrain)
   # predictions_test['RF_test_Pred']=y_pred_rf
    predictions_all['NN_full_Pred']=y_pred_dt_all
    col1=predictions_all.columns.tolist()[0]

    r2s=r2_score(predictions_all[col1],predictions_all['NN_full_Pred'])
    rmses=np.sqrt(mse(predictions_all[col1],predictions_all['NN_full_Pred']))
    maes=mean_absolute_error(predictions_all[col1],predictions_all['NN_full_Pred'])
    evars=explained_variance_score(predictions_all[col1],predictions_all['NN_full_Pred'])
    maxs=max_error(predictions_all[col1],predictions_all['NN_full_Pred'])
   # psds=mean_poisson_deviance(predictions_all[col1],predictions_all['NN_full_Pred'])
  
    tmp=[]
    tmp.append([rmses,maes,r2s,maxs,evars])
    final_scores_test=pd.DataFrame(tmp,columns=['Root Mean Squared Error(RMSE)','Mean Absolute Error(MAE)','R2 Spearman Coefficient of Determination','Maximum Residual Error','Explained variance Score'],index=['Neural Network'])    
    final_test=test_result(ann,x_test,y_test,'Neural Network')
    
    return ann,predictions_all,final_scores_test,final_test
                    
@st.cache(persist=True, suppress_st_warning=True,allow_output_mutation=True)                    
def SVR_reg(totrain,x_train,x_test,y_train,y_test,predictions_all):
    svr=SVR(kernel='rbf',C=10)
    #grid.fit(x_train,y_train)   
    #from sklearn.model_selection import GridSearchCV 
  
    # defining parameter range 
    #param_grid = {'C': [0.1, 1, 10, 100],  
    #              'gamma': [1, 0.1, 0.01, 0.001], 
    #              'kernel': ['rbf']}  
      
     
    # fitting the model for grid search 
    svr.fit(x_train, y_train) 
    
   # y_pred_rf=rf.predict(x_test)       
    y_pred_svr_all=svr.predict(totrain)
   
   # predictions_test['RF_test_Pred']=y_pred_rf
    predictions_all['SVR_full_Pred']=y_pred_svr_all
    
    col1=predictions_all.columns.tolist()[0]

    r2s=r2_score(predictions_all[col1],predictions_all['SVR_full_Pred'])
    rmses=np.sqrt(mse(predictions_all[col1],predictions_all['SVR_full_Pred']))
    maes=mean_absolute_error(predictions_all[col1],predictions_all['SVR_full_Pred'])
    evars=explained_variance_score(predictions_all[col1],predictions_all['SVR_full_Pred'])
    maxs=max_error(predictions_all[col1],predictions_all['SVR_full_Pred'])
    #psds=mean_poisson_deviance(predictions_all[col1],predictions_all['SVR_full_Pred'])
    
    tmp=[]
    tmp.append([rmses,maes,r2s,maxs,evars])
    final_scores_test=pd.DataFrame(tmp,columns=['Root Mean Squared Error(RMSE)','Mean Absolute Error(MAE)','R2 Spearman Coefficient of Determination','Maximum Residual Error','Explained variance Score'],index=['Support Vector Regression'])    
    final_test=test_result(svr,x_test,y_test,'Support Vector Regression')
 
    return svr,predictions_all,final_scores_test,final_test

@st.cache(persist=True, suppress_st_warning=True,allow_output_mutation=True)                    
def All_reg(sub_x,nn_x,rf_x,x_train,x_test,y_train,y_test,x_train2,x_test2,y_train2,y_test2,x_train3,x_test3,y_train3,y_test3,predictions_all):

    lr,predictions_all1,fts,vts1=LR_reg(sub_x,x_train2,x_test2,y_train2,y_test2,predictions_all)
    xgb,predictions_all2,fts,vts2=XGB_reg(rf_x,x_train,x_test,y_train,y_test,predictions_all1)
    rf,predictions_all3,fts,vts3=RF_reg(rf_x,x_train,x_test,y_train,y_test,predictions_all2)
    dt,predictions_all4,fts,vts4=NN_reg(nn_x,x_train3,x_test3,y_train3,y_test3,predictions_all3)
    svr,predictions_all5,fts,vts5=SVR_reg(sub_x,x_train2,x_test2,y_train2,y_test2,predictions_all4)
    
    vtss=[vts1,vts4,vts3,vts2,vts5]
    valid_results=pd.concat(vtss,axis=0)
    
    models=['LR_full_Pred','NN_full_Pred','RF_full_Pred','XGB_full_Pred','SVR_full_Pred']
    mods=['Linear Regression','Neural Network','Random Forest Regression','XGB Regressor','Support Vector Regression']
    col1=predictions_all5.columns.tolist()[0]
    tmp=[]
    best_cors=0
    best_dev=0
    best_cor_i=''
    best_dev_i=''
    mean_cor_l=[]
    mean_dev_l=[]
    for i in range(len(models)):
        r2s=r2_score(predictions_all5[col1],predictions_all5[models[i]])
        rmses=np.sqrt(mse(predictions_all5[col1],predictions_all5[models[i]]))
        maes=mean_absolute_error(predictions_all5[col1],predictions_all5[models[i]])
        evars=explained_variance_score(predictions_all5[col1],predictions_all5[models[i]])
        maxs=max_error(predictions_all5[col1],predictions_all5[models[i]])
      #  psds=mean_poisson_deviance(predictions_all5[col1],predictions_all5[models[i]])        
        tmp.append([rmses,maes,r2s,maxs,evars])
        mean_cor_l.append(r2s)
        mean_dev_l.append(evars)
        
        if(r2s>best_cors):
            best_cors=r2s
            best_cor_i=mods[i]
            
        if(evars>best_dev):
            best_dev=evars
            best_dev_i=mods[i]
            
    final_scores_test=pd.DataFrame(tmp,columns=['Root Mean Squared Error(RMSE)','Mean Absolute Error(MAE)','R2 Spearman Coefficient of Determination','Maximum Residual Error','Explained variance Score'],index=mods)        
    cor_hm=(best_cors-np.mean(mean_cor_l))*100/best_cors
    dev_hm=(best_dev-np.mean(mean_dev_l))*100/best_dev
    
    #validation results:
    
    st.subheader(" Evaluation Report on six different metrics")
    
    st.write("According to the Statistical reports, the best model that has captured most of the variance in the data is",best_dev_i," with capturing ", dev_hm," % of Variance more than other models")
    st.write("According to the Statistical reports, the best model that has captured most of the correlationships among the variables in the data is",best_cor_i," with capturing ", cor_hm," % of Correlation more than other models")
    st.subheader(" In the End, nothing is more powerful than the human eye, so apart from the suggestions above, please check the below graphs, and choose the best model according to your intuition ")
    return lr,dt,rf,xgb,svr,predictions_all5,final_scores_test,valid_results


def viz_model_insights(model,train,y):
    dataviz_choice1=st.sidebar.selectbox("Choose Vizualization for Model-Explainability ",['None','Permutation Importance','Partial Dependency Plots'],key='7999')
    if dataviz_choice1=='Permutation Importance':
        st.subheader("Permutation Importance")
        
        def r2(model, x_train, y_train):
            return r2_score(y_train, model.predict(x_train))
        
        perm_imp_rfpimp=permutation_importances(model, train, y, r2) 
        vals=perm_imp_rfpimp.values.reshape(1,-1)[0]
        feas=perm_imp_rfpimp.index.values.reshape(1,-1)[0]
        figs=px.bar(x=vals,y=feas, orientation='h')#full
        st.plotly_chart(figs)
        st.subheader("This approach directly measures feature importance by observing how random re-shuffling (thus preserving the distribution of the variable) of different rows influences the model performance. Longer the bar, higher is its importance in deciding the Depth, calculated through permutation")
        
    if dataviz_choice1=='Partial Dependency Plots':
        fes=[]
        for i in train.columns.tolist():
            fes.append(i)
     
        #var_choice1=st.radio("Choose feature ",fes,key='601')
       # var_choice1=st.selectbox("Choose most important feature for PDP plot",train.columns.tolist(),key='6103')
        my_plots= plot_partial_dependence(model,features=[fes[0]],X=train,feature_names=train.columns.tolist(),grid_resolution=10)
        fig=plt.show(my_plots)
        st.pyplot(fig)
        st.subheader("The partial dependence plot (short PDP) shows the marginal effect of one or two features have on the predicted outcome of a machine learning model. One way to interpret this plot is, if the trend in Y axis rises,for every change in the values along X-axis, the odds that the depth value is high, also increases. ")
                
   # if dataviz_choice1=='SHAP Advanced feature-insights':
    #    if(model==dt | model==rf | model==xgb):
     #       explainer=shap.TreeExplainer(model)
      #      sv=explainer.shap_values(train)
            #long computation
       #     fgg=shap.summary_plot(sv,train)
        #    pfs=plt.show(fgg)
         #   st.pyplot(pfs)
          #  st.write("Interpretation info: The goal of SHAP is to explain the prediction of an instance x by computing the contribution of each feature to the prediction. SHAP dependence plots are an alternative to partial dependence plots and accumulated local effects. While PDP and ALE plot show average effects, SHAP dependence also shows the variance on the y-axis. Especially in case of interactions, the SHAP dependence plot will be much more dispersed in the y-axis. Each position on the x-axis is an instance of the data. Red SHAP values increase the prediction, blue values decrease it.")
        #else:
         #   st.subheader("This advanced feature insight is only applicable for Random forest, Decision Tree and XGBoost and other tree based machine learning algotihms")
        
def viz_model_insights_all(lr,dt,rf,xgb,svr,train,y):
    dataviz_choice1=st.sidebar.selectbox("Choose Vizualization for Model-Explainability ",['None','Permutation Importance','Partial Dependency Plots','SHAP Advanced feature-insights'],key='7999')
    if dataviz_choice1=='Permutation Importance':
        alg=st.selectbox("Select a ML model: ",['None','Linear Regression','Decision Tree','Random Forest','XGBoost','Support Vector Machines(SVR)'])
        if(alg=='Linear Regression'):                   
            st.subheader("Permutation Importance")
            def r2(lr, x_train, y_train):
                return r2_score(y_train, lr.predict(x_train))
    
            perm_imp_rfpimp=permutation_importances(lr, train, y, r2) 
            vals=perm_imp_rfpimp.values.reshape(1,-1)[0]
            feas=perm_imp_rfpimp.index.values.reshape(1,-1)[0]
            figs=px.bar(x=vals,y=feas, orientation='h')#full
            st.plotly_chart(figs)
            st.subheader("Interpretation info: The above graph shows an approach that directly measures feature importance by observing how random re-shuffling (thus preserving the distribution of the variable) of different rows influences the model performance. Longer the bar, higher is its importance in deciding the Depth, calculated through permutation")

        if(alg=='Decision Tree'):                   
            st.subheader("Permutation Importance")
            def r2(dt, x_train, y_train):
                return r2_score(y_train, dt.predict(x_train))
    
            perm_imp_rfpimp=permutation_importances(dt, train, y, r2) 
            vals=perm_imp_rfpimp.values.reshape(1,-1)[0]
            feas=perm_imp_rfpimp.index.values.reshape(1,-1)[0]
            figs=px.bar(x=vals,y=feas, orientation='h')#full
            st.plotly_chart(figs)
            st.subheader("Interpretation info: The above graph shows an approach that directly measures feature importance by observing how random re-shuffling (thus preserving the distribution of the variable) of different rows influences the model performance. Longer the bar, higher is its importance in deciding the Depth, calculated through permutation")

        if(alg=='Random Forest'):                   
            st.subheader("Permutation Importance")
            def r2(rf, x_train, y_train):
                return r2_score(y_train, rf.predict(x_train))
    
            perm_imp_rfpimp=permutation_importances(rf, train, y, r2) 
            vals=perm_imp_rfpimp.values.reshape(1,-1)[0]
            feas=perm_imp_rfpimp.index.values.reshape(1,-1)[0]
            figs=px.bar(x=vals,y=feas, orientation='h')#full
            st.plotly_chart(figs)
            st.subheader("Interpretation info: The above graph shows an approach that directly measures feature importance by observing how random re-shuffling (thus preserving the distribution of the variable) of different rows influences the model performance. Longer the bar, higher is its importance in deciding the Depth, calculated through permutation")

        if(alg=='XGBoost'):                   
            st.subheader("Permutation Importance")
            def r2(xgb, x_train, y_train):
                return r2_score(y_train, xgb.predict(x_train))
    
            perm_imp_rfpimp=permutation_importances(xgb, train, y, r2) 
            vals=perm_imp_rfpimp.values.reshape(1,-1)[0]
            feas=perm_imp_rfpimp.index.values.reshape(1,-1)[0]
            figs=px.bar(x=vals,y=feas, orientation='h')#full
            st.plotly_chart(figs)
            st.subheader("Interpretation info: The above graph shows an approach that directly measures feature importance by observing how random re-shuffling (thus preserving the distribution of the variable) of different rows influences the model performance. Longer the bar, higher is its importance in deciding the Depth, calculated through permutation")

        if(alg=='Support Vector Machines(SVR)'):                   
            st.subheader("Permutation Importance")
            def r2(svr, x_train, y_train):
                return r2_score(y_train, svr.predict(x_train))
    
            perm_imp_rfpimp=permutation_importances(svr, train, y, r2) 
            vals=perm_imp_rfpimp.values.reshape(1,-1)[0]
            feas=perm_imp_rfpimp.index.values.reshape(1,-1)[0]
            figs=px.bar(x=vals,y=feas, orientation='h')#full
            st.plotly_chart(figs)
            st.subheader("Interpretation info: The above graph shows an approach that directly measures feature importance by observing how random re-shuffling (thus preserving the distribution of the variable) of different rows influences the model performance. Longer the bar, higher is its importance in deciding the Depth, calculated through permutation")

            
    if dataviz_choice1=='Partial Dependency Plots':
        fes=[]
        for i in train.columns.tolist():
            fes.append(i)  
        alg=st.selectbox("Select a ML model: ",['None','Linear Regression','Decision Tree','Random Forest','XGBoost','Support Vector Machines(SVR)'])
        if(alg=='Linear Regression'):                           
#            var_choice1=st.selectbox("Choose most important feature for PDP plot",train.columns.tolist(),key='6103')
            my_plots= plot_partial_dependence(lr,features=[fes[0]],X=train,feature_names=train.columns.tolist(),grid_resolution=10)
            fig=plt.show(my_plots)
            st.pyplot(fig)
            st.subheader("Interpretation info: The partial dependence plot (short PDP) shows the marginal effect of some features have on the predicted outcome of a machine learning model. One way to interpret this plot is, if the trend in Y axis rises,for every change in the values along X-axis, the odds that the depth value is high, also increases. ")

        if(alg=='Decision Tree'):                           
  #          var_choice1=st.selectbox("Choose most important feature for PDP plot",train.columns.tolist(),key='6103')

            my_plots= plot_partial_dependence(dt,features=[fes[0]],X=train,feature_names=train.columns.tolist(),grid_resolution=10)
            fig=plt.show(my_plots)
            st.pyplot(fig)
            st.subheader("Interpretation info: The partial dependence plot (short PDP) shows the marginal effect of some features have on the predicted outcome of a machine learning model. One way to interpret this plot is, if the trend in Y axis rises,for every change in the values along X-axis, the odds that the depth value is high, also increases. ")

        if(alg=='Random Forest'):                           
            my_plots= plot_partial_dependence(rf,features=[fes[0]],X=train,feature_names=train.columns.tolist(),grid_resolution=10)
            fig=plt.show(my_plots)
            st.pyplot(fig)
            st.subheader("Interpretation info: The partial dependence plot (short PDP) shows the marginal effect of some features have on the predicted outcome of a machine learning model. One way to interpret this plot is, if the trend in Y axis rises,for every change in the values along X-axis, the odds that the depth value is high, also increases. ")
 
        if(alg=='XGBoost'):                           
            my_plots= plot_partial_dependence(rf,features=[fes[0]],X=train,feature_names=train.columns.tolist(),grid_resolution=10)
            fig=plt.show(my_plots)
            st.pyplot(fig)
            st.subheader("Interpretation info: The partial dependence plot (short PDP) shows the marginal effect of some features have on the predicted outcome of a machine learning model. One way to interpret this plot is, if the trend in Y axis rises,for every change in the values along X-axis, the odds that the depth value is high, also increases. ")

        if(alg=='Support Vector Machines(SVR)'):                           
            my_plots= plot_partial_dependence(svr,features=[fes[0]],X=train,feature_names=train.columns.tolist(),grid_resolution=10)
            fig=plt.show(my_plots)
            st.pyplot(fig)
            st.subheader("Interpretation info: The partial dependence plot (short PDP) shows the marginal effect of some features have on the predicted outcome of a machine learning model. One way to interpret this plot is, if the trend in Y axis rises,for every change in the values along X-axis, the odds that the depth value is high, also increases. ")
               
    if dataviz_choice1=='SHAP Advanced feature-insights':
        alg=st.selectbox("Select a ML model: ",['None','Decision Tree','Random Forest','XGBoost'])
        if(alg=='Decision Tree'):        
            explainer=shap.TreeExplainer(dt)
            sv=explainer.shap_values(train)
            #long computation
            fgg=shap.summary_plot(sv,train)
            pfs=plt.show(fgg)
            st.pyplot(pfs)
            st.write("Interpretation info: The goal of SHAP is to explain the prediction of an instance x by computing the contribution of each feature to the prediction. SHAP dependence plots are an alternative to partial dependence plots and accumulated local effects. While PDP and ALE plot show average effects, SHAP dependence also shows the variance on the y-axis. Especially in case of interactions, the SHAP dependence plot will be much more dispersed in the y-axis. Each position on the x-axis is an instance of the data. Red SHAP values increase the prediction, blue values decrease it.")
        if(alg=='Random Forest'):        
            explainer=shap.TreeExplainer(rf)
            sv=explainer.shap_values(train)
            #long computation
            fgg=shap.summary_plot(sv,train)
            pfs=plt.show(fgg)
            st.pyplot(pfs)
            st.write("Interpretation info: The goal of SHAP is to explain the prediction of an instance x by computing the contribution of each feature to the prediction. SHAP dependence plots are an alternative to partial dependence plots and accumulated local effects. While PDP and ALE plot show average effects, SHAP dependence also shows the variance on the y-axis. Especially in case of interactions, the SHAP dependence plot will be much more dispersed in the y-axis. Each position on the x-axis is an instance of the data. Red SHAP values increase the prediction, blue values decrease it.")
        if(alg=='XGBoost'):        
            explainer=shap.TreeExplainer(xgb)
            sv=explainer.shap_values(train)
            #long computation
            fgg=shap.summary_plot(sv,train)
            pfs=plt.show(fgg)
            st.pyplot(pfs)
            st.write("Interpretation info: The goal of SHAP is to explain the prediction of an instance x by computing the contribution of each feature to the prediction. SHAP dependence plots are an alternative to partial dependence plots and accumulated local effects. While PDP and ALE plot show average effects, SHAP dependence also shows the variance on the y-axis. Especially in case of interactions, the SHAP dependence plot will be much more dispersed in the y-axis. Each position on the x-axis is an instance of the data. Red SHAP values increase the prediction, blue values decrease it.")
        

def tts(df,y,test_split):#0.15 for hutt
    x_train,x_test,y_train,y_test=train_test_split(df,y,test_size=test_split,random_state=0,shuffle=True)
    return x_train,x_test,y_train,y_test
   
if __name__ == "__main__":
    main()

