import json
import pandas as pd

df = pd.DataFrame([{"test":"w","param":1},{"test":"w2","param":2}])
print(df)

list(df.to_dict(orient='records')[0].keys())[0]

df.to_json(orient='records')

json.loads(df.to_json())

json.loads(df.to_json(orient='records'))

df.to_dict()['test']

json.loads(df.to_json(orient='records'))[0]['test']